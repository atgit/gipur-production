﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Form1 : Form
    {
        // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל
        //public Excel.Application xl = new Microsoft.Office.Interop.Excel.Application();
        //public Excel.Workbook wb;
        //public Excel.Worksheet ws;
        //public Excel.Range rangeF;
        //public Excel.Range rangeB;

        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Application.Exit();  
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AboutBox1 Abox = new AboutBox1();
            Abox.Show();
        }

        
        //public string Scrap_Tire(string Tmp_Catalog,string Tmp_Date)
        //{
        // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל   
        //    Tmp_Date = Tmp_Date.Substring(6, 2) + "/" + Tmp_Date.Substring(4, 2) + "/" + Tmp_Date.Substring(0, 4);
        //    // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל
        //    Excel.Range rangeF = ws.get_Range("C440:C2000", Type.Missing);     
        //    string SDate = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
        //    int ScrapCount = 0;
            
        //    if (rangeF != null)
        //    {
        //        foreach (Excel.Range r in rangeF)
        //        {
        //            if (r.Text.ToString().Trim() != "")
        //            {
        //                if (r.Text.ToString().Trim() == Tmp_Catalog) // בדיקה אם היה דיווח למק"ט בקובץ פסולים
        //                {                            
        //                    rangeB = ws.get_Range("F" + r.Row.ToString() + ":F" + r.Row.ToString(), Type.Missing);
        //                    if (rangeB.Text == Tmp_Date)
        //                        ScrapCount += 1;
        //                }
        //            }
        //            else
        //                return ScrapCount.ToString();                                                            
        //        }
        //    }
        //    return ScrapCount.ToString();
        //}

        private void button1_Click(object sender, EventArgs e)
        {

            //              19/09/2012 שינוי מתאריך 
            // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
            int TotalForCatalog = 0;
            string LastCatalog = "";

            //           24/03/2010 שינוי מתאריך 
            // הוספתי אפשרות לחיתוך לפי דגם צמיג

            string[] Des_Arr = new string[10];
            int A, B, C;
            B = 0;
            C = 0;

            // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
            //           אפשרתי למשתמש לבחור עד 10 דגמים
            //          מערך המכיל את הדגמים שהמשתמש בחר
            if (Txt_Design.Text.Trim() != "")
            {
                for (A = 0; A <= Txt_Design.Text.Trim().Length - 1; A++)
                {
                    if (Txt_Design.Text.Substring(A, 1) == ".")
                    {
                        Des_Arr[C] = Txt_Design.Text.Substring(B, 3);
                        B = A + 1;
                        C += 1;
                    }
                }
                // תפיסת רשומה אחרונה
                Des_Arr[C] = Txt_Design.Text.Trim().Substring(Txt_Design.Text.Trim().Length - 3, 3);
            }

            if (!checkBox4.Checked && !checkBox5.Checked )
            {
                MessageBox.Show("Mark Bulding / Curing or Both !", "Curing / Building Production");
                return;
            }
           
            if (!checkBox2.Checked && !checkBox3.Checked && checkBox4.Checked)
            {
                MessageBox.Show("At Least Mark One Dept !", "Curing / Building Production");                
                return; 
            }

            Cursor.Current = Cursors.WaitCursor;
            
            string Str_S_Date;        // תאריך התחלה לצורך השאילתא
            string Str_E_Date;        // תאריך סיום לצורך השאילתא
            DateTime S_Date;          // תאריך התחלה
            DateTime E_Date;          // תאריך סיום            
            string StrSql;
            string TmpStr;           
           
            int I;
            I = 3;            

            /////
            // אנגלית OFFEICE הרצת דוח בגרסת 
            System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
            System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;
            thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            /////

            if (checkBox4.Checked )
            {
                // תיקון מתאריך 11/07/2011
                // הוספת שני שדות לדוח גיפור
                string MFSZ = "";
                string MFNO = "";
                string MFVRN = "";
                string[] PressArr = new string[14];
                PressArr[0] = "353";
                PressArr[1] = "354";                
                PressArr[2] = "338";
                PressArr[3] = "339";
                PressArr[4] = "328";
                PressArr[5] = "329";
                PressArr[6] = "325";
                PressArr[7] = "322";
                PressArr[8] = "323";
                PressArr[9] = "336";
                PressArr[10] = "337";
                PressArr[11] = "624";
                PressArr[12] = "602";
                PressArr[13] = "606";
                
                // יצירת קובץ אקסל
                // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                Excel.Range chartRange;

                xlApp = new Excel.ApplicationClass();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlWorkSheet.Cells[1, 1] = "Curing Production Report Between : " + dateTimePicker1.Value.ToString().Substring(0, 10) + " And  " + dateTimePicker2.Value.ToString().Substring(0, 10);

                // הצגת גיליון משמאל לימין
                xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;                
                xlWorkSheet.DisplayRightToLeft = false; 

                // מירכוז + מיזוג + פונט מודגש לתחום            
                chartRange = xlWorkSheet.get_Range("A1", "E1");
                chartRange.MergeCells = true;
                chartRange.HorizontalAlignment = 3;
                chartRange.VerticalAlignment = 3;
                chartRange.Font.Bold = true;

                // הגדרת שדות הדוח                
                xlWorkSheet.Cells[2, 1] = "Date";
                xlWorkSheet.Cells[2, 2] = "Catalog";
                xlWorkSheet.Cells[2, 3] = "Size";
                xlWorkSheet.Cells[2, 4] = "Dept";
                xlWorkSheet.Cells[2, 5] = "Quantity";
                xlWorkSheet.Cells[2, 6] = "Weight Without Flap";
                xlWorkSheet.Cells[2, 7] = "Cost Without Flap";
                xlWorkSheet.Cells[2, 8] = "Specification";
                xlWorkSheet.Cells[2, 9] = "Press";
                
                // תיקון מתאריך 11/07/2011
                xlWorkSheet.Cells[2, 10] = "PI";
                xlWorkSheet.Cells[2, 11] = "Loader";

                // מירכוז + פונט מודגש לתחום            
                chartRange = xlWorkSheet.get_Range("A2", "K2");
                chartRange.HorizontalAlignment = 3;
                chartRange.VerticalAlignment = 3;
                chartRange.Font.Bold = true;

                // הגדרת רוחב עמודה של תחום
                chartRange = xlWorkSheet.get_Range("A1", "A5000");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("B1", "B5000");
                chartRange.ColumnWidth = 15;
                chartRange = xlWorkSheet.get_Range("C1", "C5000");
                chartRange.ColumnWidth = 25;
                chartRange = xlWorkSheet.get_Range("D1", "D5000");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("E1", "P5000");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("F1", "P5000");
                chartRange.ColumnWidth = 18;
                chartRange = xlWorkSheet.get_Range("G1", "P5000");
                chartRange.ColumnWidth = 16;
                chartRange = xlWorkSheet.get_Range("H1", "P5000");
                chartRange.ColumnWidth = 15;
                chartRange = xlWorkSheet.get_Range("I1", "P5000");
                chartRange.ColumnWidth = 10;

                // תיקון מתאריך 11/07/2011
                chartRange = xlWorkSheet.get_Range("J1", "P5000");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("K1", "P5000");
                chartRange.ColumnWidth = 10;

                // התאמת תאריך
                S_Date = dateTimePicker1.Value;
                E_Date = dateTimePicker2.Value;
                Str_S_Date = (S_Date.Year - 1928).ToString("00") + S_Date.Month.ToString("00") + S_Date.Day.ToString("00");
                Str_E_Date = (E_Date.Year - 1928).ToString("00") + E_Date.Month.ToString("00") + E_Date.Day.ToString("00");

                // AS400 התקשרות ל   
                DBConn DBC;
                //string Qry_Str;
                string Con_Str;

                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";                
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);

                // הרכבת משפט שליפה
                StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                         "from BPCSFV30.FLTL89 join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                                               "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (";
                
                if (checkBox2.Checked)
                    StrSql += "161,";
                if (checkBox3.Checked)
                    StrSql += "162";
                if (StrSql.Substring(StrSql.Length - 1, 1) == ",")
                    StrSql = StrSql.Substring(0, StrSql.Length - 1);

                StrSql += ") ";

                if (Txt_Prod.Text.Trim() != "")
                    StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";

                if (Txt_Size.Text.Trim() != "")
                {
                    Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                    StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                }

                StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";

                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                    // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                    if (Txt_Design.Text.Trim() != "")
                    {
                        for (A = 0; A <= C; A++)
                        {
                            if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                            {
                                //              19/09/2012 שינוי מתאריך 
                                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                if (LastCatalog == "")
                                {
                                    LastCatalog = DBC.Rs1.GetString(1).Substring(0, 8);
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                }
                                else
                                {
                                    if (LastCatalog == DBC.Rs1.GetString(1).Substring(0, 8))
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    else
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1).Substring(0, 8);
                                        xlWorkSheet.Cells[I, 4] = "Total";
                                        xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                        I += 1;
                                        TotalForCatalog = DBC.Rs1.GetInt16(4);
                                    }
                                }
                                xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Substring(0, 8);
                                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();

                                // תיקון מתאריך 11/07/2011
                                // בדיקה אם צמיג דיגונלי ובהמשך בדיקה אם צריך מתקן קירור                                                
                                if (DBC.Rs1.GetString(2).Trim().IndexOf("R") < 0)
                                {
                                    if (DBC.Rs1.GetString(7).Trim().Length == 11)
                                    {
                                        MFSZ = DBC.Rs1.GetString(7).Trim().Substring(2, 2);
                                        MFNO = DBC.Rs1.GetString(7).Trim().Substring(5, 3);
                                        MFVRN = DBC.Rs1.GetString(7).Trim().Substring(9, 2);
                                    }
                                    else if (DBC.Rs1.GetString(7).Trim().Length == 12)
                                    {
                                        MFSZ = DBC.Rs1.GetString(7).Trim().Substring(3, 2);
                                        MFNO = DBC.Rs1.GetString(7).Trim().Substring(6, 3);
                                        MFVRN = DBC.Rs1.GetString(7).Trim().Substring(10, 2);
                                    }
                                    else if (DBC.Rs1.GetString(7).Trim().Length == 13)
                                    {
                                        MFSZ = DBC.Rs1.GetString(7).Trim().Substring(4, 2);
                                        MFNO = DBC.Rs1.GetString(7).Trim().Substring(7, 3);
                                        MFVRN = DBC.Rs1.GetString(7).Trim().Substring(11, 2);
                                    }
                                    StrSql = "select MFPING from MFRT.MFRTP where MFSZ='" + MFSZ + "' and MFNO=" + MFNO + " and MFVRN=" + MFVRN + "";
                                    DBC.Q_Run2(StrSql);
                                    if (DBC.Rs2.Read())
                                    {
                                        if (DBC.Rs2.GetString(0).Trim() == "0")
                                        {
                                        }
                                        else
                                        {
                                            xlWorkSheet.Cells[I, 10] = "Yes";
                                        }
                                    }
                                }
                                // בדיקה אם צמיג רדיאלי ובהמשך בדיקה אם יש לאודר 
                                if (DBC.Rs1.GetString(2).Trim().IndexOf("R") > 0)
                                {
                                    for (B = 0; B < 14; B++)
                                    {
                                        if (DBC.Rs1.GetString(8).Trim() == PressArr[B])
                                        {
                                            xlWorkSheet.Cells[I, 11] = "No";
                                        }
                                    }
                                }

                                I += 1;
                            }
                        }
                    }
                    else
                    {
                        //              19/09/2012 שינוי מתאריך 
                        // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                        if (LastCatalog == "")
                        {
                            LastCatalog = DBC.Rs1.GetString(1).Substring(0, 8);
                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                        }
                        else
                        {
                            if (LastCatalog == DBC.Rs1.GetString(1).Substring(0, 8))
                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                            else
                            {
                                LastCatalog = DBC.Rs1.GetString(1).Substring(0, 8);
                                xlWorkSheet.Cells[I, 4] = "Total";
                                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                I += 1;
                                TotalForCatalog = DBC.Rs1.GetInt16(4);
                            }
                        }                        
                        
                        xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                        xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Substring(0, 8);
                        xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                        xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                        xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                        xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                        xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                        xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                        xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();

                        // תיקון מתאריך 11/07/2011
                        // בדיקה אם צמיג דיגונלי ובהמשך בדיקה אם צריך מתקן קירור                                                
                        if (DBC.Rs1.GetString(2).Trim().IndexOf("R") < 0)
                        {
                            if (DBC.Rs1.GetString(7).Trim().Length == 11)
                                {
                                    MFSZ = DBC.Rs1.GetString(7).Trim().Substring(2, 2);
                                    MFNO = DBC.Rs1.GetString(7).Trim().Substring(5, 3);
                                    MFVRN = DBC.Rs1.GetString(7).Trim().Substring(9, 2);
                                }
                                else if (DBC.Rs1.GetString(7).Trim().Length == 12)
                                    {
                                        MFSZ = DBC.Rs1.GetString(7).Trim().Substring(3, 2);
                                        MFNO = DBC.Rs1.GetString(7).Trim().Substring(6, 3);
                                        MFVRN = DBC.Rs1.GetString(7).Trim().Substring(10, 2);
                                    }
                                    else if (DBC.Rs1.GetString(7).Trim().Length == 13)
                                        {
                                            MFSZ = DBC.Rs1.GetString(7).Trim().Substring(4, 2);
                                            MFNO = DBC.Rs1.GetString(7).Trim().Substring(7, 3);
                                            MFVRN = DBC.Rs1.GetString(7).Trim().Substring(11, 2);
                                        }
                            StrSql = "select MFPING from MFRT.MFRTP where MFSZ='"+MFSZ+"' and MFNO="+MFNO+" and MFVRN="+MFVRN+"";
                            DBC.Q_Run2(StrSql);
                            if (DBC.Rs2.Read())
                            {
                                if (DBC.Rs2.GetString(0).Trim() == "0")
                                {
                                }
                                else
                                {
                                    xlWorkSheet.Cells[I, 10] = "Yes";
                                }
                            }
                        }                        
                        // בדיקה אם צמיג רדיאלי ובהמשך בדיקה אם יש לאודר 
                        if (DBC.Rs1.GetString(2).Trim().IndexOf("R") > 0)
                        {
                            for (B = 0; B < 14; B++)
                            {
                                if (DBC.Rs1.GetString(8).Trim() == PressArr[B])
                                {
                                    xlWorkSheet.Cells[I, 11] = "No";
                                }
                            }
                        }

                        I += 1;
                    }
                }
                //              19/09/2012 שינוי מתאריך 
                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                xlWorkSheet.Cells[I, 4] = "Total";
                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                TotalForCatalog = 0;
                LastCatalog = "";

                // בדיקה אם מבקשים להפיק דוח בניה
                if (checkBox5.Checked==true)
                {                    
                    I += 1;                    
                    xlWorkSheet.Cells[I, 1] = "Building Production Report Between : " + dateTimePicker1.Value.ToString().Substring(0, 10) + " And  " + dateTimePicker2.Value.ToString().Substring(0, 10);

                    // מירכוז + מיזוג + פונט מודגש לתחום            
                    chartRange = xlWorkSheet.get_Range("A"+I+"", "E"+I+"");
                    chartRange.MergeCells = true;
                    chartRange.HorizontalAlignment = 3;
                    chartRange.VerticalAlignment = 3;
                    chartRange.Font.Bold = true;
                    
                    I += 1;
                    // הגדרת שדות הדוח
                    xlWorkSheet.Cells[I, 1] = "Date";
                    xlWorkSheet.Cells[I, 2] = "Catalog";
                    xlWorkSheet.Cells[I, 3] = "Size";
                    xlWorkSheet.Cells[I, 4] = "Dept";
                    xlWorkSheet.Cells[I, 5] = "Quantity";
                    xlWorkSheet.Cells[I, 6] = "Weight Without Flap";
                    xlWorkSheet.Cells[I, 7] = "Cost Without Flap";
                    xlWorkSheet.Cells[I, 8] = "Specification";
                    xlWorkSheet.Cells[I, 9] = "Machine";

                    // מירכוז + פונט מודגש לתחום            
                    chartRange = xlWorkSheet.get_Range("A" + I + "", "I" + I + "");
                    chartRange.HorizontalAlignment = 3;
                    chartRange.VerticalAlignment = 3;
                    chartRange.Font.Bold = true;

                    I += 1;
                    // הרכבת משפט שליפה
                    StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                             "from BPCSFV30.FLTL90 join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                                                   "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                             "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                             "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') ";
                    if (Txt_Prod.Text.Trim() != "")
                        StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";
                    if (Txt_Size.Text.Trim() != "")
                    {
                        Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                        StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                    }
                    StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";

                    DBC.Q_Run1(StrSql);
                    while (DBC.Rs1.Read())
                    {
                        TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                        // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                        if (Txt_Design.Text.Trim() != "")
                        {
                            for (A = 0; A <= C; A++)
                            {
                                if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                                {
                                    //              19/09/2012 שינוי מתאריך 
                                    // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                    if (LastCatalog == "")
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1);
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    }
                                    else
                                    {
                                        if (LastCatalog == DBC.Rs1.GetString(1))
                                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                                        else
                                        {
                                            LastCatalog = DBC.Rs1.GetString(1);
                                            xlWorkSheet.Cells[I, 4] = "Total";
                                            xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                            I += 1;
                                            TotalForCatalog = DBC.Rs1.GetInt16(4);
                                        }
                                    }
                                    
                                    xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                    xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                    xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                    xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                    xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                    xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                    xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                    xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                                    xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();
                                    I += 1;
                                }
                            }
                        }
                        else
                        {
                            //              19/09/2012 שינוי מתאריך 
                            // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                            if (LastCatalog == "")
                            {
                                LastCatalog = DBC.Rs1.GetString(1);
                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                            }
                            else
                            {
                                if (LastCatalog == DBC.Rs1.GetString(1))
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                else
                                {
                                    LastCatalog = DBC.Rs1.GetString(1);
                                    xlWorkSheet.Cells[I, 4] = "Total";
                                    xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                    I += 1;
                                    TotalForCatalog = DBC.Rs1.GetInt16(4);
                                }
                            }

                            xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                            xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                            xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                            xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                            xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                            xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                            xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                            xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                            xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();
                            I += 1;
                        }
                    }
                    
                    //              19/09/2012 שינוי מתאריך 
                    // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                    xlWorkSheet.Cells[I, 4] = "Total";
                    xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                    TotalForCatalog = 0;
                    LastCatalog = "";

                    // Member יכול להיות מצב שהנתונים נמצאים בקובץ וירטואלי - מה שנקרא
                    //  מצב שלא היה קליטת נתונים, זה מה שליבה הסבירה והראתה לי. על מנת
                    // לשלוף את הנתונים צריך לבצע את המשפט הבא כדי שהנתונים יהיו בקובץ
                    //                             שממנו ניתן לשלוף במידה ויש מה לשלוף
                    StrSql = "Create alias majd.workall for bpcsfv30.flt(workall)";
                    DBC.Run(StrSql);
                    // הרכבת משפט שליפה
                    StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                             "from majd.workall join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                                                   "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                             "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                             "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') ";
                    if (Txt_Prod.Text.Trim() != "")
                        StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";
                    if (Txt_Size.Text.Trim() != "")
                    {
                        Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                        StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                    }
                    StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";

                    DBC.Q_Run1(StrSql);
                    if (DBC.Rs1.Read())
                    {
                        while (DBC.Rs1.Read())
                        {
                            TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                            // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                            if (Txt_Design.Text.Trim() != "")
                            {
                                for (A = 0; A <= C; A++)
                                {
                                    if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                                    {
                                        //              19/09/2012 שינוי מתאריך 
                                        // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                        if (LastCatalog == "")
                                        {
                                            LastCatalog = DBC.Rs1.GetString(1);
                                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                                        }
                                        else
                                        {
                                            if (LastCatalog == DBC.Rs1.GetString(1))
                                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                                            else
                                            {
                                                LastCatalog = DBC.Rs1.GetString(1);
                                                xlWorkSheet.Cells[I, 4] = "Total";
                                                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                                I += 1;
                                                TotalForCatalog = DBC.Rs1.GetInt16(4);
                                            }
                                        }

                                        xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                        xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                        xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                        xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                        xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                        xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                        xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                        xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                                        xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();
                                        I += 1;
                                    }
                                }
                            }
                            else
                            {
                                //              19/09/2012 שינוי מתאריך 
                                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                if (LastCatalog == "")
                                {
                                    LastCatalog = DBC.Rs1.GetString(1);
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                }
                                else
                                {
                                    if (LastCatalog == DBC.Rs1.GetString(1))
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    else
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1);
                                        xlWorkSheet.Cells[I, 4] = "Total";
                                        xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                        I += 1;
                                        TotalForCatalog = DBC.Rs1.GetInt16(4);
                                    }
                                }

                                xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();
                                I += 1;
                            }
                        }
                        
                        //              19/09/2012 שינוי מתאריך 
                        // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                        xlWorkSheet.Cells[I, 4] = "Total";
                        xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                        TotalForCatalog = 0;
                        LastCatalog = "";
                    }
                    // מחיקת קובץ שהקמתי על סמך קובץ וירטואלי
                    StrSql = "drop alias majd.workall";
                    DBC.Run(StrSql);

                }
                xlApp.Visible = true;
            }
            // בדיקה אם מבקשים להפיק דוח בניה
            if (checkBox5.Checked && !checkBox4.Checked)
            {

                // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל
                //Excel.Workbook wb = xl.ActiveWorkbook;
                //wb = xl.Workbooks.Open("T:\\green scrap\\green scrap.xls", false, false, Type.Missing, Type.Missing, Type.Missing, false, Type.Missing, Type.Missing, true, Type.Missing, Type.Missing, false, Type.Missing, Type.Missing);
                //ws = (Excel.Worksheet)wb.Worksheets["בסיס הנתונים databse"];                

                // יצירת קובץ אקסל
                // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                Excel.Range chartRange;

                xlApp = new Excel.ApplicationClass();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlWorkSheet.Cells[1, 1] = "Building Production Report Between : " + dateTimePicker1.Value.ToString().Substring(0, 10) + " And  " + dateTimePicker2.Value.ToString().Substring(0, 10);

                // הצגת גיליון משמאל לימין
                xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
                xlWorkSheet.DisplayRightToLeft = false; 

                // מירכוז + מיזוג + פונט מודגש לתחום            
                chartRange = xlWorkSheet.get_Range("A1", "E1");
                chartRange.MergeCells = true;
                chartRange.HorizontalAlignment = 3;
                chartRange.VerticalAlignment = 3;
                chartRange.Font.Bold = true;

                // הגדרת שדות הדוח
                xlWorkSheet.Cells[2, 1] = "Date";
                xlWorkSheet.Cells[2, 2] = "Catalog";
                xlWorkSheet.Cells[2, 3] = "Size";
                xlWorkSheet.Cells[2, 4] = "Dept";
                xlWorkSheet.Cells[2, 5] = "Quantity";
                xlWorkSheet.Cells[2, 6] = "Weight Without Flap";
                xlWorkSheet.Cells[2, 7] = "Cost Without Flap";
                xlWorkSheet.Cells[2, 8] = "Specification";
                xlWorkSheet.Cells[2, 9] = "Machine";


                // מירכוז + פונט מודגש לתחום            
                chartRange = xlWorkSheet.get_Range("A2", "I2");
                chartRange.HorizontalAlignment = 3;
                chartRange.VerticalAlignment = 3;
                chartRange.Font.Bold = true;

                // הגדרת רוחב עמודה של תחום
                chartRange = xlWorkSheet.get_Range("A1", "A5000");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("B1", "B5000");
                chartRange.ColumnWidth = 15;
                chartRange = xlWorkSheet.get_Range("C1", "C5000");
                chartRange.ColumnWidth = 25;
                chartRange = xlWorkSheet.get_Range("D1", "D5000");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("E1", "P5000");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("F1", "P5000");
                chartRange.ColumnWidth = 18;
                chartRange = xlWorkSheet.get_Range("G1", "P5000");
                chartRange.ColumnWidth = 16;
                chartRange = xlWorkSheet.get_Range("H1", "P5000");
                chartRange.ColumnWidth = 15;
                chartRange = xlWorkSheet.get_Range("I1", "P5000");
                chartRange.ColumnWidth = 10;

                // התאמת תאריך
                S_Date = dateTimePicker1.Value;
                E_Date = dateTimePicker2.Value;
                Str_S_Date = (S_Date.Year - 1928).ToString("00") + S_Date.Month.ToString("00") + S_Date.Day.ToString("00");
                Str_E_Date = (E_Date.Year - 1928).ToString("00") + E_Date.Month.ToString("00") + E_Date.Day.ToString("00");

                // AS400 התקשרות ל   
                DBConn DBC;
                //string Qry_Str;
                string Con_Str;

                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);

                // הרכבת משפט שליפה
                StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                         "from BPCSFV30.FLTL90 join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                                               "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') "; 
                if (Txt_Prod.Text.Trim() != "")
                    StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";
                if (Txt_Size.Text.Trim() != "")
                {
                    Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                    StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                }
                StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                    // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                    if (Txt_Design.Text.Trim() != "")
                    {
                        for (A = 0; A <= C; A++)
                        {
                            if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                            {
                                //              19/09/2012 שינוי מתאריך 
                                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                if (LastCatalog == "")
                                {
                                    LastCatalog = DBC.Rs1.GetString(1);
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                }
                                else
                                {
                                    if (LastCatalog == DBC.Rs1.GetString(1))
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    else
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1);
                                        xlWorkSheet.Cells[I, 4] = "Total";
                                        xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                        I += 1;
                                        TotalForCatalog = DBC.Rs1.GetInt16(4);
                                    }
                                }
                                
                                xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                xlWorkSheet.Cells[I, 6] = Math.Round (Convert.ToDouble(DBC.Rs1.GetString(5).Trim()),1);
                                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();                                

                                // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל
                                //xlWorkSheet.Cells[I, 10] = Scrap_Tire(DBC.Rs1.GetString(1).Trim(), TmpStr);
                                I += 1;
                            }
                        }
                    }
                    else
                    {
                        //              19/09/2012 שינוי מתאריך 
                        // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                        if (LastCatalog == "")
                        {
                            LastCatalog = DBC.Rs1.GetString(1);
                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                        }
                        else
                        {
                            if (LastCatalog == DBC.Rs1.GetString(1))
                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                            else
                            {
                                LastCatalog = DBC.Rs1.GetString(1);
                                xlWorkSheet.Cells[I, 4] = "Total";
                                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                I += 1;
                                TotalForCatalog = DBC.Rs1.GetInt16(4);
                            }
                        }

                        xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                        xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                        xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                        xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                        xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                        xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                        xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                        xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                        xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();
                        
                        // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל                        
                        //xlWorkSheet.Cells[I, 10] = Scrap_Tire(DBC.Rs1.GetString(1).Trim(), TmpStr);
                        I += 1;
                    }
                }
                //              19/09/2012 שינוי מתאריך 
                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                xlWorkSheet.Cells[I, 4] = "Total";
                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                TotalForCatalog = 0;
                LastCatalog = "";

                // Member יכול להיות מצב שהנתונים נמצאים בקובץ וירטואלי - מה שנקרא
                //  מצב שלא היה קליטת נתונים, זה מה שליבה הסבירה והראתה לי. על מנת
                // לשלוף את הנתונים צריך לבצע את המשפט הבא כדי שהנתונים יהיו בקובץ
                //                             שממנו ניתן לשלוף במידה ויש מה לשלוף

                StrSql = "Create alias majd.workall for bpcsfv30.flt(workall)";                         
                DBC.Run(StrSql);
                // הרכבת משפט שליפה
                StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                         "from majd.workall join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                                               "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') "; 
                if (Txt_Prod.Text.Trim() != "")
                    StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";
                if (Txt_Size.Text.Trim() != "")
                {
                    Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                    StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                }
                StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";
               
                DBC.Q_Run1(StrSql);
                if (DBC.Rs1.Read())
                {
                    while (DBC.Rs1.Read())
                    {
                        TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                        // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                        if (Txt_Design.Text.Trim() != "")
                        {
                            for (A = 0; A <= C; A++)
                            {
                                if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                                {
                                    //              19/09/2012 שינוי מתאריך 
                                    // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                    if (LastCatalog == "")
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1);
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    }
                                    else
                                    {
                                        if (LastCatalog == DBC.Rs1.GetString(1))
                                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                                        else
                                        {
                                            LastCatalog = DBC.Rs1.GetString(1);
                                            xlWorkSheet.Cells[I, 4] = "Total";
                                            xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                            I += 1;
                                            TotalForCatalog = DBC.Rs1.GetInt16(4);
                                        }
                                    }

                                    xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                    xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                    xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                    xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                    xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                    xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                    xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                    xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                                    xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();

                                    // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל                                    
                                    //xlWorkSheet.Cells[I, 10] = Scrap_Tire(DBC.Rs1.GetString(1).Trim(), TmpStr);
                                    I += 1;
                                }
                            }
                        }
                        else
                        {
                            //              19/09/2012 שינוי מתאריך 
                            // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                            if (LastCatalog == "")
                            {
                                LastCatalog = DBC.Rs1.GetString(1);
                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                            }
                            else
                            {
                                if (LastCatalog == DBC.Rs1.GetString(1))
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                else
                                {
                                    LastCatalog = DBC.Rs1.GetString(1);
                                    xlWorkSheet.Cells[I, 4] = "Total";
                                    xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                    I += 1;
                                    TotalForCatalog = DBC.Rs1.GetInt16(4);
                                }
                            }

                            xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                            xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                            xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                            xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                            xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                            xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                            xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                            xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                            xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8).Trim();

                            // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל                            
                            //xlWorkSheet.Cells[I, 10] = Scrap_Tire(DBC.Rs1.GetString(1).Trim(), TmpStr);
                            I += 1;
                        }
                    }

                    //              19/09/2012 שינוי מתאריך 
                    // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                    xlWorkSheet.Cells[I, 4] = "Total";
                    xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                    TotalForCatalog = 0;
                    LastCatalog = "";
                }

                // מחיקת קובץ שהקמתי על סמך קובץ וירטואלי
                StrSql = "drop alias majd.workall";
                DBC.Run(StrSql);

                xlApp.Visible = true;
            }
                
            Cursor.Current = Cursors.PanEast;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void scrapHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_ScrapHistory SH = new Frm_ScrapHistory();
            SH.Show(); 
        }

        private void iDAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_IDAnalysis IDA = new Frm_IDAnalysis();
            IDA.Show(); 
        }

        private void tireTraceabilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_TireTraceability TT = new Frm_TireTraceability();
            TT.Show(); 
        }
    }
}
