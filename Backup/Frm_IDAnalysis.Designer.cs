﻿namespace Gipur_Production
{
    partial class Frm_IDAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_IDAnalysis));
            this.Btn_OK = new System.Windows.Forms.Button();
            this.DTPicker_To = new System.Windows.Forms.DateTimePicker();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.DTPicker_From = new System.Windows.Forms.DateTimePicker();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox_S1 = new System.Windows.Forms.CheckBox();
            this.checkBox_S2 = new System.Windows.Forms.CheckBox();
            this.checkBox_S4 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(88, 220);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(123, 28);
            this.Btn_OK.TabIndex = 6;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // DTPicker_To
            // 
            this.DTPicker_To.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_To.Location = new System.Drawing.Point(123, 141);
            this.DTPicker_To.Name = "DTPicker_To";
            this.DTPicker_To.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_To.TabIndex = 2;
            // 
            // Lbl_To
            // 
            this.Lbl_To.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_To.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(56, 143);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.Size = new System.Drawing.Size(73, 24);
            this.Lbl_To.TabIndex = 24;
            this.Lbl_To.Text = "To";
            // 
            // DTPicker_From
            // 
            this.DTPicker_From.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_From.Location = new System.Drawing.Point(123, 94);
            this.DTPicker_From.Name = "DTPicker_From";
            this.DTPicker_From.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_From.TabIndex = 1;
            // 
            // Lbl_From
            // 
            this.Lbl_From.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_From.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(56, 95);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.Size = new System.Drawing.Size(73, 24);
            this.Lbl_From.TabIndex = 21;
            this.Lbl_From.Text = "From";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(56, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 29);
            this.label2.TabIndex = 20;
            this.label2.Text = "Scrap History";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox_S1
            // 
            this.checkBox_S1.AutoSize = true;
            this.checkBox_S1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.checkBox_S1.Location = new System.Drawing.Point(60, 184);
            this.checkBox_S1.Name = "checkBox_S1";
            this.checkBox_S1.Size = new System.Drawing.Size(53, 28);
            this.checkBox_S1.TabIndex = 3;
            this.checkBox_S1.Text = "S1";
            this.checkBox_S1.UseVisualStyleBackColor = true;
            // 
            // checkBox_S2
            // 
            this.checkBox_S2.AutoSize = true;
            this.checkBox_S2.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.checkBox_S2.Location = new System.Drawing.Point(123, 184);
            this.checkBox_S2.Name = "checkBox_S2";
            this.checkBox_S2.Size = new System.Drawing.Size(53, 28);
            this.checkBox_S2.TabIndex = 4;
            this.checkBox_S2.Text = "S2";
            this.checkBox_S2.UseVisualStyleBackColor = true;
            // 
            // checkBox_S4
            // 
            this.checkBox_S4.AutoSize = true;
            this.checkBox_S4.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.checkBox_S4.Location = new System.Drawing.Point(184, 184);
            this.checkBox_S4.Name = "checkBox_S4";
            this.checkBox_S4.Size = new System.Drawing.Size(53, 28);
            this.checkBox_S4.TabIndex = 5;
            this.checkBox_S4.Text = "S4";
            this.checkBox_S4.UseVisualStyleBackColor = true;
            // 
            // Frm_IDAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(292, 262);
            this.Controls.Add(this.checkBox_S4);
            this.Controls.Add(this.checkBox_S2);
            this.Controls.Add(this.checkBox_S1);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.DTPicker_To);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DTPicker_From);
            this.Controls.Add(this.Lbl_From);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_IDAnalysis";
            this.Text = "ID Analysis";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.DateTimePicker DTPicker_To;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.DateTimePicker DTPicker_From;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox_S1;
        private System.Windows.Forms.CheckBox checkBox_S2;
        private System.Windows.Forms.CheckBox checkBox_S4;

    }
}