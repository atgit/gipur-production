﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_IDAnalysis : Form
    {
        string StrSql;
        string Con_Str;

        public Frm_IDAnalysis()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (!checkBox_S1.Checked && !checkBox_S2.Checked && !checkBox_S4.Checked)
            {
                MessageBox.Show("Error : Must mark at least one building machine.", "Curing / Building Production");
                return; 
            }
            Cursor.Current = Cursors.WaitCursor;
            
            string StrMachine="";
            string Str_S_Date;        // תאריך התחלה לצורך השאילתא
            string Str_E_Date;        // תאריך סיום לצורך השאילתא
            DateTime S_Date;          // תאריך התחלה
            DateTime E_Date;          // תאריך סיום                                    

            int Tmp1 =0;
            int Tmp2 = 0;
            int I=3;            

            /////
            // אנגלית OFFEICE הרצת דוח בגרסת 
            System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
            System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;
            thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            /////

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            // Freeze rows            
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A2", "A2").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd1.FreezePanes = true;

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "I1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "ID Analyses Report Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + מיזוג + פונט מודגש לתחום סיווג
            chartRange = xlWorkSheet.get_Range("A2", "I2");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;            

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[2, 1] = "Date";
            xlWorkSheet.Cells[2, 2] = "Machine";
            xlWorkSheet.Cells[2, 3] = "Size";
            xlWorkSheet.Cells[2, 4] = "Catalog";
            xlWorkSheet.Cells[2, 5] = "ID No.";            
            xlWorkSheet.Cells[2, 6] = "ID Num Of \n Rotations";
            xlWorkSheet.Cells[2, 7] = "Actual Num Of \n Rotations";
            xlWorkSheet.Cells[2, 8] = "%";
            xlWorkSheet.Cells[2, 9] = "Quantity"; 

            // הגדרת רוחב עמודה של תחום סיווג
            chartRange = xlWorkSheet.get_Range("A1", "I5000");
            chartRange.ColumnWidth = 10;

            chartRange = xlWorkSheet.get_Range("C1", "D5000");
            chartRange.ColumnWidth = 15;
                
            // התאמת תאריך
            S_Date = DTPicker_From.Value;
            E_Date = DTPicker_To.Value;            

            Str_S_Date = S_Date.Date.AddDays(-1).ToString("yyyy-MM-dd") + "-23.30.00.000000";
            Str_E_Date = E_Date.Date.ToString("yyyy-MM-dd") + "-23.30.00.000000";
            // AS400 התקשרות ל   
            DBConn DBC;

            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";            
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);
                        
            if (checkBox_S1 .Checked)
                StrMachine ="'S1-50','S1-51',";
            if (checkBox_S2 .Checked)
                StrMachine +="'S2-52','S2-53',";
            if (checkBox_S4 .Checked)
                StrMachine +="'S4-54','S4-55'";

            if (StrMachine.Substring(StrMachine.Trim().Length - 1, 1) == ",")
                StrMachine = StrMachine.Substring(0,StrMachine.Trim().Length - 1);
            
            StrSql = "select substring(char(PDATE),1,10),MACHINE,max(DESCRIPTION),PRODUCT,max(DATA12),max(DATA5),DATA6,count(DATA6) " +
                     "from STWIND.PRRP " +
                     "where PDATE >= '" + Str_S_Date + "' and PDATE <= '" + Str_E_Date + "' and MACHINE in (" + StrMachine + ") " +
                     "group by substring(char(PDATE),1,10),MACHINE,PRODUCT,DATA6";            
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {                
                if (DBC.Rs1.GetDecimal(7) >= 2 && DBC.Rs1.GetFloat(5) != 0) 
                {
                    if (Math.Abs(Math.Round((DBC.Rs1.GetFloat(6) - DBC.Rs1.GetFloat(5)) / DBC.Rs1.GetFloat(5) * 100, 2)) >= 2)
                    {
                        xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(0);
                        xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1);
                        xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2);
                        xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(3);
                                                
                        Tmp1 = DBC.Rs1.GetString(4).IndexOf("ID=", 0);
                        Tmp2 = DBC.Rs1.GetString(4).IndexOf("Spec=", Tmp1);
                        
                        xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetString(4).Substring(Tmp1 + 3, Tmp2 - (Tmp1 + 4));
                        xlWorkSheet.Cells[I, 6] = Math.Round(DBC.Rs1.GetFloat(5), 2).ToString();
                        xlWorkSheet.Cells[I, 7] = Math.Round(DBC.Rs1.GetFloat(6), 2).ToString();
                        
                        if (DBC.Rs1.GetFloat(5) != 0)
                            xlWorkSheet.Cells[I, 8] = Math.Round((DBC.Rs1.GetFloat(6) - DBC.Rs1.GetFloat(5)) / DBC.Rs1.GetFloat(5) * 100, 2).ToString();
                        else
                            xlWorkSheet.Cells[I, 8] = "0";
                        
                        xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetDecimal(7).ToString() ;
                        
                        I += 1;
                    }
                }
            }            
            Cursor.Current = Cursors.PanEast;
            xlApp.Visible = true;  
        }
    }
}
