﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_ScrapHistory : Form
    {
        string StrSql;
        string Con_Str;
        
        public Frm_ScrapHistory()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            string Str_S_Date;        // תאריך התחלה לצורך השאילתא
            string Str_E_Date;        // תאריך סיום לצורך השאילתא
            DateTime S_Date;          // תאריך התחלה
            DateTime E_Date;          // תאריך סיום                                    

            int I;
            I = 4;

            /////
            // אנגלית OFFEICE הרצת דוח בגרסת 
            System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
            System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;
            thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            /////

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            
            // Freeze rows            
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A3", "A3").get_Offset(1, 0).EntireRow;
            chartRange.Select();            
            xlWnd1.FreezePanes = true;            

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;
            
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "F1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;            
            xlWorkSheet.Cells[1, 1] = "Scrap History Report Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);            

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AI3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;            

            // מירכוז + מיזוג + פונט מודגש לתחום סיווג
            chartRange = xlWorkSheet.get_Range("A2", "F2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            // יצירת תא עם גבולות
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 1] = "CLASS";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Catalog";
            xlWorkSheet.Cells[3, 3] = "Serial";
            xlWorkSheet.Cells[3, 4] = "Class";
            xlWorkSheet.Cells[3, 5] = "D. Code1";
            xlWorkSheet.Cells[3, 6] = "No. Class";

            // הגדרת רוחב עמודה של תחום סיווג
            chartRange = xlWorkSheet.get_Range("A1", "A5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("B1", "B5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("C1", "C5000");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("D1", "D5000");
            chartRange.ColumnWidth = 5;
            chartRange = xlWorkSheet.get_Range("E1", "E5000");
            chartRange.ColumnWidth = 9;
            chartRange = xlWorkSheet.get_Range("F1", "F5000");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום ביקורת חמה
            chartRange = xlWorkSheet.get_Range("G2", "K2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 7] = "HOT INSPECTION";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 7] = "Date";
            xlWorkSheet.Cells[3, 8] = "Time";
            xlWorkSheet.Cells[3, 9] = "D. Code";
            xlWorkSheet.Cells[3, 10] = "Emp No.";
            xlWorkSheet.Cells[3, 11] = "Name";

            chartRange = xlWorkSheet.get_Range("G1", "G5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("H1", "H5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("I1", "I5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("K1", "K5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("L2", "P2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 12] = "Curing - Finished goods";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 12] = "Date";
            xlWorkSheet.Cells[3, 13] = "Time";
            xlWorkSheet.Cells[3, 14] = "Press";
            xlWorkSheet.Cells[3, 15] = "Emp No.";
            xlWorkSheet.Cells[3, 16] = "Name";            

            chartRange = xlWorkSheet.get_Range("L1", "L5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("M1", "M5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N5000");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("O1", "O5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("P1", "P5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("Q2", "Y2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 17] = "Building Machine Stage 2";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 17] = "Date";            
            xlWorkSheet.Cells[3, 18] = "Time";
            xlWorkSheet.Cells[3, 19] = "Catalog";
            xlWorkSheet.Cells[3, 20] = "Serial";
            xlWorkSheet.Cells[3, 21] = "B. Machine";
            xlWorkSheet.Cells[3, 22] = "Spc. Weight";
            xlWorkSheet.Cells[3, 23] = "Act. Weight";
            xlWorkSheet.Cells[3, 24] = "Emp No.";
            xlWorkSheet.Cells[3, 25] = "Name";

            chartRange = xlWorkSheet.get_Range("Q1", "Q5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("R1", "R5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("S1", "S5000");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("T1", "T5000");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("U1", "U5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("V1", "V5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("W1", "W5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("X1", "X5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("Y1", "Y5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("Z2", "AI2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 26] = "Building Machine Stage 1";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 26] = "Date";
            xlWorkSheet.Cells[3, 27] = "Time";
            xlWorkSheet.Cells[3, 28] = "Catalog";
            xlWorkSheet.Cells[3, 29] = "Serial";
            xlWorkSheet.Cells[3, 30] = "B. Machine";
            xlWorkSheet.Cells[3, 31] = "Spc. Weight";
            xlWorkSheet.Cells[3, 32] = "Act. Weight";
            xlWorkSheet.Cells[3, 33] = "Emp No.";
            xlWorkSheet.Cells[3, 34] = "Name";
            xlWorkSheet.Cells[3, 35] = "Size";

            chartRange = xlWorkSheet.get_Range("Z1", "Z5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("AA1", "AA5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AB1", "AB5000");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("AC1", "AC5000");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("AD1", "AD5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("AE1", "AE5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("AF1", "AF5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("AG1", "AG5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AH1", "AH5000");            
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("AI1", "AI5000");
            chartRange.ColumnWidth = 25;

            // התאמת תאריך
            S_Date = DTPicker_From .Value;
            E_Date = DTPicker_To.Value;
            Str_S_Date = (S_Date.Year - 1928).ToString("00") + S_Date.Month.ToString("00") + S_Date.Day.ToString("00");
            Str_E_Date = (E_Date.Year - 1928).ToString("00") + E_Date.Month.ToString("00") + E_Date.Day.ToString("00");

            // AS400 התקשרות ל   
            DBConn DBC;                        

            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            // שליפת פסולים לתקופה מבוקשת            
            StrSql = "select SSVGDT,SPROD,SMIFRT,SSIVUG,SPGAM1,SMSVEG,INSIZ " +
                      "from ALIQUAL.CTSVGP join BPCSFALI.IIMN on SUBSTRING(SPROD,1,8)=INPROD " +
                      "where SSVGDT between " + Str_S_Date + " and " + Str_E_Date + " and SSIVUG in ('פ','ב')";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (DBC.Rs1.GetString(2).Trim().Length >= 6) // בדיקה רק במצב שזה סידורי חוקי - יכול להיות שמשרשרים לסידורי ערכים בקובץ סיווג לטענת מיכאל רדת
                {
                    // נתוני סיווג
                    xlWorkSheet.Cells[I, 1] = (Convert.ToInt64(DBC.Rs1.GetDecimal(0).ToString().Substring(0, 2)) + 1928).ToString() + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(2, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2);
                    xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1);
                    xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Substring(0, 6);
                    if (DBC.Rs1.GetString(3) == "פ")
                        xlWorkSheet.Cells[I, 4] = "S";
                    else
                        xlWorkSheet.Cells[I, 4] = "DA";
                    xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(4);
                    xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetDecimal(5);
                    xlWorkSheet.Cells[I, 35] = DBC.Rs1.GetString(6); 
                    // נתוני ביקורת חמה
                    StrSql = "select LSTTDT,LSTTTM,LERRDS,LOVED from TAPIALI.LABELGL3 where LPROD='" + DBC.Rs1.GetString(1) + "' and LLBLNO=" + DBC.Rs1.GetString(2).Substring(0, 6) + " and LACTAN=45";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        xlWorkSheet.Cells[I, 7] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                        xlWorkSheet.Cells[I, 8] = FixTime(DBC.Rs2.GetString(1));
                        xlWorkSheet.Cells[I, 9] = DBC.Rs2.GetString(2);
                        xlWorkSheet.Cells[I, 10] = DBC.Rs2.GetDecimal(3);
                        xlWorkSheet.Cells[I, 11] = GetUserName(DBC.Rs2.GetDecimal(3).ToString());
                    }

                    // נתוני גיפור
                    StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED from TAPIALI.LABELGL3 where LPROD='" + DBC.Rs1.GetString(1) + "' and LLBLNO=" + DBC.Rs1.GetString(2).Substring(0, 6) + " and LACTAN=42";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                        xlWorkSheet.Cells[I, 13] = FixTime(DBC.Rs2.GetString(1));
                        xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(2);
                        xlWorkSheet.Cells[I, 15] = DBC.Rs2.GetString(3);
                        xlWorkSheet.Cells[I, 16] = GetUserName(DBC.Rs2.GetString(3));
                    }

                    // נתוני בניה שלב ב
                    StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED,LKGMF*2.205,LKGAC*2.205,KPROD1,KSERIAL1 " +
                             "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                             "where KPROD2='" + DBC.Rs1.GetString(1) + "' and KSERIAL2=" + DBC.Rs1.GetString(2).Substring(0, 6) + " and LACTAN=3";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        xlWorkSheet.Cells[I, 17] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                        xlWorkSheet.Cells[I, 18] = FixTime(DBC.Rs2.GetString(1));
                        xlWorkSheet.Cells[I, 19] = DBC.Rs2.GetString(6);
                        xlWorkSheet.Cells[I, 20] = DBC.Rs2.GetString(7);
                        xlWorkSheet.Cells[I, 21] = DBC.Rs2.GetString(2);
                        xlWorkSheet.Cells[I, 22] = Math.Round(DBC.Rs2.GetDecimal(4),2);
                        xlWorkSheet.Cells[I, 23] = Math.Round(DBC.Rs2.GetDecimal(5),2);
                        xlWorkSheet.Cells[I, 24] = DBC.Rs2.GetString(3);
                        xlWorkSheet.Cells[I, 25] = GetUserName(DBC.Rs2.GetString(3));

                        // נתוני בניה שלב א
                        StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED,LKGMF*2.205,LKGAC*2.205,KPROD1,KSERIAL1 " +
                                 "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                                 "where KPROD2='" + DBC.Rs2.GetString(6) + "' and KSERIAL2=" + DBC.Rs2.GetString(7) + " and LACTAN=3";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet.Cells[I, 26] = DBC.Rs3.GetString(0).Substring(6, 2) + "/" + DBC.Rs3.GetString(0).Substring(4, 2) + "/" + DBC.Rs3.GetString(0).Substring(0, 4);
                            xlWorkSheet.Cells[I, 27] = FixTime(DBC.Rs3.GetString(1));
                            xlWorkSheet.Cells[I, 28] = DBC.Rs3.GetString(6);
                            xlWorkSheet.Cells[I, 29] = DBC.Rs3.GetString(7);
                            xlWorkSheet.Cells[I, 30] = DBC.Rs3.GetString(2);
                            xlWorkSheet.Cells[I, 31] = Math.Round(DBC.Rs3.GetDecimal(4),2);
                            xlWorkSheet.Cells[I, 32] = Math.Round(DBC.Rs3.GetDecimal(5),2);
                            xlWorkSheet.Cells[I, 33] = DBC.Rs3.GetString(3);
                            xlWorkSheet.Cells[I, 34] = GetUserName(DBC.Rs3.GetString(3));
                        }
                    }
                    I += 1;
                }
            }

            //chartRange = xlWorkSheet.get_Range("A1", "AL"+(I-1).ToString());          
            //chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlDashDot;

            Cursor.Current = Cursors.PanEast;
            xlApp.Visible = true;  
        }

        private string GetUserName(string EmpNo) 
        {
            EmpNo= "00" + EmpNo.Trim();

            // AS400 התקשרות ל   
            DBConn DBC2;                        

            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC2 = new DBConn();
            DBC2.Initialize_Conn(Con_Str);

            // שליפת פסולים לתקופה מבוקשת            
            StrSql = "select NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 from ISUFKV.ISAV where OVED='" + EmpNo + "'";
            DBC2.Q_Run1(StrSql);
            if (DBC2.Rs1.Read())
                return EmpNo = strReverse(DBC2.Rs1.GetString(0).Trim()) +" "+ strReverse(DBC2.Rs1.GetString(1).Trim());
            else
                return "";
            
        }
        
        private string strReverse(string s)
        {
            int j = 0;
            char[] c = new char[s.Length];
            for (int i = s.Length - 1; i >= 0; i--) c[j++] = s[i];
            return new string(c);
        }

        private string FixTime(string S)
        {
            if (S.Length == 1)
                S = "00:00:0" + S;
            else 
                if (S.Length == 2)
                    S = "00:00:" + S;
                else
                    if (S.Length == 3)
                        S = "00:0" + S.Substring(0, 1) + ":" + S.Substring(0, 2);
                    else
                        if (S.Length == 4)
                            S = "00:" + S.Substring(0, 2) + ":" + S.Substring(2, 2);
                        else
                            if (S.Length == 5)
                                S = "0" + S.Substring(0, 1) + ":" + S.Substring(1, 2) + ":" + S.Substring(3, 2);
                            else
                                S =  S.Substring(0, 2) + ":" + S.Substring(2, 2) + ":" + S.Substring(4, 2);
            return S;
        }
    }
}
