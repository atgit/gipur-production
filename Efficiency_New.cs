﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Efficiency_New : Form
    {
        public Efficiency_New()
        {
            InitializeComponent();
        }

        private void Efficiancy_New_Load(object sender, EventArgs e)
        {
          
        }

        private void Eficiancyreport()
        {
            string StrSql;
            string machine = "";
            int shifts = 0;
            decimal pcs = 0;
            int j = 4, k = 2;
            int[] months;
            string[,] shft = new string[20, 2];
            string[] monname;
            decimal weight = 0;
            string date = "", previousDate = "";
            string Str_S_Date;
            string Str_E_Date;

            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            DataTable Machs = new DataTable();
            DataTable DT1 = new DataTable();

            // יצירת אקסל
            Excel._Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;
            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            // מחיקת גיליונות אקסל מיותרים
            int WN = 0;
            foreach (Excel.Worksheet xlworksheet in xlWorkBook.Worksheets)
            {
                if (WN != 0) // על מנת לא למחוק את הגיליון הראשון - חייבים להשאיר גיליון 1 
                    xlworksheet.Delete();
                WN++;
            }
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "Efficiency Report By Year";
            xlWorkSheet.Activate();
            //עיצוב תאים 
            chartRange = xlWorkSheet.get_Range("B2", "F2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("G2", "K2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("L2", "P2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("Q2", "U2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("V2", "Z2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AA2", "AE2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AF2", "AJ2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AK2", "AO2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AP2", "AT2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AU2", "AY2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AZ2", "BD2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("BE2", "BI2");
            chartRange.Merge();

            //שמות תאים
            monname = new string[13];
            monname[0] = "January";
            monname[1] = "February";
            monname[2] = "March";
            monname[3] = "April";
            monname[4] = "May";
            monname[5] = "June";
            monname[6] = "July";
            monname[7] = "August";
            monname[8] = "September";
            monname[9] = "October";
            monname[10] = "November";
            monname[11] = "December";
            int p = 0;
            for (int y = 2; y < 62; y = y + 5)
            {
                xlWorkSheet.Cells[2, y] = monname[p];
                p++;
            }

            // התאמת תאריך
            Str_S_Date = (dateTimePicker1.Value.Year - 1928).ToString("00") + dateTimePicker1.Value.Month.ToString("00") + dateTimePicker1.Value.Day.ToString("00");
            Str_E_Date = (dateTimePicker2.Value.Year - 1928).ToString("00") + dateTimePicker2.Value.Month.ToString("00") + dateTimePicker2.Value.Day.ToString("00");

            DateTime date1, date2;
            date1 = dateTimePicker1.Value;
            date2 = dateTimePicker2.Value;
            var diffMonths = (date2.Month + date2.Year * 12) - (date1.Month + date1.Year * 12) + 1;
            // בניית מערך חודשים 
            StrSql = "select distinct substring(ttdte,1,4) " +
                      "from BPCSFV30.Flt " +
                      "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and " +
                      "tdept in (71,160) and tpcs<>0 ";
            DT = dbs.executeSelectQueryNoParam(StrSql);

            months = new int[diffMonths];
            int i = 0;
            foreach (DataRow row in DT.Rows)
            {
                months[i] = int.Parse((int.Parse((row[0].ToString().Substring(0, 2))) + 1928).ToString().Substring(2, 2)) * 100 + (int.Parse(row[0].ToString().Substring(2, 2)));
                i++;
            }

            // שליפת רשימת כלל המכונות
            StrSql = "select distinct tmach from bpcsfali.fltqv " +
                     "where tdept in (71,160) and tpcs<>0";
            Machs = dbs.executeSelectQueryNoParam(StrSql);

            for (i = 2; i < 62; i++)
            {
                xlWorkSheet.Cells[3, i] = "Shifts";
                xlWorkSheet.Cells[3, ++i] = "Pcs";
                xlWorkSheet.Cells[3, ++i] = "W (Ton)";
                xlWorkSheet.Cells[3, ++i] = "Pcs/Shift";
                xlWorkSheet.Cells[3, ++i] = "W/S (Ton)";
            }

            foreach (DataRow mach in Machs.Rows)
            {
                DT = TotalInfo(mach["tmach"].ToString(), Str_S_Date, Str_E_Date);
                foreach (DataRow row in DT.Rows)
                {
                    date = row["date"].ToString().Substring(0,2);
                    shifts = int.Parse(row["shift"].ToString());
                    pcs = decimal.Parse(row["pcs"].ToString());
                    weight = Math.Round(decimal.Parse(row["weight"].ToString()), 2);

                    k = ((int.Parse(date)) * 5) - 3;// ווידוא השמת נתונים בחודש מתאים
                    xlWorkSheet.Cells[j, 1] = mach["tmach"].ToString();
                    xlWorkSheet.Cells[j, k] = shifts;
                    k++;
                    xlWorkSheet.Cells[j, k] = pcs;
                    k++;
                    xlWorkSheet.Cells[j, k] = weight;
                    k++;
                    xlWorkSheet.Cells[j, k] = Math.Round(pcs / shifts, 2);
                    k++;
                    xlWorkSheet.Cells[j, k] = Math.Round(weight / shifts, 2);
                }
                if (DT.Rows.Count > 0)
                {
                    j++;
                }
            }

            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
        }

        // הפונקציה תחזיר את כמות היחידות שנוצרו בכל חודש בטווח הנבדק
        /// <param name="mach"></param>
        /// <param name="dateFrom"></param>Str_S_Date 
        /// <param name="dateTo"></param>Str_E_Date
        public DataTable TotalInfo(string mach, string dateFrom, string dateTo)
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();

            string StrSql = "select sum(TPCS) as pcs,count(distinct ttdte || tshft) as Shift, substring(ttdte, 3, 2) as date,sum(TPCS * TWGHT) / 1000 as Weight " +
                            "from BPCSFALI.FltQV " +
                            "where TTDTE between " + dateFrom + " and " + dateTo + " and tdept in (71,160) and tpcs<> 0 and tmach = '" + mach + "' " +
                            "group by right(left(ttdte,4),2)||left(ttdte,2), substring(ttdte,3, 2) ";
           return  DT = dbs.executeSelectQueryNoParam(StrSql);
        }

        // הפונקציה מחזירה את כמות המשמרות שעבדה המכונה בטווח הנבדק
        /// <param name="mach"></param>
        /// <param name="dateFrom"></param>Str_S_Date 
        /// <param name="dateTo"></param>Str_E_Date
        public DataTable TotalShifts(string mach, string dateFrom, string dateTo)
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();

            string StrSql = "select count(distinct ttdte || tshft) as Shift, substring(ttdte,3,2) " +
                            "from BPCSFALI.FltQV " +
                            "where TTDTE between " + dateFrom + " and " + dateTo + " and tdept in (71,160) and tpcs<> 0 and tmach = '" + mach + "' " +
                            "group by right(left(ttdte, 4), 2) || left(ttdte, 2), substring(ttdte,3,2) ";
            return DT = dbs.executeSelectQueryNoParam(StrSql);
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            Eficiancyreport();
        }
    }
}
