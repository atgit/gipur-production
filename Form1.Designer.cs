﻿namespace Gipur_Production
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.hMICurringReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.weighingReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hMICuringControllerInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mixersReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overWeightAnalysisReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hotInspectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scrapHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.traceabilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tireDefectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classifiedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rNTRROTKINOUTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rNTRROToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.efficiencyReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.michelinReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pressesWarmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qastopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_Prod = new System.Windows.Forms.TextBox();
            this.Txt_Size = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_Design = new System.Windows.Forms.TextBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CBox_Machine = new System.Windows.Forms.ComboBox();
            this.Lbl_Machine = new System.Windows.Forms.Label();
            this.carcasWeighingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.menuStrip2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(962, 115);
            this.panel1.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(361, 47);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(238, 46);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.SlateGray;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip2.Size = new System.Drawing.Size(962, 24);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.classifiedToolStripMenuItem,
            this.carcasWeighingToolStripMenuItem,
            this.efficiencyReportToolStripMenuItem,
            this.hMICuringControllerInfoToolStripMenuItem,
            this.hMICurringReportToolStripMenuItem,
            this.hotInspectionToolStripMenuItem,
            this.michelinReportToolStripMenuItem,
            this.mixersReportToolStripMenuItem,
            this.overWeightAnalysisReportToolStripMenuItem,
            this.pressesWarmToolStripMenuItem,
            this.qastopToolStripMenuItem,
            this.rNTRROToolStripMenuItem,
            this.rNTRROTKINOUTToolStripMenuItem,
            this.scrapHistoryToolStripMenuItem,
            this.tireDefectToolStripMenuItem,
            this.traceabilityToolStripMenuItem,
            this.weighingReportToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // hMICurringReportToolStripMenuItem
            // 
            this.hMICurringReportToolStripMenuItem.Name = "hMICurringReportToolStripMenuItem";
            this.hMICurringReportToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.hMICurringReportToolStripMenuItem.Text = "HMI Curring Report (Taxi)";
            this.hMICurringReportToolStripMenuItem.Click += new System.EventHandler(this.hMICurringReportToolStripMenuItem_Click);
            // 
            // weighingReportToolStripMenuItem
            // 
            this.weighingReportToolStripMenuItem.Name = "weighingReportToolStripMenuItem";
            this.weighingReportToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.weighingReportToolStripMenuItem.Text = "Weighing Report";
            this.weighingReportToolStripMenuItem.Click += new System.EventHandler(this.weighingReportToolStripMenuItem_Click);
            // 
            // hMICuringControllerInfoToolStripMenuItem
            // 
            this.hMICuringControllerInfoToolStripMenuItem.Name = "hMICuringControllerInfoToolStripMenuItem";
            this.hMICuringControllerInfoToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.hMICuringControllerInfoToolStripMenuItem.Text = "HMI Curing Controller Info";
            this.hMICuringControllerInfoToolStripMenuItem.Click += new System.EventHandler(this.hMICuringControllerInfoToolStripMenuItem_Click);
            // 
            // mixersReportToolStripMenuItem
            // 
            this.mixersReportToolStripMenuItem.Name = "mixersReportToolStripMenuItem";
            this.mixersReportToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.mixersReportToolStripMenuItem.Text = "Mixers Report";
            this.mixersReportToolStripMenuItem.Click += new System.EventHandler(this.mixersReportToolStripMenuItem_Click);
            // 
            // overWeightAnalysisReportToolStripMenuItem
            // 
            this.overWeightAnalysisReportToolStripMenuItem.Name = "overWeightAnalysisReportToolStripMenuItem";
            this.overWeightAnalysisReportToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.overWeightAnalysisReportToolStripMenuItem.Text = "Over Weight Analysis Report";
            this.overWeightAnalysisReportToolStripMenuItem.Click += new System.EventHandler(this.overWeightAnalysisReportToolStripMenuItem_Click);
            // 
            // hotInspectionToolStripMenuItem
            // 
            this.hotInspectionToolStripMenuItem.Name = "hotInspectionToolStripMenuItem";
            this.hotInspectionToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.hotInspectionToolStripMenuItem.Text = "Hot Inspection (All)";
            this.hotInspectionToolStripMenuItem.Click += new System.EventHandler(this.hotInspectionToolStripMenuItem_Click);
            // 
            // scrapHistoryToolStripMenuItem
            // 
            this.scrapHistoryToolStripMenuItem.Name = "scrapHistoryToolStripMenuItem";
            this.scrapHistoryToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.scrapHistoryToolStripMenuItem.Text = "Scrap History";
            this.scrapHistoryToolStripMenuItem.Click += new System.EventHandler(this.scrapHistoryToolStripMenuItem_Click);
            // 
            // traceabilityToolStripMenuItem
            // 
            this.traceabilityToolStripMenuItem.Name = "traceabilityToolStripMenuItem";
            this.traceabilityToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.traceabilityToolStripMenuItem.Text = "Traceability";
            this.traceabilityToolStripMenuItem.Click += new System.EventHandler(this.traceabilityToolStripMenuItem_Click);
            // 
            // tireDefectToolStripMenuItem
            // 
            this.tireDefectToolStripMenuItem.Name = "tireDefectToolStripMenuItem";
            this.tireDefectToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.tireDefectToolStripMenuItem.Text = "Tire Defect";
            this.tireDefectToolStripMenuItem.Click += new System.EventHandler(this.tireDefectToolStripMenuItem_Click);
            // 
            // classifiedToolStripMenuItem
            // 
            this.classifiedToolStripMenuItem.Name = "classifiedToolStripMenuItem";
            this.classifiedToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.classifiedToolStripMenuItem.Text = "Classified";
            this.classifiedToolStripMenuItem.Click += new System.EventHandler(this.classifiedToolStripMenuItem_Click);
            // 
            // rNTRROTKINOUTToolStripMenuItem
            // 
            this.rNTRROTKINOUTToolStripMenuItem.Name = "rNTRROTKINOUTToolStripMenuItem";
            this.rNTRROTKINOUTToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.rNTRROTKINOUTToolStripMenuItem.Text = "RNT RRO TK ( IN OUT )";
            this.rNTRROTKINOUTToolStripMenuItem.Click += new System.EventHandler(this.rNTRROTKINOUTToolStripMenuItem_Click);
            // 
            // rNTRROToolStripMenuItem
            // 
            this.rNTRROToolStripMenuItem.Name = "rNTRROToolStripMenuItem";
            this.rNTRROToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.rNTRROToolStripMenuItem.Text = "RNT RRO";
            this.rNTRROToolStripMenuItem.Visible = false;
            this.rNTRROToolStripMenuItem.Click += new System.EventHandler(this.rNTRROToolStripMenuItem_Click);
            // 
            // efficiencyReportToolStripMenuItem
            // 
            this.efficiencyReportToolStripMenuItem.Name = "efficiencyReportToolStripMenuItem";
            this.efficiencyReportToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.efficiencyReportToolStripMenuItem.Text = "Efficiency Report";
            this.efficiencyReportToolStripMenuItem.Click += new System.EventHandler(this.efficiancyReportToolStripMenuItem_Click);
            // 
            // michelinReportToolStripMenuItem
            // 
            this.michelinReportToolStripMenuItem.Name = "michelinReportToolStripMenuItem";
            this.michelinReportToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.michelinReportToolStripMenuItem.Text = "Michelin Report";
            this.michelinReportToolStripMenuItem.Click += new System.EventHandler(this.michelinReportToolStripMenuItem_Click);
            // 
            // pressesWarmToolStripMenuItem
            // 
            this.pressesWarmToolStripMenuItem.Name = "pressesWarmToolStripMenuItem";
            this.pressesWarmToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.pressesWarmToolStripMenuItem.Text = "Presses Warm Up";
            this.pressesWarmToolStripMenuItem.Click += new System.EventHandler(this.pressesWarmToolStripMenuItem_Click);
            // 
            // qastopToolStripMenuItem
            // 
            this.qastopToolStripMenuItem.Name = "qastopToolStripMenuItem";
            this.qastopToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.qastopToolStripMenuItem.Text = "Qastop";
            this.qastopToolStripMenuItem.Click += new System.EventHandler(this.qastopToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(225, 22);
            this.toolStripMenuItem2.Text = "About";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(225, 22);
            this.toolStripMenuItem3.Text = "Exit";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.BackColor = System.Drawing.Color.PowderBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(304, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "From";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dateTimePicker1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(402, 202);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(114, 25);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.BackColor = System.Drawing.Color.PowderBlue;
            this.label3.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(304, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 22);
            this.label3.TabIndex = 11;
            this.label3.Text = "To";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dateTimePicker2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(402, 235);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(114, 25);
            this.dateTimePicker2.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button1.Location = new System.Drawing.Point(420, 505);
            this.button1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 28);
            this.button1.TabIndex = 18;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.BackColor = System.Drawing.Color.PowderBlue;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(304, 375);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 22);
            this.label4.TabIndex = 17;
            this.label4.Text = "Catalog";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.BackColor = System.Drawing.Color.PowderBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label5.Location = new System.Drawing.Point(303, 404);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 22);
            this.label5.TabIndex = 18;
            this.label5.Text = "Size";
            // 
            // Txt_Prod
            // 
            this.Txt_Prod.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Txt_Prod.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Prod.Location = new System.Drawing.Point(402, 375);
            this.Txt_Prod.MaxLength = 8;
            this.Txt_Prod.Name = "Txt_Prod";
            this.Txt_Prod.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Prod.Size = new System.Drawing.Size(256, 21);
            this.Txt_Prod.TabIndex = 12;
            // 
            // Txt_Size
            // 
            this.Txt_Size.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Txt_Size.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Size.Location = new System.Drawing.Point(402, 403);
            this.Txt_Size.Name = "Txt_Size";
            this.Txt_Size.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Size.Size = new System.Drawing.Size(256, 21);
            this.Txt_Size.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.BackColor = System.Drawing.Color.PowderBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label6.Location = new System.Drawing.Point(303, 432);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 22);
            this.label6.TabIndex = 19;
            this.label6.Text = "Pattern";
            // 
            // Txt_Design
            // 
            this.Txt_Design.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Txt_Design.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Design.Location = new System.Drawing.Point(402, 431);
            this.Txt_Design.MaxLength = 39;
            this.Txt_Design.Name = "Txt_Design";
            this.Txt_Design.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Design.Size = new System.Drawing.Size(256, 21);
            this.Txt_Design.TabIndex = 14;
            this.Txt_Design.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Design_KeyPress);
            // 
            // checkBox4
            // 
            this.checkBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox4.BackColor = System.Drawing.Color.PowderBlue;
            this.checkBox4.Checked = true;
            this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox4.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.checkBox4.Location = new System.Drawing.Point(304, 292);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(95, 22);
            this.checkBox4.TabIndex = 8;
            this.checkBox4.Text = "Curing";
            this.checkBox4.UseVisualStyleBackColor = false;
            // 
            // checkBox5
            // 
            this.checkBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox5.BackColor = System.Drawing.Color.PowderBlue;
            this.checkBox5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.checkBox5.Location = new System.Drawing.Point(304, 322);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(95, 22);
            this.checkBox5.TabIndex = 9;
            this.checkBox5.Text = "Building";
            this.checkBox5.UseVisualStyleBackColor = false;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(267, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(429, 29);
            this.label2.TabIndex = 7;
            this.label2.Text = "Curing / Building Production Report";
            // 
            // CBox_Machine
            // 
            this.CBox_Machine.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CBox_Machine.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.CBox_Machine.FormattingEnabled = true;
            this.CBox_Machine.Location = new System.Drawing.Point(402, 460);
            this.CBox_Machine.Name = "CBox_Machine";
            this.CBox_Machine.Size = new System.Drawing.Size(101, 23);
            this.CBox_Machine.TabIndex = 34;
            this.CBox_Machine.Visible = false;
            // 
            // Lbl_Machine
            // 
            this.Lbl_Machine.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Lbl_Machine.BackColor = System.Drawing.Color.PowderBlue;
            this.Lbl_Machine.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.Lbl_Machine.Location = new System.Drawing.Point(303, 460);
            this.Lbl_Machine.Name = "Lbl_Machine";
            this.Lbl_Machine.Size = new System.Drawing.Size(95, 22);
            this.Lbl_Machine.TabIndex = 33;
            this.Lbl_Machine.Text = "Building M.";
            this.Lbl_Machine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Lbl_Machine.Visible = false;
            // 
            // carcasWeighingToolStripMenuItem
            // 
            this.carcasWeighingToolStripMenuItem.Name = "carcasWeighingToolStripMenuItem";
            this.carcasWeighingToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.carcasWeighingToolStripMenuItem.Text = "Carcas Weighing Comparison";
            this.carcasWeighingToolStripMenuItem.Click += new System.EventHandler(this.carcasWeighingToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(963, 655);
            this.Controls.Add(this.CBox_Machine);
            this.Controls.Add(this.Lbl_Machine);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.Txt_Design);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_Size);
            this.Controls.Add(this.Txt_Prod);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Curing / Building Production";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Txt_Prod;
        private System.Windows.Forms.TextBox Txt_Size;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_Design;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.ToolStripMenuItem scrapHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qastopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tireDefectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classifiedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hotInspectionToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem rNTRROToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem traceabilityToolStripMenuItem;
        private System.Windows.Forms.ComboBox CBox_Machine;
        private System.Windows.Forms.Label Lbl_Machine;
        private System.Windows.Forms.ToolStripMenuItem weighingReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hMICurringReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rNTRROTKINOUTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem efficiencyReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overWeightAnalysisReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mixersReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hMICuringControllerInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem michelinReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pressesWarmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem carcasWeighingToolStripMenuItem;
    }
}

