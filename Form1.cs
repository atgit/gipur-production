﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Form1 : Form
    {
        // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל
        //public Excel.Application xl = new Microsoft.Office.Interop.Excel.Application();
        //public Excel.Workbook wb;
        //public Excel.Worksheet ws;
        //public Excel.Range rangeF;
        //public Excel.Range rangeB;

        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Application.Exit();  
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AboutBox1 Abox = new AboutBox1();
            Abox.Show();
        }

        
        //public string Scrap_Tire(string Tmp_Catalog,string Tmp_Date)
        //{
        // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל   
        //    Tmp_Date = Tmp_Date.Substring(6, 2) + "/" + Tmp_Date.Substring(4, 2) + "/" + Tmp_Date.Substring(0, 4);
        //    // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל
        //    Excel.Range rangeF = ws.get_Range("C440:C2000", Type.Missing);     
        //    string SDate = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
        //    int ;rapCount = 0;
            
        //    if (rangeF != null)
        //    {
        //        foreach (Excel.Range r in rangeF)
        //        {
        //            if (r.Text.ToString().Trim() != "")
        //            {
        //                if (r.Text.ToString().Trim() == Tmp_Catalog) // בדיקה אם היה דיווח למק"ט בקובץ פסולים
        //                {                            
        //                    rangeB = ws.get_Range("F" + r.Row.ToString() + ":F" + r.Row.ToString(), Type.Missing);
        //                    if (rangeB.Text == Tmp_Date)
        //                        ScrapCount += 1;
        //                }
        //            }
        //            else
        //                return ScrapCount.ToString();                                                            
        //        }
        //    }
        //    return ScrapCount.ToString();
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            if (dateTimePicker1.Value.Date > dateTimePicker2.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                dateTimePicker1.Focus();
                return;
            }

            //              19/09/2012 שינוי מתאריך 
            // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
            int TotalForCatalog = 0;
            string LastCatalog = "";

            //           24/03/2010 שינוי מתאריך 
            // הוספתי אפשרות לחיתוך לפי דגם צמיג

            string[] Des_Arr = new string[10];
            int A, B, C;
            B = 0;
            C = 0;

            // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
            //           אפשרתי למשתמש לבחור עד 10 דגמים
            //          מערך המכיל את הדגמים שהמשתמש בחר
            if (Txt_Design.Text.Trim() != "")
            {
                for (A = 0; A <= Txt_Design.Text.Trim().Length - 1; A++)
                {
                    if (Txt_Design.Text.Substring(A, 1) == ".")
                    {
                        Des_Arr[C] = Txt_Design.Text.Substring(B, 3);
                        B = A + 1;
                        C += 1;
                    }
                }
                // תפיסת רשומה אחרונה
                Des_Arr[C] = Txt_Design.Text.Trim().Substring(Txt_Design.Text.Trim().Length - 3, 3);
            }

            if (!checkBox4.Checked && !checkBox5.Checked )
            {
                MessageBox.Show("Mark Bulding / Curing or Both !", "Curing / Building Production");
                return;
            }                       

            Cursor.Current = Cursors.WaitCursor;
            
            string Str_S_Date;        // תאריך התחלה לצורך השאילתא
            string Str_E_Date;        // תאריך סיום לצורך השאילתא
            DateTime S_Date;          // תאריך התחלה
            DateTime E_Date;          // תאריך סיום            
            string StrSql;
            string TmpStr;           
           
            int I;
            I = 3;            

            /////
            // אנגלית OFFEICE הרצת דוח בגרסת 
            System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
            System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;
            thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            /////

            if (checkBox4.Checked )
            {
                // תיקון מתאריך 11/07/2011
                // הוספת שני שדות לדוח גיפור
                string MFSZ = "";
                string MFNO = "";
                string MFVRN = "";
                string[] PressArr = new string[14];
                PressArr[0] = "353";
                PressArr[1] = "354";                
                PressArr[2] = "338";
                PressArr[3] = "339";
                PressArr[4] = "328";
                PressArr[5] = "329";
                PressArr[6] = "325";
                PressArr[7] = "322";
                PressArr[8] = "323";
                PressArr[9] = "336";
                PressArr[10] = "337";
                PressArr[11] = "624";
                PressArr[12] = "602";
                PressArr[13] = "606";
                
                // יצירת קובץ אקסל
                // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                Excel.Range chartRange;

                xlApp = new Excel.ApplicationClass();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlWorkSheet.Cells[1, 1] = "Curing Production Report Between : " + dateTimePicker1.Value.ToString().Substring(0, 10) + " And  " + dateTimePicker2.Value.ToString().Substring(0, 10);

                // הצגת גיליון משמאל לימין
                xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;                
                xlWorkSheet.DisplayRightToLeft = false; 

                // מירכוז + מיזוג + פונט מודגש לתחום            
                chartRange = xlWorkSheet.get_Range("A1", "E1");
                chartRange.MergeCells = true;
                chartRange.HorizontalAlignment = 3;
                chartRange.VerticalAlignment = 3;
                chartRange.Font.Bold = true;

                // הגדרת שדות הדוח                
                xlWorkSheet.Cells[2, 1] = "Date";
                xlWorkSheet.Cells[2, 2] = "Catalog";
                xlWorkSheet.Cells[2, 3] = "Size";
                xlWorkSheet.Cells[2, 4] = "Dept";
                xlWorkSheet.Cells[2, 5] = "Quantity";
                xlWorkSheet.Cells[2, 6] = "Weight Without Flap";
                
                // תוספת מתאריך 18/06/2015
                // הורדת הנתון מחיר
                //xlWorkSheet.Cells[2, 7] = "Cost Without Flap";
                xlWorkSheet.Cells[2, 7] = "Specification";
                xlWorkSheet.Cells[2, 8] = "Press";
                
                // תיקון מתאריך 11/07/2011
                xlWorkSheet.Cells[2, 9] = "PI";
                xlWorkSheet.Cells[2, 10] = "Loader";
                xlWorkSheet.Cells[2, 11] = "Fabric";
                xlWorkSheet.Cells[2, 12] = "Paint";
                xlWorkSheet.Cells[2, 13] = "D/R";
                xlWorkSheet.Cells[2, 14] = "Michelin"; 

                // מירכוז + פונט מודגש לתחום            
                chartRange = xlWorkSheet.get_Range("A2", "N2");
                chartRange.HorizontalAlignment = 3;
                chartRange.VerticalAlignment = 3;
                chartRange.Font.Bold = true;

                // הגדרת רוחב עמודה של תחום
                chartRange = xlWorkSheet.get_Range("A1", "A1");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("B1", "B1");
                chartRange.ColumnWidth = 15;
                chartRange = xlWorkSheet.get_Range("C1", "C1");
                chartRange.ColumnWidth = 25;
                chartRange = xlWorkSheet.get_Range("D1", "D1");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("E1", "E1");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("F1", "F1");
                chartRange.ColumnWidth = 18;
                //chartRange = xlWorkSheet.get_Range("G1", "G1");
                //chartRange.ColumnWidth = 16;
                chartRange = xlWorkSheet.get_Range("G1", "G1");
                chartRange.ColumnWidth = 15;
                chartRange = xlWorkSheet.get_Range("H1", "H1");
                chartRange.ColumnWidth = 10;
                // תיקון מתאריך 11/07/2011
                chartRange = xlWorkSheet.get_Range("I1", "I1");
                chartRange.ColumnWidth = 13;
                chartRange = xlWorkSheet.get_Range("J1", "J1");
                chartRange.ColumnWidth = 10;

                chartRange = xlWorkSheet.get_Range("A3", "A50000");
                chartRange.NumberFormat = "@";
                
                // Freeze rows            
                Excel.Window xlWnd1 = xlApp.ActiveWindow;
                chartRange = xlWorkSheet.get_Range("A2", "A2").get_Offset(1, 0).EntireRow;
                chartRange.Select();            
                xlWnd1.FreezePanes = true;

                // AutoFilter
                chartRange = xlWorkSheet.get_Range("A2", "N2");
                chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

                // התאמת תאריך
                S_Date = dateTimePicker1.Value;
                E_Date = dateTimePicker2.Value;
                Str_S_Date = (S_Date.Year - 1928).ToString("00") + S_Date.Month.ToString("00") + S_Date.Day.ToString("00");
                Str_E_Date = (E_Date.Year - 1928).ToString("00") + E_Date.Month.ToString("00") + E_Date.Day.ToString("00");

                // AS400 התקשרות ל   
                DBConn DBC;
                //string Qry_Str;
                string Con_Str;

                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";                
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);

                // תוספת מתאריך 18/06/2015
                // הורדת הנתון מחיר
                // הרכבת משפט שליפה
                //StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                //         "from BPCSFV30.FLTL89 join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                //                               "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                //         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (161,162)";
                StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(TMFRT),TMACH,max(INFAB),max(SUBSTRING(EFIL3,1,1)),max(INDOR) " +
                         "from BPCSFV30.FLTL89 join " +
                              "BPCSFALI.IIMNl01 on SUBSTRING(TPROD,1,8)=INPROD left join " +
                              "BPCSFALI.IIMNE on SUBSTRING(TPROD,1,8)=EMKT15 " +
                         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (161,162)";                                
                if (Txt_Prod.Text.Trim() != "")
                    StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";

                if (Txt_Size.Text.Trim() != "")
                {
                    Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                    StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                }

                StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";

                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                    TmpStr = TmpStr.Substring(6, 2) + "/" + TmpStr.Substring(4, 2) + "/" + TmpStr.Substring(0, 4);
                    // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                    if (Txt_Design.Text.Trim() != "")
                    {
                        for (A = 0; A <= C; A++)
                        {
                            if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                            {
                                //              19/09/2012 שינוי מתאריך 
                                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                if (LastCatalog == "")
                                {
                                    LastCatalog = DBC.Rs1.GetString(1).Substring(0, 8);
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                }
                                else
                                {
                                    if (LastCatalog == DBC.Rs1.GetString(1).Substring(0, 8))
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    else
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1).Substring(0, 8);
                                        xlWorkSheet.Cells[I, 4] = "Total";
                                        xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                        I += 1;
                                        TotalForCatalog = DBC.Rs1.GetInt16(4);
                                    }
                                }
                                xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Substring(0, 8);
                                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                                xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetString(8).Trim();
                                if (DBC.Rs1.GetString(9) == "Y")
                                    xlWorkSheet.Cells[I, 12] = "N";
                                else
                                    xlWorkSheet.Cells[I, 12] = "Y";
                                xlWorkSheet.Cells[I, 13] = DBC.Rs1.GetString(10).Trim();

                                // תוספת מתאריך 19/04/2017
                                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                                if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                                    xlWorkSheet.Cells[I, 14] = "Michelin";

                                // תיקון מתאריך 11/07/2011
                                // בדיקה אם צמיג דיגונלי ובהמשך בדיקה אם צריך מתקן קירור                                                
                                if (DBC.Rs1.GetString(2).Trim().IndexOf("R") < 0)
                                {
                                    if (DBC.Rs1.GetString(6).Trim().Length == 11)
                                    {
                                        MFSZ = DBC.Rs1.GetString(6).Trim().Substring(2, 2);
                                        MFNO = DBC.Rs1.GetString(6).Trim().Substring(5, 3);
                                        MFVRN = DBC.Rs1.GetString(6).Trim().Substring(9, 2);
                                    }
                                    else if (DBC.Rs1.GetString(6).Trim().Length == 12)
                                    {
                                        MFSZ = DBC.Rs1.GetString(6).Trim().Substring(3, 2);
                                        MFNO = DBC.Rs1.GetString(6).Trim().Substring(6, 3);
                                        MFVRN = DBC.Rs1.GetString(6).Trim().Substring(10, 2);
                                    }
                                    else if (DBC.Rs1.GetString(6).Trim().Length == 13)
                                    {
                                        MFSZ = DBC.Rs1.GetString(6).Trim().Substring(4, 2);
                                        MFNO = DBC.Rs1.GetString(6).Trim().Substring(7, 3);
                                        MFVRN = DBC.Rs1.GetString(6).Trim().Substring(11, 2);
                                    }
                                    StrSql = "select MFPING from MFRT.MFRTP where MFSZ='" + MFSZ + "' and MFNO=" + MFNO + " and MFVRN=" + MFVRN + "";
                                    DBC.Q_Run2(StrSql);
                                    if (DBC.Rs2.Read())
                                    {
                                        if (DBC.Rs2.GetString(0).Trim() == "0")
                                        {
                                        }
                                        else
                                        {
                                            xlWorkSheet.Cells[I, 9] = "Yes";
                                        }
                                    }
                                    DBC.Rs2.Close(); 
                                }
                                // בדיקה אם צמיג רדיאלי ובהמשך בדיקה אם יש לאודר 
                                if (DBC.Rs1.GetString(2).Trim().IndexOf("R") > 0)
                                {
                                    for (B = 0; B < 14; B++)
                                    {
                                        if (DBC.Rs1.GetString(7).Trim() == PressArr[B])
                                        {
                                            xlWorkSheet.Cells[I, 10] = "No";
                                        }
                                    }
                                }

                                I += 1;
                            }
                        }
                    }
                    else
                    {
                        //              19/09/2012 שינוי מתאריך 
                        // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                        if (LastCatalog == "")
                        {
                            LastCatalog = DBC.Rs1.GetString(1).Substring(0, 8);
                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                        }
                        else
                        {
                            if (LastCatalog == DBC.Rs1.GetString(1).Substring(0, 8))
                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                            else
                            {
                                LastCatalog = DBC.Rs1.GetString(1).Substring(0, 8);
                                xlWorkSheet.Cells[I, 4] = "Total";
                                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                I += 1;
                                TotalForCatalog = DBC.Rs1.GetInt16(4);
                            }
                        }                        
                        
                        xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                        xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Substring(0, 8);
                        xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                        xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                        xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                        xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                        // תוספת מתאריך 18/06/2015
                        // הורדת הנתון מחיר
                        //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                        xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                        xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                        xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetString(8).Trim();

                        if (DBC.Rs1.GetString(9) == "Y")
                            xlWorkSheet.Cells[I, 12] = "N";
                        else
                            xlWorkSheet.Cells[I, 12] = "Y";
                        xlWorkSheet.Cells[I, 13] = DBC.Rs1.GetString(10).Trim();

                        // תוספת מתאריך 19/04/2017
                        // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                        if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                            xlWorkSheet.Cells[I, 14] = "Michelin";

                        // תיקון מתאריך 11/07/2011
                        // בדיקה אם צמיג דיגונלי ובהמשך בדיקה אם צריך מתקן קירור                                                
                        if (DBC.Rs1.GetString(2).Trim().IndexOf("R") < 0)
                        {
                            if (DBC.Rs1.GetString(6).Trim().Length == 11)
                                {
                                    MFSZ = DBC.Rs1.GetString(6).Trim().Substring(2, 2);
                                    MFNO = DBC.Rs1.GetString(6).Trim().Substring(5, 3);
                                    MFVRN = DBC.Rs1.GetString(6).Trim().Substring(9, 2);
                                }
                                else if (DBC.Rs1.GetString(6).Trim().Length == 12)
                                    {
                                        MFSZ = DBC.Rs1.GetString(6).Trim().Substring(3, 2);
                                        MFNO = DBC.Rs1.GetString(6).Trim().Substring(6, 3);
                                        MFVRN = DBC.Rs1.GetString(6).Trim().Substring(10, 2);
                                    }
                                    else if (DBC.Rs1.GetString(6).Trim().Length == 13)
                                        {
                                            MFSZ = DBC.Rs1.GetString(6).Trim().Substring(4, 2);
                                            MFNO = DBC.Rs1.GetString(6).Trim().Substring(7, 3);
                                            MFVRN = DBC.Rs1.GetString(6).Trim().Substring(11, 2);
                                        }
                            StrSql = "select MFPING from MFRT.MFRTP where MFSZ='"+MFSZ+"' and MFNO="+MFNO+" and MFVRN="+MFVRN+"";
                            DBC.Q_Run2(StrSql);
                            if (DBC.Rs2.Read())
                            {
                                if (DBC.Rs2.GetString(0).Trim() == "0")
                                {
                                }
                                else
                                {
                                    xlWorkSheet.Cells[I, 9] = "Yes";
                                }
                            }
                            DBC.Rs2.Close(); 
                        }                        
                        // בדיקה אם צמיג רדיאלי ובהמשך בדיקה אם יש לאודר 
                        if (DBC.Rs1.GetString(2).Trim().IndexOf("R") > 0)
                        {
                            for (B = 0; B < 14; B++)
                            {
                                if (DBC.Rs1.GetString(7).Trim() == PressArr[B])
                                {
                                    xlWorkSheet.Cells[I, 10] = "No";
                                }
                            }
                        }

                        I += 1;
                    }
                }
                DBC.Rs1.Close();

                //              19/09/2012 שינוי מתאריך 
                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                xlWorkSheet.Cells[I, 4] = "Total";
                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                TotalForCatalog = 0;
                LastCatalog = "";

                // בדיקה אם מבקשים להפיק דוח בניה
                if (checkBox5.Checked==true)
                {                    
                    I += 1;                    
                    xlWorkSheet.Cells[I, 1] = "Building Production Report Between : " + dateTimePicker1.Value.ToString().Substring(0, 10) + " And  " + dateTimePicker2.Value.ToString().Substring(0, 10);

                    // מירכוז + מיזוג + פונט מודגש לתחום            
                    chartRange = xlWorkSheet.get_Range("A"+I+"", "E"+I+"");
                    chartRange.MergeCells = true;
                    chartRange.HorizontalAlignment = 3;
                    chartRange.VerticalAlignment = 3;
                    chartRange.Font.Bold = true;
                    
                    I += 1;
                    // הגדרת שדות הדוח
                    xlWorkSheet.Cells[I, 1] = "Date";
                    xlWorkSheet.Cells[I, 2] = "Catalog";
                    xlWorkSheet.Cells[I, 3] = "Size";
                    xlWorkSheet.Cells[I, 4] = "Dept";
                    xlWorkSheet.Cells[I, 5] = "Quantity";
                    xlWorkSheet.Cells[I, 6] = "Weight Without Flap";
                    // תוספת מתאריך 18/06/2015
                    // הורדת הנתון מחיר
                    //xlWorkSheet.Cells[I, 7] = "Cost Without Flap";
                    xlWorkSheet.Cells[I, 7] = "Specification";
                    xlWorkSheet.Cells[I, 8] = "Machine";
                    xlWorkSheet.Cells[I, 9] = "Carcas/Green";
                    xlWorkSheet.Cells[I, 10] = "Michelin";
                    xlWorkSheet.Cells[I, 11] = "R / D";

                    // מירכוז + פונט מודגש לתחום            
                    chartRange = xlWorkSheet.get_Range("A" + I + "", "J" + I + "");
                    chartRange.HorizontalAlignment = 3;
                    chartRange.VerticalAlignment = 3;
                    chartRange.Font.Bold = true;

                    chartRange = xlWorkSheet.get_Range("A3", "A10000");
                    chartRange.NumberFormat = "@";

                    I += 1;
                    // הרכבת משפט שליפה
                    // תוספת מתאריך 18/06/2015
                    // הורדת הנתון מחיר
                    //StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                    //         "from BPCSFV30.FLTL90 join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                    //                               "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                    //         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                    //         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') ";
                    StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(TMFRT),TMACH,Max(INDOR) as Type " +
                             "from BPCSFV30.FLTL90 join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                              "join BPCSFALI.IIMNl01 on SUBSTRING(TPROD,1,8)=INPROD " +
                             "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                             "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') ";

                    if (Txt_Prod.Text.Trim() != "")
                        StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";
                    if (Txt_Size.Text.Trim() != "")
                    {
                        Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                        StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                    }

                    if (CBox_Machine.Text.Trim()!= "")
                        StrSql += "and TMACH ='" + CBox_Machine.Text.Trim() + "' ";

                    StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";

                    DBC.Q_Run1(StrSql);
                    while (DBC.Rs1.Read())
                    {
                        TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                        TmpStr = TmpStr.Substring(6, 2) + "/" + TmpStr.Substring(4, 2) + "/" + TmpStr.Substring(0, 4);
                        // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                        if (Txt_Design.Text.Trim() != "")
                        {
                            for (A = 0; A <= C; A++)
                            {
                                if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                                {
                                    //              19/09/2012 שינוי מתאריך 
                                    // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                    if (LastCatalog == "")
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1);
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    }
                                    else
                                    {
                                        if (LastCatalog == DBC.Rs1.GetString(1))
                                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                                        else
                                        {
                                            LastCatalog = DBC.Rs1.GetString(1);
                                            xlWorkSheet.Cells[I, 4] = "Total";
                                            xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                            I += 1;
                                            TotalForCatalog = DBC.Rs1.GetInt16(4);
                                        }
                                    }
                                    
                                    xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                    xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                    xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                    xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                    xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                    xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                    // תוספת מתאריך 18/06/2015
                                    // הורדת הנתון מחיר
                                    //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                    xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                    xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();

                                    // תוספת מתאריך 19/04/2017
                                    // עמודה חדשה קרקס או ירוק לבקשת אלכס פלדמן
                                    if (DBC.Rs1.GetString(1).Substring(8, 2) == "-0")
                                        xlWorkSheet.Cells[I, 9] = "Green";
                                    else
                                        xlWorkSheet.Cells[I, 9] = "Carcas";

                                    // תוספת מתאריך 19/04/2017
                                    // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                                    if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                                        xlWorkSheet.Cells[I, 10] = "Michelin";
                                    // רדיאלי או דיאגונלי
                                    xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetString(8).Trim();
                                    I += 1;
                                }
                            }
                        }
                        else
                        {
                            //              19/09/2012 שינוי מתאריך 
                            // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                            if (LastCatalog == "")
                            {
                                LastCatalog = DBC.Rs1.GetString(1);
                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                            }
                            else
                            {
                                if (LastCatalog == DBC.Rs1.GetString(1))
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                else
                                {
                                    LastCatalog = DBC.Rs1.GetString(1);
                                    xlWorkSheet.Cells[I, 4] = "Total";
                                    xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                    I += 1;
                                    TotalForCatalog = DBC.Rs1.GetInt16(4);
                                }
                            }

                            xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                            xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                            xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                            xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                            xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                            xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                            // תוספת מתאריך 18/06/2015
                            // הורדת הנתון מחיר
                            //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                            xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                            xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();

                            // תוספת מתאריך 19/04/2017
                            // עמודה חדשה קרקס או ירוק לבקשת אלכס פלדמן
                            if (DBC.Rs1.GetString(1).Substring(8, 2) == "-0")
                                xlWorkSheet.Cells[I, 9] = "Green";
                            else
                                xlWorkSheet.Cells[I, 9] = "Carcas";

                            // תוספת מתאריך 19/04/2017
                            // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                            if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                                xlWorkSheet.Cells[I, 10] = "Michelin";

                            I += 1;
                        }
                    }
                    DBC.Rs1.Close();
 
                    //              19/09/2012 שינוי מתאריך 
                    // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                    xlWorkSheet.Cells[I, 4] = "Total";
                    xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                    TotalForCatalog = 0;
                    LastCatalog = "";

                    // Member יכול להיות מצב שהנתונים נמצאים בקובץ וירטואלי - מה שנקרא
                    //  מצב שלא היה קליטת נתונים, זה מה שליבה הסבירה והראתה לי. על מנת
                    // לשלוף את הנתונים צריך לבצע את המשפט הבא כדי שהנתונים יהיו בקובץ
                    //                             שממנו ניתן לשלוף במידה ויש מה לשלוף
                    //StrSql = "Create alias majd.workall for bpcsfv30.flt(workall)";
                    StrSql = "Create alias TAPIALI.WORKALL for bpcsfv30.flt(workall)";
                    DBC.Run(StrSql);
                    // הרכבת משפט שליפה
                    // תוספת מתאריך 18/06/2015
                    // הורדת הנתון מחיר
                    //StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                    //         "from majd.workall join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                    //                               "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                    //         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                    //         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') ";
                    //StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(TMFRT),TMACH " +
                    //         "from majd.workall join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                    //         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                    //         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') ";
                    StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(TMFRT),TMACH " +
                             "from TAPIALI.WORKALL join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                             "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                             "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') ";

                    if (Txt_Prod.Text.Trim() != "")
                        StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";
                    if (Txt_Size.Text.Trim() != "")
                    {
                        Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                        StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                    }
                    StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";

                    DBC.Q_Run1(StrSql);
                    if (DBC.Rs1.Read())
                    {
                        while (DBC.Rs1.Read())
                        {
                            TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                            TmpStr = TmpStr.Substring(6, 2) + "/" + TmpStr.Substring(4, 2) + "/" + TmpStr.Substring(0, 4);
                            // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                            if (Txt_Design.Text.Trim() != "")
                            {
                                for (A = 0; A <= C; A++)
                                {
                                    if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                                    {
                                        //              19/09/2012 שינוי מתאריך 
                                        // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                        if (LastCatalog == "")
                                        {
                                            LastCatalog = DBC.Rs1.GetString(1);
                                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                                        }
                                        else
                                        {
                                            if (LastCatalog == DBC.Rs1.GetString(1))
                                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                                            else
                                            {
                                                LastCatalog = DBC.Rs1.GetString(1);
                                                xlWorkSheet.Cells[I, 4] = "Total";
                                                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                                I += 1;
                                                TotalForCatalog = DBC.Rs1.GetInt16(4);
                                            }
                                        }

                                        xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                        xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                        xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                        xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                        xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                        xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                        // הרכבת משפט שליפה
                                        // תוספת מתאריך 18/06/2015
                                        //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                        xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                        xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();

                                        // תוספת מתאריך 19/04/2017
                                        // עמודה חדשה קרקס או ירוק לבקשת אלכס פלדמן
                                        if (DBC.Rs1.GetString(1).Substring(8, 2) == "-0")
                                            xlWorkSheet.Cells[I, 9] = "Green";
                                        else
                                            xlWorkSheet.Cells[I, 9] = "Carcas";

                                        // תוספת מתאריך 19/04/2017
                                        // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                                        if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                                            xlWorkSheet.Cells[I, 10] = "Michelin";

                                        I += 1;
                                    }
                                }
                            }
                            else
                            {
                                //              19/09/2012 שינוי מתאריך 
                                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                if (LastCatalog == "")
                                {
                                    LastCatalog = DBC.Rs1.GetString(1);
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                }
                                else
                                {
                                    if (LastCatalog == DBC.Rs1.GetString(1))
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    else
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1);
                                        xlWorkSheet.Cells[I, 4] = "Total";
                                        xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                        I += 1;
                                        TotalForCatalog = DBC.Rs1.GetInt16(4);
                                    }
                                }

                                xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                // הרכבת משפט שליפה
                                // תוספת מתאריך 18/06/2015
                                //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();

                                // תוספת מתאריך 19/04/2017
                                // עמודה חדשה קרקס או ירוק לבקשת אלכס פלדמן
                                if (DBC.Rs1.GetString(1).Substring(8, 2) == "-0")
                                    xlWorkSheet.Cells[I, 9] = "Green";
                                else
                                    xlWorkSheet.Cells[I, 9] = "Carcas";

                                // תוספת מתאריך 19/04/2017
                                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                                if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                                    xlWorkSheet.Cells[I, 10] = "Michelin";

                                I += 1;
                            }
                        }
                        
                        //              19/09/2012 שינוי מתאריך 
                        // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                        xlWorkSheet.Cells[I, 4] = "Total";
                        xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                        TotalForCatalog = 0;
                        LastCatalog = "";
                    }
                    DBC.Rs1.Close();

                    // מחיקת קובץ שהקמתי על סמך קובץ וירטואלי
                    //StrSql = "drop alias majd.workall";
                    StrSql = "drop alias TAPIALI.WORKALL";
                    DBC.Run(StrSql);

                }
                xlApp.Visible = true;
            }
            // בדיקה אם מבקשים להפיק דוח בניה
            if (checkBox5.Checked && !checkBox4.Checked)
            {

                // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל
                //Excel.Workbook wb = xl.ActiveWorkbook;
                //wb = xl.Workbooks.Open("T:\\green scrap\\green scrap.xls", false, false, Type.Missing, Type.Missing, Type.Missing, false, Type.Missing, Type.Missing, true, Type.Missing, Type.Missing, false, Type.Missing, Type.Missing);
                //ws = (Excel.Worksheet)wb.Worksheets["בסיס הנתונים databse"];                

                // יצירת קובץ אקסל
                // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                Excel.Range chartRange;

                xlApp = new Excel.ApplicationClass();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlWorkSheet.Cells[1, 1] = "Building Production Report Between : " + dateTimePicker1.Value.ToString().Substring(0, 10) + " And  " + dateTimePicker2.Value.ToString().Substring(0, 10);

                // הצגת גיליון משמאל לימין
                xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
                xlWorkSheet.DisplayRightToLeft = false; 

                // מירכוז + מיזוג + פונט מודגש לתחום            
                chartRange = xlWorkSheet.get_Range("A1", "E1");
                chartRange.MergeCells = true;
                chartRange.HorizontalAlignment = 3;
                chartRange.VerticalAlignment = 3;
                chartRange.Font.Bold = true;

                // הגדרת שדות הדוח
                xlWorkSheet.Cells[2, 1] = "Date";
                xlWorkSheet.Cells[2, 2] = "Catalog";
                xlWorkSheet.Cells[2, 3] = "Size";
                xlWorkSheet.Cells[2, 4] = "Dept";
                xlWorkSheet.Cells[2, 5] = "Quantity";
                xlWorkSheet.Cells[2, 6] = "Weight Without Flap";
                // הרכבת משפט שליפה
                // תוספת מתאריך 18/06/2015
                //xlWorkSheet.Cells[2, 7] = "Cost Without Flap";
                xlWorkSheet.Cells[2, 7] = "Specification";
                xlWorkSheet.Cells[2, 8] = "Machine";
                xlWorkSheet.Cells[2, 9] = "Carcas/Green";
                xlWorkSheet.Cells[2, 10] = "Michelin";
                xlWorkSheet.Cells[2, 11] = "R / D";

                // מירכוז + פונט מודגש לתחום            
                chartRange = xlWorkSheet.get_Range("A2", "J2");
                chartRange.HorizontalAlignment = 3;
                chartRange.VerticalAlignment = 3;
                chartRange.Font.Bold = true;

                // הגדרת רוחב עמודה של תחום
                chartRange = xlWorkSheet.get_Range("A1", "A1");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("B1", "B1");
                chartRange.ColumnWidth = 15;
                chartRange = xlWorkSheet.get_Range("C1", "C1");
                chartRange.ColumnWidth = 25;
                chartRange = xlWorkSheet.get_Range("D1", "D1");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("E1", "E1");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("F1", "F1");
                chartRange.ColumnWidth = 18;
                // הרכבת משפט שליפה
                // תוספת מתאריך 18/06/2015
                //chartRange = xlWorkSheet.get_Range("G1", "G1");
                //chartRange.ColumnWidth = 16;
                chartRange = xlWorkSheet.get_Range("G1", "G1");
                chartRange.ColumnWidth = 15;
                chartRange = xlWorkSheet.get_Range("H1", "J1");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("I1", "I1");
                chartRange.ColumnWidth = 13;

                chartRange = xlWorkSheet.get_Range("A3", "A10000");
                chartRange.NumberFormat = "@";

                // Freeze rows            
                Excel.Window xlWnd1 = xlApp.ActiveWindow;
                chartRange = xlWorkSheet.get_Range("A2", "A2").get_Offset(1, 0).EntireRow;
                chartRange.Select();
                xlWnd1.FreezePanes = true;

                // AutoFilter
                chartRange = xlWorkSheet.get_Range("A2", "J2");
                chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

                // התאמת תאריך
                S_Date = dateTimePicker1.Value;
                E_Date = dateTimePicker2.Value;
                Str_S_Date = (S_Date.Year - 1928).ToString("00") + S_Date.Month.ToString("00") + S_Date.Day.ToString("00");
                Str_E_Date = (E_Date.Year - 1928).ToString("00") + E_Date.Month.ToString("00") + E_Date.Day.ToString("00");

                // AS400 התקשרות ל   
                DBConn DBC;
                //string Qry_Str;
                string Con_Str;

                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);

                // הרכבת משפט שליפה                
                // תוספת מתאריך 18/06/2015
                // הורדת הנתון מחיר
                //StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                //         "from BPCSFV30.FLTL90 join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                //                               "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                //         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                //         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') "; 
                StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(TMFRT),TMACH,Max(INDOR) as Type " +
                         "from BPCSFV30.FLTL90 join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') "; 

                if (Txt_Prod.Text.Trim() != "")
                    StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";
                if (Txt_Size.Text.Trim() != "")
                {
                    Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                    StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                }

                if (CBox_Machine.Text.Trim() != "")
                    StrSql += "and TMACH ='" + CBox_Machine.Text.Trim() + "' ";

                StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                    TmpStr = TmpStr.Substring(6, 2) + "/" + TmpStr.Substring(4, 2) + "/" + TmpStr.Substring(0, 4);
                    // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                    if (Txt_Design.Text.Trim() != "")
                    {
                        for (A = 0; A <= C; A++)
                        {
                            if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                            {
                                //              19/09/2012 שינוי מתאריך 
                                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                if (LastCatalog == "")
                                {
                                    LastCatalog = DBC.Rs1.GetString(1);
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                }
                                else
                                {
                                    if (LastCatalog == DBC.Rs1.GetString(1))
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    else
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1);
                                        xlWorkSheet.Cells[I, 4] = "Total";
                                        xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                        I += 1;
                                        TotalForCatalog = DBC.Rs1.GetInt16(4);
                                    }
                                }
                                
                                xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                xlWorkSheet.Cells[I, 6] = Math.Round (Convert.ToDouble(DBC.Rs1.GetString(5).Trim()),1);
                                // תוספת מתאריך 18/06/2015
                                // הורדת הנתון מחיר             
                                //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();

                                // תוספת מתאריך 19/04/2017
                                // עמודה חדשה קרקס או ירוק לבקשת אלכס פלדמן
                                if (DBC.Rs1.GetString(1).Substring(8, 2) == "-0")
                                    xlWorkSheet.Cells[I, 9] = "Green";
                                else
                                    xlWorkSheet.Cells[I, 9] = "Carcas";

                                // תוספת מתאריך 19/04/2017
                                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                                if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                                    xlWorkSheet.Cells[I, 10] = "Michelin";
                                // רדיאלי או דיאגונלי
                                xlWorkSheet.Cells[I,11] = DBC.Rs1.GetString(8).Trim();
                                // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל
                                //xlWorkSheet.Cells[I, 10] = Scrap_Tire(DBC.Rs1.GetString(1).Trim(), TmpStr);
                                I += 1;
                            }
                        }
                    }
                    else
                    {
                        //              19/09/2012 שינוי מתאריך 
                        // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                        if (LastCatalog == "")
                        {
                            LastCatalog = DBC.Rs1.GetString(1);
                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                        }
                        else
                        {
                            if (LastCatalog == DBC.Rs1.GetString(1))
                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                            else
                            {
                                LastCatalog = DBC.Rs1.GetString(1);
                                xlWorkSheet.Cells[I, 4] = "Total";
                                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                I += 1;
                                TotalForCatalog = DBC.Rs1.GetInt16(4);
                            }
                        }

                        xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                        xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                        xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                        xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                        xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                        xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                        // תוספת מתאריך 18/06/2015
                        // הורדת הנתון מחיר
                        //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                        xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                        xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();
                        
                        // תוספת מתאריך 19/04/2017
                        // עמודה חדשה קרקס או ירוק לבקשת אלכס פלדמן
                        if (DBC.Rs1.GetString(1).Substring(8, 2) == "-0")
                            xlWorkSheet.Cells[I, 9] = "Green";
                        else
                            xlWorkSheet.Cells[I, 9] = "Carcas";

                        // תוספת מתאריך 19/04/2017
                        // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                        if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                            xlWorkSheet.Cells[I, 10] = "Michelin";

                        // רדיאלי או דיאגונלי
                        xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetString(8).Trim();
                        // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל                        
                        //xlWorkSheet.Cells[I, 10] = Scrap_Tire(DBC.Rs1.GetString(1).Trim(), TmpStr);
                        I += 1;
                    }
                }
                DBC.Rs1.Close();
 
                //              19/09/2012 שינוי מתאריך 
                // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                xlWorkSheet.Cells[I, 4] = "Total";
                xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                TotalForCatalog = 0;
                LastCatalog = "";

                // Member יכול להיות מצב שהנתונים נמצאים בקובץ וירטואלי - מה שנקרא
                //  מצב שלא היה קליטת נתונים, זה מה שליבה הסבירה והראתה לי. על מנת
                // לשלוף את הנתונים צריך לבצע את המשפט הבא כדי שהנתונים יהיו בקובץ
                //                             שממנו ניתן לשלוף במידה ויש מה לשלוף

                //StrSql = "Create alias majd.workall for bpcsfv30.flt(workall)";                         
                StrSql = "Create alias TAPIALI.WORKALL for bpcsfv30.flt(workall)";                         
                DBC.Run(StrSql);
                // הרכבת משפט שליפה
                // תוספת מתאריך 18/06/2015
                // הורדת הנתון מחיר
                //StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(MCOST),max(TMFRT),TMACH " +
                //         "from majd.workall join ALLTAB.COSTP on SUBSTRING(TPROD,1,8)=CATNUM " +
                //                               "join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                //         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                //         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') "; 
                //StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(TMFRT),TMACH " +
                //         "from majd.workall join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                //         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                //         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') "; 
                StrSql = "select TTDTE,TPROD,max(INSIZ),TDEPT,sum(TPCS),max(TWGHT),max(TMFRT),TMACH " +
                         "from TAPIALI.WORKALL join BPCSFALI.IIMN on SUBSTRING(TPROD,1,8)=INPROD " +
                         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and TDEPT in (59,60,71,74,160) " +
                         "and (substring(TPROD,9,3)='-00' or substring(TPROD,9,3)='-10') "; 

                if (Txt_Prod.Text.Trim() != "")
                    StrSql += "and substring(TPROD,1,8)='" + Txt_Prod.Text.Trim() + "' ";
                if (Txt_Size.Text.Trim() != "")
                {
                    Txt_Size.Text = Txt_Size.Text.ToUpper().Trim();
                    StrSql += "and substring(INSIZ,1," + Txt_Size.Text.Trim().Length + ")='" + Txt_Size.Text.Trim() + "' ";
                }
                StrSql += "group by TTDTE,TPROD,TDEPT,TMACH order by TPROD,TTDTE";
               
                DBC.Q_Run1(StrSql);
                if (DBC.Rs1.Read())
                {
                    while (DBC.Rs1.Read())
                    {
                        TmpStr = (Convert.ToDouble(DBC.Rs1.GetString(0).Substring(0, 2)) + 1928).ToString() + DBC.Rs1.GetString(0).Substring(2, 2) + DBC.Rs1.GetString(0).Substring(4, 2);
                        TmpStr = TmpStr.Substring(6, 2) + "/" + TmpStr.Substring(4, 2) + "/" + TmpStr.Substring(0, 4);
                        // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
                        if (Txt_Design.Text.Trim() != "")
                        {
                            for (A = 0; A <= C; A++)
                            {
                                if (Des_Arr[A] == DBC.Rs1.GetString(1).Substring(0, 3))
                                {
                                    //              19/09/2012 שינוי מתאריך 
                                    // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                                    if (LastCatalog == "")
                                    {
                                        LastCatalog = DBC.Rs1.GetString(1);
                                        TotalForCatalog += DBC.Rs1.GetInt16(4);
                                    }
                                    else
                                    {
                                        if (LastCatalog == DBC.Rs1.GetString(1))
                                            TotalForCatalog += DBC.Rs1.GetInt16(4);
                                        else
                                        {
                                            LastCatalog = DBC.Rs1.GetString(1);
                                            xlWorkSheet.Cells[I, 4] = "Total";
                                            xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                            I += 1;
                                            TotalForCatalog = DBC.Rs1.GetInt16(4);
                                        }
                                    }

                                    xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                                    xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                                    xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                                    xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                                    xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                                    xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                                    // תוספת מתאריך 18/06/2015
                                    // הורדת הנתון מחיר
                                    //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                    xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                                    xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();

                                    // תוספת מתאריך 19/04/2017
                                    // עמודה חדשה קרקס או ירוק לבקשת אלכס פלדמן
                                    if (DBC.Rs1.GetString(1).Substring(8, 2) == "-0")
                                        xlWorkSheet.Cells[I, 9] = "Green";
                                    else
                                        xlWorkSheet.Cells[I, 9] = "Carcas";

                                    // תוספת מתאריך 19/04/2017
                                    // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                                    if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                                        xlWorkSheet.Cells[I, 10] = "Michelin";
                                    // רדיאלי או דיאגונלי
                                    xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetString(8).Trim();
                                    // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל                                    
                                    //xlWorkSheet.Cells[I, 10] = Scrap_Tire(DBC.Rs1.GetString(1).Trim(), TmpStr);
                                    I += 1;
                                }
                            }
                        }
                        else
                        {
                            //              19/09/2012 שינוי מתאריך 
                            // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                            if (LastCatalog == "")
                            {
                                LastCatalog = DBC.Rs1.GetString(1);
                                TotalForCatalog += DBC.Rs1.GetInt16(4);
                            }
                            else
                            {
                                if (LastCatalog == DBC.Rs1.GetString(1))
                                    TotalForCatalog += DBC.Rs1.GetInt16(4);
                                else
                                {
                                    LastCatalog = DBC.Rs1.GetString(1);
                                    xlWorkSheet.Cells[I, 4] = "Total";
                                    xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                                    I += 1;
                                    TotalForCatalog = DBC.Rs1.GetInt16(4);
                                }
                            }

                            xlWorkSheet.Cells[I, 1] = TmpStr; //DBC.Rs1.GetString(0).Trim();
                            xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(1).Trim(); //DBC.Rs1.GetString(1).Substring(0, 8);
                            xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2).Trim();
                            xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetDecimal(3).ToString();
                            xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetInt16(4).ToString();
                            xlWorkSheet.Cells[I, 6] = Math.Round(Convert.ToDouble(DBC.Rs1.GetString(5).Trim()), 1);
                            // תוספת מתאריך 18/06/2015
                            // הורדת הנתון מחיר
                            //xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                            xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetString(6).Trim();
                            xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim();

                            // תוספת מתאריך 19/04/2017
                            // עמודה חדשה קרקס או ירוק לבקשת אלכס פלדמן
                            if (DBC.Rs1.GetString(1).Substring(8, 2) == "-0")
                                xlWorkSheet.Cells[I, 9] = "Green";
                            else
                                xlWorkSheet.Cells[I, 9] = "Carcas";

                            // תוספת מתאריך 19/04/2017
                            // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                            if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                                xlWorkSheet.Cells[I, 10] = "Michelin";
                            // רדיאלי או דיאגונלי
                            xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetString(8).Trim();
                            // רז רצה להציג פסולים מתוך אקסל כרגע מבוטל                            
                            //xlWorkSheet.Cells[I, 10] = Scrap_Tire(DBC.Rs1.GetString(1).Trim(), TmpStr);
                            I += 1;
                        }
                    }

                    //              19/09/2012 שינוי מתאריך 
                    // הוספת סיכום ברמת מק"ט לבקשת רז מת"פי
                    xlWorkSheet.Cells[I, 4] = "Total";
                    xlWorkSheet.Cells[I, 5] = TotalForCatalog.ToString();
                    TotalForCatalog = 0;
                    LastCatalog = "";
                }
                DBC.Rs1.Close();

                // מחיקת קובץ שהקמתי על סמך קובץ וירטואלי
                //StrSql = "drop alias majd.workall";
                StrSql = "drop alias TAPIALI.WORKALL";
                DBC.Run(StrSql);
                DBC.Close_Conn(); 

                xlApp.Visible = true;

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);
            }
                
            Cursor.Current = Cursors.PanEast;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {                       
            // AS400 התקשרות ל   
            DBConn DBC;
            string Qry_Str;
            string Con_Str;

            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);
            Qry_Str  = "select MAC_MCOVIP From STWIND.MACHINES order by MAC_MCOVIP";
            // שליפת רשימת מכונות בניה למחלקה
            DBC.Q_Run1(Qry_Str);
            while (DBC.Rs1.Read())
            {
                CBox_Machine.Items.Add(DBC.Rs1.GetString(0));                
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
        }

        private void scrapHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // בדיקה אם חלון פתוח
            Frm_ScrapHistory frm = null;
            if ((frm = (Frm_ScrapHistory)IsFormAlreadyOpen(typeof(Frm_ScrapHistory))) == null)
            {
                frm = new Frm_ScrapHistory();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_ScrapHistory"].BringToFront();
            }
        }        

        private void tireTraceabilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // בדיקה אם חלון פתוח
            Frm_TireTraceability frm = null;
            if ((frm = (Frm_TireTraceability)IsFormAlreadyOpen(typeof(Frm_TireTraceability))) == null)
            {
                frm = new Frm_TireTraceability();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_TireTraceability"].BringToFront();
            }
        }

        private void qastopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // בדיקה אם חלון פתוח
            Frm_QaStop frm = null;
            if ((frm = (Frm_QaStop)IsFormAlreadyOpen(typeof(Frm_QaStop))) == null)
            {
                frm = new Frm_QaStop();
                frm.Show();
            }
            else
            {                
                Application.OpenForms["Frm_QaStop"].BringToFront();
            }                                    
        }

        public static Form IsFormAlreadyOpen(Type FormType)
        {
            foreach (Form OpenForm in Application.OpenForms)
            {
                if (OpenForm.GetType() == FormType)
                    return OpenForm;
            }
            return null;
        }

        private void tireDefectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 22/07/2013
            // דוח חדש עבור דיווה נתן ההודי, הצגת נתוני צמיגים שעברו ביקורת חמה
            // 1. ולא היה להם תיקון קל או כבד בחודש האחרון OK - כמה צמיגים היו
            // 2. FAILED - כמה צמיגים היו
            // 3. REPAIR - כמה צמיגים היו

            // בדיקה אם חלון פתוח
            Frm_TireDefect frm = null;
            if ((frm = (Frm_TireDefect)IsFormAlreadyOpen(typeof(Frm_TireDefect))) == null)
            {
                frm = new Frm_TireDefect();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_TireDefect"].BringToFront();
            }                    
        }

        private void classifiedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 30/07/2013
            //  דוח המציג נתוני סיווגים לפי חתיוך של תאריך \ קוד פגם, הדוח עבור חזי לוי

            // בדיקה אם חלון פתוח
            Frm_Classified frm = null;
            if ((frm = (Frm_Classified)IsFormAlreadyOpen(typeof(Frm_Classified))) == null)
            {
                frm = new Frm_Classified();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_Classified"].BringToFront();
            }                    

        }

        private void Txt_Design_KeyPress(object sender, KeyPressEventArgs e)
        {
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<-----     Delete     ----->|    |<----- . ----->| 
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete || e.KeyChar == '.')
            {
                if (e.KeyChar == '.')
                {
                    // אסור להקליד נקודה בהתחלה
                    if (Txt_Design.Text.Length < 3)
                        e.Handled = true;    
                    // אין להקליד ברצף שתי נקודות או יותר
                    else if ( Txt_Design.Text.Substring(Txt_Design.Text.Trim().Length -1,1) == ".")
                        e.Handled = true;
                }
                else
                    e.Handled = false;
            }
            else e.Handled = true;
        }

        private void hotInspectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 30/07/2013
            //  דוח המציג נתוני פגמים בביקורת חמה לפי חתיוך של תאריך \ קוד פגם, הדוח עבור חזי לוי

            // בדיקה אם חלון פתוח
            Frm_HotInspection frm = null;
            if ((frm = (Frm_HotInspection)IsFormAlreadyOpen(typeof(Frm_HotInspection))) == null)
            {
                frm = new Frm_HotInspection();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_HotInspection"].BringToFront();
            }                    
        }

        private void rNTRROToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 09/02/2014
            // RNT OR RRO הוספת דוח צמיגים הממתינים  או שעברו בדיקת 
            // לבקשת דיווה ההודי
            // בדיקה אם חלון פתוח
            Frm_RNT_RRO frm = null;
            if ((frm = (Frm_RNT_RRO)IsFormAlreadyOpen(typeof(Frm_RNT_RRO))) == null)
            {
                frm = new Frm_RNT_RRO();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_RNT_RRO"].BringToFront();
            }     
        }

        private void tireTraceabilityBMStage1ToHotInspectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 14/05/2014
            // דוח עקיבות צמיג מבניית צמיג שלב א עד ביקורת חמה

            // בדיקה אם חלון פתוח
            Frm_TireTraceabilityBMTOHI frm = null;
            if ((frm = (Frm_TireTraceabilityBMTOHI)IsFormAlreadyOpen(typeof(Frm_TireTraceabilityBMTOHI))) == null)
            {
                frm = new Frm_TireTraceabilityBMTOHI();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_TireTraceabilityBMTOHI"].BringToFront();
            }           
        }

        private void traceabilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 10/08/2014
            // דוח עקיבות צמיג - חיתוך הנתונים לפי בחירת המשתמש

            // בדיקה אם חלון פתוח
            Frm_Traceability frm = null;
            if ((frm = (Frm_Traceability)IsFormAlreadyOpen(typeof(Frm_Traceability))) == null)
            {
                frm = new Frm_Traceability();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_Traceability"].BringToFront();
            }           
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
            {
                Lbl_Machine.Visible = true;
                CBox_Machine.Visible = true;
                CBox_Machine.Text = "";
            }
            else
            {
                Lbl_Machine.Visible = false;
                CBox_Machine.Visible = false;
            }
        }

        private void weighingReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תוספת מתאריך 13/12/2015
            // דוח בנית צמיגים
            
            // בדיקה אם חלון פתוח
            Frm_WeighingReport frm = null;
            if ((frm = (Frm_WeighingReport)IsFormAlreadyOpen(typeof(Frm_WeighingReport))) == null)
            {
                frm = new Frm_WeighingReport();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_WeighingReport"].BringToFront();
            }           
        }

        private void hMICurringReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תוספת מתאריך 20/12/2015
            // דוח גיפור + עצירות לפי טקסי

            // בדיקה אם חלון פתוח
            Frm_HMICurringReport frm = null;
            if ((frm = (Frm_HMICurringReport)IsFormAlreadyOpen(typeof(Frm_HMICurringReport))) == null)
            {
                frm = new Frm_HMICurringReport();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_HMICurringReport"].BringToFront();
            }           
        }

        private void rNTRROTKINOUTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 11/04/2016
            // כניסה יציאה RNT OR RRO הוספת דוח צמיגים  
            // לבקשת קובי מתפ"י ובוריס עוזר מנכ"ל

            // בדיקה אם חלון פתוח
            Frm_RNT_RRO_TK frm = null;
            if ((frm = (Frm_RNT_RRO_TK)IsFormAlreadyOpen(typeof(Frm_RNT_RRO_TK))) == null)
            {
                frm = new Frm_RNT_RRO_TK();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_RNT_RRO_TK"].BringToFront();
            }
        }

        private void weighingOverWeightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תוספת מתאריך 04/08/2016
            // הוספת דוח עודף משקל בשקילות, לבקשת פאדי
            // פיתוח של רן

            // בדיקה אם חלון פתוח
            Frm_WeighingOverWeight frm = null;
            if ((frm = (Frm_WeighingOverWeight)IsFormAlreadyOpen(typeof(Frm_WeighingOverWeight))) == null)
            {
                frm = new Frm_WeighingOverWeight();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_WeighingOverWeight"].BringToFront();
            }
        }

        private void efficiancyReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //תוספת מתאריך 09/08/2016
            //הוספת דוח יעילות על פי בקשת בוריס
            //פיתוח של רן
            //תוספת מתאריך 22/06/2017
            // ייעול הדו"ח ומעבר לשליפת נתונים ישירות מבסיס הנתונים
            //בדיקה אם חלון פתוח
            Efficiency_New frm = null;
            if ((frm = (Efficiency_New)IsFormAlreadyOpen(typeof(Efficiency_New))) == null)
            {
                frm = new Efficiency_New();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Efficiancy_New"].BringToFront();
            }
        }

        private void overWeightAnalysisReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //תוספת מתאריך 14/08/2016
             //הוספת דוח אנליזת משלקים חורגים על פי בקשת בוריס
             //פיתוח של רן

             //בדיקה אם חלון פתוח
            Frm_WeigingAnalysis frm = null;
            if ((frm = (Frm_WeigingAnalysis)IsFormAlreadyOpen(typeof(Frm_WeigingAnalysis))) == null)
            {
                frm = new Frm_WeigingAnalysis();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_WeigingAnalysis"].BringToFront();
            }
        }

        private void mixersReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //תוספת מתאריך 23/11/2016
            //הוספת דוח מיקסרים
            //פיתוח של רן

            //בדיקה אם חלון פתוח
            Frm_MixersReports frm = null;
            if ((frm = (Frm_MixersReports)IsFormAlreadyOpen(typeof(Frm_MixersReports))) == null)
            {
                frm = new Frm_MixersReports();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_MixersReports"].BringToFront();
            }
        }

        private void hMICuringControllerInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תוספת מתאריך 30/04/2017
            //SQL הוספת דוח גיפור על פי בסיס נתונים שב
            // פיתוח של רן הויכמן
            //בדיקה אם חלון פתוח
            Frm_CuringInfo frm = null;
            if ((frm = (Frm_CuringInfo)IsFormAlreadyOpen(typeof(Frm_CuringInfo))) == null)
            {
                frm = new Frm_CuringInfo();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_CuringInfo"].BringToFront();
            }
        }

        private void michelinReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MichelinReport();
        }

        private void MichelinReport()
        {
            Cursor.Current = Cursors.WaitCursor;

            int I = 0;

            DBConn DBC;
            string Con_Str;
            string StrSql = "";

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);                        

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);            
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            
            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;

            xlWorkSheet.Name = "Michelin Tire";            
                        
            xlWorkSheet.Cells[2, 1] = "מישלן - חלק יחסי מסך הגיפור - יחידות";
            xlWorkSheet.Cells[3, 1] = "מישלן - חלק יחסי מסך הגיפור - טון";
            xlWorkSheet.Cells[4, 1] = "מישלן - משקל ממוצע";
            xlWorkSheet.Cells[5, 1] = "% DA - Michelin";
            xlWorkSheet.Cells[6, 1] = "% DA Tot.";
            xlWorkSheet.Cells[7, 1] = "% Scrap - Michelin";

            xlWorkSheet.Cells[1, 2] = DateTime.Now.ToString("MM.yyyyy");
            xlWorkSheet.Cells[1, 3] = "ממוצע 3 חודשים אחרונים";
            xlWorkSheet.Cells[1, 4] = "חריגה";
            xlWorkSheet.Cells[1, 5] = "חריגה ב %";

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "E1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A2", "A7");
            chartRange.HorizontalAlignment = 1;
            //chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 38;
            chartRange.HorizontalAlignment = 1;

            chartRange = xlWorkSheet.get_Range("B1", "E1");
            chartRange.ColumnWidth = 20;
            chartRange.WrapText = true;

            chartRange = xlWorkSheet.get_Range("A1", "E7");
            chartRange.Font.Size = 12;            

            for (I = 1; I <= 7; I++) // קביעת קו במקום מסויים
            {
                chartRange = xlWorkSheet.get_Range("B" + I.ToString(), "B" + I.ToString());                
                chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                chartRange = xlWorkSheet.get_Range("C" + I.ToString(), "C" + I.ToString());
                chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                chartRange = xlWorkSheet.get_Range("D" + I.ToString(), "D" + I.ToString());
                chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            }

            for (I = 2; I < 7; I++) // קביעת קו במקום מסויים
            {
                chartRange = xlWorkSheet.get_Range("A" + I.ToString(), "E" + I.ToString());
                chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            }

            // קו עבה thick borders
            chartRange = xlWorkSheet.get_Range("A2", "A7");
            chartRange.BorderAround(misValue, Excel.XlBorderWeight.xlThick, Excel.XlColorIndex.xlColorIndexAutomatic);            
            chartRange = xlWorkSheet.get_Range("B1", "E1");
            chartRange.BorderAround(misValue, Excel.XlBorderWeight.xlThick, Excel.XlColorIndex.xlColorIndexAutomatic);            
            chartRange = xlWorkSheet.get_Range("B2", "E7");
            chartRange.BorderAround(misValue, Excel.XlBorderWeight.xlThick, Excel.XlColorIndex.xlColorIndexAutomatic);

            // Last 3 Monthes
            DateTime Last_3_Months_StrDt =new  DateTime(DateTime.Today.AddMonths(-3).Year, DateTime.Today.AddMonths(-3).Month, 1);
            DateTime Last_3_Months_EndDt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1);

            String S_Last_3_Months_StrDt = (Last_3_Months_StrDt.Year - 1928).ToString("00") + Last_3_Months_StrDt.Month.ToString("00") + Last_3_Months_StrDt.Day.ToString("00");
            String S_Last_3_Months_EndDt = (Last_3_Months_EndDt.Year - 1928).ToString("00") + Last_3_Months_EndDt.Month.ToString("00") + Last_3_Months_EndDt.Day.ToString("00");

            // Current Month
            DateTime Current_Month_StrDt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            DateTime Current_Month_EndDt = DateTime.Today;            

            String S_Current_Month_StrDt = (Current_Month_StrDt.Year - 1928).ToString("00") + Current_Month_StrDt.Month.ToString("00") + Current_Month_StrDt.Day.ToString("00");
            String S_Current_Month_EndDt = (Current_Month_EndDt.Year - 1928).ToString("00") + Current_Month_EndDt.Month.ToString("00") + Current_Month_EndDt.Day.ToString("00");                       

            I = 0;

            double L_3_M_PCS = 0;
            double L_3_M_Wgt = 0;
            // Total curing PCS & WGT last 3 monthes            
            StrSql = "select sum(TPCS),sum(TPCS*TWGHT) from BPCSFV30.FLTL89 " +
                     "where TTDTE between " + S_Last_3_Months_StrDt + " and " + S_Last_3_Months_EndDt + " and TDEPT in (161,162)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                L_3_M_PCS = Math.Round(DBC.Rs1.GetDouble(0),0);
                L_3_M_Wgt = Math.Round(DBC.Rs1.GetDouble(1), 1);                
            }
            DBC.Rs1.Close();

            double C_M_PCS = 0;
            double C_M_Wgt = 0;
            // Total curing PCS & WGT Current monthe
            StrSql = "select sum(TPCS),sum(TPCS*TWGHT) from BPCSFV30.FLTL89 " +
                     "where TTDTE between " + S_Current_Month_StrDt + " and " + S_Current_Month_EndDt + " and TDEPT in (161,162)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                C_M_PCS = Math.Round(DBC.Rs1.GetDouble(0), 0);
                C_M_Wgt = Math.Round(DBC.Rs1.GetDouble(1), 1);
            }
            DBC.Rs1.Close();


            double L_3_M_PCS_Michelin = 0;
            double L_3_M_Wgt_Michelin = 0;
            // Total curing PCS & WGT last 3 monthes Michelin            
            StrSql = "select sum(TPCS),sum(TPCS*TWGHT) from BPCSFV30.FLTL89 " +
                     "where TTDTE between " + S_Last_3_Months_StrDt + " and " + S_Last_3_Months_EndDt + " and TDEPT in (161,162) and substring(TPROD,1,3) in ('771','781','791','772')";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                L_3_M_PCS_Michelin = Math.Round(DBC.Rs1.GetDouble(0), 0);
                L_3_M_Wgt_Michelin = Math.Round(DBC.Rs1.GetDouble(1), 1);
            }
            DBC.Rs1.Close();

            double C_M_PCS_Michelin = 0;
            double C_M_Wgt_Michelin = 0;
            // Total curing PCS & WGT Current monthe Michelin            
            StrSql = "select sum(TPCS),sum(TPCS*TWGHT) from BPCSFV30.FLTL89 " +
                     "where TTDTE between " + S_Current_Month_StrDt + " and " + S_Current_Month_EndDt + " and TDEPT in (161,162) and substring(TPROD,1,3) in ('771','781','791','772')";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                C_M_PCS_Michelin = Math.Round(DBC.Rs1.GetDouble(0), 0);
                C_M_Wgt_Michelin = Math.Round(DBC.Rs1.GetDouble(1), 1);
            }
            DBC.Rs1.Close();



            xlWorkSheet.Cells[2, 2] = Math.Round(C_M_PCS_Michelin / C_M_PCS*100,1);
            xlWorkSheet.Cells[2, 3] = Math.Round(L_3_M_PCS_Michelin / L_3_M_PCS * 100, 1);
            xlWorkSheet.Cells[2, 4] = "=B2-C2";
            xlWorkSheet.Cells[2, 5] = "=round(B2/C2*100-100,0)";

            xlWorkSheet.Cells[3, 2] = Math.Round(C_M_Wgt_Michelin / C_M_Wgt * 100, 1);
            xlWorkSheet.Cells[3, 3] = Math.Round(L_3_M_Wgt_Michelin / L_3_M_Wgt * 100, 1);
            xlWorkSheet.Cells[3, 4] = "=B3-C3";
            xlWorkSheet.Cells[3, 5] = "=round(B3/C3*100-100,0)";
            
            xlWorkSheet.Cells[4, 2] = Math.Round(C_M_Wgt_Michelin / C_M_PCS_Michelin, 1);
            xlWorkSheet.Cells[4, 3] = Math.Round(L_3_M_Wgt_Michelin / L_3_M_PCS_Michelin, 1);
            xlWorkSheet.Cells[4, 4] = "=B4-C4";
            xlWorkSheet.Cells[4, 5] = "=round(B4/C4*100-100,0)";


            double L_3_M_DA_PCS_Michelin = 0;            
            // DA Michelin last 3 monthes            
            StrSql = "select count(*) from ALIQUAL.CTSVGP " +
                     "where SSVGDT between " + S_Last_3_Months_StrDt + " and " + S_Last_3_Months_EndDt + " and substring(SPROD,1,3) in (771,781,791,772) and SSIVUG='ב'";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                L_3_M_DA_PCS_Michelin = Math.Round(DBC.Rs1.GetDouble(0), 0);                
            }
            DBC.Rs1.Close();

            double C_M_DA_PCS_Michelin = 0;
            // DA Michelin Current monthe
            StrSql = "select count(*) from ALIQUAL.CTSVGP " +
                     "where SSVGDT between " + S_Current_Month_StrDt + " and " + S_Current_Month_EndDt + " and substring(SPROD,1,3) in (771,781,791,772) and SSIVUG='ב'";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                C_M_DA_PCS_Michelin = Math.Round(DBC.Rs1.GetDouble(0), 0);
            }
            DBC.Rs1.Close();



            double L_3_M_Scrap_PCS_Michelin = 0;
            // SCRAP Michelin last 3 monthes                
            StrSql = "select count(*) from ALIQUAL.CTSVGP " +
                     "where SSVGDT between " + S_Last_3_Months_StrDt + " and " + S_Last_3_Months_EndDt + " and substring(SPROD,1,3) in (771,781,791,772) and SSIVUG='פ'";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                L_3_M_Scrap_PCS_Michelin = Math.Round(DBC.Rs1.GetDouble(0), 0);
            }
            DBC.Rs1.Close();

            double C_M_Scrap_PCS_Michelin = 0;
            // SCRAP Michelin Current monthe
            StrSql = "select count(*) from ALIQUAL.CTSVGP " +
                     "where SSVGDT between " + S_Current_Month_StrDt + " and " + S_Current_Month_EndDt + " and substring(SPROD,1,3) in (771,781,791,772) and SSIVUG='פ'";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                C_M_Scrap_PCS_Michelin = Math.Round(DBC.Rs1.GetDouble(0), 0);
            }
            DBC.Rs1.Close();

            xlWorkSheet.Cells[5, 2] = Math.Round(C_M_DA_PCS_Michelin / (C_M_PCS_Michelin - C_M_Scrap_PCS_Michelin)*100, 2);
            xlWorkSheet.Cells[5, 3] = Math.Round(L_3_M_DA_PCS_Michelin / (L_3_M_PCS_Michelin - L_3_M_Scrap_PCS_Michelin)*100, 2);
            xlWorkSheet.Cells[5, 4] = "=B5-C5";
            xlWorkSheet.Cells[5, 5] = "=round(B5/C5*100-100,0)";



            xlWorkSheet.Cells[7, 2] = Math.Round(C_M_Scrap_PCS_Michelin / (C_M_PCS_Michelin - C_M_Scrap_PCS_Michelin) * 100, 2);
            xlWorkSheet.Cells[7, 3] = Math.Round(L_3_M_Scrap_PCS_Michelin / (L_3_M_PCS_Michelin - L_3_M_Scrap_PCS_Michelin) * 100, 2);
            xlWorkSheet.Cells[7, 4] = "=B7-C7";
            xlWorkSheet.Cells[7, 5] = "=round(B7/C7*100-100,0)";


            DateTime DT;
            string Str_DT = "";
            double L_3_M_DA_Wgt_Michelin = 0;
            // % Da - Tot.
            // בהתאם לחוקיות של משקל בדוח סיווג DA שליפת סה"כ משקל לצמיגים שסווגו  
            StrSql = "select SPROD,A.LKGMF,SSVGDT,CASE WHEN SMIFRT IS NULL THEN '999999' ELSE CASE WHEN SMIFRT='' THEN '999999' END END,B.LMACH " + 
                     "from ALIQUAL.CTSVGP "+
                          "left join TAPIALI.LABELL1 A on SPROD=A.LPROD and SUBSTRING(SMIFRT,1,6)= A.LLBLNO " +
                          "left join TAPIALI.LABELGL3 B on SPROD=B.LPROD and SUBSTRING(SMIFRT,1,6)= B.LLBLNO and B.LACTAN=42 and trim(SMIFRT)<>'' " +  // הוספתי על מנת ליישר קו ולשמור על חוקיות מול סה"כ משקל בדוח סיווג
                     "where SSVGDT between " + S_Last_3_Months_StrDt + " and " + S_Last_3_Months_EndDt + " and SSIVUG='ב'";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {                
                //if (!DBC.Rs1.IsDBNull(1))
                //    L_3_M_DA_Wgt_Michelin += Math.Round(Convert.ToDouble(DBC.Rs1.GetValue(1).ToString()), 1);
                //else
                if (DBC.Rs1.IsDBNull(1) || DBC.Rs1.IsDBNull(4))
                {
                    DT = Convert.ToDateTime((Convert.ToInt64(DBC.Rs1.GetDecimal(2).ToString().Substring(0, 2)) + 1928).ToString() + "/" + DBC.Rs1.GetDecimal(2).ToString().Substring(2, 2) + "/" + DBC.Rs1.GetDecimal(2).ToString().Substring(4, 2));
                    Str_DT = (DT.Year - 1928).ToString("00") + DT.Month.ToString("00") + DT.Day.ToString("00");
                    /////
                    if (DBC.Rs1.GetString(0).Trim().Length >= 13)
                    {
                        if (DBC.Rs1.GetString(0).Trim().Substring(12, 1) == "B")
                        {
                            string TmpCat = (DBC.Rs1.GetString(0).Trim().Substring(0, 11) + " " + DBC.Rs1.GetString(0).Trim().Substring(13, DBC.Rs1.GetString(0).Trim().Length - 13)).Trim();
                            StrSql = "select TWGHT from BPCSFV30.FLTL01 " +
                            "where TTDTE<=" + Str_DT + " and substring(TPROD,1," + TmpCat.Length + ")='" + TmpCat + "' order by TTDTE desc";
                        }
                        else
                        {
                            StrSql = "select TWGHT from BPCSFV30.FLTL01 " +
                                "where TTDTE<=" + Str_DT + " and substring(TPROD,1," + DBC.Rs1.GetString(0).Trim().Length + ")='" + DBC.Rs1.GetString(0).Trim() + "' order by TTDTE desc";
                        }
                    }
                    else
                    {
                        StrSql = "select TWGHT from BPCSFV30.FLTL01 " +
                            "where TTDTE<=" + Str_DT + " and substring(TPROD,1," + DBC.Rs1.GetString(0).Trim().Length + ")='" + DBC.Rs1.GetString(0).Trim() + "' order by TTDTE desc";
                    }
                    /////                    
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        L_3_M_DA_Wgt_Michelin += Math.Round(DBC.Rs2.GetDouble(0), 1);
                    }
                    DBC.Rs2.Close();
                }    
                else
                    L_3_M_DA_Wgt_Michelin += Math.Round(Convert.ToDouble(DBC.Rs1.GetValue(1).ToString()), 1);
            }
            DBC.Rs1.Close();

            double C_M_DA_Wgt_Michelin = 0;
            // % Da - Tot.
            // בהתאם לחוקיות של משקל בדוח סיווג DA שליפת סה"כ משקל לצמיגים שסווגו  
            StrSql = "select SPROD,A.LKGMF,SSVGDT,SSIVUG,CASE WHEN SMIFRT IS NULL THEN '999999' ELSE CASE WHEN SMIFRT='' THEN '999999' END END " +
                     "from ALIQUAL.CTSVGP left join TAPIALI.LABELL1 A on SPROD=A.LPROD and SUBSTRING(SMIFRT,1,6)= A.LLBLNO " +
                     "where SSVGDT between " + S_Current_Month_StrDt + " and " + S_Current_Month_EndDt + " and SSIVUG='ב'";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {                
                if (!DBC.Rs1.IsDBNull(1))
                    C_M_DA_Wgt_Michelin += Math.Round(Convert.ToDouble(DBC.Rs1.GetValue(1).ToString()), 1);
                else
                {
                    DT = Convert.ToDateTime((Convert.ToInt64(DBC.Rs1.GetDecimal(2).ToString().Substring(0, 2)) + 1928).ToString() + "/" + DBC.Rs1.GetDecimal(2).ToString().Substring(2, 2) + "/" + DBC.Rs1.GetDecimal(2).ToString().Substring(4, 2));
                    Str_DT = (DT.Year - 1928).ToString("00") + DT.Month.ToString("00") + DT.Day.ToString("00");
                    /////
                    if (DBC.Rs1.GetString(0).Trim().Length >= 13)
                    {
                        if (DBC.Rs1.GetString(0).Trim().Substring(12, 1) == "B")
                        {
                            string TmpCat = (DBC.Rs1.GetString(0).Trim().Substring(0, 11) + " " + DBC.Rs1.GetString(0).Trim().Substring(13, DBC.Rs1.GetString(0).Trim().Length - 13)).Trim();
                            StrSql = "select TWGHT from BPCSFV30.FLTL01 " +
                            "where TTDTE<=" + Str_DT + " and substring(TPROD,1," + TmpCat.Length + ")='" + TmpCat + "' order by TTDTE desc";
                        }
                        else
                        {
                            StrSql = "select TWGHT from BPCSFV30.FLTL01 " +
                                "where TTDTE<=" + Str_DT + " and substring(TPROD,1," + DBC.Rs1.GetString(0).Trim().Length + ")='" + DBC.Rs1.GetString(0).Trim() + "' order by TTDTE desc";
                        }
                    }
                    else
                    {
                        StrSql = "select TWGHT from BPCSFV30.FLTL01 " +
                            "where TTDTE<=" + Str_DT + " and substring(TPROD,1," + DBC.Rs1.GetString(0).Trim().Length + ")='" + DBC.Rs1.GetString(0).Trim() + "' order by TTDTE desc";
                    }
                    /////                    
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        C_M_DA_Wgt_Michelin += Math.Round(DBC.Rs2.GetDouble(0), 1);
                    }
                    DBC.Rs2.Close();                   
                }
            }
            DBC.Rs1.Close();

            double L_3_M_Scrap_Wgt = 0;
            // last 3 monthes   שליפת סה"כ משקל פסילה במשקל - ע"י ריטה
            StrSql = "select sum(TQTY) from BPCSFALI.ITHL74 "+
                     "where TTDTE between " + S_Last_3_Months_StrDt + " and " + S_Last_3_Months_EndDt + " and TPROD='SC-CT350'";  // TPROD in ('SC-CT350','SC-CT355','SC-CT850','SC-CT999')
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                L_3_M_Scrap_Wgt += Math.Round(DBC.Rs1.GetDouble(0), 1);
            }
            DBC.Rs1.Close();

            double C_M_Scrap_Wgt = 0;
            // Current monthe   שליפת סה"כ משקל פסילה במשקל - ע"י ריטה
            StrSql = "select sum(TQTY) from BPCSFALI.ITHL74 " +
                     "where TTDTE between " + S_Current_Month_StrDt + " and " + S_Current_Month_EndDt + " and TPROD='SC-CT350'";  // TPROD in ('SC-CT350','SC-CT355','SC-CT850','SC-CT999')
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                C_M_Scrap_Wgt += Math.Round(DBC.Rs1.GetDouble(0), 1);
            }
            DBC.Rs1.Close();

            xlWorkSheet.Cells[6, 2] = Math.Round(C_M_DA_Wgt_Michelin / (C_M_Wgt - C_M_Scrap_Wgt) * 100, 2);
            xlWorkSheet.Cells[6, 3] = Math.Round(L_3_M_DA_Wgt_Michelin / (L_3_M_Wgt - L_3_M_Scrap_Wgt) * 100, 2);
            xlWorkSheet.Cells[6, 4] = "=B6-C6";
            xlWorkSheet.Cells[6, 5] = "=round(B6/C6*100-100,0)";



            DBC.Close_Conn();
            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void pressesWarmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תוספת מתאריך 22/05/2017
            // דוח חימום מכבשים            
            Frm_PressesWarm frm = null;
            if ((frm = (Frm_PressesWarm)IsFormAlreadyOpen(typeof(Frm_PressesWarm))) == null)
            {
                frm = new Frm_PressesWarm();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_PressesWarm"].BringToFront();
            }
        }

        private void carcasWeighingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // תוספת מתאריך 17/07/2017
            // דוח השוואת משקל קרקס במכונות בניה שלב ב' לעומת שלב א            
            Frm_CarcasWeighingReport frm = null;
            if ((frm = (Frm_CarcasWeighingReport)IsFormAlreadyOpen(typeof(Frm_CarcasWeighingReport))) == null)
            {
                frm = new Frm_CarcasWeighingReport();
                frm.Show();
            }
            else
            {
                Application.OpenForms["Frm_CarcasWeighingReport"].BringToFront();
            }
        }

      
    }
}
