﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_CarcasWeighingReport : Form
    {
        public Frm_CarcasWeighingReport()
        {
            InitializeComponent();
        }

        // תוספת מתאריך 17/07/2017
        // דוח השוואת משקל קרקס במכונות בניה שלב ב' לעומת שלב א            

        //  Load

        private void Frm_CarcasWeighingReport_Load(object sender, EventArgs e)
        {
            PrepareFilterDatasBetweenDates();  
        }        

        // Leave

        private void DTPicker_ToW_Leave(object sender, EventArgs e)
        {
            if (DTPicker_FromW.Value.Date > DTPicker_ToW.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_FromW.Focus();
                return;
            }
            PrepareFilterDatasBetweenDates();
        }

        private void comboB_Machine_Leave(object sender, EventArgs e)
        {
            if (comboB_Machine.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";

                comboB_Size.Text = "";
                comboB_Size.Items.Clear();
                comboB_Emp.Text = "";
                comboB_Emp.Items.Clear();
                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);
                // שליפת רשימת פריטים לתקופה מבוקשת
                StrSql = "select distinct PRODUCT from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                         "MACHINE='" + comboB_Machine.Text.Trim() + "' and substring(PRODUCT,9,2)= '-0' " +                
                         "order by PRODUCT";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_Size.Items.Add(DBC.Rs1.GetString(0).Trim()); // Catalog
                }
                DBC.Rs1.Close();
                // שליפת רשימת עובדים לתקופה מבוקשת
                StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                         "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                         "MACHINE='" + comboB_Machine.Text.Trim() + "' and substring(PRODUCT,9,2)= '-0' " +
                         "order by EMPLOYEE";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                    else
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
                }
                DBC.Rs1.Close();
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                               "substring(DATA12,1,3)='ID=' and MACHINE='" + comboB_Machine.Text.Trim() + "' and substring(PRODUCT,9,2)= '-0' " +
                        "order by substring(DATA12,4,locate(';',DATA12)-4)";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        private void comboB_Size_Leave(object sender, EventArgs e)
        {
            if (comboB_Size.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";

                comboB_Emp.Text = "";
                comboB_Emp.Items.Clear();
                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);
                // שליפת רשימת עובדים לתקופה מבוקשת
                StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                         "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and substring(PRODUCT,9,2)= '-0' ";
                
                if (comboB_Machine.Text.Trim() != "")
                    StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
                
                StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' order by EMPLOYEE";                
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                    else
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
                }
                DBC.Rs1.Close();
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                               "substring(DATA12,1,3)='ID=' and substring(PRODUCT,9,2)= '-0' ";

                if (comboB_Machine.Text.Trim() != "")
                    StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' order by substring(DATA12,4,locate(';',DATA12)-4)";               
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        private void comboB_Emp_Leave(object sender, EventArgs e)
        {
            if (comboB_Emp.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";

                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                               "substring(DATA12,1,3)='ID=' and substring(PRODUCT,9,2)= '-0' ";

                if (comboB_Machine.Text.Trim() != "")
                    StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
                if (comboB_Size.Text.Trim() != "")
                    StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' ";
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Substring(0, 5) + "0' order by substring(DATA12,4,locate(';',DATA12)-4)";                                
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        // Click
        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            DTPicker_FromW.Value = DateTime.Now.Date;
            DTPicker_ToW.Value = DateTime.Now.Date;
            PrepareFilterDatasBetweenDates();           
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (DTPicker_FromW.Value.Date > DTPicker_ToW.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_FromW.Focus();
                return;
            }
            CarcasWeighingReport();
        }

        // Routines

        private void PrepareFilterDatasBetweenDates()
        {
            Cursor.Current = Cursors.WaitCursor;

            string Con_Str;
            string StrSql = "";

            comboB_Machine.Text = "";
            comboB_Machine.Items.Clear();
            comboB_Size.Text = "";
            comboB_Size.Items.Clear();
            comboB_Emp.Text = "";
            comboB_Emp.Items.Clear();
            comboB_ID.Text = "";
            comboB_ID.Items.Clear();

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBC;
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);
            // שליפת רשימת מכונת לתקופה מבוקשת
            StrSql = "select distinct MACHINE from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and substring(PRODUCT,9,2)= '-0' " +
                     "order by MACHINE";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_Machine.Items.Add(DBC.Rs1.GetString(0).Trim()); // Machine
            }
            DBC.Rs1.Close();

            // שליפת רשימת פריטים לתקופה מבוקשת
            StrSql = "select distinct PRODUCT from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                     "substring(PRODUCT,9,2)= '-0' " +
                     "order by PRODUCT";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_Size.Items.Add(DBC.Rs1.GetString(0).Trim()); // Catalog
            }
            DBC.Rs1.Close();
            // שליפת רשימת עובדים לתקופה מבוקשת
            StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                     "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "'and  " +
                     "substring(PRODUCT,9,2)= '-0' " +
                     "order by EMPLOYEE";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                    comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                else
                    comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
            }
            DBC.Rs1.Close();
            // ליפוף ID שליפת רשימת             
            StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                           "substring(DATA12,1,3)='ID=' and substring(PRODUCT,9,2)= '-0' " +
                     "order by substring(DATA12,4,locate(';',DATA12)-4)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            Cursor.Current = Cursors.PanEast;
        }        

        private void CarcasWeighingReport()
        {
            Cursor.Current = Cursors.WaitCursor;

            string Con_Str;
            string StrSql = "";
            int i = 3;
            Int64 RecordNo = 0;
            decimal RecordPr = 0;
            
            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBCW;
            DBCW = new DBConn();
            DBCW.Initialize_Conn(Con_Str);

            // יצירת קובץ אקסל            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "CarcasWeighingComparison";

            chartRange = xlWorkSheet.get_Range("A1", "K1");
            chartRange.Cells.MergeCells = true;
            chartRange.Cells.Interior.Color = System.Drawing.Color.SpringGreen.ToArgb();
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Building Machine Stage 2 - Carcas Weight";

            xlWorkSheet.Cells[2, 1] = "Machine";
            xlWorkSheet.Cells[2, 2] = "Tire Spec";
            xlWorkSheet.Cells[2, 3] = "Size";
            xlWorkSheet.Cells[2, 4] = "Catalog";
            xlWorkSheet.Cells[2, 5] = "Serial";
            xlWorkSheet.Cells[2, 6] = "DNA";
            xlWorkSheet.Cells[2, 7] = "Date";
            xlWorkSheet.Cells[2, 8] = "Emp. No.";
            xlWorkSheet.Cells[2, 9] = "Emp. Name";
            xlWorkSheet.Cells[2, 10] = "Spec Carcas Wt.";
            xlWorkSheet.Cells[2, 11] = "Actual Carcas Wt.";

            chartRange = xlWorkSheet.get_Range("L1", "Q1");
            chartRange.Cells.MergeCells = true;
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 12] = "Building Machine Stage 1 - Carcas Weight";
            xlWorkSheet.Cells[2, 12] = "Actual Carcas Wt.";
            xlWorkSheet.Cells[2, 13] = "Deviation";
            xlWorkSheet.Cells[2, 14] = "Machine";
            xlWorkSheet.Cells[2, 15] = "Date";
            xlWorkSheet.Cells[2, 16] = "Emp. No.";
            xlWorkSheet.Cells[2, 17] = "Emp. Name";             

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "P2");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // Freeze rows            
            Excel.Window xlWnd3 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A2", "A2").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd3.FreezePanes = true;

            // AutoFilter            
            chartRange = xlWorkSheet.get_Range("A2", "P2");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // Wrap Text
            chartRange = xlWorkSheet.get_Range("A2", "P2");
            chartRange.Cells.WrapText = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "P1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 15;                
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("J1", "K1");
            chartRange.ColumnWidth = 8;

            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("M1", "P1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 13;                    


            chartRange = xlWorkSheet.get_Range("G2", "G10000");
            chartRange.NumberFormat = "dd/MM/yyyy HH:mm";

            //chartRange = xlWorkSheet.get_Range("M2", "M10000");
            //chartRange.NumberFormat = "dd/MM/yyyy HH:mm";

            // שליפת מס' רשומות
            StrSql = "select count(*) from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and substring(PRODUCT,9,2)= '-0' ";
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0,5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";            

            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {                
                RecordNo = DBCW.Rs1.GetInt64(0);
            }
            DBCW.Rs1.Close();
            //toolStripStatusLabel1.Text = RecordNo.ToString() + " Records";
            toolStripProgressBar1.Value = 0;
            RecordPr += RecordNo / 100; // חישוב כמה זה אחוז 1 מהכמות
            StrSql ="select MACHINE,A.LSPEC,DESC,B.LPROD,B.LLBLNO,A.LLBLNA,PDATE,EMPLOYEE,value(I1.PRATI,'*')||' '||value(I1.FAMILY,'*'),DATA3,round(DATA4,1)," +
                           "round(B.LKGAC*2.2045,1),B.LMACH,SUBSTR(CHAR(B.LACNDT), 7, 2) Concat '/' Concat SUBSTR(CHAR(B.LACNDT), 5, 2) Concat '/' Concat SUBSTR(CHAR(B.LACNDT), 1, 4)||' ' ||" +
                           "CASE  when B.LACNTM >= 100000 then SUBSTR(CHAR(B.LACNTM), 1, 2) Concat ':' Concat SUBSTR(CHAR(B.LACNTM), 3, 2) Concat ':' Concat SUBSTR(CHAR(B.LACNTM), 5, 2) " +
                                 "when B.LACNTM >= 10000 then  '0' Concat SUBSTR(CHAR(B.LACNTM), 1, 1) Concat ':' Concat SUBSTR(CHAR(B.LACNTM), 2, 2) Concat ':' Concat SUBSTR(CHAR(B.LACNTM), 4, 2) " +
                                 "when B.LACNTM >= 1000 then '00:' Concat SUBSTR(CHAR(B.LACNTM), 1, 2) Concat ':' Concat SUBSTR(CHAR(B.LACNTM), 3, 2) " +
                                 "when B.LACNTM >= 100 then '00:0' Concat SUBSTR(CHAR(B.LACNTM), 1, 1) Concat ':' Concat SUBSTR(CHAR(B.LACNTM), 2, 2) " +
                                 "when B.LACNTM >= 10 then '00:00:' Concat SUBSTR(CHAR(B.LACNTM), 1, 2) " +
                                 "Else '00:00:0' Concat SUBSTR(CHAR(B.LACNTM), 1, 1) END," +
                           "B.LOVED,value(I2.PRATI,'*')||' '||value(I2.FAMILY,'*') " +
                    "from STWIND.PRRP left join  " +
                         "TAPIALI.LABELP A on PRODUCT=A.LPROD and PRODUCTID=A.LLBLNO left join " +
                         "TAPIALI.LABELGP B on A.LLBLNA=B.LLBLNA and B.LACTAN=3 and B.LERRCD='' left join " +
                         "ISUFKV.ISAVL10 I1 on substring(EMPLOYEE,1,7)=trim(I1.OVED) and I1.MIFAL='01' left join " +
                         "ISUFKV.ISAVL10 I2 on TRIM(B.LOVED)=SUBSTRING(trim(I2.OVED),3,5) and I2.MIFAL='01' " +         
                    "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and substring(PRODUCT,9,2)= '-0'  ";            
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";
            StrSql += "order by MACHINE,PDATE";
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                if (!DBCW.Rs1.IsDBNull(3))
                {
                    xlWorkSheet.Cells[i, 1] = DBCW.Rs1.GetString(0).Trim(); //"Machine Stage B";
                    if (!DBCW.Rs1.IsDBNull(1)) xlWorkSheet.Cells[i, 2] = DBCW.Rs1.GetString(1).Trim().Substring(DBCW.Rs1.GetString(1).Trim().Length - 9, 9); //"Tire Spec"
                    xlWorkSheet.Cells[i, 3] = DBCW.Rs1.GetString(2).Trim(); //"Size"
                    xlWorkSheet.Cells[i, 4] = DBCW.Rs1.GetString(3).Trim(); //"Catalog"
                    xlWorkSheet.Cells[i, 5] = DBCW.Rs1.GetString(4).Trim(); //"Serial"
                    xlWorkSheet.Cells[i, 6] = DBCW.Rs1.GetValue(5).ToString(); //"ID (Small Label)"
                    xlWorkSheet.Cells[i, 7] = DBCW.Rs1.GetDateTime(6).ToString("dd/MM/yyyy HH:mm:ss"); //"Date"
                    xlWorkSheet.Cells[i, 8] = DBCW.Rs1.GetString(7).Trim().Substring(2, 5); //"Emp. No."
                    if (!DBCW.Rs1.IsDBNull(8)) xlWorkSheet.Cells[i, 9] = DBCW.Rs1.GetString(8).Trim(); //"Emp. Name"
                    xlWorkSheet.Cells[i, 10] = DBCW.Rs1.GetDecimal(9); //"Spec Carcas Wt."
                    xlWorkSheet.Cells[i, 11] = DBCW.Rs1.GetDecimal(10); //"Actual Carcas Wt. in Stage B"                    

                    xlWorkSheet.Cells[i, 12] = DBCW.Rs1.GetString(11).Trim(); //"Actual Carcas Wt. in Stage A"
                    xlWorkSheet.Cells[i, 13] = "=round((L" + i.ToString() + "/K" + i.ToString() + "-1)*100,2)";//Deviation
                    xlWorkSheet.Cells[i, 14] = DBCW.Rs1.GetValue(12); //"Machine Stage A"
                    xlWorkSheet.Cells[i, 15] = DBCW.Rs1.GetString(13); //"Date"
                    xlWorkSheet.Cells[i, 16] = DBCW.Rs1.GetString(14).Trim(); //"Emp. No."
                    xlWorkSheet.Cells[i, 17] = DBCW.Rs1.GetString(15).Trim(); //"Emp. Name"                   

                    i++;
                    toolStripStatusLabel2.Text = i.ToString();
                    if (RecordNo >= 100)
                        if (i % RecordPr == 0 && toolStripProgressBar1.Value < 100)
                            toolStripProgressBar1.Value = toolStripProgressBar1.Value + 1;

                }

            }

            xlWorkSheet.Cells[i, 1] = "Total";

            xlWorkSheet.Cells[i, 11] = "=sum(K3:K" + (i - 1).ToString() + ")";
            xlWorkSheet.Cells[i, 12] = "=sum(L3:L" + (i - 1).ToString() + ")";
            xlWorkSheet.Cells[i, 13] = "=round((L" + i.ToString() + "/K" + i.ToString() + "-1)*100,2)";           

            chartRange = xlWorkSheet.get_Range("K1", "K" + i.ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = Excel.XlBorderWeight.xlThick;

            chartRange = xlWorkSheet.get_Range("Q1", "Q" + i.ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = Excel.XlBorderWeight.xlThick;

            chartRange = xlWorkSheet.get_Range("A1", "Q1");
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = Excel.XlBorderWeight.xlThick;

            chartRange = xlWorkSheet.get_Range("A2", "Q2");
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = Excel.XlBorderWeight.xlThick;

            chartRange = xlWorkSheet.get_Range("A" + (i-1).ToString(), "Q" + (i-1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = Excel.XlBorderWeight.xlThick;

            chartRange = xlWorkSheet.get_Range("A" + i.ToString(), "Q" + i.ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = Excel.XlBorderWeight.xlThick;                

            DBCW.Rs1.Close();
            DBCW.Close_Conn();

            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

            toolStripStatusLabel1.Text = "     0 Records";
            toolStripProgressBar1.Value = 0;
            toolStripStatusLabel2.Text = "     0";
            Cursor.Current = Cursors.PanEast;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
