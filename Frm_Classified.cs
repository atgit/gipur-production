﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_Classified : Form
    {
        public Frm_Classified()
        {
            InitializeComponent();
        }

        private void Txt_DefectCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<----- . ----->| 
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.') e.Handled = false;            
            else e.Handled = true;
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 30/07/2013
            //  דוח המציג נתוני סיווגים לפי חתיוך של תאריך \ קוד פגם, הדוח עבור חזי לוי

            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("Error : Begining Date Bigger Than End Date", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }
            
            DefectCode();
        }

        private void DefectCode()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            DateTime DT;
            string Str_DT="";
            string Str_TM = "";

            DBConn DBC;
            string Con_Str;
            string StrSql = "";
            string From_Date;        // תאריך התחלה לצורך השאילתא
            string TO_Date;        // תאריך סיום לצורך השאילתא
            DateTime S_Date;          // תאריך התחלה
            DateTime E_Date;          // תאריך סיום               
            string[] DefectCode_Arr = new string[10];  // מערך המשמש עבור קודי פגם
            int A, B, C;

            for (int j = 0; j < DefectCode_Arr.Length; j++)
                DefectCode_Arr[j] = "";
            B = 0;
            C = 0;            

            // בדיקה אם המשתמש ביקש חיתוך קוד פגם מסויים
            //         אפשרתי למשתמש לבחור עד 10 קודי פגם
            //        מערך המכיל את הפגמים אשר המשתמש בחר
            if (Txt_DefectCode.Text.Trim() != "")
            {
                for (A = 0; A <= Txt_DefectCode.Text.Trim().Length - 1; A++)
                {
                    if (Txt_DefectCode.Text.Substring(A, 1) == ".")
                    {
                        DefectCode_Arr[C] = Txt_DefectCode.Text.Substring(B, 3);
                        B = A + 1;
                        C += 1;
                    }
                }
                // תפיסת רשומה אחרונה
                DefectCode_Arr[C] = Txt_DefectCode.Text.Trim().Substring(Txt_DefectCode.Text.Trim().Length - 3, 3);
            }            

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            int I = 3;
            int J = 0;
            double Wheight=0;

            // התאמת תאריך
            S_Date = DTPicker_From.Value;
            E_Date = DTPicker_To.Value;
            From_Date = (S_Date.Year - 1928).ToString("00") + S_Date.Month.ToString("00") + S_Date.Day.ToString("00");
            TO_Date = (E_Date.Year - 1928).ToString("00") + E_Date.Month.ToString("00") + E_Date.Day.ToString("00");

            // תוספת מתאריך 28/07/2015
            // הוספת שדות לדוח לבקשת בוריס קזקיבץ עוזר מנכ"ל
            // הוספת גיליון סיכום משקל לקוד פסילה
            double[,] ArrDefectTot = new double[3, 120];
            // ArrDefectTot[0, 0] --> קוד פגם
            // ArrDefectTot[1, 0] --> סה"כ משקל
            // ArrDefectTot[2, 0] --> סה"כ צמיגים

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet1, xlWorkSheet2;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            // Office 2013 עקב שדרוג ל 
            //xlWorkSheet1 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //xlWorkSheet2 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);                        
            xlWorkSheet2 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet1 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet1.DisplayRightToLeft = false;

            xlWorkSheet1.Name = "Tire Classified";
            xlWorkSheet1.Cells[1, 1] = "Tire Classified Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            // מירכוז + מיזוג + פונט מודגש לתחום                        
            chartRange = xlWorkSheet1.get_Range("A1", "E1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            xlWorkSheet1.Cells[2, 1] = "Date";
            xlWorkSheet1.Cells[2, 2] = "Catalog";
            xlWorkSheet1.Cells[2, 3] = "Size";
            xlWorkSheet1.Cells[2, 4] = "Defect Code";
            xlWorkSheet1.Cells[2, 5] = "Defect Desc Eng.";
            xlWorkSheet1.Cells[2, 6] = "Defect Desc Heb.";
            xlWorkSheet1.Cells[2, 7] = "Class"; // Classified

            // תוספת מתאריך 28/07/2015
            // הוספת שדות לדוח לבקשת בוריס קזקיבץ עוזר מנכ"ל
            xlWorkSheet1.Cells[2, 8] = "Spec Weight [kg]";
            xlWorkSheet1.Cells[2, 9] = "Press No.";
            xlWorkSheet1.Cells[2, 10] = "Michelin";

            xlWorkSheet1.Cells[2, 11] = "Serial";
            xlWorkSheet1.Cells[2, 12] = "DNA";
            xlWorkSheet1.Cells[2, 13] = "Fabric";
            xlWorkSheet1.Cells[2, 14] = "Cost $";
            
            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("A1", "N2");            
            chartRange.HorizontalAlignment = 1;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet1.get_Range("A1", "A1");
            chartRange.ColumnWidth = 18;
            chartRange = xlWorkSheet1.get_Range("B1", "B1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet1.get_Range("C1", "C1");
            chartRange.ColumnWidth = 22;
            chartRange = xlWorkSheet1.get_Range("D1", "D1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet1.get_Range("E1", "F1");
            chartRange.ColumnWidth = 23;            
            chartRange = xlWorkSheet1.get_Range("G1", "I1");
            chartRange.ColumnWidth = 7;            

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("C1", "C2");
            chartRange.ColumnWidth = 20;

            // Freeze rows            
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet1.get_Range("A2", "A2").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd1.FreezePanes = true;

            // AutoFilter
            chartRange = xlWorkSheet1.get_Range("A2", "N2");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // Wrap Text
            chartRange = xlWorkSheet1.get_Range("A2", "N2");
            chartRange.WrapText = true;

            chartRange = xlWorkSheet1.get_Range("A3", "A5000");
            chartRange.NumberFormat = "@";

            // תוספת מתאריך 28/07/2015
            // הוספת שדות לדוח לבקשת בוריס קזקיבץ עוזר מנכ"ל
            // בניית משפט שליפה
            //StrSql = "select INSIZ,SPROD,SSVGDT,SSIVUG,SPGAM1 " +
            //         "from ALIQUAL.CTSVGP join BPCSFALI.IIMN on SUBSTRING(SPROD,1,8)=INPROD " +
            //         "where SSVGDT between " + From_Date + " and " + TO_Date + "";

            StrSql = "select distinct INSIZ,SPROD,SSVGDT,SSIVUG,SPGAM1," +
                            "CASE WHEN SMIFRT IS NULL THEN '999999' ELSE CASE WHEN SMIFRT='' THEN '999999' else SMIFRT END END," +
                            "EGPGAM,HBPGAM,A.LKGMF,B.LMACH,A.LSPEC,SDTUPD,B.LLBLNA,INFAB,MCOST " +
                     "from ALIQUAL.CTSVGP " +
                          "left join ALIQUAL.TPGAMP on SPGAM1=CDPGAM " +
                          "left join BPCSFALI.IIMN on SUBSTRING(SPROD,1,8)=INPROD " +
                          "left join TAPIALI.LABELL1 A on SPROD=A.LPROD and SUBSTRING(SMIFRT,1,6)= A.LLBLNO " +
                          "left join TAPIALI.LABELGL3 B on SPROD=B.LPROD and SUBSTRING(SMIFRT,1,6)= B.LLBLNO and B.LACTAN=42 and trim(SMIFRT)<>'' " +
                          "left join ALLTAB.COSTP on SUBSTRING(SPROD,1,8)=CATNUM " +
                     "where SSVGDT between " + From_Date + " and " + TO_Date + " ";
            // התאמת משפט שליפה לפי קוד פגם
            if (Txt_DefectCode.Text.Trim() != "")
            {                            
                string Tmpstr = " and SPGAM1 in (";
                for (int j = 0; j < DefectCode_Arr.Length; j++)
                    if (DefectCode_Arr[j].Trim() == "")
                        j = DefectCode_Arr.Length;
                    else
                        Tmpstr += "'" + DefectCode_Arr[j] + "',";                
                StrSql += Tmpstr.Substring(0,Tmpstr.Length -1) + ") ";
            }

            if (CheckB_DA.Checked && CheckB_Scrap.Checked || !CheckB_DA.Checked && !CheckB_Scrap.Checked)
            {}
            else if (CheckB_DA.Checked && !CheckB_Scrap.Checked)
                StrSql += "and SSIVUG='ב' ";
            else if (!CheckB_DA.Checked && CheckB_Scrap.Checked)
                StrSql += "and SSIVUG='פ' ";

            StrSql += "order by SSVGDT,SDTUPD"; 

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {                
                //xlWorkSheet1.Cells[I, 1] = (Convert.ToInt64(DBC.Rs1.GetDecimal(2).ToString().Substring(0, 2)) + 1928).ToString() + "/" + DBC.Rs1.GetDecimal(2).ToString().Substring(2, 2) + "/" + DBC.Rs1.GetDecimal(2).ToString().Substring(4, 2);                
                
                Str_TM = (DBC.Rs1.GetValue(11).ToString().Substring(0, DBC.Rs1.GetValue(11).ToString().Length - 6)).PadLeft(6, '0');
                Str_TM = " " + Str_TM.Substring(0, 2) + ":" + Str_TM.Substring(2, 2) + ":" + Str_TM.Substring(4, 2);

                xlWorkSheet1.Cells[I, 1] = DBC.Rs1.GetDecimal(2).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(2).ToString().Substring(2, 2) + "/" + (Convert.ToInt64(DBC.Rs1.GetDecimal(2).ToString().Substring(0, 2)) + 1928).ToString() + Str_TM;
                xlWorkSheet1.Cells[I, 2] = DBC.Rs1.GetString(1).Trim();
                xlWorkSheet1.Cells[I, 3] = DBC.Rs1.GetString(0).Trim();
                xlWorkSheet1.Cells[I, 4] = DBC.Rs1.GetValue(4);
                xlWorkSheet1.Cells[I, 5] = DBC.Rs1.GetString(6).Trim(); 
                xlWorkSheet1.Cells[I, 6] = DBC.Rs1.GetString(7).Trim(); 
                if (DBC.Rs1.GetString(3) == "פ")
                    xlWorkSheet1.Cells[I, 7] = "S";
                else
                    xlWorkSheet1.Cells[I, 7] = "DA";

                // תוספת מתאריך 19/04/2017
                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                if (DBC.Rs1.GetString(1).Substring(0, 3) == "771" || DBC.Rs1.GetString(1).Substring(0, 3) == "781" || DBC.Rs1.GetString(1).Substring(0, 3) == "791" || DBC.Rs1.GetString(1).Substring(0, 3) == "772")
                    xlWorkSheet1.Cells[I, 10] = "Michelin";
                
                // תוספת מתאריך 19/04/2017
                // הוספת עמודות חדשות לדוח לבקשת יהודה כהן מנהל אבטחת איכות
                if (! DBC.Rs1.IsDBNull(5))
                    xlWorkSheet1.Cells[I, 11] = DBC.Rs1.GetString(5).Trim();
                xlWorkSheet1.Cells[I, 12] = DBC.Rs1.GetValue(12);
                xlWorkSheet1.Cells[I, 13] = DBC.Rs1.GetString(13).Trim();
                xlWorkSheet1.Cells[I, 14] = DBC.Rs1.GetValue(14);


                // תוספת מתאריך 28/07/2015                
                // הוספת שדות לדוח לבקשת בוריס קזקיבץ עוזר מנכ"ל                                
                Wheight=0;
                if (DBC.Rs1.IsDBNull(8) || DBC.Rs1.IsDBNull(9))
                {
                    DT = Convert.ToDateTime((Convert.ToInt64(DBC.Rs1.GetDecimal(2).ToString().Substring(0, 2)) + 1928).ToString() + "/" + DBC.Rs1.GetDecimal(2).ToString().Substring(2, 2) + "/" + DBC.Rs1.GetDecimal(2).ToString().Substring(4, 2));
                    Str_DT = (DT.Year - 1928).ToString("00") + DT.Month.ToString("00") + DT.Day.ToString("00");                    
                    /////
                    if (DBC.Rs1.GetString(1).Trim().Length >= 13)
                    {
                        if (DBC.Rs1.GetString(1).Trim().Substring(12, 1) == "B")
                        {
                            string TmpCat = (DBC.Rs1.GetString(1).Trim().Substring(0, 11) + " " + DBC.Rs1.GetString(1).Trim().Substring(13, DBC.Rs1.GetString(1).Trim().Length - 13)).Trim();
                            StrSql = "select TWGHT,TMACH " +
                            "from BPCSFV30.FLTL01 " +
                            "where TTDTE<=" + Str_DT + " and substring(TPROD,1," + TmpCat.Length + ")='" + TmpCat + "' order by TTDTE desc";
                        }
                        else
                        {
                            StrSql = "select TWGHT,TMACH " +
                             "from BPCSFV30.FLTL01 " +
                             "where TTDTE<=" + Str_DT + " and substring(TPROD,1," + DBC.Rs1.GetString(1).Trim().Length + ")='" + DBC.Rs1.GetString(1).Trim() + "' order by TTDTE desc";
                        }
                    }
                    else
                    {
                         StrSql = "select TWGHT,TMACH " +
                             "from BPCSFV30.FLTL01 " +
                             "where TTDTE<=" + Str_DT + " and substring(TPROD,1,"+DBC.Rs1.GetString(1).Trim().Length+")='" + DBC.Rs1.GetString(1).Trim() + "' order by TTDTE desc";
                    }
                    /////                    
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        xlWorkSheet1.Cells[I, 8] = Math.Round(DBC.Rs2.GetDecimal(0), 1);
                        xlWorkSheet1.Cells[I, 9] = DBC.Rs2.GetString(1).Trim();
                        Wheight=Convert.ToDouble(Math.Round(DBC.Rs2.GetDecimal(0), 1));
                    }
                    DBC.Rs2.Close();
                }                
                else
                {
                    xlWorkSheet1.Cells[I, 8] = Math.Round(DBC.Rs1.GetDecimal(8), 1);
                    xlWorkSheet1.Cells[I, 9] = DBC.Rs1.GetString(9).Trim();
                    Wheight=Convert.ToDouble(Math.Round(DBC.Rs1.GetDecimal(8), 1));
                }

                for (J = 0; J < 120; J++)
                {
                    if (ArrDefectTot[0, J] == 0)
                    {
                        ArrDefectTot[0, J] = Convert.ToDouble(DBC.Rs1.GetValue(4).ToString());                    
                        ArrDefectTot[1, J] = Wheight;
                        ArrDefectTot[2, J] = 1;
                        J = 120;
                    }
                    else if (ArrDefectTot[0, J] == Convert.ToDouble(DBC.Rs1.GetValue(4).ToString()))
                    {
                        ArrDefectTot[1, J] += Wheight;
                        ArrDefectTot[2, J] += 1;
                        J = 120;
                    }
                }

                I += 1;
            }
            DBC.Rs1.Close();

            xlWorkSheet2.Name = "Total";
            xlWorkSheet2.Cells[1, 1] = "Defect Code";
            xlWorkSheet2.Cells[1, 2] = "Total Weight [Ton]";
            xlWorkSheet2.Cells[1, 3] = "Total Tires"; 

            // מירכוז + מיזוג + פונט מודגש לתחום                        
            chartRange = xlWorkSheet2.get_Range("A1", "C1");            
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange = xlWorkSheet2.get_Range("A1", "C1");
            chartRange.ColumnWidth = 12;
            chartRange = xlWorkSheet2.get_Range("B1", "B1");
            chartRange.ColumnWidth = 20;
            
            // Freeze rows            
            xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet2.get_Range("A2", "A2").get_Offset(1, 0).EntireRow;
            //chartRange.Select();
            xlWnd1.FreezePanes = true;
            
            I = 2;
            for (J = 0; J < 120; J++)
            {
                if (ArrDefectTot[0, J] != 0)
                {
                    xlWorkSheet2.Cells[I, 1] = ArrDefectTot[0, J];
                    xlWorkSheet2.Cells[I, 2] = Math.Round(ArrDefectTot[1, J]/1000,3);
                    xlWorkSheet2.Cells[I, 3] = ArrDefectTot[2, J];
                    I++;
                }
            }

            DBC.Close_Conn();
            xlApp.Visible = true;
            releaseObject(xlWorkSheet1);            
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

            Cursor.Current = Cursors.PanEast;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        
    }
}
