﻿namespace Gipur_Production
{
    partial class Frm_CuringInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_CuringInfo));
            this.label2 = new System.Windows.Forms.Label();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.comboB_Press = new System.Windows.Forms.ComboBox();
            this.Lbl_Press = new System.Windows.Forms.Label();
            this.DTPicker_From = new System.Windows.Forms.DateTimePicker();
            this.GB_Period = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.CB_Cycle = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CB_Size = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.DTPicker_To = new System.Windows.Forms.DateTimePicker();
            this.CB_Item = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CB_Shift = new System.Windows.Forms.ComboBox();
            this.GB_Daily = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CB_pressDaily = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DT_Daily = new System.Windows.Forms.DateTimePicker();
            this.RB_Daily = new System.Windows.Forms.RadioButton();
            this.RB_Period = new System.Windows.Forms.RadioButton();
            this.GB_Choice = new System.Windows.Forms.GroupBox();
            this.GB_Period.SuspendLayout();
            this.GB_Daily.SuspendLayout();
            this.GB_Choice.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(19, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(356, 29);
            this.label2.TabIndex = 67;
            this.label2.Text = "HMI Curring Controller Info";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(161, 400);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(100, 28);
            this.Btn_OK.TabIndex = 68;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // comboB_Press
            // 
            this.comboB_Press.DropDownHeight = 150;
            this.comboB_Press.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_Press.FormattingEnabled = true;
            this.comboB_Press.IntegralHeight = false;
            this.comboB_Press.Location = new System.Drawing.Point(152, 98);
            this.comboB_Press.Name = "comboB_Press";
            this.comboB_Press.Size = new System.Drawing.Size(139, 26);
            this.comboB_Press.TabIndex = 72;
            // 
            // Lbl_Press
            // 
            this.Lbl_Press.AutoSize = true;
            this.Lbl_Press.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_Press.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.Lbl_Press.Location = new System.Drawing.Point(38, 99);
            this.Lbl_Press.Name = "Lbl_Press";
            this.Lbl_Press.Size = new System.Drawing.Size(71, 24);
            this.Lbl_Press.TabIndex = 75;
            this.Lbl_Press.Text = "Press:";
            // 
            // DTPicker_From
            // 
            this.DTPicker_From.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_From.Location = new System.Drawing.Point(152, 31);
            this.DTPicker_From.Name = "DTPicker_From";
            this.DTPicker_From.Size = new System.Drawing.Size(190, 25);
            this.DTPicker_From.TabIndex = 70;
            // 
            // GB_Period
            // 
            this.GB_Period.Controls.Add(this.label9);
            this.GB_Period.Controls.Add(this.label10);
            this.GB_Period.Controls.Add(this.CB_Cycle);
            this.GB_Period.Controls.Add(this.label8);
            this.GB_Period.Controls.Add(this.CB_Size);
            this.GB_Period.Controls.Add(this.label7);
            this.GB_Period.Controls.Add(this.DTPicker_To);
            this.GB_Period.Controls.Add(this.CB_Item);
            this.GB_Period.Controls.Add(this.label6);
            this.GB_Period.Controls.Add(this.DTPicker_From);
            this.GB_Period.Controls.Add(this.comboB_Press);
            this.GB_Period.Controls.Add(this.Lbl_Press);
            this.GB_Period.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.GB_Period.Location = new System.Drawing.Point(21, 140);
            this.GB_Period.Name = "GB_Period";
            this.GB_Period.Size = new System.Drawing.Size(380, 240);
            this.GB_Period.TabIndex = 78;
            this.GB_Period.TabStop = false;
            this.GB_Period.Text = "Period Report";
            this.GB_Period.Visible = false;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.SlateGray;
            this.label9.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label9.Location = new System.Drawing.Point(38, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 24);
            this.label9.TabIndex = 85;
            this.label9.Text = "Start Date:";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.SlateGray;
            this.label10.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label10.Location = new System.Drawing.Point(38, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 24);
            this.label10.TabIndex = 86;
            this.label10.Text = "End Date:";
            // 
            // CB_Cycle
            // 
            this.CB_Cycle.DropDownHeight = 150;
            this.CB_Cycle.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_Cycle.FormattingEnabled = true;
            this.CB_Cycle.IntegralHeight = false;
            this.CB_Cycle.Location = new System.Drawing.Point(152, 203);
            this.CB_Cycle.Name = "CB_Cycle";
            this.CB_Cycle.Size = new System.Drawing.Size(139, 26);
            this.CB_Cycle.TabIndex = 80;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.SlateGray;
            this.label8.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(38, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 24);
            this.label8.TabIndex = 81;
            this.label8.Text = "C. Cycle:";
            // 
            // CB_Size
            // 
            this.CB_Size.DropDownHeight = 150;
            this.CB_Size.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_Size.FormattingEnabled = true;
            this.CB_Size.IntegralHeight = false;
            this.CB_Size.Location = new System.Drawing.Point(152, 168);
            this.CB_Size.Name = "CB_Size";
            this.CB_Size.Size = new System.Drawing.Size(139, 26);
            this.CB_Size.TabIndex = 78;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.SlateGray;
            this.label7.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(38, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 24);
            this.label7.TabIndex = 79;
            this.label7.Text = "Size:";
            // 
            // DTPicker_To
            // 
            this.DTPicker_To.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_To.Location = new System.Drawing.Point(152, 64);
            this.DTPicker_To.Name = "DTPicker_To";
            this.DTPicker_To.Size = new System.Drawing.Size(190, 25);
            this.DTPicker_To.TabIndex = 71;
            this.DTPicker_To.Leave += new System.EventHandler(this.DTPicker_To_Leave);
            // 
            // CB_Item
            // 
            this.CB_Item.DropDownHeight = 150;
            this.CB_Item.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_Item.FormattingEnabled = true;
            this.CB_Item.IntegralHeight = false;
            this.CB_Item.Location = new System.Drawing.Point(152, 133);
            this.CB_Item.Name = "CB_Item";
            this.CB_Item.Size = new System.Drawing.Size(139, 26);
            this.CB_Item.TabIndex = 76;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.SlateGray;
            this.label6.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(38, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 24);
            this.label6.TabIndex = 77;
            this.label6.Text = "Catalog:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.SlateGray;
            this.label1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(35, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 24);
            this.label1.TabIndex = 80;
            this.label1.Text = "Shift:";
            // 
            // CB_Shift
            // 
            this.CB_Shift.DropDownHeight = 150;
            this.CB_Shift.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_Shift.FormattingEnabled = true;
            this.CB_Shift.IntegralHeight = false;
            this.CB_Shift.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.CB_Shift.Location = new System.Drawing.Point(149, 93);
            this.CB_Shift.Name = "CB_Shift";
            this.CB_Shift.Size = new System.Drawing.Size(139, 26);
            this.CB_Shift.TabIndex = 79;
            // 
            // GB_Daily
            // 
            this.GB_Daily.Controls.Add(this.label3);
            this.GB_Daily.Controls.Add(this.CB_pressDaily);
            this.GB_Daily.Controls.Add(this.label5);
            this.GB_Daily.Controls.Add(this.DT_Daily);
            this.GB_Daily.Controls.Add(this.CB_Shift);
            this.GB_Daily.Controls.Add(this.label1);
            this.GB_Daily.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.GB_Daily.Location = new System.Drawing.Point(21, 140);
            this.GB_Daily.Name = "GB_Daily";
            this.GB_Daily.Size = new System.Drawing.Size(380, 132);
            this.GB_Daily.TabIndex = 83;
            this.GB_Daily.TabStop = false;
            this.GB_Daily.Text = "Daily Report";
            this.GB_Daily.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.SlateGray;
            this.label3.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(35, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 24);
            this.label3.TabIndex = 87;
            this.label3.Text = "Date:";
            // 
            // CB_pressDaily
            // 
            this.CB_pressDaily.DropDownHeight = 150;
            this.CB_pressDaily.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_pressDaily.FormattingEnabled = true;
            this.CB_pressDaily.IntegralHeight = false;
            this.CB_pressDaily.Location = new System.Drawing.Point(149, 59);
            this.CB_pressDaily.Name = "CB_pressDaily";
            this.CB_pressDaily.Size = new System.Drawing.Size(139, 26);
            this.CB_pressDaily.TabIndex = 85;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.SlateGray;
            this.label5.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(35, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 24);
            this.label5.TabIndex = 86;
            this.label5.Text = "Press:";
            // 
            // DT_Daily
            // 
            this.DT_Daily.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DT_Daily.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DT_Daily.Location = new System.Drawing.Point(149, 26);
            this.DT_Daily.Name = "DT_Daily";
            this.DT_Daily.Size = new System.Drawing.Size(139, 25);
            this.DT_Daily.TabIndex = 81;
            // 
            // RB_Daily
            // 
            this.RB_Daily.AutoSize = true;
            this.RB_Daily.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RB_Daily.Location = new System.Drawing.Point(39, 28);
            this.RB_Daily.Name = "RB_Daily";
            this.RB_Daily.Size = new System.Drawing.Size(129, 23);
            this.RB_Daily.TabIndex = 84;
            this.RB_Daily.Text = "Daily Report";
            this.RB_Daily.UseVisualStyleBackColor = true;
            this.RB_Daily.CheckedChanged += new System.EventHandler(this.RB_Daily_CheckedChanged);
            // 
            // RB_Period
            // 
            this.RB_Period.AutoSize = true;
            this.RB_Period.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RB_Period.Location = new System.Drawing.Point(185, 28);
            this.RB_Period.Name = "RB_Period";
            this.RB_Period.Size = new System.Drawing.Size(141, 23);
            this.RB_Period.TabIndex = 85;
            this.RB_Period.Text = "Period Report";
            this.RB_Period.UseVisualStyleBackColor = true;
            this.RB_Period.CheckedChanged += new System.EventHandler(this.RB_Period_CheckedChanged);
            // 
            // GB_Choice
            // 
            this.GB_Choice.Controls.Add(this.RB_Daily);
            this.GB_Choice.Controls.Add(this.RB_Period);
            this.GB_Choice.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GB_Choice.Location = new System.Drawing.Point(21, 51);
            this.GB_Choice.Name = "GB_Choice";
            this.GB_Choice.Size = new System.Drawing.Size(380, 67);
            this.GB_Choice.TabIndex = 92;
            this.GB_Choice.TabStop = false;
            this.GB_Choice.Text = "Select";
            // 
            // Frm_CuringInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(420, 445);
            this.Controls.Add(this.GB_Choice);
            this.Controls.Add(this.GB_Daily);
            this.Controls.Add(this.GB_Period);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Frm_CuringInfo";
            this.Text = "Curing Controller Info";
            this.Load += new System.EventHandler(this.Frm_CuringInfo_Load);
            this.GB_Period.ResumeLayout(false);
            this.GB_Period.PerformLayout();
            this.GB_Daily.ResumeLayout(false);
            this.GB_Daily.PerformLayout();
            this.GB_Choice.ResumeLayout(false);
            this.GB_Choice.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.ComboBox comboB_Press;
        private System.Windows.Forms.Label Lbl_Press;
        private System.Windows.Forms.DateTimePicker DTPicker_From;
        private System.Windows.Forms.GroupBox GB_Period;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CB_Shift;
        private System.Windows.Forms.GroupBox GB_Daily;
        private System.Windows.Forms.DateTimePicker DT_Daily;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox CB_Cycle;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox CB_Size;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CB_Item;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox CB_pressDaily;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker DTPicker_To;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton RB_Daily;
        private System.Windows.Forms.RadioButton RB_Period;
        private System.Windows.Forms.GroupBox GB_Choice;
    }
}