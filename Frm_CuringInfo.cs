﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_CuringInfo : Form
    {
        DataTable Info = new DataTable();
        DataTable SQLTables = new DataTable();
        DataTable SQLTablesDual = new DataTable();
        int i = 2;
        int j = 4;
        // יצירת קובץ אקסל
        // Add reference -> com -> Microsoft Excel 12.0 Object Library            
        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet, xlWorkSheet2;
        object misValue = System.Reflection.Missing.Value;
        Excel.Range chartRange;


        public Frm_CuringInfo()
        {
            InitializeComponent();
            DTPicker_From.Format = DateTimePickerFormat.Custom;
            DTPicker_From.CustomFormat = "yyyy-MM-dd HH.mm";
            DTPicker_To.Format = DateTimePickerFormat.Custom;
            DTPicker_To.CustomFormat = "yyyy-MM-dd HH.mm";
        }

        private void Frm_CuringInfo_Load(object sender, EventArgs e)
        {

            RB_Daily.Checked = true;
            GB_Daily.Visible = true;
            PrepareFilterDatasBetweenDates();
        }

        private void PrepareFilterDatasBetweenDates()
        {
            Cursor.Current = Cursors.WaitCursor;
            DbServiceSQL dbSql = new DbServiceSQL();
            DataTable DT = new DataTable();
            string StrSql = "";

            comboB_Press.Text = "";
            comboB_Press.Items.Clear();

            // יצירת טבלאת כלל הטבלאות שבבסיס הנתונים ומילוי נתונים לקומבובוקסים
            StrSql = "SELECT TABLE_NAME, substring(TABLE_NAME,10,3) as machine " +
                     "FROM INFORMATION_SCHEMA.TABLES " +
                     "WHERE TABLE_TYPE = 'BASE TABLE'  AND TABLE_CATALOG = 'PRESERVER' and TABLE_NAME like '%DATA%' and TABLE_NAME not in ('DATA_162_301_63','DATA_162_307_55','DATA_162_322_655','PROGRAM-DATA','PROGRAM-DATA-TEST') " +
                     "order by substring(TABLE_NAME,10,3) ";
            SQLTables = dbSql.executeSelectQueryNoParam(StrSql);
            if (SQLTables.Rows.Count > 0)
            {
                foreach (DataRow row in SQLTables.Rows)
                {
                    comboB_Press.Items.Add(row[1].ToString()); // PRESS
                    CB_pressDaily.Items.Add(row[1].ToString()); // PRESS)
                }
            }

            foreach (DataRow mach in SQLTables.Rows)
            {
                StrSql = "select CURE_RECIPE_NAME,TIRE_SIZE,CAT_NUM " +
                    "from " + mach[0].ToString() + " " +
                    "where HOUR_END_CURE between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd 23:30:00.000") + "' ";
                DT = dbSql.executeSelectQueryNoParam(StrSql);
                if (DT.Rows.Count > 0)
                {
                    CB_Cycle.Items.Add(DT.Rows[0][0].ToString());
                    CB_Size.Items.Add(DT.Rows[0][1].ToString());
                    CB_Item.Items.Add(DT.Rows[0][2].ToString());
                }
            }

            //DoubleCuringHeads();
            StrSql = "";
            Cursor.Current = Cursors.PanEast;
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            i = 2;
            j = 4;
            GetDataFromDB();
        }

        //SQL הפונקצייה מביאה את נתוני הגיפור מבסיס הנתונים שב 
        // הנתונים בבסיס הנתונים מגיעים מהבקרים שבמכונות
        public void GetDataFromDB()
        {
            DbServiceSQL dbs = new DbServiceSQL();
            DataTable DT = new DataTable();


            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);

            // מחיקת גיליונות אקסל מיותרים
            int WN = 0;
            foreach (Excel.Worksheet xlworksheet in xlWorkBook.Worksheets)
            {
                if (WN != 0) // על מנת לא למחוק את הגיליון הראשון - חייבים להשאיר גיליון 1 
                    xlworksheet.Delete();
                WN++;
            }

            xlWorkSheet2 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet2.Name = "Details";
            xlWorkSheet = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);//(Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "HMI Curing Controller Info";

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "W1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // Freeze rows            
            Excel.Window xlWnd3 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A1", "A1").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd3.FreezePanes = true;

            // AutoFilter            
            chartRange = xlWorkSheet.get_Range("A1", "P1");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // Wrap Text
            chartRange = xlWorkSheet.get_Range("A1", "P1");
            chartRange.Cells.WrapText = true;

            ////// AutoFilter            
            //chartRange = xlWorkSheet2.get_Range("A3", "P3");
            //chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet2.get_Range("A1", "P3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // Wrap Text
            chartRange = xlWorkSheet2.get_Range("A1", "P1");
            chartRange.Cells.WrapText = true;

            chartRange = xlWorkSheet2.get_Range("B1", "E1");
            chartRange.Merge();

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 12;
            chartRange = xlWorkSheet.get_Range("B1", "C1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("D1", "E1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("F1", "G1");
            chartRange.ColumnWidth = 12;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 20;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("L1", "N1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet2.get_Range("B1", "B1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet2.get_Range("C1", "D1");
            chartRange.ColumnWidth = 15;

            //xlWorkSheet.Cells[1, 1] = "תאריך";
            xlWorkSheet.Cells[1, 1] = "מכבש";
            xlWorkSheet.Cells[1, 2] = "מס גפר";
            xlWorkSheet.Cells[1, 3] = "שם + משפחה";
            xlWorkSheet.Cells[1, 4] = "מס קטלוגי";
            xlWorkSheet.Cells[1, 5] = "גודל צמיג";
            xlWorkSheet.Cells[1, 6] = "משקל צמיג";
            xlWorkSheet.Cells[1, 7] = "מחזור גיפור";
            xlWorkSheet.Cells[1, 8] = "זמן תחילת גיפור";
            xlWorkSheet.Cells[1, 9] = "זמן סיום גיפור";
            xlWorkSheet.Cells[1, 10] = "זמן גיפור";
            xlWorkSheet.Cells[1, 11] = "O/C זמן תקן ל";
            xlWorkSheet.Cells[1, 12] = "זמן המתנה בין גיפורים";
            xlWorkSheet.Cells[1, 13] = "O/C סטיית זמן ";
            xlWorkSheet.Cells[1, 14] = "סטיה מזמן גיפור";
            xlWorkSheet.Cells[1, 15] = "משמרת";
            xlWorkSheet.Cells[1, 16] = "זמן עד סוף משמרת";

            if (RB_Daily.Checked)
            {
                xlWorkSheet2.Cells[1, 2] = "דוח כמויות לתאריך " + DT_Daily.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                xlWorkSheet2.Cells[1, 2] = "דוח כמויות לתאריך " + DTPicker_From.Value.ToString("dd/MM/yyyy") + "  עד " + DTPicker_To.Value.ToString("dd/MM/yyyy");
            }
            xlWorkSheet2.Cells[3, 1] = "מכבש";
            xlWorkSheet2.Cells[3, 2] = "מק'ט";
            xlWorkSheet2.Cells[3, 3] = "צמיגים שגופרו";
            xlWorkSheet2.Cells[3, 4] = "זמן בין גיפורים";




            //int gipurTime = 0;
            // דוח תקופתי
            if (RB_Period.Checked)
            {
                // אם לא נבחר אף מכבש ספציפי נרוץ על כולם
                if (comboB_Press.Text == "")
                {
                    foreach (DataRow mach in SQLTables.Rows)
                    {
                        ReportByDate(mach[1].ToString());
                    }

                    // נרוץ גם על הכפולים
                    //foreach (DataRow mach in SQLTablesDual.Rows)
                    //{
                    //   ReportByDateDualHead(mach[1].ToString());
                    //}
                }
                else
                {
                    ReportByDate(comboB_Press.Text.Trim());
                }
            }
            //    // דוח יומי
            else if (RB_Daily.Checked)
            {
                if (CB_pressDaily.Text == "")
                {
                    foreach (DataRow mach in SQLTables.Rows)
                    {
                        DailyReport(mach[1].ToString());
                    }
                }
                else
                {
                    DailyReport(CB_pressDaily.Text.Trim());
                }
            }
            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        public void ReportByDate(string mach)
        {
            DbServiceSQL dbs = new DbServiceSQL();
            DataTable DT = new DataTable();
            DataTable DTAS400 = new DataTable();
            string StrSql = "";
            int gipurTime = 0;

            var selectedMach = SQLTables.Select("machine = " + mach);

            try
            {
                StrSql = "SELECT  HOUR_END_CURE,PRESS_NUM,CAT_NUM,TIRE_SIZE, CURE_RECIPE_NAME,HOUR_ST_CURE,HOUR_END_CURE,DATEDIFF(MINUTE, HOUR_ST_CURE, HOUR_END_CURE) as 'Curing Time', " +
                         "DATEDIFF(MINUTE, pDataDate, HOUR_ST_CURE) as 'Waiting Time',[open_close_standard(min)]  " +
                         "FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate " +
                         "FROM " + selectedMach[0][0] + " ) q " +
                         "Left join [dbo].[Open_Close_Norms] on press_no = PRESS_NUM " +
                         "where HOUR_END_CURE between '" + DTPicker_From.Value.ToString("yyyy-MM-dd HH:mm:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd HH:mm:00.000") + "' ";
                //"where HOUR_END_CURE between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd 23:30:00.000") + "' ";
                if (CB_Item.Text != "")
                    StrSql += "and CAT_NUM='" + CB_Item.Text.Trim() + "' ";
                if (CB_Size.Text != "")
                    StrSql += "and TIRE_SIZE='" + CB_Size.Text.Trim() + "' ";
                if (CB_Cycle.Text != "")
                    StrSql += "and CURE_RECIPE_NAME='" + CB_Cycle.Text.Trim() + "' ";
                DT = dbs.executeSelectQueryNoParam(StrSql);
                if (DT.Rows.Count > 0)
                    DTAS400 = GetDataAS400(DT_Daily.Value, DT.Rows[0]["PRESS_NUM"].ToString());

                foreach (DataRow row in DT.Rows)
                {
                    xlWorkSheet.Cells[i, 1] = row["PRESS_NUM"].ToString();
                    // קביעת המשמרת 
                    string t = Shift(Convert.ToDateTime(row["HOUR_END_CURE"].ToString()));
                    // שליפת נתוני העובדים הרלוונטית לאותה משמרת
                    DataRow[] n = DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ");
                    xlWorkSheet.Cells[i, 2] = n.Length > 0 ? DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["oved"].ToString() : "";
                    xlWorkSheet.Cells[i, 3] = n.Length > 0 ? DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["prati"].ToString() + " " + DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["family"].ToString() : "";
                    xlWorkSheet.Cells[i, 4] = row["CAT_NUM"].ToString();
                    xlWorkSheet.Cells[i, 5] = row["TIRE_SIZE"].ToString();
                    xlWorkSheet.Cells[i, 6] = GetSpecWeight(row["CAT_NUM"].ToString());
                    xlWorkSheet.Cells[i, 7] = row["CURE_RECIPE_NAME"].ToString();
                    xlWorkSheet.Cells[i, 8] = row["HOUR_ST_CURE"].ToString();
                    xlWorkSheet.Cells[i, 9] = row["HOUR_END_CURE"].ToString();
                    xlWorkSheet.Cells[i, 10] = row["Curing Time"].ToString();
                    xlWorkSheet.Cells[i, 11] = row["open_close_standard(min)"].ToString();
                    xlWorkSheet.Cells[i, 12] = row["Waiting Time"].ToString();
                    xlWorkSheet.Cells[i, 13] = row["Waiting Time"].ToString() != "" ? decimal.Parse(row["Waiting Time"].ToString()) - decimal.Parse(row["open_close_standard(min)"].ToString()) : 0 - decimal.Parse(row["open_close_standard(min)"].ToString());
                    if ((row[4].ToString().Trim().Split('-').Length - 1) == 2) // בדיקה אם מחזור הגיפור תקין
                        xlWorkSheet.Cells[i, 14] = Convert.ToInt32(row["Curing Time"].ToString()) - int.Parse(row["CURE_RECIPE_NAME"].ToString().Trim().Substring(7, (row["CURE_RECIPE_NAME"].ToString().Trim().Length - 7)));
                    xlWorkSheet.Cells[i, 15] = int.Parse(t) + 1;
                    gipurTime += row["Waiting Time"].ToString() != "" ? int.Parse(row["Waiting Time"].ToString()) : 0;

                    if (row == DT.Rows[DT.Rows.Count - 1])
                    {
                        xlWorkSheet.Cells[i, 16] = Math.Round((DTPicker_To.Value - Convert.ToDateTime(row["HOUR_END_CURE"].ToString())).TotalMinutes,2);
                    }

                    i++;
                }
                //xlWorkSheet2.Cells[j, 1] = mach;
                //xlWorkSheet2.Cells[j, 2] = DT.Rows.Count;
                //xlWorkSheet2.Cells[j, 3] = gipurTime;
                ///////   שינוי הדוח שיכלול פירוט על פי מק'ט
                StrSql = @"SELECT  PRESS_NUM as mach,COUNT(CAT_NUM) AS counter, CAT_NUM, sum(DATEDIFF(MINUTE, pDataDate, HOUR_ST_CURE)) as 'Waiting Time'
                        FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate " +
                       "FROM " + selectedMach[0][0] + " ) q " +
                       "where HOUR_END_CURE between '" + DTPicker_From.Value.ToString("yyyy-MM-dd HH:mm:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd HH:mm:00.000") + "' " +
                       "GROUP BY PRESS_NUM,CAT_NUM";
                DT = dbs.executeSelectQueryNoParam(StrSql);
                foreach (DataRow row in DT.Rows)
                {
                    xlWorkSheet2.Cells[j, 1] = row["mach"].ToString();
                    xlWorkSheet2.Cells[j, 2] = row["CAT_NUM"].ToString();
                    xlWorkSheet2.Cells[j, 3] = row["counter"].ToString();
                    xlWorkSheet2.Cells[j, 4] = row["Waiting Time"].ToString();
                    j++;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // נתוני מכבשים כפולים
        public void ReportByDateDualHead(string mach)
        {
            DbServiceSQL dbs = new DbServiceSQL();
            DataTable DT = new DataTable();
            DataTable DTAS400 = new DataTable();
            string StrSql = "";
            int gipurTime = 0;
            string side = "";

            var selectedMach = SQLTablesDual.Select("machine = " + mach);

            for (int l = 0; l < 2; l++)
            {
                side = l == 0 ? "L" : "R";
                try
                {
                    StrSql = "SELECT  HOUR_END_CURE,PRESS_NUM,CAT_" + side + "_NUM,TIRE_" + side + "_SIZE, CURE_" + side + "_RECIPE_NAME,HOUR_ST_CURE,HOUR_END_CURE,DATEDIFF(MINUTE, HOUR_ST_CURE, HOUR_END_CURE) as 'Curing Time', " +
                             "DATEDIFF(MINUTE, pDataDate, HOUR_ST_CURE) as 'Waiting Time',[open_close_standard(min)]  " +
                             "FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate " +
                             "FROM " + selectedMach[0][0] + " ) q " +
                             "Left join [dbo].[Open_Close_Norms] on press_no = PRESS_NUM " +
                             "where HOUR_END_CURE between '" + DTPicker_From.Value.ToString("yyyy-MM-dd HH:mm:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd HH:mm:00.000") + "' ";
                    if (CB_Item.Text != "")
                        StrSql += "and CAT_" + side + "_NUM='" + CB_Item.Text.Trim() + "' ";
                    if (CB_Size.Text != "")
                        StrSql += "and TIRE_" + side + "_SIZE='" + CB_Size.Text.Trim() + "' ";
                    if (CB_Cycle.Text != "")
                        StrSql += "and CURE_" + side + "_RECIPE_NAME='" + CB_Cycle.Text.Trim() + "' ";
                    DT = dbs.executeSelectQueryNoParam(StrSql);
                    if (DT.Rows.Count > 0)
                        DTAS400 = GetDataAS400(DT_Daily.Value, DT.Rows[0]["PRESS_NUM"].ToString());

                    foreach (DataRow row in DT.Rows)
                    {
                        xlWorkSheet.Cells[i, 1] = row["PRESS_NUM"].ToString();
                        // קביעת המשמרת 
                        string t = Shift(Convert.ToDateTime(row["HOUR_END_CURE"].ToString()));
                        // שליפת נתוני העובדים הרלוונטית לאותה משמרת
                        DataRow[] n = DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ");
                        xlWorkSheet.Cells[i, 2] = n.Length > 0 ? DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["oved"].ToString() : "";
                        xlWorkSheet.Cells[i, 3] = n.Length > 0 ? DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["prati"].ToString() + " " + DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["family"].ToString() : "";
                        xlWorkSheet.Cells[i, 4] = row["CAT_" + side + "_NUM"].ToString();
                        xlWorkSheet.Cells[i, 5] = row["TIRE_" + side + "_SIZE"].ToString();
                        xlWorkSheet.Cells[i, 6] = GetSpecWeight(row["CAT_" + side + "_NUM"].ToString());
                        xlWorkSheet.Cells[i, 7] = row["CURE_" + side + "_RECIPE_NAME"].ToString();
                        xlWorkSheet.Cells[i, 8] = row["HOUR_ST_CURE"].ToString();
                        xlWorkSheet.Cells[i, 9] = row["HOUR_END_CURE"].ToString();
                        xlWorkSheet.Cells[i, 10] = row["Curing Time"].ToString();
                        xlWorkSheet.Cells[i, 11] = row["open_close_standard(min)"].ToString();
                        xlWorkSheet.Cells[i, 12] = row["Waiting Time"].ToString();
                        //xlWorkSheet.Cells[i, 13] = decimal.Parse(row["Waiting Time"].ToString()) - decimal.Parse(row["open_close_standard(min)"].ToString());
                        xlWorkSheet.Cells[i, 13] = row["Waiting Time"].ToString() != "" ? decimal.Parse(row["Waiting Time"].ToString()) - decimal.Parse(row["open_close_standard(min)"].ToString()) : 0 - decimal.Parse(row["open_close_standard(min)"].ToString());
                        if ((row[4].ToString().Trim().Split('-').Length - 1) == 2) // בדיקה אם מחזור הגיפור תקין
                            xlWorkSheet.Cells[i, 14] = Convert.ToInt32(row["Curing Time"].ToString()) - int.Parse(row["CURE_" + side + "_RECIPE_NAME"].ToString().Trim().Substring(7, (row["CURE_" + side + "_RECIPE_NAME"].ToString().Trim().Length - 7)));
                        xlWorkSheet.Cells[i, 15] = int.Parse(t) + 1;
                        gipurTime += row["Waiting Time"].ToString() != "" ? int.Parse(row["Waiting Time"].ToString()) : 0;

                        if (row == DT.Rows[DT.Rows.Count - 1])
                        {
                            xlWorkSheet.Cells[i, 16] = Math.Round((DTPicker_To.Value - Convert.ToDateTime(row["HOUR_END_CURE"].ToString())).TotalMinutes, 2);
                        }

                        i++;
                    }
                    xlWorkSheet2.Cells[j, 1] = l == 0 ?  mach : (int.Parse(mach)+1).ToString() ;
                    xlWorkSheet2.Cells[j, 2] = DT.Rows.Count;
                    xlWorkSheet2.Cells[j, 3] = gipurTime;
                    j++;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
           
        }



        // תוספות מתאריך 01/06/2017 ע"י רן הויכמן
        // AS400 הוספת שליפת נתונים נוספים מ 
        public void DailyReport(string mach)
        {
            DbServiceSQL dbs = new DbServiceSQL();
            DataTable DT = new DataTable();
            DataTable DTAS400 = new DataTable();
            string StrSql = "";
            int gipurTime = 0;
            int shift = 2;

            string ShiftNo = "";

           
            xlWorkSheet2.Cells[3, 1] = "מכבש";
            xlWorkSheet2.Cells[3, 2] = "משמרת לילה";
            xlWorkSheet2.Cells[3, 3] = "משמרת בוקר";
            xlWorkSheet2.Cells[3, 4] = "משמרת ערב";
            int counter = 0;

            var selectedMach = SQLTables.Select("machine = " + mach);
            try
            {
                if (CB_Shift.Text.Trim() == "1") //if (CB_Shift.Text.Trim() == "1" || CB_Shift.Text == "")
                {
                    StrSql = "SELECT PRESS_NUM,CAT_NUM,TIRE_SIZE, CURE_RECIPE_NAME,HOUR_ST_CURE,HOUR_END_CURE,DATEDIFF(MINUTE, HOUR_ST_CURE, HOUR_END_CURE) as 'Curing Time', " +
                             "DATEDIFF(MINUTE, pDataDate, HOUR_ST_CURE) as 'Waiting Time',[open_close_standard(min)] " +
                             "FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate " +
                             "FROM " + "" + selectedMach[0][0] + ") q " +
                             "left join [dbo].[Open_Close_Norms] on press_no = PRESS_NUM " +
                             "where HOUR_ST_CURE between '" + DT_Daily.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DT_Daily.Value.ToString("yyyy-MM-dd 06:29:59.000") + "' ";
                    DT = dbs.executeSelectQueryNoParam(StrSql);
                    if (DT.Rows.Count > 0)
                        DTAS400 = GetDataAS400(DT_Daily.Value, DT.Rows[0]["PRESS_NUM"].ToString());
                    FillExcelData(DT, gipurTime, counter, shift);
                    #region
                    ////AS400 שליפת נתונים מ 
                    //if (DT.Rows.Count > 0)
                    //{
                    //    //DateTime date1;
                    //    //DateTime.TryParse(DT.Rows[0]["HOUR_ST_CURE"].ToString(), out date1);
                    //    DTAS400 = GetDataAS400(DT_Daily.Value, DT.Rows[0]["PRESS_NUM"].ToString(),"1");

                    //    foreach (DataRow row in DT.Rows)
                    //    {
                    //            xlWorkSheet.Cells[i, 1] = row["PRESS_NUM"].ToString();
                    //            xlWorkSheet.Cells[i, 2] = DTAS400.Rows.Count > 0 ? DTAS400.Rows[0]["oved"] : "";
                    //            xlWorkSheet.Cells[i, 3] = DTAS400.Rows.Count > 0 ? DTAS400.Rows[0]["prati"] + " " + DTAS400.Rows[0]["family"] : "";
                    //            xlWorkSheet.Cells[i, 4] = row["CAT_NUM"].ToString();
                    //            xlWorkSheet.Cells[i, 5] = row["TIRE_SIZE"].ToString();
                    //            xlWorkSheet.Cells[i, 6] = GetSpecWeight(row["CAT_NUM"].ToString());
                    //            xlWorkSheet.Cells[i, 7] = row["CURE_RECIPE_NAME"].ToString();
                    //            xlWorkSheet.Cells[i, 8] = row["HOUR_ST_CURE"].ToString();
                    //            xlWorkSheet.Cells[i, 9] = row["HOUR_END_CURE"].ToString();
                    //            xlWorkSheet.Cells[i, 10] = row["Curing Time"].ToString();
                    //            xlWorkSheet.Cells[i, 11] = row["Waiting Time"].ToString();
                    //            if ((row[4].ToString().Trim().Split('-').Length - 1) == 2) // בדיקה אם מחזור הגיפור תקין
                    //                xlWorkSheet.Cells[i, 12] = Convert.ToInt16(row["Curing Time"].ToString()) - int.Parse(row["CURE_RECIPE_NAME"].ToString().Trim().Substring(7, (row["CURE_RECIPE_NAME"].ToString().Trim().Length - 7)));
                    //            xlWorkSheet.Cells[i, 13] = 1;
                    //            gipurTime += row["Waiting Time"].ToString() != "" ? int.Parse(row["Waiting Time"].ToString()) : 0;
                    //            counter++;
                    //            i++;                            
                    //    }
                    //}

                    //xlWorkSheet2.Cells[j, shift] = counter;
                    #endregion
                }
                else if (CB_Shift.Text.Trim() == "2") //if (CB_Shift.Text.Trim() == "2" || CB_Shift.Text == "")
                {
                    StrSql = "SELECT PRESS_NUM,CAT_NUM,TIRE_SIZE, CURE_RECIPE_NAME,HOUR_ST_CURE,HOUR_END_CURE,DATEDIFF(MINUTE, HOUR_ST_CURE, HOUR_END_CURE) as 'Curing Time', " +
                             "DATEDIFF(MINUTE, pDataDate, HOUR_ST_CURE) as 'Waiting Time',[open_close_standard(min)] " +
                             "FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate " +
                             "FROM " + "" + selectedMach[0][0] + " ) q " +
                             "left join [dbo].[Open_Close_Norms] on press_no = PRESS_NUM " +
                             "where HOUR_ST_CURE between '" + DT_Daily.Value.ToString("yyyy-MM-dd 06:30:00.000") + "' and '" + DT_Daily.Value.ToString("yyyy-MM-dd 14:59:59.000") + "' ";
                    DT = dbs.executeSelectQueryNoParam(StrSql);
                    counter = 0;
                    shift++;
                    if (DT.Rows.Count > 0)
                        DTAS400 = GetDataAS400(DT_Daily.Value, DT.Rows[0]["PRESS_NUM"].ToString());
                    FillExcelData(DT, gipurTime, counter, shift);
                }
                else if (CB_Shift.Text.Trim() == "3") //if (CB_Shift.Text.Trim() == "3" || CB_Shift.Text == "")
                {
                    StrSql = "SELECT PRESS_NUM,CAT_NUM,TIRE_SIZE, CURE_RECIPE_NAME,HOUR_ST_CURE,HOUR_END_CURE,DATEDIFF(MINUTE, HOUR_ST_CURE, HOUR_END_CURE) as 'Curing Time', " +
                          "DATEDIFF(MINUTE, pDataDate, HOUR_ST_CURE) as 'Waiting Time',[open_close_standard(min)] " +
                          "FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate " +
                          "FROM  " + "" + selectedMach[0][0] + " ) q " +
                          "left join [dbo].[Open_Close_Norms] on press_no = PRESS_NUM " +
                          "where HOUR_ST_CURE between '" + DT_Daily.Value.ToString("yyyy-MM-dd 15:00:00.000") + "' and '" + DT_Daily.Value.ToString("yyyy-MM-dd 23:29:59.000") + "' ";
                    DT = dbs.executeSelectQueryNoParam(StrSql);
                    counter = 0;
                    shift = 4;
                    if(DT.Rows.Count > 0)
                        DTAS400 = GetDataAS400(DT_Daily.Value, DT.Rows[0]["PRESS_NUM"].ToString());
                    FillExcelData(DT, gipurTime, counter, shift);
                    #region
                    //foreach (DataRow row in DT.Rows)
                    //{
                    //    //xlWorkSheet.Cells[i, 1] = row[0].ToString();
                    //    xlWorkSheet.Cells[i, 1] = row["PRESS_NUM"].ToString();
                    //    xlWorkSheet.Cells[i, 2] = row["CAT_NUM"].ToString();
                    //    xlWorkSheet.Cells[i, 3] = row["TIRE_SIZE"].ToString();
                    //    xlWorkSheet.Cells[i, 4] = row["CURE_RECIPE_NAME"].ToString();
                    //    xlWorkSheet.Cells[i, 5] = row["HOUR_ST_CURE"].ToString();
                    //    xlWorkSheet.Cells[i, 6] = row["HOUR_END_CURE"].ToString();
                    //    xlWorkSheet.Cells[i, 7] = row["Curing Time"].ToString();
                    //    xlWorkSheet.Cells[i, 8] = row["Waiting Time"].ToString();
                    //    if ((row[4].ToString().Trim().Split('-').Length - 1) == 2) // בדיקה אם מחזור הגיפור תקין
                    //        xlWorkSheet.Cells[i, 9] = Convert.ToInt16(row["Curing Time"].ToString()) - int.Parse(row["CURE_RECIPE_NAME"].ToString().Trim().Substring(7, (row["CURE_RECIPE_NAME"].ToString().Trim().Length - 7)));
                    //    xlWorkSheet.Cells[i, 10] = 1;
                    //    gipurTime += row["Waiting Time"].ToString() != "" ? int.Parse(row["Waiting Time"].ToString()) : 0;
                    //    counter++;
                    //    i++;
                    //}

                    //xlWorkSheet2.Cells[j, shift] = counter;
                    #endregion
                }
                else // כל המשמרות
                {
                    StrSql = "SELECT  PRESS_NUM,CAT_NUM,TIRE_SIZE, CURE_RECIPE_NAME,HOUR_ST_CURE,HOUR_END_CURE,DATEDIFF(MINUTE, HOUR_ST_CURE, HOUR_END_CURE) as 'Curing Time', " +
                             "DATEDIFF(MINUTE, pDataDate, HOUR_ST_CURE) as 'Waiting Time',[open_close_standard(min)] " +
                             "FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate " +
                             "FROM " + "" + selectedMach[0][0] + ") q " +
                             "left join [dbo].[Open_Close_Norms] on press_no = PRESS_NUM " +
                             "where HOUR_ST_CURE between '" + DT_Daily.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DT_Daily.Value.ToString("yyyy-MM-dd 23:29:59.000") + "' ";
                    DT = dbs.executeSelectQueryNoParam(StrSql);
                    if (DT.Rows.Count > 0)
                        DTAS400 = GetDataAS400(DT_Daily.Value, DT.Rows[0]["PRESS_NUM"].ToString());

                    foreach (DataRow row in DT.Rows)
                    {
                        xlWorkSheet.Cells[i, 1] = row["PRESS_NUM"].ToString();
                        // קביעת המשמרת 
                        string t = Shift(Convert.ToDateTime(row["HOUR_END_CURE"].ToString()));
                        // שליפת נתוני העובדים הרלוונטית לאותה משמרת
                        DataRow[] n = DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ");
                        xlWorkSheet.Cells[i, 2] = n.Length > 0 ? DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["oved"].ToString() : "";
                        xlWorkSheet.Cells[i, 3] = n.Length > 0 ? DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["prati"].ToString() + " " + DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["family"].ToString() : "";
                        xlWorkSheet.Cells[i, 4] = row["CAT_NUM"].ToString();
                        xlWorkSheet.Cells[i, 5] = row["TIRE_SIZE"].ToString();
                        xlWorkSheet.Cells[i, 6] = GetSpecWeight(row["CAT_NUM"].ToString());
                        xlWorkSheet.Cells[i, 7] = row["CURE_RECIPE_NAME"].ToString();
                        xlWorkSheet.Cells[i, 8] = row["HOUR_ST_CURE"].ToString();
                        xlWorkSheet.Cells[i, 9] = row["HOUR_END_CURE"].ToString();
                        xlWorkSheet.Cells[i, 10] = row["Curing Time"].ToString();
                        xlWorkSheet.Cells[i, 11] = row["open_close_standard(min)"].ToString();
                        xlWorkSheet.Cells[i, 12] = row["Waiting Time"].ToString();
                        xlWorkSheet.Cells[i, 13] = decimal.Parse(row["Waiting Time"].ToString()) - decimal.Parse(row["open_close_standard(min)"].ToString());
                        if ((row["CURE_RECIPE_NAME"].ToString().Trim().Split('-').Length - 1) == 2) // בדיקה אם מחזור הגיפור תקין
                            xlWorkSheet.Cells[i, 14] = Convert.ToInt16(row["Curing Time"].ToString()) - int.Parse(row["CURE_RECIPE_NAME"].ToString().Trim().Substring(7, (row["CURE_RECIPE_NAME"].ToString().Trim().Length - 7)));
                        if (Convert.ToDateTime(row["HOUR_ST_CURE"]).TimeOfDay >= DateTime.Parse("2017 /05/30 06:30:00.000").TimeOfDay && Convert.ToDateTime(row["HOUR_ST_CURE"]).TimeOfDay <= DateTime.Parse("2017 /05/30 14:59:59.000").TimeOfDay)
                            ShiftNo = "2";
                        else if (Convert.ToDateTime(row["HOUR_ST_CURE"]).TimeOfDay >= DateTime.Parse("2017 /05/30 15:00:00.000").TimeOfDay && Convert.ToDateTime(row["HOUR_ST_CURE"]).TimeOfDay <= DateTime.Parse("2017 /05/30 23:29:59.000").TimeOfDay)
                            ShiftNo = "3";
                        else
                            ShiftNo = "1";
                        xlWorkSheet.Cells[i, 15] = ShiftNo; //1;
                        gipurTime += row["Waiting Time"].ToString() != "" ? int.Parse(row["Waiting Time"].ToString()) : 0;
                        counter++;
                        i++;
                    }   
                    xlWorkSheet2.Cells[j, shift] = counter;
                }

                xlWorkSheet2.Cells[j, 1] = mach;
                j++;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //  פונקציה המחזירה את טבלת המכבשים הכפולים
        public void DoubleCuringHeads()
        {
            DbServiceSQL dbs = new DbServiceSQL();

            string StrSql = "SELECT TABLE_NAME, substring(TABLE_NAME,10,3) as machine " +
                     "FROM INFORMATION_SCHEMA.TABLES " +
                     "WHERE TABLE_TYPE = 'BASE TABLE'  AND TABLE_CATALOG = 'PRESERVER' and TABLE_NAME like '%DATA%' and TABLE_NAME in ('DATA_162_301_63','DATA_162_307_55','DATA_162_322_655') " +
                     "order by substring(TABLE_NAME,10,3) ";
            SQLTablesDual = dbs.executeSelectQueryNoParam(StrSql);
            if (SQLTablesDual.Rows.Count > 0)
            {
                foreach (DataRow row in SQLTablesDual.Rows)
                {
                    comboB_Press.Items.Add(row[1].ToString()); // PRESS
                    CB_pressDaily.Items.Add(row[1].ToString()); // PRESS)
                }
            }

            foreach (DataRow mach in SQLTablesDual.Rows)
            {
                StrSql = "select CURE_RECIPE_NAME,TIRE_SIZE,CAT_NUM " +
                    "from " + mach[0].ToString() + " " +
                    "where HOUR_END_CURE between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd 23:30:00.000") + "' ";
                SQLTablesDual = dbs.executeSelectQueryNoParam(StrSql);
                if (SQLTablesDual.Rows.Count > 0)
                {
                    CB_Cycle.Items.Add(SQLTablesDual.Rows[0][0].ToString());
                    CB_Size.Items.Add(SQLTablesDual.Rows[0][1].ToString());
                    CB_Item.Items.Add(SQLTablesDual.Rows[0][2].ToString());
                }
            }
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        // פונקציה המחזירה מס משמרת בהתאם לשעה
        public string Shift(DateTime time)
        {
            DBService dbs = new DBService();
            DataTable DT;
            string StrSql = "";
            StrSql = "select sshft from BPCSFALI.shf " +
                     "where sday=0 and " + time.Hour.ToString().PadLeft(2,'0') + time.Minute.ToString().PadLeft(2, '0') + " between sfhrs and sthrs";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count == 0)
            {
                return "0";
            }
            else
            {
                return (int.Parse(DT.Rows[0][0].ToString()) -1).ToString();
            }
        }

        public List<string> Hours(string shift)
        {
            DBService dbs = new DBService();
            DataTable DT;
            List<string> FromTo = new List<string>();
            //DTPicker_To.Value.AddDays(1).ToString("yyyy-MM-dd-03.29.59.999999")
            string StrSql = "select SFHRS,STHRS from BPCSFALI.shf " +
                            "where sday=0 and sshft='" + shift + "' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                FromTo.Add(DT.Rows[0].ToString());
                FromTo.Add(DT.Rows[1].ToString());
            }
            return FromTo;
        }

        private void RB_Daily_CheckedChanged(object sender, EventArgs e)
        {
            GB_Daily.Visible = RB_Daily.Checked ? true : false;
        }

        private void RB_Period_CheckedChanged(object sender, EventArgs e)
        {
            GB_Period.Visible = RB_Period.Checked ? true : false;
        }

        private void DTPicker_To_Leave(object sender, EventArgs e)
        {
            //DbServiceSQL dbs = new DbServiceSQL();
            //DataTable DT = new DataTable();
            //string StrSql = "";
            //foreach (DataRow mach in SQLTables.Rows)
            //{
            //    StrSql = "select CURE_RECIPE_NAME,TIRE_SIZE,CAT_NUM " +
            //        "from " + mach[0].ToString() + " " +
            //        "where HOUR_END_CURE between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd 23:30:00.000") + "' ";
            //    DT = dbs.executeSelectQueryNoParam(StrSql);
            //    if (DT.Rows.Count > 0)
            //    {
            //        CB_Cycle.Items.Add(DT.Rows[0][0].ToString());
            //        CB_Size.Items.Add(DT.Rows[0][1].ToString());
            //        CB_Item.Items.Add(DT.Rows[0][2].ToString());
            //    }
            //}
        }

        // פונקציה להבאת נתונים נוספים לדוח
        private DataTable GetDataAS400(DateTime date1, string press)
        {
            //AS400 שליפת נתונים מ 
            DBService dbs400 = new DBService();
            DataTable DTAS400 = new DataTable();

            string StrSql = "select distinct a.loved as oved, prati, family,a.lshift as s " +
                     "from tapiali.labelgp as a " +
                     "left join tapiali.labelp as b on a.lprod = b.lprod and a.llblno = b.llblno " +
                     "left join isufkv.isavl10 on '00'||substr(a.loved,4,5)=oved " +
                     "where a.lacndt = '" + date1.Year.ToString() + date1.Month.ToString().PadLeft(2, '0') + date1.Day.ToString().PadLeft(2, '0') + "' and a.lmach = '" + press + "' and a.LACTAN = 42 " +
                     "order by a.lshift ";
            return DTAS400 = dbs400.executeSelectQueryNoParam(StrSql);
        }

        private string GetSpecWeight(string item)
        {
            DBService dbs400 = new DBService();
            DataTable DT = new DataTable();
            string StrSql = "select ICSCP1 from bpcsfv30.cicl01 where ICFAC='F1' and ICPROD ='" + item.Trim() + "' ";
            DT = dbs400.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                return DT.Rows[0][0].ToString();
            }
            return "";
        }

        // פונקציה להזנת הנתונים לקובץ האקסל הנבנה.
        private void FillExcelData(DataTable DT, int gipurTime, int counter, int shift)
        {
            DataTable DTAS400 = new DataTable();
            //AS400 שליפת נתונים מ 
            if (DT.Rows.Count > 0)
            {
                DTAS400 = GetDataAS400(DT_Daily.Value, DT.Rows[0]["PRESS_NUM"].ToString());

                foreach (DataRow row in DT.Rows)
                {
                    xlWorkSheet.Cells[i, 1] = row["PRESS_NUM"].ToString();
                    // קביעת המשמרת 
                    string t = Shift(Convert.ToDateTime(row["HOUR_END_CURE"].ToString()));
                    // שליפת נתוני העובדים הרלוונטית לאותה משמרת
                    DataRow[] n = DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ");
                    xlWorkSheet.Cells[i, 2] = n.Length > 0 ? DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["oved"].ToString() : "";
                    xlWorkSheet.Cells[i, 3] = n.Length > 0 ? DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["prati"].ToString() + " " + DTAS400.Select("s='" + (int.Parse(t.ToString()) + 1).ToString() + "' ")[0]["family"].ToString() : "";
                    xlWorkSheet.Cells[i, 4] = row["CAT_NUM"].ToString();
                    xlWorkSheet.Cells[i, 5] = row["TIRE_SIZE"].ToString();
                    xlWorkSheet.Cells[i, 6] = GetSpecWeight(row["CAT_NUM"].ToString());
                    xlWorkSheet.Cells[i, 7] = row["CURE_RECIPE_NAME"].ToString();
                    xlWorkSheet.Cells[i, 8] = row["HOUR_ST_CURE"].ToString();
                    xlWorkSheet.Cells[i, 9] = row["HOUR_END_CURE"].ToString();
                    xlWorkSheet.Cells[i, 10] = row["Curing Time"].ToString();
                    xlWorkSheet.Cells[i, 11] = row["open_close_standard(min)"].ToString();
                    xlWorkSheet.Cells[i, 12] = row["Waiting Time"].ToString();
                    xlWorkSheet.Cells[i, 13] = row["Waiting Time"].ToString() != "" ? decimal.Parse(row["Waiting Time"].ToString()) - decimal.Parse(row["open_close_standard(min)"].ToString()) : 0 - decimal.Parse(row["open_close_standard(min)"].ToString());
                    //xlWorkSheet.Cells[i, 13] = decimal.Parse(row["Waiting Time"].ToString()) - decimal.Parse(row["open_close_standard(min)"].ToString());
                    if ((row[4].ToString().Trim().Split('-').Length - 1) == 2) // בדיקה אם מחזור הגיפור תקין
                        xlWorkSheet.Cells[i, 14] = Convert.ToInt32(row["Curing Time"].ToString()) - int.Parse(row["CURE_RECIPE_NAME"].ToString().Trim().Substring(7, (row["CURE_RECIPE_NAME"].ToString().Trim().Length - 7)));
                    xlWorkSheet.Cells[i, 15] = 1;
                    gipurTime += row["Waiting Time"].ToString() != "" ? int.Parse(row["Waiting Time"].ToString()) : 0;
                    counter++;
                    i++;
                }
                xlWorkSheet2.Cells[j, shift] = counter;
                xlWorkSheet2.Cells[j, shift+1] = gipurTime;
            }
        }
    }
}
