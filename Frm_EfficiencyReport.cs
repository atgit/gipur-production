﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_EfficiencyReport : Form
    {
        public Frm_EfficiencyReport()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Eficiancyreport();
        }

        private void Eficiancyreport()
        {
            string StrSql;
            string machine = "";
            int shifts = 0;
            decimal pcs = 0;
            int j = 4, k = 2;
            int[] months;
            string[,] shft = new string[20, 2];
            string[] monname;
            decimal weight = 0;
            string date = "", previousDate = "";
            string Str_S_Date;
            string Str_E_Date;
            // AS400 התקשרות ל        
            string Con_Str;
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBCW;
            DBCW = new DBConn();
            DBCW.Initialize_Conn(Con_Str);
            // יצירת אקסל
            Excel._Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;
            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            // מחיקת גיליונות אקסל מיותרים
            int WN = 0;
            foreach (Excel.Worksheet xlworksheet in xlWorkBook.Worksheets)
            {
                if (WN != 0) // על מנת לא למחוק את הגיליון הראשון - חייבים להשאיר גיליון 1 
                    xlworksheet.Delete();
                WN++;
            }
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "Efficiency Report By Year";
            xlWorkSheet.Activate();
            //עיצוב תאים 
            chartRange = xlWorkSheet.get_Range("B2", "F2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("G2", "K2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("L2", "P2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("Q2", "U2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("V2", "Z2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AA2", "AE2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AF2", "AJ2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AK2", "AO2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AP2", "AT2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AU2", "AY2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("AZ2", "BD2");
            chartRange.Merge();
            chartRange = xlWorkSheet.get_Range("BE2", "BI2");
            chartRange.Merge();

            //שמות תאים
            monname = new string[13];
            monname[0] = "January";
            monname[1] = "February";
            monname[2] = "March";
            monname[3] = "April";
            monname[4] = "May";
            monname[5] = "June";
            monname[6] = "July";
            monname[7] = "August";
            monname[8] = "September";
            monname[9] = "October";
            monname[10] = "November";
            monname[11] = "December";
            int p = 0;
            for (int y = 2; y < 62; y = y + 5)
            {
                xlWorkSheet.Cells[2, y] = monname[p];
                p++;
            }

            // התאמת תאריך
            Str_S_Date = (dateTimePicker1.Value.Year - 1928).ToString("00") + dateTimePicker1.Value.Month.ToString("00") + dateTimePicker1.Value.Day.ToString("00");
            Str_E_Date = (dateTimePicker2.Value.Year - 1928).ToString("00") + dateTimePicker2.Value.Month.ToString("00") + dateTimePicker2.Value.Day.ToString("00");

            DateTime date1, date2;
            date1 = dateTimePicker1.Value;
            date2 = dateTimePicker2.Value;
            var diffMonths = (date2.Month + date2.Year * 12) - (date1.Month + date1.Year * 12) + 1;
            // בניית מערך חודשים 
            StrSql = "select distinct substring(ttdte,1,4) " +
                      "from BPCSFV30.Flt " +
                      "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and " +
                      "tdept in (71,160) and tpcs<>0 ";
            DBCW.Q_Run1(StrSql);
            months = new int[diffMonths];
            int i = 0;
            while (DBCW.Rs1.Read())
            {
                months[i] = int.Parse((int.Parse((DBCW.Rs1.GetString(0).Substring(0, 2))) + 1928).ToString().Substring(2, 2)) * 100 + (int.Parse(DBCW.Rs1.GetString(0).Substring(2, 2)));
                i++;
            }
            DBCW.Rs1.Close();

            //שליפת נתונים
            //StrSql = "select TMACH, sum(TPCS) as pcs, count(distinct ttdte||tshft) as Shift, sum(TPCS)*sum(TWGHT) as Weight, ttdte||tshft " +
            //        "from BPCSFALI.FltQV " +
            //        "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and " +
            //        "tdept in (71,160) and tpcs<>0 " +
            //        "group by TMACH, ttdte||tshft " +
            //        "order by TMACH, ttdte||tshft ";

            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            DataTable Machs = new DataTable();
            DataTable DT1 = new DataTable();

            // שליפת רשימת כלל המכונות
            StrSql = "select distinct tmach from bpcsfali.fltqv " +
                     "where tdept in (71,160) and tpcs<>0";
            Machs = dbs.executeSelectQueryNoParam(StrSql);

            for (i = 2; i < 62; i++)
            {
                xlWorkSheet.Cells[3, i] = "Shifts";
                xlWorkSheet.Cells[3, ++i] = "Pcs";
                xlWorkSheet.Cells[3, ++i] = "W (Ton)";
                xlWorkSheet.Cells[3, ++i] = "Pcs/Shift";
                xlWorkSheet.Cells[3, ++i] = "W/S (Ton)";
            }

            foreach (DataRow mach in Machs.Rows)
            {
               // machine = "";
                // שליפת כלל הדיווחים לתקופה
                StrSql = "select TMACH, sum(TPCS) as pcs,  ttdte||tshft as Shift, sum(TPCS)*sum(TWGHT) as Weight,tshft as Tshft,right(left(ttdte,4),2)||left(ttdte,2) " +
                         "from BPCSFALI.FltQV  " +
                         "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and  tdept in (71,160) and tpcs<>0 and tmach='" + mach[0].ToString().Trim() + "' " +       
                         "group by TMACH,ttdte||tshft,tpcs,tshft,right(left(ttdte,4),2)||left(ttdte,2) " +
                         "order by TMACH, ttdte||tshft ";
                DT = dbs.executeSelectQueryNoParam(StrSql);

                foreach (DataRow row in DT.Rows)
                {
                    if (machine != "" && machine != row[0].ToString().Trim())
                    {
                        k = ((int.Parse(date)) * 5) - 3;// ווידוא השמת נתונים בחודש מתאים
                        for (int q = 0; q < shft.Length / 2; q++)
                        {
                            if (row[5].ToString() == shft[q, 0])
                            {
                                xlWorkSheet.Cells[j, k] = shft[q, 1];
                                shifts = int.Parse(shft[q, 1]);
                                break;
                            }
                        }
                        //xlWorkSheet.Cells[j, k] = shifts;
                        k++;
                        //j++;
                        xlWorkSheet.Cells[j, k] = pcs;
                        k++;
                        xlWorkSheet.Cells[j, k] = Math.Round(((double)weight) / 1000, 2);
                        k++;
                        xlWorkSheet.Cells[j, k] = Math.Round(pcs / shifts, 2);
                        k++;
                        xlWorkSheet.Cells[j, k] = Math.Round(((((double)weight) / 1000) / shifts), 2);
                        j++;
                        //pcs = DBCW.Rs1.GetDecimal(1);
                        pcs = decimal.Parse(row[1].ToString());
                        //weight = DBCW.Rs1.GetDecimal(3);
                        weight = decimal.Parse(row[3].ToString());
                        //shifts = int.Parse(DBCW.Rs1.GetValue(4).ToString());
                        //date = DBCW.Rs1.GetString(2).Substring(2, 2);
                        date = row[2].ToString().Substring(2, 2);
                        k++;
                 
                        machine = "";
                    }
                    //בקריאה ראשונה של המכונה נזין את שם המכונה והחודש
                    else if (machine == "")
                    {
                        machine = row[0].ToString().Trim();
                        date = row[2].ToString().Substring(2, 2);

                        // שליפת מספר משמרות למכונה
                        StrSql = "select count(distinct ttdte||tshft) as Shift, right(left(ttdte,4),2)||left(ttdte,2) " +
                                 "from BPCSFALI.FltQV  " +
                                 "where TTDTE between " + Str_S_Date + " and " + Str_E_Date + " and  tdept in (71,160) and tpcs<>0 and tmach='" + machine + "' " +
                                 "group by TMACH, right(left(ttdte,4),2)||left(ttdte,2) " +
                                 "order by TMACH ";
                        //DBCW.Q_Run2(StrSql);
                        DT1 = dbs.executeSelectQueryNoParam(StrSql);
                        int y = 0;
                        foreach (DataRow rows in DT1.Rows)
                        {
                            shft[y, 0] = rows[1].ToString();
                            shft[y, 1] = rows[0].ToString();
                            y++;
                        }
                    }

                    if (row[2].ToString().Substring(2, 2) == date)
                    {
                        pcs += decimal.Parse(row[1].ToString());
                        weight += decimal.Parse(row[3].ToString());
                    }

                    else
                    {
                        machine = row[0].ToString().Trim();
                        xlWorkSheet.Cells[j, 1] = machine;
                        k = ((int.Parse(date)) * 5) - 3;// ווידוא השמת נתונים בחודש מתאים
                        //if (j == 4)
                        //{
                        //    xlWorkSheet.Cells[2, k] = monname[int.Parse(date) - 1];
                        //} 
                        //xlWorkSheet.Cells[j, k] = shifts;
                        for (int q = 0; q < shft.Length / 2; q++)
                        {
                            if (row[5].ToString() == shft[q, 0])
                            {
                                xlWorkSheet.Cells[j, k] = shft[q, 1];
                                shifts = int.Parse(shft[q, 1]);
                                break;
                            }
                        }
                        k++;
                        xlWorkSheet.Cells[j, k] = pcs;
                        k++;
                        xlWorkSheet.Cells[j, k] = Math.Round(((double)weight) / 1000, 2); // המרה מליברות לטון
                        k++;
                        xlWorkSheet.Cells[j, k] = Math.Round(pcs / shifts, 2);
                        k++;
                        xlWorkSheet.Cells[j, k] = Math.Round(((((double)weight) / 1000) / shifts), 2); // המרה מליברות לטון
                        k = 2;
                        //j++;
                        //pcs = DBCW.Rs1.GetDecimal(1);
                        pcs = decimal.Parse(row[1].ToString());
                        //shifts = DBCW.Rs1.GetInt32(4);
                        //weight = DBCW.Rs1.GetDecimal(3);
                        weight = decimal.Parse(row[3].ToString());
                        //pcs = 0;
                        //shifts = 0;
                        //weight = 0;
                        //machine = DBCW.Rs1.GetString(0);
                        machine = row[0].ToString().Trim();
                        //date = DBCW.Rs1.GetValue(2).ToString().Substring(2, 2);
                        date = row[2].ToString().Substring(2, 2);

                    }
                }
            }
            xlWorkSheet.Cells[j, 1] = machine;   // שם מכונה אחרונה  
            //k = ((int.Parse(date)) * 5) - 3;// ווידוא השמת נתונים בחודש מתאים   
            //xlWorkSheet.Cells[j, k] = shifts;
            //k++;
            //xlWorkSheet.Cells[j, k] = pcs;
            //k++;
            //xlWorkSheet.Cells[j, k] = Math.Round(((double)weight) / 1000, 2); // המרה מליברות לטון
            //k++;
            //xlWorkSheet.Cells[j, k] = Math.Round(pcs / shifts, 2);
            //k++;
            //xlWorkSheet.Cells[j, k] = Math.Round(((((double)weight) / 1000) / shifts), 2); // המרה מליברות לטון    
            DBCW.Rs1.Close();
            DBCW.Close_Conn();

            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Frm_EfficiencyReport_Load(object sender, EventArgs e)
        {

        }
    }
}
