﻿namespace Gipur_Production
{
    partial class Frm_HMICurringReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_HMICurringReport));
            this.Btn_OK = new System.Windows.Forms.Button();
            this.comboB_Stop = new System.Windows.Forms.ComboBox();
            this.Lbl_Stop = new System.Windows.Forms.Label();
            this.comboB_Press = new System.Windows.Forms.ComboBox();
            this.Lbl_Press = new System.Windows.Forms.Label();
            this.Btn_Reset = new System.Windows.Forms.Button();
            this.DTPicker_To = new System.Windows.Forms.DateTimePicker();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.DTPicker_From = new System.Windows.Forms.DateTimePicker();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radioB_Stop = new System.Windows.Forms.RadioButton();
            this.radioB_Curring = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(125, 190);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(100, 28);
            this.Btn_OK.TabIndex = 7;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // comboB_Stop
            // 
            this.comboB_Stop.DropDownHeight = 150;
            this.comboB_Stop.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_Stop.FormattingEnabled = true;
            this.comboB_Stop.IntegralHeight = false;
            this.comboB_Stop.Location = new System.Drawing.Point(164, 139);
            this.comboB_Stop.Name = "comboB_Stop";
            this.comboB_Stop.Size = new System.Drawing.Size(277, 26);
            this.comboB_Stop.TabIndex = 6;
            this.comboB_Stop.Visible = false;
            // 
            // Lbl_Stop
            // 
            this.Lbl_Stop.AutoSize = true;
            this.Lbl_Stop.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_Stop.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.Lbl_Stop.Location = new System.Drawing.Point(100, 140);
            this.Lbl_Stop.Name = "Lbl_Stop";
            this.Lbl_Stop.Size = new System.Drawing.Size(54, 24);
            this.Lbl_Stop.TabIndex = 70;
            this.Lbl_Stop.Text = "Stop";
            this.Lbl_Stop.Visible = false;
            // 
            // comboB_Press
            // 
            this.comboB_Press.DropDownHeight = 150;
            this.comboB_Press.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_Press.FormattingEnabled = true;
            this.comboB_Press.IntegralHeight = false;
            this.comboB_Press.Location = new System.Drawing.Point(164, 101);
            this.comboB_Press.Name = "comboB_Press";
            this.comboB_Press.Size = new System.Drawing.Size(277, 26);
            this.comboB_Press.TabIndex = 5;
            this.comboB_Press.Leave += new System.EventHandler(this.comboB_Press_Leave);
            // 
            // Lbl_Press
            // 
            this.Lbl_Press.AutoSize = true;
            this.Lbl_Press.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_Press.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.Lbl_Press.Location = new System.Drawing.Point(100, 103);
            this.Lbl_Press.Name = "Lbl_Press";
            this.Lbl_Press.Size = new System.Drawing.Size(64, 24);
            this.Lbl_Press.TabIndex = 69;
            this.Lbl_Press.Text = "Press";
            // 
            // Btn_Reset
            // 
            this.Btn_Reset.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_Reset.Location = new System.Drawing.Point(262, 190);
            this.Btn_Reset.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_Reset.Name = "Btn_Reset";
            this.Btn_Reset.Size = new System.Drawing.Size(100, 28);
            this.Btn_Reset.TabIndex = 8;
            this.Btn_Reset.Text = "Reset";
            this.Btn_Reset.UseVisualStyleBackColor = true;
            this.Btn_Reset.Click += new System.EventHandler(this.Btn_Reset_Click);
            // 
            // DTPicker_To
            // 
            this.DTPicker_To.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_To.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPicker_To.Location = new System.Drawing.Point(281, 63);
            this.DTPicker_To.Name = "DTPicker_To";
            this.DTPicker_To.Size = new System.Drawing.Size(160, 25);
            this.DTPicker_To.TabIndex = 2;
            this.DTPicker_To.Leave += new System.EventHandler(this.DTPicker_To_Leave);
            // 
            // Lbl_To
            // 
            this.Lbl_To.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_To.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(243, 63);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.Size = new System.Drawing.Size(73, 24);
            this.Lbl_To.TabIndex = 68;
            this.Lbl_To.Text = "To";
            // 
            // DTPicker_From
            // 
            this.DTPicker_From.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_From.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPicker_From.Location = new System.Drawing.Point(77, 63);
            this.DTPicker_From.Name = "DTPicker_From";
            this.DTPicker_From.Size = new System.Drawing.Size(160, 25);
            this.DTPicker_From.TabIndex = 1;
            // 
            // Lbl_From
            // 
            this.Lbl_From.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_From.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(15, 64);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.Size = new System.Drawing.Size(73, 24);
            this.Lbl_From.TabIndex = 67;
            this.Lbl_From.Text = "From";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(120, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(251, 29);
            this.label2.TabIndex = 66;
            this.label2.Text = "HMI Curring Report";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioB_Stop
            // 
            this.radioB_Stop.BackColor = System.Drawing.Color.PowderBlue;
            this.radioB_Stop.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioB_Stop.Location = new System.Drawing.Point(17, 140);
            this.radioB_Stop.Name = "radioB_Stop";
            this.radioB_Stop.Size = new System.Drawing.Size(80, 22);
            this.radioB_Stop.TabIndex = 4;
            this.radioB_Stop.TabStop = true;
            this.radioB_Stop.Text = "Stop";
            this.radioB_Stop.UseVisualStyleBackColor = false;
            this.radioB_Stop.Click += new System.EventHandler(this.radioB_Stop_Click);
            // 
            // radioB_Curring
            // 
            this.radioB_Curring.BackColor = System.Drawing.Color.PowderBlue;
            this.radioB_Curring.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioB_Curring.Location = new System.Drawing.Point(17, 103);
            this.radioB_Curring.Name = "radioB_Curring";
            this.radioB_Curring.Size = new System.Drawing.Size(80, 22);
            this.radioB_Curring.TabIndex = 3;
            this.radioB_Curring.TabStop = true;
            this.radioB_Curring.Text = "Curring";
            this.radioB_Curring.UseVisualStyleBackColor = false;
            this.radioB_Curring.Click += new System.EventHandler(this.radioB_Curring_Click);
            // 
            // Frm_HMICurringReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(469, 241);
            this.Controls.Add(this.radioB_Curring);
            this.Controls.Add(this.radioB_Stop);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.comboB_Stop);
            this.Controls.Add(this.Lbl_Stop);
            this.Controls.Add(this.comboB_Press);
            this.Controls.Add(this.Lbl_Press);
            this.Controls.Add(this.Btn_Reset);
            this.Controls.Add(this.DTPicker_To);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DTPicker_From);
            this.Controls.Add(this.Lbl_From);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Frm_HMICurringReport";
            this.Text = "HMI Curring Report";
            this.Load += new System.EventHandler(this.Frm_HMICurringReport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.ComboBox comboB_Stop;
        private System.Windows.Forms.Label Lbl_Stop;
        private System.Windows.Forms.ComboBox comboB_Press;
        private System.Windows.Forms.Label Lbl_Press;
        private System.Windows.Forms.Button Btn_Reset;
        private System.Windows.Forms.DateTimePicker DTPicker_To;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.DateTimePicker DTPicker_From;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioB_Stop;
        private System.Windows.Forms.RadioButton radioB_Curring;
    }
}