﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_HMICurringReport : Form
    {

        public Frm_HMICurringReport()
        {
            InitializeComponent();
            DTPicker_From.Format = DateTimePickerFormat.Custom;
            DTPicker_From.CustomFormat = "yyyy-MM-dd HH.mm";
            DTPicker_To.Format = DateTimePickerFormat.Custom;
            DTPicker_To.CustomFormat = "yyyy-MM-dd HH.mm";
        }

        //  Click

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }
            if (radioB_Curring.Checked)
                HMICurringReport();
            else if (radioB_Stop.Checked)
                HMIStopReport();

        }

        private void radioB_Curring_Click(object sender, EventArgs e)
        {
            Lbl_Stop.Visible = false;
            comboB_Stop.Visible = false;
        }

        private void radioB_Stop_Click(object sender, EventArgs e)
        {
            Lbl_Stop.Visible = true;
            comboB_Stop.Visible = true;
        }

        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            DTPicker_From.Value = DateTime.Now.Date;
            DTPicker_To.Value = DateTime.Now.Date;
            PrepareFilterDatasBetweenDates();
        }

        //  Leave 

        private void DTPicker_To_Leave(object sender, EventArgs e)
        {
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }
            PrepareFilterDatasBetweenDates();
        }

        private void comboB_Press_Leave(object sender, EventArgs e)
        {
            if (comboB_Press.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";

                comboB_Stop.Text = "";
                comboB_Stop.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);
                // שליפת רשימת תקלות לתקופה מבוקשת
                StrSql = "select distinct LTAK,IDESC " +
                         "from RZPALI.TAXILOGP left join BPCSFV30.IIML01 on IPROD='TAK-161'||LTAK " +
                         "where LACT='4' and LTIMESTAMP between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                               "LPRESS = '" + comboB_Press.Text.Trim() + "' ";
                StrSql += "order by LTAK";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_Stop.Items.Add(DBC.Rs1.GetString(0).Trim() + " - " + DBC.Rs1.GetString(1).Trim()); // Stop
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        // Load

        private void Frm_HMICurringReport_Load(object sender, EventArgs e)
        {
            radioB_Curring.Checked = true;
            PrepareFilterDatasBetweenDates();
        }

        // Routines

        private void HMICurringReport()
        {
            Cursor.Current = Cursors.WaitCursor;

            string Con_Str;
            string StrSql = "";
            int i = 2;

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBC;
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "HMI Curring Report";

            xlWorkSheet.Cells[1, 1] = "מכבש";
            xlWorkSheet.Cells[1, 2] = "שעת התחלה";
            xlWorkSheet.Cells[1, 3] = "שעת סיום";
            xlWorkSheet.Cells[1, 4] = "סה''כ זמן בדקות";
            xlWorkSheet.Cells[1, 5] = "מס' עובד";
            xlWorkSheet.Cells[1, 6] = "שם עובד";
            xlWorkSheet.Cells[1, 7] = "גודל צמיג";
            xlWorkSheet.Cells[1, 8] = "מק''ט תוצ''ג";
            xlWorkSheet.Cells[1, 9] = "סידורי תוצ''ג";
            xlWorkSheet.Cells[1, 10] = "מק''ט ירוק";
            xlWorkSheet.Cells[1, 11] = "סידורי ירוק";
            xlWorkSheet.Cells[1, 12] = "DNA";

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "L1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // Freeze rows            
            Excel.Window xlWnd3 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A1", "A1").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd3.FreezePanes = true;

            // AutoFilter            
            chartRange = xlWorkSheet.get_Range("A1", "L1");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // Wrap Text
            chartRange = xlWorkSheet.get_Range("A1", "L1");
            chartRange.Cells.WrapText = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "F1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "C1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 20;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 30;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("K1", "L1");
            chartRange.ColumnWidth = 8;

            // Column Format
            chartRange = xlWorkSheet.get_Range("B2", "C10000");
            chartRange.NumberFormat = "dd/MM/yyyy HH:mm";
            chartRange = xlWorkSheet.get_Range("H2", "H10000");
            chartRange.NumberFormat = "@";

            string LastPress = "";
            string LastStt = "0";
            string EmpNo = "";
            string EmpName = "";
            DateTime StartCurringDT = DateTime.Now;

            // שליפת נתוני דוח, לסיום דוח הוספתי 4 שעות על מנת לתפוס סיום גיפור במידה וגולש אחרי סיום דוח מבוקש
            // כמובן ההתייחסות לתחילת גיפור רק עד סיום דוח מבוקש
            StrSql = "select LPRESS,LTIMESTAMP,LEMP,value(PRATI,'*'),value(FAMILY,'*'),LACT,INSIZ " +
                     "from RZPALI.TAXILOGP left join " +
                          "ISUFKV.ISAVL10  on LEMP=substring(OVED,3,5) and MIFAL='01' left join " +
                          "BPCSFALI.IIMNL01 on substring(LPROD,1,8)=INPROD " +
                     "where LTIMESTAMP between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and " +
                                              "'" + DTPicker_To.Value.AddDays(1).ToString("yyyy-MM-dd-03.29.59.999999") + "' and LACT<>'4' ";
            if (radioB_Curring.Checked)
                if (comboB_Press.Text.Trim() != "")
                    StrSql += "and LPRESS='" + comboB_Press.Text.Trim() + "' ";
            StrSql += "order by LPRESS,LTIMESTAMP";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (i == 2) // אתחול מכבש בתחילת ריצה
                    LastPress = DBC.Rs1.GetString(0).Trim(); // מכבש


                if (DBC.Rs1.GetString(5).Trim() == "1" && DBC.Rs1.GetString(0).Trim() == LastPress && DBC.Rs1.GetDateTime(1) <= Convert.ToDateTime(DTPicker_To.Value.ToString("yyyy-MM-dd 23:29:59")))
                {   // בדיקה אם תחילת גיפור ואותו מכבש וזמן דיווח בתחום המבוקש - לא במורחב
                    LastPress = DBC.Rs1.GetString(0).Trim(); // מכבש
                    StartCurringDT = DBC.Rs1.GetDateTime(1); // שעת התחלה
                    LastStt = "1";
                    EmpNo = DBC.Rs1.GetString(2).Trim();
                    if (!DBC.Rs1.IsDBNull(3) && !DBC.Rs1.IsDBNull(4))
                        EmpName = DBC.Rs1.GetString(3).Trim() + " " + DBC.Rs1.GetString(4).Trim(); // שם עובד   
                }
                else if (DBC.Rs1.GetString(5).Trim() == "0" && DBC.Rs1.GetString(0).Trim() == LastPress && LastStt == "1")
                {   // בדיקה אם סיום גיפור ואותו מכבש
                    xlWorkSheet.Cells[i, 1] = DBC.Rs1.GetString(0).Trim(); // מכבש
                    xlWorkSheet.Cells[i, 2] = StartCurringDT; // שעת התחלה
                    xlWorkSheet.Cells[i, 3] = DBC.Rs1.GetDateTime(1).ToString(); // שעת סיום
                    xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DBC.Rs1.GetDateTime(1) - StartCurringDT).TotalMinutes); // סה''כ זמן
                    xlWorkSheet.Cells[i, 5] = EmpNo; // מס' עובד
                    if (!DBC.Rs1.IsDBNull(3) && !DBC.Rs1.IsDBNull(4))
                        xlWorkSheet.Cells[i, 6] = EmpName; // שם עובד
                    xlWorkSheet.Cells[i, 7] = DBC.Rs1.GetString(6).Trim(); // גודל צמיג
                    //xlWorkSheet.Cells[i, 8] = DBC.Rs1.GetString(7).Trim(); // מק''ט

                    // שליפת ממק"ט וסידורי של תוצ"ג                    
                    StrSql = "select LPROD,LLBLNO " +
                             "from TAPIALI.LABELGL10 " +
                             "where LMACH='" + DBC.Rs1.GetString(0).Trim() + "' and LACNDT=" + DBC.Rs1.GetDateTime(1).ToString("yyyyMMdd") + " and " +
                                   "LACNTM=" + DBC.Rs1.GetDateTime(1).ToString("HHmmss") + " and LACTAN =42";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        xlWorkSheet.Cells[i, 8] = DBC.Rs2.GetString(0).Trim(); // מק''ט תוצ''ג
                        xlWorkSheet.Cells[i, 9] = DBC.Rs2.GetValue(1); // סידור תוצ''ג

                    }
                    DBC.Rs2.Close();

                    // שליפת ממק"ט וסידורי של ירוק
                    StrSql = "select LPROD,LLBLNO, LLBLNA " +
                             "from TAPIALI.LABELGL10 " +
                             "where LMACH='" + DBC.Rs1.GetString(0).Trim() + "' and LACNDT=" + StartCurringDT.ToString("yyyyMMdd") + " and " +
                                   "LACNTM=" + StartCurringDT.ToString("HHmmss") + " and LACTAN =41";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        xlWorkSheet.Cells[i, 10] = DBC.Rs2.GetString(0).Trim(); // מק''ט תוצ''ג
                        xlWorkSheet.Cells[i, 11] = Math.Round(Convert.ToDecimal(DBC.Rs2.GetValue(1)), 2); // סידור תוצ''ג
                        xlWorkSheet.Cells[i, 12] = DBC.Rs2.GetValue(2); // DNA
                    }
                    DBC.Rs2.Close();

                    i++;
                    LastStt = "0";
                    EmpNo = "";
                    EmpName = "";
                }
                else if (DBC.Rs1.GetString(5).Trim() == "1" && DBC.Rs1.GetString(0).Trim() != LastPress && DBC.Rs1.GetDateTime(1) <= Convert.ToDateTime(DTPicker_To.Value.ToString("yyyy-MM-dd 23:29:59")))
                {   // בדיקה אם תחילת גיפור ומכבש התחלף וזמן דיווח בתחום המבוקש - לא במורחב
                    LastPress = DBC.Rs1.GetString(0).Trim(); // מכבש
                    StartCurringDT = DBC.Rs1.GetDateTime(1); // שעת התחלה
                    LastStt = "1";
                    EmpNo = DBC.Rs1.GetString(2).Trim();
                    if (!DBC.Rs1.IsDBNull(3) && !DBC.Rs1.IsDBNull(4))
                        EmpName = DBC.Rs1.GetString(3).Trim() + " " + DBC.Rs1.GetString(4).Trim(); // שם עובד   
                }
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();

            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void HMIStopReport()
        {
            // שינויים מתאריך 24.11.2016 ע"י רן הויכמן
            // עדכון אופן שליפת מק"ט נכון 

            Cursor.Current = Cursors.WaitCursor;



            string Con_Str;
            string StrSql = "";
            int i = 2;
            int[] ArrErrCode = new int[20];
            ArrErrCode[0] = 900266; ArrErrCode[1] = 900267; ArrErrCode[2] = 900332; ArrErrCode[3] = 900333; ArrErrCode[4] = 900334; ArrErrCode[5] = 900335;
            ArrErrCode[6] = 900460; ArrErrCode[7] = 900461; ArrErrCode[8] = 900462; ArrErrCode[9] = 900463; ArrErrCode[10] = 900464; ArrErrCode[11] = 900941; ArrErrCode[12] = 900945;
            ArrErrCode[13] = 900798; ArrErrCode[14] = 900799; ArrErrCode[15] = 901155; ArrErrCode[16] = 901310; ArrErrCode[17] = 901311; ArrErrCode[18] = 903192; ArrErrCode[19] = 904429;

            int PressCount = 0;
            string[,] ArrPress;
            // ArrMac[0, 0] --> מכונה            
            // ArrMac[1, 0] --> תאריך שעה

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBC;
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "HMI Stop Report";

            xlWorkSheet.Cells[1, 1] = "מכבש";
            xlWorkSheet.Cells[1, 2] = "שעת התחלה";
            xlWorkSheet.Cells[1, 3] = "שעת סיום";
            xlWorkSheet.Cells[1, 4] = "סה''כ זמן בדקות";
            xlWorkSheet.Cells[1, 5] = "מס' עובד";
            xlWorkSheet.Cells[1, 6] = "שם עובד";
            xlWorkSheet.Cells[1, 7] = "קוד תקלה";
            xlWorkSheet.Cells[1, 8] = "תיאור תקלה";
            xlWorkSheet.Cells[1, 9] = "מק'ט";
            xlWorkSheet.Cells[1, 10] = "תקן ליום";
            xlWorkSheet.Cells[1, 11] = "משקל";
            xlWorkSheet.Cells[1, 12] = "מס צמיגים שלא גופרו";
            xlWorkSheet.Cells[1, 13] = "משקל שלא גופר (טון)";
            xlWorkSheet.Cells[1, 14] = "משמרת";
            xlWorkSheet.Cells[1, 15] = "גודל";

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "P1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // Freeze rows            
            Excel.Window xlWnd3 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A1", "A1").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd3.FreezePanes = true;

            // AutoFilter            
            chartRange = xlWorkSheet.get_Range("A1", "P1");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // Wrap Text
            chartRange = xlWorkSheet.get_Range("A1", "P1");
            chartRange.Cells.WrapText = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "F1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "C1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 20;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 30;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("K1", "K1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 20;

            // Column Format
            chartRange = xlWorkSheet.get_Range("B2", "C10000");
            chartRange.NumberFormat = "dd/MM/yyyy HH:mm";

            string LastPress = "";
            string StopStt = "0";
            string LastTak = "";
            string TakCode = "";
            string TakDesc = "";
            string EmpNo = "";
            string EmpName = "";
            string newtime = "0";

            bool FlgBeforDt = false;  // דגל תקלה התחילה לפני תחילת דוח
            DateTime StartStopDT = DateTime.Now;

            // שליפת סה"כ מכבשים
            StrSql = "select count(distinct lpress) " +
                     "from RZPALI.TAXILOGP " +
                     "where LTIMESTAMP <= '" + DTPicker_From.Value.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "' ";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                PressCount = DBC.Rs1.GetInt32(0);
            }
            DBC.Rs1.Close();

            ArrPress = new string[2, PressCount];

            // שליפת דיווח אחרון למכונה לפני תאריך תחילת הדוח
            // שלפית סה"כ מכבשים
            StrSql = "select LPRESS,max(LTIMESTAMP),WDEPT " +
                     "from RZPALI.TAXILOGP " +
                     "Left join bpcsfv30.lmhl01 on LPRESS=MHMACH " +
                     "left join bpcsfv30.lwkl01 on mhwrkc=wwrkc " +
                     "where LTIMESTAMP <= '" + DTPicker_From.Value.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "' and MHMACH is not null and WDEPT in (161,162) ";
            if (comboB_Press.Text.Trim() != "")
                StrSql += "and LPRESS='" + comboB_Press.Text.Trim() + "' ";
            StrSql += "group by LPRESS,WDEPT ";
            StrSql += "order by LPRESS,WDEPT ";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                // שליפת דיווחים למכונה החל מתאריך דיווח אחרון למכונה עד סיום תקופת הדוח  
                StrSql = "select LPRESS,LTIMESTAMP,LEMP,value(PRATI,'*'),value(FAMILY,'*'),LACT,LTAK,IDESC " +
                     "from RZPALI.TAXILOGP left join " +
                          "ISUFKV.ISAVL10  on LEMP=substring(OVED,3,5) and MIFAL='01' left join " +
                          "BPCSFV30.IIML01 on IPROD='TAK-161'||LTAK " +
                     "where LTIMESTAMP between '" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "' and " +
                                              "'" + DTPicker_To.Value.ToString("yyyy-MM-dd-HH.mm.ss.999999") + "' and LPRESS='" + DBC.Rs1.GetString(0).Trim() + "'";
                if (radioB_Stop.Checked)
                    if (comboB_Stop.Text.Trim() != "")
                        StrSql += "and LTAK='" + comboB_Stop.Text.Trim().Substring(0, 2) + "' ";
                StrSql += "order by LTIMESTAMP";
                DBC.Q_Run2(StrSql);
                while (DBC.Rs2.Read())
                {
                    if (LastPress == "") // אתחול מכבש בתחילת ריצה
                        LastPress = DBC.Rs2.GetString(0).Trim(); // מכבש                    

                    if (DBC.Rs2.GetString(5).Trim() == "4" && DBC.Rs2.GetString(0).Trim() == LastPress && StopStt == "0")
                    {   // בדיקה אם דיווח תקלה + אותו מכבש + ולא היה דיווח תקלה                        
                        // בדיקה אם דיווח תקלה אחרי תחילת דוח                        
                        if (DBC.Rs2.GetDateTime(1) >= DTPicker_From.Value)
                        {
                            StartStopDT = DBC.Rs2.GetDateTime(1); // שעת התחלה
                            FlgBeforDt = false;
                        }
                        else // אם דיווח תקלה לפני תחילת דוח אז זמן תחילת תקלה הוא מתחילת הדוח
                        {
                            StartStopDT = DTPicker_From.Value;
                            FlgBeforDt = true;
                        }
                        LastTak = DBC.Rs2.GetString(6).Trim();
                        TakCode = DBC.Rs2.GetString(6).Trim();
                        if (!DBC.Rs2.IsDBNull(7))
                        {
                            TakDesc = DBC.Rs2.GetString(7).Trim();
                        }
                        EmpNo = DBC.Rs2.GetString(2).Trim();
                        if (!DBC.Rs2.IsDBNull(3) && !DBC.Rs2.IsDBNull(4))
                            EmpName = DBC.Rs2.GetString(3).Trim() + " " + DBC.Rs2.GetString(4).Trim();
                        StopStt = "1";
                    }
                    else
                    {
                        if (DBC.Rs2.GetString(5).Trim() == "4" && DBC.Rs2.GetString(0).Trim() == LastPress && StopStt == "1" && StartStopDT < DBC.Rs2.GetDateTime(1))
                        {   // מקרה של תקלה סוגרת תקלה
                            // בדיקה אם דיווח תקלה + אותו מכבש + תאריך דיווח גדול מתאריך תחלת תקלה
                            xlWorkSheet.Cells[i, 1] = DBC.Rs2.GetString(0).Trim(); // מכבש
                            xlWorkSheet.Cells[i, 2] = StartStopDT; // שעת התחלה                            
                            xlWorkSheet.Cells[i, 3] = DBC.Rs2.GetDateTime(1).ToString(); // שעת סיום
                            xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes); // סה''כ זמן
                            xlWorkSheet.Cells[i, 5] = EmpNo; // מס' עובד
                            xlWorkSheet.Cells[i, 6] = EmpName; // שם עובד                                   
                            xlWorkSheet.Cells[i, 7] = TakCode; // קוד תקלה
                            xlWorkSheet.Cells[i, 8] = TakDesc; // תיאור תקלה   

                            //// תוספות מתאריך 09/11/2016 בוצע ע"י רן הויכמן
                            StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                     "from rzpali.taxilogp " +
                                     "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                     "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                     "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                     "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                     "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                     "order by ltimestamp ";
                            DBC.Q_Run3(StrSql);
                            string ProdBefore = "";
                            string ProdAfter = "";
                            string Selected = "";
                            while (DBC.Rs3.Read())
                            {
                                if (ProdBefore == "")
                                {
                                    ProdBefore = DBC.Rs3.GetString(1).ToString().Trim();
                                }
                                else
                                {
                                    ProdAfter = DBC.Rs3.GetString(1).ToString().Trim();
                                }
                            }
                            //בחירת הפריט שישובץ
                            if (ProdAfter == "")
                            {
                                // שינויים מתאריך 28.11.2016
                                // ובמקום בדיקה מול השיבוץ mcovip ביטול בדיקה מול  
                                //StrSql = "select  omach, OPRIT,oadif " +
                                //          "from rzpali.mcovil13 " +
                                //          "where ODEPW in (161,162) and ODATE =" + StartStopDT.ToString("1yyMMdd") + "  and omade<oplan and OSHIFT ='1' and OMACH ='" + DBC.Rs1.GetString(0).Trim() + "'  and oadif=1 " +
                                //          "order by omach, oshift, oadif ";
                                StrSql = "select izprod,izqreq,izqprd,IZREQ " +
                                    "from bpcsfali.shmal01 " +
                                    "where IZDEPT='" + DBC.Rs1.GetValue(2).ToString().Trim() + "' and  IZMACH='" + DBC.Rs1.GetValue(0).ToString().Trim() + "' ";
                                DBC.Q_Run3(StrSql);
                                if (DBC.Rs3.Read())
                                {
                                    ProdAfter = DBC.Rs3.GetValue(0).ToString();
                                }
                            }
                            if (ProdBefore == ProdAfter || ProdAfter== "")
                            {
                                xlWorkSheet.Cells[i, 9] = ProdBefore;
                                Selected = ProdBefore;
                            }
                            else
                            {
                                xlWorkSheet.Cells[i, 9] = ProdAfter;
                                Selected = ProdAfter;
                            }

                            // StrSql = "select  RMPROD,rmtkn,ICSCP1,insiz " +
                            //"from  bpcsfali.frtml01 " +
                            //"left join bpcsfv30.cicl01 on ICPROD=RMPROD " +
                            // "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                            // "where RMPROD='" + Selected.Trim() + "' and RMMACI='" + DBC.Rs1.GetString(0).Trim() + "' "; 
                            // DBC.Q_Run3(StrSql);
                            StrSql = "select  RMPROD,rmtkn,ICSCP1, insiz " +
                                     "from  bpcsfali.frtml01 " +
                                     "left join bpcsfv30.cicl01 on ICPROD=RMPROD " +
                                     "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                                     "where RMPROD='" + Selected.Trim() + "' and RMMACI='" + DBC.Rs1.GetString(0).Trim() + "' ";
                            DBC.Q_Run3(StrSql);
                            if (!DBC.Rs3.HasRows)
                            {
                                // אם אין תקן לצמיג, נריץ פונקציה להוספת התקן ונשלוף שוב 
                                DBService dbs = new DBService();
                                DataTable DT = new DataTable();
                                string str = "select MHWRKC " +
                                         "from bpcsfv30.lmhl01 " +
                                         "where mhmach='" + DBC.Rs1.GetString(0).Trim() + "' ";
                                DT = dbs.executeSelectQueryNoParam(str);
                                string rt = "Call TAPIALI.TKNC4('" + Selected.PadRight(15,' ') + "','F1','" + DT.Rows[0][0] + "','" + DBC.Rs1.GetString(0).Trim().PadRight(10,' ') + "' ) ";
                                DBC.Run(rt);
                                DBC.Q_Run3(StrSql);
                            }
                            while (DBC.Rs3.Read())
                            {
                                try
                                {
                                    if (DBC.Rs3.GetValue(0).ToString().Trim() == Selected.Trim())
                                    {
                                        xlWorkSheet.Cells[i, 10] = DBC.Rs3.GetDecimal(1).ToString().Trim();
                                        xlWorkSheet.Cells[i, 11] = Math.Round(DBC.Rs3.GetDecimal(2), 2);
                                        xlWorkSheet.Cells[i, 12] = Math.Round(DBC.Rs3.GetDecimal(1) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440, 2); // זמן תקלה
                                        xlWorkSheet.Cells[i, 13] = Math.Round((DBC.Rs3.GetDecimal(2) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440) / 1000 * DBC.Rs3.GetDecimal(1), 2); // משקל שאבד        
                                        if (!DBC.Rs3.IsDBNull(3))
                                        {
                                            xlWorkSheet.Cells[i, 15] = DBC.Rs3.GetString(3).Trim();
                                        }
                                        newtime = (StartStopDT.Hour * 100 + StartStopDT.Minute).ToString();
                                    }
                                }
                                catch (Exception)
                                {
                                    throw;
                                }
                               
                            }

                            StrSql = "select sshft from BPCSFALI.shf where SDAY=0 and " + newtime + " between sfhrs and sthrs ";
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read() && !DBC.Rs3.IsDBNull(0))
                            {
                                xlWorkSheet.Cells[i, 14] = DBC.Rs3.GetValue(0).ToString().Trim();
                            }
                            else
                            {
                                xlWorkSheet.Cells[i, 14] = "1";
                            }


                            ///////////////////////////////////////////////////////////////////////
                            LastTak = DBC.Rs2.GetString(6).Trim();
                            TakCode = DBC.Rs2.GetString(6).Trim();
                            if (!DBC.Rs2.IsDBNull(7))
                            {
                                TakDesc = DBC.Rs2.GetString(7).Trim();
                            }

                            EmpNo = DBC.Rs2.GetString(2).Trim();
                            if (!DBC.Rs2.IsDBNull(3) && !DBC.Rs2.IsDBNull(4))
                                EmpName = DBC.Rs2.GetString(3).Trim() + " " + DBC.Rs2.GetString(4).Trim();
                            StopStt = "1";
                            if (FlgBeforDt)
                            {   // Cell Back Color
                                chartRange = xlWorkSheet.get_Range("B" + i.ToString(), "B" + i.ToString());
                                chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                            }
                            StartStopDT = DBC.Rs2.GetDateTime(1); // שעת התחלה
                            FlgBeforDt = false;
                            i++;
                        }
                        else if (DBC.Rs2.GetString(5).Trim() == "1" && DBC.Rs2.GetString(0).Trim() == LastPress && StopStt == "1")
                        {
                            // מקרה של תחילת גיפור
                            // בדיקה אם דיווח תקלה + אותו מכבש + תאריל דיווח גדול מתאריך תחלת תקלה
                            xlWorkSheet.Cells[i, 1] = DBC.Rs2.GetString(0).Trim(); // מכבש
                            xlWorkSheet.Cells[i, 2] = StartStopDT; // שעת התחלה
                            xlWorkSheet.Cells[i, 3] = DBC.Rs2.GetDateTime(1).ToString(); // שעת סיום
                            xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes); // סה''כ זמן
                            xlWorkSheet.Cells[i, 5] = EmpNo; // מס' עובד
                            xlWorkSheet.Cells[i, 6] = EmpName; // שם עובד                                   
                            xlWorkSheet.Cells[i, 7] = TakCode; // קוד תקלה
                            xlWorkSheet.Cells[i, 8] = TakDesc; // תיאור תקלה  
                            //// תוספות מתאריך 09/11/2016 בוצע ע"י רן הויכמן
                            //StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                            //         "from rzpali.taxilogp " +
                            //         "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                            //         "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                            //         "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                            //         "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                            //         "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                            //         "order by ltimestamp ";
                            StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                    "from rzpali.taxilogp " +
                                    "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                    "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                    "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                    "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                    "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                    "order by ltimestamp ";
                            DBC.Q_Run3(StrSql);
                            string ProdBefore = "";
                            string ProdAfter = "";
                            string Selected = "";
                            while (DBC.Rs3.Read())
                            {
                                if (ProdBefore == "")
                                {
                                    ProdBefore = DBC.Rs3.GetString(1).ToString().Trim();
                                }
                                else
                                {
                                    ProdAfter = DBC.Rs3.GetString(1).ToString().Trim();
                                }
                            }
                            // בחירת הפריט שישובץ
                            //if (ProdAfter == "")
                            //{
                            //    StrSql = "select  omach, OPRIT,oadif " +
                            //              "from rzpali.mcovil13 " +
                            //              "where ODEPW in (161,162) and ODATE =1161108  and omade<oplan and OSHIFT ='1' and OMACH ='301'  and oadif=1 " +
                            //              "order by omach, oshift, oadif ";
                            //    DBC.Q_Run3(StrSql);
                            //    if (DBC.Rs3.Read())
                            //    {
                            //        ProdAfter = DBC.Rs3.GetValue(1).ToString();
                            //    }
                            //}
                            if (ProdBefore == ProdAfter || ProdAfter== "")
                            {
                               
                                xlWorkSheet.Cells[i, 9] = ProdBefore;
                                Selected = ProdBefore;
                            }
                            else
                            {
                                xlWorkSheet.Cells[i, 9] = ProdAfter;
                                Selected = ProdAfter;
                            }

                            StrSql = "select  RMPROD,rmtkn,ICSCP1, insiz " +
                                     "from  bpcsfali.frtml01 " +
                                     "left join bpcsfv30.cicl01 on ICPROD=RMPROD " +
                                     "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                                     "where RMPROD='" + Selected.Trim() + "' and RMMACI='" + DBC.Rs1.GetString(0).Trim() + "' ";
                            DBC.Q_Run3(StrSql);
                            if (!DBC.Rs3.HasRows)
                            {
                                // אם אין תקן לצמיג, נריץ פונקציה להוספת התקן ונשלוף שוב 
                                DBService dbs = new DBService();
                                DataTable DT = new DataTable();
                                string str = "select MHWRKC " +
                                         "from bpcsfv30.lmhl01 " +
                                         "where mhmach='" + DBC.Rs1.GetString(0).Trim() + "' ";
                                DT = dbs.executeSelectQueryNoParam(str);
                                string rt = "Call TAPIALI.TKNC4('" + Selected.PadRight(15, ' ') + "','F1','" + DT.Rows[0][0] + "','" + DBC.Rs1.GetString(0).Trim().PadRight(10, ' ') + "' ) ";
                                DBC.Run(rt);
                                DBC.Q_Run3(StrSql);
                            }
                            while (DBC.Rs3.Read())
                            {
                                try
                                {
                                    if (DBC.Rs3.GetValue(0).ToString().Trim() == Selected.Trim())
                                    {
                                        xlWorkSheet.Cells[i, 10] = DBC.Rs3.GetDecimal(1).ToString().Trim();
                                        xlWorkSheet.Cells[i, 11] = Math.Round(DBC.Rs3.GetDecimal(2), 2);
                                        xlWorkSheet.Cells[i, 12] = Math.Round(DBC.Rs3.GetDecimal(1) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440, 2); // זמן תקלה
                                        xlWorkSheet.Cells[i, 13] = Math.Round((DBC.Rs3.GetDecimal(2) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440) / 1000 * DBC.Rs3.GetDecimal(1), 2); // משקל שאבד        
                                        xlWorkSheet.Cells[i, 15] = DBC.Rs3.GetString(3).Trim();
                                        newtime = (StartStopDT.Hour * 100 + StartStopDT.Minute).ToString();
                                    }
                                }
                                catch (Exception)
                                {
                                    
                                    throw;
                                }
                               

                            }

                            StrSql = "select sshft from BPCSFALI.shf where SDAY=0 and " + newtime + " between sfhrs and sthrs ";
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read() && !DBC.Rs3.IsDBNull(0))
                            {
                                xlWorkSheet.Cells[i, 14] = DBC.Rs3.GetValue(0).ToString().Trim();
                            }
                            else
                            {
                                xlWorkSheet.Cells[i, 14] = "1";
                            }
                            ///////////////////////////////////////////////////////////////////////
                            StopStt = "0";
                            LastTak = "";
                            TakCode = "";
                            TakDesc = "";
                            EmpNo = "";
                            EmpName = "";
                            if (FlgBeforDt)
                            {   // Cell Back Color
                                chartRange = xlWorkSheet.get_Range("B" + i.ToString(), "B" + i.ToString());
                                chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                            }
                            i++;
                        }
                        else if (DBC.Rs2.GetString(5).Trim() == "0" && DBC.Rs2.GetString(0).Trim() == LastPress && StopStt == "1")
                        {   // מקרה של תחילת גיפור
                            // בדיקה אם דיווח תקלה + אותו מכבש + תאריל דיווח גדול מתאריך תחלת תקלה
                            xlWorkSheet.Cells[i, 1] = DBC.Rs2.GetString(0).Trim(); // מכבש
                            xlWorkSheet.Cells[i, 2] = StartStopDT; // שעת התחלה
                            xlWorkSheet.Cells[i, 3] = DBC.Rs2.GetDateTime(1).ToString(); // שעת סיום
                            xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes); // סה''כ זמן
                            xlWorkSheet.Cells[i, 5] = EmpNo; // מס' עובד
                            xlWorkSheet.Cells[i, 6] = EmpName; // שם עובד                                   
                            xlWorkSheet.Cells[i, 7] = TakCode; // קוד תקלה
                            xlWorkSheet.Cells[i, 8] = TakDesc; // תיאור תקלה 

                            //// תוספות מתאריך 09/11/2016 בוצע ע"י רן הויכמן
                            //StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                            //         "from rzpali.taxilogp " +
                            //         "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                            //         "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                            //         "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                            //         "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                            //         "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                            //         "order by ltimestamp ";

                            StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                    "from rzpali.taxilogp " +
                                    "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                    "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                    "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                    "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                    "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                    "order by ltimestamp ";
                            DBC.Q_Run3(StrSql);
                            string ProdBefore = "";
                            string ProdAfter = "";
                            string Selected = "";
                            while (DBC.Rs3.Read())
                            {
                                if (ProdBefore == "")
                                {
                                    ProdBefore = DBC.Rs3.GetString(1).ToString().Trim();
                                }
                                else
                                {
                                    ProdAfter = DBC.Rs3.GetString(1).ToString().Trim();
                                }
                            }
                            // בחירת הפריט שישובץ
                            if (ProdBefore == ProdAfter || ProdAfter== "")
                            {
                                xlWorkSheet.Cells[i, 9] = ProdBefore;
                                Selected = ProdBefore;
                            }
                            else
                            {
                                
                                xlWorkSheet.Cells[i, 9] = ProdAfter;
                                Selected = ProdAfter;
                            }

                            //StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                            //         "from rzpali.taxilogp " +
                            //         "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                            //         "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                            //         "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                            //         "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                            //         "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                            //         "order by ltimestamp ";
                            StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1,insiz " +
                                    "from rzpali.taxilogp " +
                                    "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                    "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                    "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                                    "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                    "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                    "group by ltimestamp,lprod,rmtkn,ICSCP1,insiz " +
                                    "order by ltimestamp ";
                            DBC.Q_Run3(StrSql);
                            if (!DBC.Rs3.HasRows)
                            {
                                // אם אין תקן לצמיג, נריץ פונקציה להוספת התקן ונשלוף שוב 
                                DBService dbs = new DBService();
                                DataTable DT = new DataTable();
                                string str = "select MHWRKC " +
                                         "from bpcsfv30.lmhl01 " +
                                         "where mhmach='" + DBC.Rs1.GetString(0).Trim() + "' ";
                                DT = dbs.executeSelectQueryNoParam(str);
                                string rt = "Call TAPIALI.TKNC4('" + Selected.PadRight(15, ' ') + "','F1','" + DT.Rows[0][0] + "','" + DBC.Rs1.GetString(0).Trim().PadRight(10, ' ') + "' ) ";
                                DBC.Run(rt);
                                DBC.Q_Run3(StrSql);
                            }
                            while (DBC.Rs3.Read())
                            {
                                if (DBC.Rs3.GetValue(1).ToString().Trim() == Selected)
                                {
                                    try
                                    {
                                        if (DBC.Rs3.GetValue(2).ToString().Trim() != "")
                                        {
                                            xlWorkSheet.Cells[i, 10] = DBC.Rs3.GetDecimal(2).ToString().Trim();
                                            xlWorkSheet.Cells[i, 12] = Math.Round(DBC.Rs3.GetDecimal(2) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440, 2); // זמן תקלה
                                            xlWorkSheet.Cells[i, 13] = Math.Round((DBC.Rs3.GetDecimal(3) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440) / 1000 * DBC.Rs3.GetDecimal(2), 2); // משקל שאבד            
                                            xlWorkSheet.Cells[i, 15] = DBC.Rs3.GetString(4).Trim();
                                            newtime = (StartStopDT.Hour * 100 + StartStopDT.Minute).ToString();
                                        }

                                        xlWorkSheet.Cells[i, 11] = Math.Round(DBC.Rs3.GetDecimal(3), 2);
                                    }
                                    catch (Exception)
                                    {
                                        
                                        throw;
                                    }
                                   

                                }
                            }

                            StrSql = "select sshft from BPCSFALI.shf where SDAY=0 and " + newtime + " between sfhrs and sthrs ";
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read() && !DBC.Rs3.IsDBNull(0))
                            {
                                xlWorkSheet.Cells[i, 14] = DBC.Rs3.GetValue(0).ToString().Trim();
                            }
                            else
                            {
                                xlWorkSheet.Cells[i, 14] = "1";
                            }

                            i++;
                            ///////////////////////////////////////////////////////////////////////
                            StopStt = "0";
                            LastTak = "";
                            TakCode = "";
                            TakDesc = "";
                            EmpNo = "";
                            EmpName = "";
                            if (FlgBeforDt)
                            {   // Cell Back Color
                                chartRange = xlWorkSheet.get_Range("B" + i.ToString(), "B" + i.ToString());
                                chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                            }
                        }
                    }
                }
                // בדיקת מצב שהיה דיווח תקלה, והתקלה לא הסתיימת בתקופת הדוח
                if (StopStt == "1")
                {
                    xlWorkSheet.Cells[i, 1] = LastPress; // מכבש
                    xlWorkSheet.Cells[i, 2] = StartStopDT; // שעת התחלה
                    // בדיקה אם תאריך סיום דוח הוא יום נוכחי, במידה וכן אז שעת סיום תקלה נכון לעכשיו
                    if (DTPicker_To.Value.Date == DateTime.Now.Date)
                    {
                        //xlWorkSheet.Cells[i, 3] = DateTime.Now; // שעת סיום נכון לעכשיו
                        //xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DateTime.Now - StartStopDT).TotalMinutes); // סה''כ זמן
                        xlWorkSheet.Cells[i, 3] = DTPicker_To.Value.ToString("dd/MM/yyyy HH:mm");
                        xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DTPicker_To.Value - StartStopDT).TotalMinutes);// סה''כ זמן
                        // Cell Back Color
                        chartRange = xlWorkSheet.get_Range("C" + i.ToString(), "C" + i.ToString());
                        chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.OrangeRed);
                    }
                    else
                    {
                        xlWorkSheet.Cells[i, 3] = Convert.ToDateTime(DTPicker_To.Value.ToString("dd/MM/yyyy HH:mm")); // שעת סיום
                        xlWorkSheet.Cells[i, 4] = Convert.ToInt64((Convert.ToDateTime(DTPicker_To.Value.ToString("dd/MM/yyyy HH:mm")) - StartStopDT).TotalMinutes); // סה''כ זמן
                        // Cell Back Color
                        chartRange = xlWorkSheet.get_Range("C" + i.ToString(), "C" + i.ToString());
                        chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                    }
                    xlWorkSheet.Cells[i, 5] = EmpNo; // מס' עובד
                    xlWorkSheet.Cells[i, 6] = EmpName; // שם עובד                                   
                    xlWorkSheet.Cells[i, 7] = TakCode; // קוד תקלה
                    xlWorkSheet.Cells[i, 8] = TakDesc; // תיאור תקלה

                    //// תוספות מתאריך 09/11/2016 בוצע ע"י רן הויכמן
                    //StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                    //                 "from rzpali.taxilogp " +
                    //                 "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                    //                 "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                    //                 "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                    //                 "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                    //                 "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                    //                 "order by ltimestamp ";

                    StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                    "from rzpali.taxilogp " +
                                    "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                    "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                    "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                    "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                    "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                    "order by ltimestamp ";
                    DBC.Q_Run3(StrSql);
                    string ProdBefore = "";
                    string ProdAfter = "";
                    string Selected = "";
                    while (DBC.Rs3.Read())
                    {
                        if (ProdBefore == "")
                        {
                            ProdBefore = DBC.Rs3.GetString(1).ToString().Trim();
                        }
                        else
                        {
                            ProdAfter = DBC.Rs3.GetString(1).ToString().Trim();
                        }
                    }
                    // בחירת הפריט שישובץ
                    if (ProdAfter == "")
                    {
                        StrSql = "select izprod,izqreq,izqprd,IZREQ " +
                                    "from bpcsfali.shmal01 " +
                                    "where IZDEPT='" + DBC.Rs1.GetValue(2).ToString().Trim() + "' and  IZMACH='" + DBC.Rs1.GetValue(0).ToString().Trim() + "' ";
                        DBC.Q_Run3(StrSql);
                        while (DBC.Rs3.Read())
                        {
                            if (DBC.Rs3.GetValue(0).ToString().Trim() != "STOP")
                            {
                                if (DBC.Rs3.GetDecimal(1) > DBC.Rs3.GetDecimal(2))
                                {
                                    ProdAfter = DBC.Rs3.GetValue(0).ToString().Trim();
                                    break;
                                }
                            }
                            else
                            {
                                for (int l = 0; l < 20; l++)
                                {
                                    if (ArrErrCode[l] == DBC.Rs3.GetDecimal(3))
                                    {
                                        ProdAfter = DBC.Rs3.GetValue(0).ToString().Trim();
                                        break;
                                    }
                                }
                            }

                        }
                    }
                    if (ProdBefore == ProdAfter || ProdAfter== "")
                    {
                        xlWorkSheet.Cells[i, 9] = ProdBefore;
                        Selected = ProdBefore;
                    }
                    else
                    {
                        xlWorkSheet.Cells[i, 9] = ProdAfter;
                        Selected = ProdAfter;
                    }
                    //StrSql = "select  RMPROD,rmtkn,ICSCP1,insiz " +
                    //         "from  bpcsfali.frtml01 " +
                    //         "left join bpcsfv30.cicl01 on ICPROD=RMPROD " +
                    //         "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                    //         "where RMPROD='" + Selected.Trim() + "' and RMMACI='" + DBC.Rs1.GetString(0).Trim() + "' ";
                    if (Selected.Length < 10)
                    {
                        Selected += "      ";
                    }
                    StrSql = "select  RMPROD,rmtkn,ICSCP1, insiz " +
                                    "from  bpcsfali.frtml01 " +
                                    "left join bpcsfv30.cicl01 on ICPROD=RMPROD " +
                                    "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                                    "where RMPROD='" + Selected.Trim() + "' and RMMACI='" + DBC.Rs1.GetString(0).Trim() + "' ";
                    DBC.Q_Run3(StrSql);
                    if (!DBC.Rs3.HasRows)
                    {
                        // אם אין תקן לצמיג, נריץ פונקציה להוספת התקן ונשלוף שוב 
                        DBService dbs = new DBService();
                        DataTable DT = new DataTable();
                        string str = "select MHWRKC " +
                                 "from bpcsfv30.lmhl01 " +
                                 "where mhmach='" + DBC.Rs1.GetString(0).Trim() + "' ";
                        DT = dbs.executeSelectQueryNoParam(str);
                        string rt = "Call TAPIALI.TKNC4('" + Selected.PadRight(15, ' ') + "','F1','" + DT.Rows[0][0] + "','" + DBC.Rs1.GetString(0).Trim().PadRight(10, ' ') + "' ) ";
                        DBC.Run(rt);
                        DBC.Q_Run3(StrSql);
                    }
                    while (DBC.Rs3.Read())
                    {
                        try
                        {
                            if (DBC.Rs3.GetValue(0).ToString().Trim() == Selected.Trim())
                            {
                                xlWorkSheet.Cells[i, 10] = DBC.Rs3.GetDecimal(1).ToString().Trim();
                                xlWorkSheet.Cells[i, 11] = Math.Round(DBC.Rs3.GetDecimal(2), 2);
                                xlWorkSheet.Cells[i, 12] = Math.Round(DBC.Rs3.GetDecimal(1) * (Convert.ToInt64((DTPicker_To.Value - StartStopDT).TotalMinutes)) / 1440, 2); // זמן תקלה
                                xlWorkSheet.Cells[i, 13] = Math.Round((DBC.Rs3.GetDecimal(2) * (Convert.ToInt64((DTPicker_To.Value - StartStopDT).TotalMinutes)) / 1440) / 1000 * DBC.Rs3.GetDecimal(1), 2); // משקל שאבד               
                                xlWorkSheet.Cells[i, 15] = DBC.Rs3.GetString(3).Trim();
                                newtime = (StartStopDT.Hour * 100 + StartStopDT.Minute).ToString();
                            }
                        }
                        catch (Exception)
                        {
                            
                        }
                      
                    }

                    StrSql = "select sshft from BPCSFALI.shf where SDAY=0 and " + newtime + " between sfhrs and sthrs ";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read() && !DBC.Rs3.IsDBNull(0))
                    {
                        xlWorkSheet.Cells[i, 14] = DBC.Rs3.GetValue(0).ToString().Trim();
                    }
                    else
                    {
                        xlWorkSheet.Cells[i, 14] = "1";
                    }
                    ///////////////////////////////////////////////////////////////////////

                    if (FlgBeforDt)
                    {   // Cell Back Color
                        chartRange = xlWorkSheet.get_Range("B" + i.ToString(), "B" + i.ToString());
                        chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                    }
                    i++;
                }
                LastPress = "";
                StopStt = "0";
                LastTak = "";
                TakCode = "";
                TakDesc = "";
                EmpNo = "";
                EmpName = "";


            }
            DBC.Rs1.Close();
            DBC.Rs2.Close();
            DBC.Rs3.Close();
            DBC.Close_Conn();
            ////////////////////
            CopyForOleg(xlWorkSheet);





            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        public void CopyForOleg(Excel.Worksheet worksheet)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;

            xlApp = new Excel.Application();
            //xlWorkBook = xlApp.Workbooks.Open(@"T:\AA_TAPI\Oleg\Taxi\Taxi Check.xlsm", 0, false, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkBook = xlApp.Workbooks.Open(Filename: @"T:\AA_TAPI\Oleg\Taxi\Taxi Check.xlsm", ReadOnly: false);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item("HMI Stop Report");


            Excel.Range from = worksheet.UsedRange;
            Excel.Range to = xlWorkSheet.Range["A1:O800"];
            to.Clear();
            //from.Copy(to);
            xlWorkSheet.Range["B2:C800"].EntireColumn.NumberFormat = "DD/MM/YYYY HH:mm";
            to.Value2 = from.Value2;

            xlApp.DisplayAlerts = false;
            xlWorkBook.SaveAs(@"T:\AA_TAPI\Oleg\Taxi\Taxi Check.xlsm", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkBook.Close();
            xlApp.Quit();

            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

        }

        private void PrepareFilterDatasBetweenDates()
        {
            Cursor.Current = Cursors.WaitCursor;

            string Con_Str;
            string StrSql = "";

            comboB_Stop.Text = "";
            comboB_Stop.Items.Clear();
            comboB_Press.Text = "";
            comboB_Press.Items.Clear();

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBC;
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);
            // שליפת רשימת מכבשים לתקופה מבוקשת
            StrSql = "select distinct LPRESS from RZPALI.TAXILOGP " +
                     "where LTIMESTAMP between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            StrSql += "order by LPRESS";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_Press.Items.Add(DBC.Rs1.GetString(0).Trim()); // PRESS
            }
            DBC.Rs1.Close();

            // שליפת רשימת תקלות לתקופה מבוקשת
            StrSql = "select distinct LTAK,IDESC " +
                     "from RZPALI.TAXILOGP left join BPCSFV30.IIML01 on IPROD='TAK-161'||LTAK " +
                     "where LACT='4' and LTIMESTAMP between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            StrSql += "order by LTAK";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (!DBC.Rs1.IsDBNull(1))
                    comboB_Stop.Items.Add(DBC.Rs1.GetString(0).Trim() + " - " + DBC.Rs1.GetString(1).Trim()); // Stop
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            Cursor.Current = Cursors.PanEast;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
