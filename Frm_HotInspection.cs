﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_HotInspection : Form
    {
        public Frm_HotInspection()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 30/07/2013
            //  דוח המציג נתוני פגמים בביקורת חמה לפי חתיוך של תאריך \ קוד פגם, הדוח עבור חזי לוי
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("Error : Begining Date Bigger Than End Date", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }

            HotInspection();
        }

        private void HotInspection()
        {
            Cursor.Current = Cursors.WaitCursor;

            DBConn DBC;
            string Con_Str;
            string StrSql = "";
            string From_Date;      // תאריך התחלה לצורך השאילתא
            string TO_Date;        // תאריך סיום לצורך השאילתא            
            string[] DefectCode_Arr = new string[10];  // מערך המשמש עבור קודי פגם
            int A, B, C;
            string MFSZ = "";
            string MFNO = "";
            string MFVRN = "";
            string FixSpec = "";

            for (int j = 0; j < DefectCode_Arr.Length; j++)
                DefectCode_Arr[j] = "";
            B = 0;
            C = 0;

            // בדיקה אם המשתמש ביקש חיתוך קוד פגם מסויים
            //         אפשרתי למשתמש לבחור עד 10 קודי פגם
            //        מערך המכיל את הפגמים אשר המשתמש בחר
            if (Txt_DefectCode.Text.Trim() != "")
            {
                for (A = 0; A <= Txt_DefectCode.Text.Trim().Length - 1; A++)
                {
                    if (Txt_DefectCode.Text.Substring(A, 1) == ".")
                    {
                        DefectCode_Arr[C] = Txt_DefectCode.Text.Substring(B, 3);
                        B = A + 1;
                        C += 1;
                    }
                }
                // תפיסת רשומה אחרונה
                DefectCode_Arr[C] = Txt_DefectCode.Text.Trim().Substring(Txt_DefectCode.Text.Trim().Length - 3, 3);
            }

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            int I = 3;
            From_Date = DTPicker_From.Value.ToString("yyyyMMdd");
            TO_Date = DTPicker_To.Value.ToString("yyyyMMdd");

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet1;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet1 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet1.DisplayRightToLeft = false;

            xlWorkSheet1.Name = "Hot Inspection";
            xlWorkSheet1.Cells[1, 1] = "Hot Inspection Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            // מירכוז + מיזוג + פונט מודגש לתחום                        
            chartRange = xlWorkSheet1.get_Range("A1", "H1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            xlWorkSheet1.Cells[2, 1] = "Date";
            xlWorkSheet1.Cells[2, 2] = "Time";
            xlWorkSheet1.Cells[2, 3] = "Catalog";
            xlWorkSheet1.Cells[2, 4] = "Serial No.";
            xlWorkSheet1.Cells[2, 5] = "Size";
            xlWorkSheet1.Cells[2, 6] = "Spec No.";            
            xlWorkSheet1.Cells[2, 7] = "Tire Weight";
            xlWorkSheet1.Cells[2, 8] = "Defect Code";
            xlWorkSheet1.Cells[2, 9] = "Press";

            // תוספת מתאריך 18/06/2015
            // SMO הוספת הנתון אם תבנית
            xlWorkSheet1.Cells[2, 10] = "SMO Mold";

            chartRange = xlWorkSheet1.get_Range("A3", "A10000");
            chartRange.NumberFormat = "@";

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("A1", "J2");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("E1", "E2");
            chartRange.ColumnWidth = 20;

            // Freeze rows            
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet1.get_Range("A2", "A2").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd1.FreezePanes = true;

            // AutoFilter
            chartRange = xlWorkSheet1.get_Range("A2", "J2");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // בניית משפט שליפה                                                                                
            // תיקון מתאריך 10/03/2014
            // עקב הופעת מק"ט + סידורי כמה פעמים - כפילות רשומות, הדוח אמור להציג בסופו של דבר
            // רק רשומה אחת למק"ט + סידורי מאחר וההתייחסות בדוח לכמות צמיגים                                   
            StrSql = "select max(LACNDT),max(LPROD),max(LLBLNO),max(INSIZ),max(LERRDS),max(LACNTM),LPROD||LLBLNO " + // TAPIALI.LABELGL08 --> log file for action code 45.
                     "from TAPIALI.LABELGL08 join BPCSFALI.IIMN on SUBSTRING(LPROD,1,8)=INPROD " +
                     "where LACNDT between " + From_Date + " and " + TO_Date + " and LACTAN=45 ";
            //StrSql = "select LACNDT,LPROD,LLBLNO,INSIZ,LERRDS,LACNTM " + // TAPIALI.LABELGL08 --> log file for action code 45.
            //         "from TAPIALI.LABELGL08 join BPCSFALI.IIMN on SUBSTRING(LPROD,1,8)=INPROD " +
            //         "where LACNDT between " + From_Date + " and " + TO_Date + " and LACTAN=45 and LSTT in ('P','R')";                        
            
            // התאמת משפט שליפה לפי מק"ט
            if (Txt_Catalog.Text.Trim() != "")
            {
                string Tmpstr = " and substring(LPROD,1," + Txt_Catalog.Text.Trim().Length+ ")='" + Txt_Catalog.Text.Trim() + "' ";
                StrSql += Tmpstr;
            }

            // התאמת משפט שליפה לפי קוד פגם
            if (Txt_DefectCode.Text.Trim() != "")
            {
                string Tmpstr = " and LERRDS in(";
                for (int j = 0; j < DefectCode_Arr.Length; j++)
                    if (DefectCode_Arr[j].Trim() == "")
                        j = DefectCode_Arr.Length;
                    else
                        Tmpstr += "'" + DefectCode_Arr[j] + "',";
                StrSql += Tmpstr.Substring(0, Tmpstr.Length - 1) + ")";
            }
            StrSql += " group by LPROD||LLBLNO";
            
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet1.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                
                // שדרוג מתאריך 29/10/2013                
                // הוספת חיתוך ושדה חדש לדוח לבקשת חזי לוי
                if (DBC.Rs1.GetDecimal(5).ToString().Length == 1)
                    xlWorkSheet1.Cells[I, 2] = "00:00:0" + DBC.Rs1.GetDecimal(5).ToString();
                else if (DBC.Rs1.GetDecimal(5).ToString().Length == 2)
                    xlWorkSheet1.Cells[I, 2] = "00:00:" + DBC.Rs1.GetDecimal(5).ToString();
                else if (DBC.Rs1.GetDecimal(5).ToString().Length == 3)
                    xlWorkSheet1.Cells[I, 2] = "00:0" + DBC.Rs1.GetDecimal(5).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(5).ToString().Substring(1, 2);
                else if (DBC.Rs1.GetDecimal(5).ToString().Length == 4)
                    xlWorkSheet1.Cells[I, 2] = "00:" + DBC.Rs1.GetDecimal(5).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(5).ToString().Substring(2, 2);
                else if (DBC.Rs1.GetDecimal(5).ToString().Length == 5)
                    xlWorkSheet1.Cells[I, 2] = "0" + DBC.Rs1.GetDecimal(5).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(5).ToString().Substring(1, 2) + ":" + DBC.Rs1.GetDecimal(5).ToString().Substring(3, 2);
                else if (DBC.Rs1.GetDecimal(5).ToString().Length == 6)
                    xlWorkSheet1.Cells[I, 2] = DBC.Rs1.GetDecimal(5).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(5).ToString().Substring(2, 2) + ":" + DBC.Rs1.GetDecimal(5).ToString().Substring(4, 2);
                                                
                xlWorkSheet1.Cells[I, 3] = DBC.Rs1.GetString(1).Trim();
                xlWorkSheet1.Cells[I, 4] = DBC.Rs1.GetValue(2);
                xlWorkSheet1.Cells[I, 5] = DBC.Rs1.GetString(3).Trim();

                // שליפת מפרט מאותה מדבקה שדווח עליה פריקה ממכבש                
                StrSql = "select LSPEC,LMACH from TAPIALI.LABELGL29 " + // TAPIALI.LABELGL29 --> log file for action code 42.                
                         "where LPROD='" + DBC.Rs1.GetString(1) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(2).ToString() + " and LACTAN=42";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    if (DBC.Rs2.GetString(0).Trim() != "")
                    {
                        xlWorkSheet1.Cells[I, 6] = DBC.Rs2.GetString(0).Trim();

                        // שדרוג מתאריך 29/10/2013                
                        // הוספת חיתוך ושדה חדש לדוח לבקשת חזי לוי
                        xlWorkSheet1.Cells[I, 9] = DBC.Rs2.GetString(1).Trim();

                        FixSpec = DBC.Rs2.GetString(0).Trim().Substring(DBC.Rs2.GetString(0).Trim().Length - 9, 9);
                        MFSZ = FixSpec.Substring(0, 2);
                        MFNO = FixSpec.Substring(3, 3);
                        MFVRN = FixSpec.Substring(7, 2);

                        // תוספת מתאריך 18/06/2015
                        // SMO הוספת הנתון אם תבנית
                        // שליפת משקל צמיג ממערכת מפרטים
                        //StrSql = "select MFWT from MFRT.MFRTP " +
                        //          "where MFSZ='" + MFSZ + "' and MFNO = " + MFNO + " and MFVRN= " + MFVRN + "";
                        StrSql = "select MFWT,TASMO " +
                                 "from MFRT.MFRTP join MFRT.TVNMHAP ON MFTVDG=TATVDG and MFTVN=TATVNT and MFTVNT=TATVNO " +
                                 "where MFSZ='" + MFSZ + "' and MFNO = " + MFNO + " and MFVRN= " + MFVRN + "";
                        DBC.Rs2.Close();
                        DBC.Q_Run2(StrSql);
                        if (DBC.Rs2.Read())
                        {
                            xlWorkSheet1.Cells[I, 7] = DBC.Rs2.GetString(0).Trim();                            
                            xlWorkSheet1.Cells[I, 10] = DBC.Rs2.GetString(1).Trim();
                        }
                        DBC.Rs2.Close();
                        FixSpec = "";
                        MFSZ = "";
                        MFNO = "";
                        MFVRN = "";
                    }
                }
                DBC.Rs2.Close();                                 

                xlWorkSheet1.Cells[I, 8] = DBC.Rs1.GetString(4).Trim();                                
                I += 1;
            }
            DBC.Rs1.Close();
          
            DBC.Close_Conn();
            xlApp.Visible = true;
            releaseObject(xlWorkSheet1);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

            Cursor.Current = Cursors.PanEast;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Txt_DefectCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<----- . ----->| 
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.') e.Handled = false;
            else e.Handled = true;
        }

        private void Txt_Catalog_Leave(object sender, EventArgs e)
        {
            Txt_Catalog.Text = Txt_Catalog.Text.ToUpper();  
        }
    }
}
