﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
namespace Gipur_Production
{
    public partial class Frm_MixersReports : Form
    {
        string Mixer = "";
        public Frm_MixersReports()
        {
            InitializeComponent();
        }

        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            Mixer = mixercombo.Text;
            MixersReport();
        }

        private void MixersReport()
        {
            Cursor.Current = Cursors.WaitCursor;
            string Con_Str;
            string StrSql = "";
            string sql = "";
            string connectionString = "Provider=Microsoft.JET.OLEDB.4.0;data source=\\\\vbmixers\\F\\alians\\Banbory\\Access\\tblBanbo.mdb";
            int i = 3;

            DBService dbs = new DBService();
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            OleDbConnection conn;
            OleDbCommand cmd;
            OleDbDataReader reader;

            // AS400 התקשרות ל               
            //Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            //DBConn DBC;
            //DBC = new DBConn();
            //DBC.Initialize_Conn(Con_Str);

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet; // xlWorkSheet2;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "Mixers Report";

            xlWorkSheet.Cells[2, 1] = "Shift ";
            xlWorkSheet.Cells[2, 2] = "AS400 Item";
            xlWorkSheet.Cells[2, 3] = "Qty Produced";
            xlWorkSheet.Cells[2, 4] = "Access Item";
            xlWorkSheet.Cells[2, 5] = "Status ";
            xlWorkSheet.Cells[2, 6] = "Rubber 1 ";
            xlWorkSheet.Cells[2, 7] = "Rubber 2 ";
            xlWorkSheet.Cells[2, 8] = "Rubber 3 ";
            xlWorkSheet.Cells[2, 9] = "Rubber 4 ";
            xlWorkSheet.Cells[2, 10] = "Chemical 1 ";
            xlWorkSheet.Cells[2, 11] = "Chemical 2 ";

            // כותרת דו"ח 
            chartRange = xlWorkSheet.get_Range("D1", "G1");
            chartRange.Merge();
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Size = 16;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 4] = "Mixers Report " + dateTimePicker1.Value.ToString("dd.MM.yyyy HH:mm") + "    Mixer: " + Mixer ;

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A2", "BB2");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange = xlWorkSheet.get_Range("A3", "M200");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;

            // הבאת נתונים כמו שהם ממקור
            chartRange = xlWorkSheet.get_Range("H2", "O100");
            chartRange.NumberFormat = "@";          

            // Freeze rows            
            Excel.Window xlWnd3 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("B2", "B2").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd3.FreezePanes = true;

            // AutoFilter            
            chartRange = xlWorkSheet.get_Range("B2", "L2");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // Wrap Text
            chartRange = xlWorkSheet.get_Range("B2", "B2");
            chartRange.Cells.WrapText = true;

            // Column Width
            chartRange = xlWorkSheet.get_Range("B2", "B2");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("C2", "C2");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("D2", "D2");
            chartRange.ColumnWidth = 20;
            chartRange = xlWorkSheet.get_Range("E2", "E2");
            chartRange.ColumnWidth = 20;
            chartRange = xlWorkSheet.get_Range("F2", "F2");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("G2", "G2");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("H2", "H2");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("I2", "I2");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("J2", "J2");
            chartRange.ColumnWidth = 15;

            // שליפת מרכיבים על פי משמרת
            conn = new OleDbConnection(connectionString);
            //StrSql = "select * from TAPIALI.RPTIP " +
            //         "where RPDEPT=52 and RPMACH in (1,2,5,6) and RPMST='' and RPDTE = '20.11.2016' and right('0'||trim(RPTME),8) between '06:30:00' and '15:00:00' " +
            //         "where RPDEPT=52 and RPMACH in (1,2,5,6) and RPMST='' and RPDTE = '20.11.2016' and right('0'||trim(RPTME),8) between '15:00:00' and '23:30:00' " +
            //         "where RPDEPT=52 and RPMACH in (1,2,5,6) and RPMST='' and (RPDTE = '19.11.2016' and right('0'||trim(RPTME),8) between '23:30:00' and '23:59:59' or " +
            //         "RPDTE = '20.11.2016' and right('0'||trim(RPTME),8) between '00:00:00' and '06:30:30') ";
            for (int t  = 0; t < 3; t++)
            {
                switch (t)
                {
                    case 0:
                        StrSql = "select  rpprod,count(rpprod),max(RPMSNM),MPRDAC, 1 as shift,max(RPDTE),max(RPTME) " +
                        "from TAPIALI.RPTIP " +
                        "left join TAPIALI.RPTMP  on  RPMSNM=MPMSNM " +
                        "where RPDEPT=52 and RPMACH =" + Mixer + " and RPMST='' and ((RPDTE = '" + dateTimePicker1.Value.AddDays(-1).ToString("dd.MM.yyyy") + "' and right('0'||trim(RPTME),8) between time'23:30:00' and time'23:59:59') or " +
                        "(RPDTE = '" + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "' and right('0'||trim(RPTME),8) between time'00:00:00' and time'06:30:30')) " +
                        "group by  rpprod,MPRDAC " +
                        "order by max(RPDTE),max(RPTME) ";
                        break;
                    case 1:
                        StrSql = "select  rpprod,count(rpprod),max(RPMSNM),MPRDAC, 2 as shift,max(RPDTE),max(RPTME) " +
                        "from TAPIALI.RPTIP " +
                        "left join TAPIALI.RPTMP  on  RPMSNM=MPMSNM " +
                        "where RPDEPT=52 and RPMACH =" + Mixer + " and RPMST='' and ((RPDTE = '" + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "' and right('0'||trim(RPTME),8) between time'23:30:00' and time'23:59:59') or " +
                        "(RPDTE = '" + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "' and right('0'||trim(RPTME),8) between time'06:30:30' and time'15:00:00')) " +
                        "group by  rpprod,MPRDAC " +
                        "order by max(RPDTE),max(RPTME) ";
                        break;
                    case 2:
                        StrSql = "select  rpprod,count(rpprod),max(RPMSNM),MPRDAC, 3 as shift,max(RPDTE),max(RPTME) " +
                        "from TAPIALI.RPTIP " +
                        "left join TAPIALI.RPTMP  on  RPMSNM=MPMSNM " +
                        "where RPDEPT=52 and RPMACH = " + Mixer + " and RPMST='' and ((RPDTE = '" + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "' and right('0'||trim(RPTME),8) between time'23:30:00' and time'23:59:59') or " +
                        "(RPDTE = '" + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "' and right('0'||trim(RPTME),8) between time'15:00:00' and time'23:30:00')) " +
                        "group by  rpprod,MPRDAC " +
                        "order by max(RPDTE),max(RPTME) ";
                        break;
                }
                //DBC.Q_Run1(StrSql);
                DT = dbs.executeSelectQueryNoParam(StrSql); 
                //while (DBC.Rs1.Read())
                foreach (DataRow row in DT.Rows)
                {
                    xlWorkSheet.Cells[i, 1] = row[4]; //DBC.Rs1.GetValue(4);
                    xlWorkSheet.Cells[i, 2] = row[0].ToString().Trim(); //DBC.Rs1.GetString(0).Trim();
                    xlWorkSheet.Cells[i, 3] = row[1]; //DBC.Rs1.GetInt32(1);
                    //if (!DBC.Rs1.IsDBNull(3))
                    if(row[3] != null)
                    {
                        xlWorkSheet.Cells[i, 4] = row[3].ToString().Trim(); //DBC.Rs1.GetString(3).Trim();
                        sql = "SELECT  TblShkilot.[MaterialCode], Group " +
                               "FROM TblShkilot " +
                               "where TblShkilot.[MixCode]='" + row[3].ToString().Trim()  + "' and  TblShkilot.[Group]  between 1 and 6 " +
                               "order by  TblShkilot.[Group] ";
                        cmd = new OleDbCommand(sql, conn);
                        int j = 5;
                        conn.Open();
                        reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            xlWorkSheet.Cells[i, j + int.Parse(reader.GetValue(1).ToString())] = reader.GetValue(0);
                        }
                        reader.Close();
                        conn.Close();
                        i++;
                        j = 6;
                        // הצגת סטטוס מפרטים לתיקון מסד הנתונים
                        // תוספות מתאריך 29/11/2016 ע"י רן הויכמן
                        StrSql = "select mpprod,case mperr when 0 then 'לא נקראו כל המרכיבים' " + 
                                 "when 1 then 'תקין' " + 
                                 "when 2 then 'אי התאמת מפרטים' " + 
                                 "else 'קריאה שגויה' end as Error " +
                                 "from tapiali.rptmp where mpmsnm = '" + row[2]  + "' ";
                        //DBC.Q_Run2(StrSql);
                        //DS.Tables.Add(dbs.executeSelectQueryNoParam(StrSql));
                        DT1 = dbs.executeSelectQueryNoParam(StrSql);
                        //while (DBC.Rs2.Read())
                        foreach (DataRow row1 in DT1.Rows)
                        {
                            xlWorkSheet.Cells[i, j] = row1[0].ToString().Trim(); //DBC.Rs2.GetString(0).Trim();
                            xlWorkSheet.Cells[--i, 5] = row1[1].ToString().Trim(); //DBC.Rs2.GetString(1).Trim();
                            i++;
                            j++;
                        }
                        //DBC.Rs2.Close();
                        StrSql = "select mpprod from tapiali.rpterrp where mpmsnm = '" + row[2] + "' ";
                        //DS.Tables[1].Clear();
                        //DS.Tables.Add(dbs.executeSelectQueryNoParam(StrSql));
                        DT1 = dbs.executeSelectQueryNoParam(StrSql);
                        //while (DBC.Rs2.Read())
                        foreach (DataRow row1 in DT1.Rows)
                        {
                            xlWorkSheet.Cells[i, j] = row1[0].ToString().Trim(); //DBC.Rs2.GetString(0).Trim();
                            chartRange = (Excel.Range)xlWorkSheet.Cells[i, j];
                            chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                            j++;
                        }
                        //DBC.Rs2.Close();
                        xlWorkSheet.Cells[i, 1] = row[4]; //DBC.Rs1.GetValue(4);
                    }
                    i++;
                }
            }
            //DBC.Rs1.Close();
            //DBC.Close_Conn();
            Cursor.Current = Cursors.PanEast;

            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
        }
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
