﻿namespace Gipur_Production
{
    partial class Frm_PressesWarm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_PressesWarm));
            this.GB_Choice = new System.Windows.Forms.GroupBox();
            this.RB_Daily = new System.Windows.Forms.RadioButton();
            this.RB_Period = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.CB_PressesDaily = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DTPicker_Daily = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.GB_Daily = new System.Windows.Forms.GroupBox();
            this.CB_Shift = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DTPicker_To = new System.Windows.Forms.DateTimePicker();
            this.GB_Period = new System.Windows.Forms.GroupBox();
            this.CB_PressesPeriod = new System.Windows.Forms.ComboBox();
            this.DTPicker_From = new System.Windows.Forms.DateTimePicker();
            this.Lbl_Press = new System.Windows.Forms.Label();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.GB_Choice.SuspendLayout();
            this.GB_Daily.SuspendLayout();
            this.GB_Period.SuspendLayout();
            this.SuspendLayout();
            // 
            // GB_Choice
            // 
            this.GB_Choice.Controls.Add(this.RB_Daily);
            this.GB_Choice.Controls.Add(this.RB_Period);
            this.GB_Choice.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GB_Choice.Location = new System.Drawing.Point(21, 51);
            this.GB_Choice.Name = "GB_Choice";
            this.GB_Choice.Size = new System.Drawing.Size(380, 67);
            this.GB_Choice.TabIndex = 91;
            this.GB_Choice.TabStop = false;
            this.GB_Choice.Text = "Select";
            // 
            // RB_Daily
            // 
            this.RB_Daily.AutoSize = true;
            this.RB_Daily.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RB_Daily.Location = new System.Drawing.Point(39, 28);
            this.RB_Daily.Name = "RB_Daily";
            this.RB_Daily.Size = new System.Drawing.Size(129, 23);
            this.RB_Daily.TabIndex = 84;
            this.RB_Daily.Text = "Daily Report";
            this.RB_Daily.UseVisualStyleBackColor = true;
            this.RB_Daily.CheckedChanged += new System.EventHandler(this.RB_Daily_CheckedChanged);
            // 
            // RB_Period
            // 
            this.RB_Period.AutoSize = true;
            this.RB_Period.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RB_Period.Location = new System.Drawing.Point(185, 28);
            this.RB_Period.Name = "RB_Period";
            this.RB_Period.Size = new System.Drawing.Size(141, 23);
            this.RB_Period.TabIndex = 85;
            this.RB_Period.Text = "Period Report";
            this.RB_Period.UseVisualStyleBackColor = true;
            this.RB_Period.CheckedChanged += new System.EventHandler(this.RB_Period_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.SlateGray;
            this.label3.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(70, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 24);
            this.label3.TabIndex = 87;
            this.label3.Text = "Date:";
            // 
            // CB_PressesDaily
            // 
            this.CB_PressesDaily.DropDownHeight = 150;
            this.CB_PressesDaily.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_PressesDaily.FormattingEnabled = true;
            this.CB_PressesDaily.IntegralHeight = false;
            this.CB_PressesDaily.Location = new System.Drawing.Point(184, 59);
            this.CB_PressesDaily.Name = "CB_PressesDaily";
            this.CB_PressesDaily.Size = new System.Drawing.Size(109, 26);
            this.CB_PressesDaily.TabIndex = 85;
            this.CB_PressesDaily.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CB_PressesDaily_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.SlateGray;
            this.label5.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(70, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 24);
            this.label5.TabIndex = 86;
            this.label5.Text = "Press:";
            // 
            // DTPicker_Daily
            // 
            this.DTPicker_Daily.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_Daily.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPicker_Daily.Location = new System.Drawing.Point(184, 26);
            this.DTPicker_Daily.Name = "DTPicker_Daily";
            this.DTPicker_Daily.Size = new System.Drawing.Size(109, 25);
            this.DTPicker_Daily.TabIndex = 81;
            this.DTPicker_Daily.Leave += new System.EventHandler(this.DTPicker_Daily_Leave);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.SlateGray;
            this.label9.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label9.Location = new System.Drawing.Point(70, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 24);
            this.label9.TabIndex = 85;
            this.label9.Text = "Start Date:";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.SlateGray;
            this.label10.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label10.Location = new System.Drawing.Point(70, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 24);
            this.label10.TabIndex = 86;
            this.label10.Text = "End Date:";
            // 
            // GB_Daily
            // 
            this.GB_Daily.Controls.Add(this.label3);
            this.GB_Daily.Controls.Add(this.CB_PressesDaily);
            this.GB_Daily.Controls.Add(this.label5);
            this.GB_Daily.Controls.Add(this.DTPicker_Daily);
            this.GB_Daily.Controls.Add(this.CB_Shift);
            this.GB_Daily.Controls.Add(this.label1);
            this.GB_Daily.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.GB_Daily.Location = new System.Drawing.Point(21, 140);
            this.GB_Daily.Name = "GB_Daily";
            this.GB_Daily.Size = new System.Drawing.Size(380, 132);
            this.GB_Daily.TabIndex = 90;
            this.GB_Daily.TabStop = false;
            this.GB_Daily.Text = "Daily Report";
            this.GB_Daily.Visible = false;
            // 
            // CB_Shift
            // 
            this.CB_Shift.DropDownHeight = 150;
            this.CB_Shift.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_Shift.FormattingEnabled = true;
            this.CB_Shift.IntegralHeight = false;
            this.CB_Shift.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.CB_Shift.Location = new System.Drawing.Point(184, 93);
            this.CB_Shift.Name = "CB_Shift";
            this.CB_Shift.Size = new System.Drawing.Size(109, 26);
            this.CB_Shift.TabIndex = 79;
            this.CB_Shift.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CB_Shift_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.SlateGray;
            this.label1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(70, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 24);
            this.label1.TabIndex = 80;
            this.label1.Text = "Shift:";
            // 
            // DTPicker_To
            // 
            this.DTPicker_To.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_To.Location = new System.Drawing.Point(184, 59);
            this.DTPicker_To.Name = "DTPicker_To";
            this.DTPicker_To.Size = new System.Drawing.Size(109, 25);
            this.DTPicker_To.TabIndex = 71;
            this.DTPicker_To.Leave += new System.EventHandler(this.DTPicker_To_Leave);
            // 
            // GB_Period
            // 
            this.GB_Period.Controls.Add(this.CB_PressesPeriod);
            this.GB_Period.Controls.Add(this.label9);
            this.GB_Period.Controls.Add(this.label10);
            this.GB_Period.Controls.Add(this.DTPicker_To);
            this.GB_Period.Controls.Add(this.DTPicker_From);
            this.GB_Period.Controls.Add(this.Lbl_Press);
            this.GB_Period.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.GB_Period.Location = new System.Drawing.Point(21, 140);
            this.GB_Period.Name = "GB_Period";
            this.GB_Period.Size = new System.Drawing.Size(380, 132);
            this.GB_Period.TabIndex = 89;
            this.GB_Period.TabStop = false;
            this.GB_Period.Text = "Period Report";
            this.GB_Period.Visible = false;
            // 
            // CB_PressesPeriod
            // 
            this.CB_PressesPeriod.DropDownHeight = 150;
            this.CB_PressesPeriod.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_PressesPeriod.FormattingEnabled = true;
            this.CB_PressesPeriod.IntegralHeight = false;
            this.CB_PressesPeriod.Location = new System.Drawing.Point(184, 93);
            this.CB_PressesPeriod.Name = "CB_PressesPeriod";
            this.CB_PressesPeriod.Size = new System.Drawing.Size(109, 26);
            this.CB_PressesPeriod.TabIndex = 87;
            this.CB_PressesPeriod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CB_PressesPeriod_KeyPress);
            // 
            // DTPicker_From
            // 
            this.DTPicker_From.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_From.Location = new System.Drawing.Point(184, 26);
            this.DTPicker_From.Name = "DTPicker_From";
            this.DTPicker_From.Size = new System.Drawing.Size(109, 25);
            this.DTPicker_From.TabIndex = 70;
            // 
            // Lbl_Press
            // 
            this.Lbl_Press.AutoSize = true;
            this.Lbl_Press.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_Press.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.Lbl_Press.Location = new System.Drawing.Point(70, 95);
            this.Lbl_Press.Name = "Lbl_Press";
            this.Lbl_Press.Size = new System.Drawing.Size(71, 24);
            this.Lbl_Press.TabIndex = 75;
            this.Lbl_Press.Text = "Press:";
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(157, 295);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(100, 28);
            this.Btn_OK.TabIndex = 88;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(16, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(356, 29);
            this.label2.TabIndex = 87;
            this.label2.Text = "Presses Warm Up";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frm_PressesWarm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(420, 342);
            this.Controls.Add(this.GB_Choice);
            this.Controls.Add(this.GB_Daily);
            this.Controls.Add(this.GB_Period);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Frm_PressesWarm";
            this.Text = "Presses Warm Up";
            this.Load += new System.EventHandler(this.Frm_PressesWarm_Load);
            this.GB_Choice.ResumeLayout(false);
            this.GB_Choice.PerformLayout();
            this.GB_Daily.ResumeLayout(false);
            this.GB_Daily.PerformLayout();
            this.GB_Period.ResumeLayout(false);
            this.GB_Period.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GB_Choice;
        private System.Windows.Forms.RadioButton RB_Daily;
        private System.Windows.Forms.RadioButton RB_Period;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CB_PressesDaily;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker DTPicker_Daily;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox GB_Daily;
        private System.Windows.Forms.ComboBox CB_Shift;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DTPicker_To;
        private System.Windows.Forms.GroupBox GB_Period;
        private System.Windows.Forms.DateTimePicker DTPicker_From;
        private System.Windows.Forms.Label Lbl_Press;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CB_PressesPeriod;
    }
}