﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_PressesWarm : Form
    {
        public Frm_PressesWarm()
        {
            InitializeComponent();
        }

        private void Frm_PressesWarm_Load(object sender, EventArgs e)
        {
            // תוספת מתאריך 22/05/2017
            // דוח חימום מכבשים             
            RB_Daily.Checked = true;
            GB_Daily.Visible = true;
            ResetScreen();
            Arrange_Daily_Presses();
            Arrange_Period_Presses();
        }

        private void RB_Daily_CheckedChanged(object sender, EventArgs e)
        {
            GB_Daily.Visible = RB_Daily.Checked ? true : false;
            ResetScreen();
        }

        private void RB_Period_CheckedChanged(object sender, EventArgs e)
        {
            GB_Period.Visible = RB_Period.Checked ? true : false;
            ResetScreen();
        }
       
        private void DTPicker_Daily_Leave(object sender, EventArgs e)
        {
            CB_PressesDaily.Text = "";
            CB_PressesDaily.Items.Clear();
            Arrange_Daily_Presses();                  
        }

        private void DTPicker_To_Leave(object sender, EventArgs e)
        {
            CB_PressesPeriod.Text = "";
            CB_PressesPeriod.Items.Clear();
            Arrange_Period_Presses();            
        }        

        private void Arrange_Daily_Presses()
        {
            string Con_Str = "Driver={SQL Server Native Client 11.0};Server=ALSQL\\ALLIANCE;Database=Production;Uid=albi;Pwd=Al5342";
            DBConn DBC_SQL = new DBConn();
            DBC_SQL.Initialize_Conn(Con_Str);

            string StrSql = "select distinct PressNumber from PLCPRESSWARM " +
                            "where StartWarm between '" + DTPicker_Daily.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 23:29:59.000") + "' " +
                            "order by PressNumber";
            DBC_SQL.Q_Run1(StrSql);
            while (DBC_SQL.Rs1.Read())
            {
                CB_PressesDaily.Items.Add(DBC_SQL.Rs1.GetString(0).Trim());
            }
            DBC_SQL.Rs1.Close();
            DBC_SQL.Close_Conn();               
        }

        private void Arrange_Period_Presses()
        {
            string Con_Str = "Driver={SQL Server Native Client 11.0};Server=ALSQL\\ALLIANCE;Database=Production;Uid=albi;Pwd=Al5342";
            DBConn DBC_SQL = new DBConn();
            DBC_SQL.Initialize_Conn(Con_Str);

            string StrSql = "select distinct PressNumber from PLCPRESSWARM " +
                            "where StartWarm between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd 23:29:59.000") + "' " +
                            "order by PressNumber";
            DBC_SQL.Q_Run1(StrSql);
            while (DBC_SQL.Rs1.Read())
            {
                CB_PressesPeriod.Items.Add(DBC_SQL.Rs1.GetString(0).Trim());
            }
            DBC_SQL.Rs1.Close();
            DBC_SQL.Close_Conn();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (RB_Daily.Checked)
                Presses_Warm_Daily();
            else
            {
                if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
                {
                    MessageBox.Show("Error, Start date greater than end date", "Curing / Building Production");
                    DTPicker_From.Focus();
                    return;
                }
                Presses_Warm_Period();
            }
        }

        private void ResetScreen()
        {            
            DTPicker_Daily.Value = DateTime.Now;
            DTPicker_From.Value = DateTime.Now;
            DTPicker_To.Value = DateTime.Now;
            CB_PressesDaily.Text = "";
            CB_Shift.Text = "";
            CB_PressesPeriod.Text = "";
        }

        private void Presses_Warm_Daily()
        {
            // דוח יומי
            Cursor.Current = Cursors.WaitCursor;

            int I = 0;

            DBConn DBC;
            string Con_Str;
            string StrSql = "";
            string Shift = "";

            Con_Str = "Driver={SQL Server Native Client 11.0};Server=ALSQL\\ALLIANCE;Database=Production;Uid=albi;Pwd=Al5342";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet1, xlWorkSheet2;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet2 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet1 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            xlWorkSheet1.Name = "Total";
            xlWorkSheet2.Name = "Details";

            // מחיקת גיליונות אקסל מיותרים            
            foreach (Excel.Worksheet xlworksheet in xlWorkBook.Worksheets)
                if (xlworksheet.Name != "Total" && xlworksheet.Name != "Details")
                    xlworksheet.Delete();

            // Sheet Total
            xlWorkSheet1.Cells[1, 1] = "Press";
            xlWorkSheet1.Cells[1, 2] = "Total Hours";
            // Sheet Details
            xlWorkSheet2.Cells[1, 1] = "Shift";
            xlWorkSheet2.Cells[1, 2] = "Press";
            xlWorkSheet2.Cells[1, 3] = "Start";
            xlWorkSheet2.Cells[1, 4] = "End";            
            xlWorkSheet2.Cells[1, 5] = "Total Minuts";

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A1", "B1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("A1", "B1");
            chartRange.ColumnWidth = 12;
            chartRange.HorizontalAlignment = 1;
            chartRange = xlWorkSheet1.get_Range("B1", "B1");
            chartRange.ColumnWidth = 15;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet2.get_Range("A1", "B1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 1;
            chartRange = xlWorkSheet2.get_Range("C1", "E1");
            chartRange.ColumnWidth = 15;

            // Freeze rows            
            Excel.Window xlWnd3 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet1.get_Range("A1", "A1").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd3.FreezePanes = true;

            // AutoFilter            
            chartRange = xlWorkSheet1.get_Range("A1", "B1");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // AutoFilter            
            chartRange = xlWorkSheet2.get_Range("A1", "E1");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            I = 2;
            StrSql = "select PressNumber,sum(TotalMinutes) from PLCPRESSWARM ";

            if (CB_Shift.Text.Trim() == "1")
                StrSql += "where StartWarm between '" + DTPicker_Daily.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 06:30:59.000") + "' ";
            else if (CB_Shift.Text.Trim() == "2")
                StrSql += "where StartWarm between '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 06:30:00.000") + "' and '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 15:00:59.000") + "' ";
            else if (CB_Shift.Text.Trim() == "3")
                StrSql += "where StartWarm between '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 15:00:00.000") + "' and '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 23:30:59.000") + "' ";
            else
                StrSql += "where StartWarm between '" + DTPicker_Daily.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 23:30:59.000") + "' ";

            if (CB_PressesDaily.Text.Trim() != "") StrSql += "and PressNumber='" + CB_PressesDaily.Text.Trim() + "' ";

            StrSql += "group by PressNumber order by PressNumber";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet1.Cells[I, 1] = DBC.Rs1.GetString(0).Trim();
                xlWorkSheet1.Cells[I, 2] = Math.Round(DBC.Rs1.GetDecimal(1) / 60, 1);
                I++;
            }
            DBC.Rs1.Close();
            xlWorkSheet1.Cells[I, 1] = "Total Hours";
            xlWorkSheet1.Cells[I, 2] = "=sum(B2:B" + (I - 1).ToString() + ")";

            I = 2;
            StrSql = "select PressNumber,StartWarm,EndWarm,TotalMinutes from PLCPRESSWARM ";

            if (CB_Shift.Text.Trim() == "1")
                StrSql += "where StartWarm between '" + DTPicker_Daily.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 06:29:59.000") + "' ";
            else if (CB_Shift.Text.Trim() == "2")
                StrSql += "where StartWarm between '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 06:30:00.000") + "' and '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 14:59:59.000") + "' ";
            else if (CB_Shift.Text.Trim() == "3")
                StrSql += "where StartWarm between '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 15:00:00.000") + "' and '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 23:29:59.000") + "' ";
            else
                StrSql += "where StartWarm between '" + DTPicker_Daily.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_Daily.Value.ToString("yyyy-MM-dd 23:29:59.000") + "' ";

            if (CB_PressesDaily.Text.Trim() != "") StrSql += "and PressNumber='" + CB_PressesDaily.Text.Trim() + "' ";

            StrSql += "order by StartWarm";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (CB_Shift.Text.Trim() == "")
                    if (DBC.Rs1.GetDateTime(1).TimeOfDay >= DateTime.Parse("2017/05/30 06:30:00.000").TimeOfDay && DBC.Rs1.GetDateTime(1).TimeOfDay <= DateTime.Parse("2017/05/30 14:59:59.000").TimeOfDay)  
                        Shift = "2";
                    else if (DBC.Rs1.GetDateTime(1).TimeOfDay >= DateTime.Parse("2017/05/30 15:00:00.000").TimeOfDay && DBC.Rs1.GetDateTime(1).TimeOfDay <= DateTime.Parse("2017/05/30 23:29:59.000").TimeOfDay)
                        Shift = "3";    
                    else
                        Shift = "1";
                else
                    Shift = CB_Shift.Text.Trim() == "1" ? "1" : CB_Shift.Text.Trim() == "2" ? "2" : CB_Shift.Text.Trim() == "3" ? "3" : "";
                xlWorkSheet2.Cells[I, 1] = Shift ;
                xlWorkSheet2.Cells[I, 2] = DBC.Rs1.GetString(0).Trim();
                xlWorkSheet2.Cells[I, 3] = DBC.Rs1.GetDateTime(1);
                xlWorkSheet2.Cells[I, 4] = DBC.Rs1.GetDateTime(2);
                xlWorkSheet2.Cells[I, 5] = DBC.Rs1.GetInt16(3);
                I++;
            }
            DBC.Rs1.Close();

            DBC.Close_Conn();
            xlApp.Visible = true;
            releaseObject(xlWorkSheet1);
            releaseObject(xlWorkSheet2);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void Presses_Warm_Period()
        {
            // דוח תקופתי
            Cursor.Current = Cursors.WaitCursor;

            int I = 0;

            DBConn DBC;
            string Con_Str;
            string StrSql = "";
           
            Con_Str = "Driver={SQL Server Native Client 11.0};Server=ALSQL\\ALLIANCE;Database=Production;Uid=albi;Pwd=Al5342";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet1, xlWorkSheet2;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);            
            xlWorkSheet2 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet1 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);            

            xlWorkSheet1.Name = "Total";
            xlWorkSheet2.Name = "Details";

            // מחיקת גיליונות אקסל מיותרים            
            foreach (Excel.Worksheet xlworksheet in xlWorkBook.Worksheets)            
                if (xlworksheet.Name != "Total" && xlworksheet.Name != "Details")
                    xlworksheet.Delete();                          

            // Sheet Total
            xlWorkSheet1.Cells[1, 1] = "Press";
            xlWorkSheet1.Cells[1, 2] = "Total Hours";
            // Sheet Details
            xlWorkSheet2.Cells[1, 1] = "Press";
            xlWorkSheet2.Cells[1, 2] = "Start";
            xlWorkSheet2.Cells[1, 3] = "End";
            xlWorkSheet2.Cells[1, 4] = "Total Minuts";
            
            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A1", "B1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;            

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("A1", "B1");
            chartRange.ColumnWidth = 12;
            chartRange.HorizontalAlignment = 1;
            chartRange = xlWorkSheet1.get_Range("B1", "B1");
            chartRange.ColumnWidth = 15;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet2.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 1;
            chartRange = xlWorkSheet2.get_Range("B1", "D1");
            chartRange.ColumnWidth = 15;

            // Freeze rows            
            Excel.Window xlWnd3 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet1.get_Range("A1", "A1").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd3.FreezePanes = true;

            // AutoFilter            
            chartRange = xlWorkSheet1.get_Range("A1", "B1");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // AutoFilter            
            chartRange = xlWorkSheet2.get_Range("A1", "D1");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            I = 2;
            StrSql = "select PressNumber,sum(TotalMinutes) from PLCPRESSWARM " +
                     "where StartWarm between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd 23:30:59.000") + "' ";            
            
            if (CB_PressesPeriod.Text.Trim() != "") StrSql += "and PressNumber='"+CB_PressesPeriod.Text.Trim()+"' ";

            StrSql += "group by PressNumber order by PressNumber";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {                
                xlWorkSheet1.Cells[I, 1] = DBC.Rs1.GetString(0).Trim();
                xlWorkSheet1.Cells[I, 2] = Math.Round(DBC.Rs1.GetDecimal(1)/60,1);               
                I++;
            }
            DBC.Rs1.Close();            
            xlWorkSheet1.Cells[I, 1] = "Total Hours";
            xlWorkSheet1.Cells[I, 2] = "=sum(B2:B" + (I - 1).ToString() + ")";            
            
            I=2;
            StrSql = "select PressNumber,StartWarm,EndWarm,TotalMinutes from PLCPRESSWARM " +
                     "where StartWarm between '" + DTPicker_From.Value.AddDays(-1).ToString("yyyy-MM-dd 23:30:00.000") + "' and '" + DTPicker_To.Value.ToString("yyyy-MM-dd 23:30:59.000") + "' ";
            
            if (CB_PressesPeriod.Text.Trim() != "") StrSql += "and PressNumber='" + CB_PressesPeriod.Text.Trim() + "' ";

            StrSql += "order by StartWarm";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet2.Cells[I, 1] = DBC.Rs1.GetString(0).Trim();
                xlWorkSheet2.Cells[I, 2] = DBC.Rs1.GetDateTime(1);
                xlWorkSheet2.Cells[I, 3] = DBC.Rs1.GetDateTime(2);
                xlWorkSheet2.Cells[I, 4] = DBC.Rs1.GetInt16(3);
                I++;
            }
            DBC.Rs1.Close();

            DBC.Close_Conn();
            xlApp.Visible = true;
            releaseObject(xlWorkSheet1);
            releaseObject(xlWorkSheet2);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void CB_PressesDaily_KeyPress(object sender, KeyPressEventArgs e)
        {
            //  |<-----   BackSpace  ----->|   |<-----     Delete     ----->| 
            if (e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete) e.Handled=  false;
            else e.Handled = true;
        }

        private void CB_Shift_KeyPress(object sender, KeyPressEventArgs e)
        {
            //  |<-----   BackSpace  ----->|   |<-----     Delete     ----->| 
            if (e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete) e.Handled = false;
            else e.Handled = true;
        }

        private void CB_PressesPeriod_KeyPress(object sender, KeyPressEventArgs e)
        {
            //  |<-----   BackSpace  ----->|   |<-----     Delete     ----->| 
            if (e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete) e.Handled = false;
            else e.Handled = true;
        }
    }
}
