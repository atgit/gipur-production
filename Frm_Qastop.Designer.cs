﻿namespace Gipur_Production
{
    partial class Frm_QaStop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_QaStop));
            this.Btn_OK = new System.Windows.Forms.Button();
            this.DTPicker_To = new System.Windows.Forms.DateTimePicker();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.DTPicker_From = new System.Windows.Forms.DateTimePicker();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(117, 235);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(164, 34);
            this.Btn_OK.TabIndex = 31;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // DTPicker_To
            // 
            this.DTPicker_To.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_To.Location = new System.Drawing.Point(166, 174);
            this.DTPicker_To.Margin = new System.Windows.Forms.Padding(4);
            this.DTPicker_To.Name = "DTPicker_To";
            this.DTPicker_To.Size = new System.Drawing.Size(151, 29);
            this.DTPicker_To.TabIndex = 29;
            // 
            // Lbl_To
            // 
            this.Lbl_To.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_To.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(77, 176);
            this.Lbl_To.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.Size = new System.Drawing.Size(97, 30);
            this.Lbl_To.TabIndex = 30;
            this.Lbl_To.Text = "To";
            // 
            // DTPicker_From
            // 
            this.DTPicker_From.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_From.Location = new System.Drawing.Point(166, 116);
            this.DTPicker_From.Margin = new System.Windows.Forms.Padding(4);
            this.DTPicker_From.Name = "DTPicker_From";
            this.DTPicker_From.Size = new System.Drawing.Size(151, 29);
            this.DTPicker_From.TabIndex = 28;
            // 
            // Lbl_From
            // 
            this.Lbl_From.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_From.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(77, 117);
            this.Lbl_From.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.Size = new System.Drawing.Size(97, 30);
            this.Lbl_From.TabIndex = 27;
            this.Lbl_From.Text = "From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(82, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(226, 35);
            this.label2.TabIndex = 26;
            this.label2.Text = "QaStop Report";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frm_QaStop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(389, 322);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.DTPicker_To);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DTPicker_From);
            this.Controls.Add(this.Lbl_From);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_QaStop";
            this.Text = "QaStop";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.DateTimePicker DTPicker_To;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.DateTimePicker DTPicker_From;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.Label label2;

    }
}