﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_QaStop : Form
    {
        public Frm_QaStop()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {

            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_From.Focus();  
                return; 
            }

            Cursor.Current = Cursors.WaitCursor;
            
            string Con_Str;
            string StrSql = "";
            string TmpDT = "";
            int i = 3;

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBC;
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet; // xlWorkSheet2;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //xlWorkSheet2 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);                                   
            xlWorkSheet.Name = "QaStop All";
            xlWorkSheet.Cells[1, 1] = "QaStop All Report Between :  " + DTPicker_From.Value.ToString() + " and " + DTPicker_To.Value.ToString();

            xlWorkSheet.Cells[2, 1] = "Loc";
            xlWorkSheet.Cells[2, 2] = "Reporter";
            xlWorkSheet.Cells[2, 3] = "Catalog";
            xlWorkSheet.Cells[2, 4] = "Serial";
            xlWorkSheet.Cells[2, 5] = "ID";
            xlWorkSheet.Cells[2, 6] = "Size";
            xlWorkSheet.Cells[2, 7] = "Qty";
            xlWorkSheet.Cells[2, 8] = "Date";
            xlWorkSheet.Cells[2, 9] = "Year";
            xlWorkSheet.Cells[2, 10] = "Month";
            xlWorkSheet.Cells[2, 11] = "Day";
            xlWorkSheet.Cells[2, 12] = "Spec Weight";
            xlWorkSheet.Cells[2, 13] = "Act Weight";
            xlWorkSheet.Cells[2, 14] = "Status";
            xlWorkSheet.Cells[2, 15] = "Defect Code";
            xlWorkSheet.Cells[2, 16] = "Description";
            xlWorkSheet.Cells[2, 17] = "Exp Date";
            xlWorkSheet.Cells[2, 18] = "Emp No.";
            xlWorkSheet.Cells[2, 19] = "Machine";
            xlWorkSheet.Cells[2, 20] = "Emp No.";
            xlWorkSheet.Cells[2, 21] = "Machine";
            xlWorkSheet.Cells[2, 22] = "Green/Carcass";

            chartRange = xlWorkSheet.get_Range("T1", "V1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 20] = "Carcass";

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // Freeze rows            
            Excel.Window xlWnd3 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A2", "A2").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd3.FreezePanes = true;

            // AutoFilter            
            chartRange = xlWorkSheet.get_Range("A2", "V2");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("D1", "E1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 20;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 4;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 5;
            chartRange = xlWorkSheet.get_Range("J1", "M1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 5;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 12;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 30;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("S1", "S1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("T1", "T1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("U1", "U1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("V1", "V1");
            chartRange.ColumnWidth = 15;

            // שינוי מתאריך 29/09/2015
            // שליפת כל אוכלוסיית הנתונים בהתאם לדוח היומי - לבקשת יונה
            //StrSql = "Select bbloc,bbprod,bbserial,bbsize,bbdate,bbkgmf,bbkgac,bbstt,bbpgmde,BBEXDATE,BBKODPGM,BBEMPNO,BBOPUSER,BBMACH from bbn.bbn100p where bbdate between " + DTPicker_From.Value.ToString("yyMMdd") + " and " + DTPicker_To.Value.ToString("yyMMdd") + " and BBSTT <> 'T' UNION " +
            //         "Select bbloc,bbprod,bbserial,bbsize,bbdate,bbkgmf,bbkgac,bbstt,bbpgmde,BBEXDATE,BBKODPGM,BBEMPNO,BBOPUSER,BBMACH from bbn.bbn200p where bbdate between " + DTPicker_From.Value.ToString("yyMMdd") + " and " + DTPicker_To.Value.ToString("yyMMdd") + " and BBSTT <> 'T'";
            
            //StrSql = "Select bbloc,bbprod,bbserial,bbsize,bbdate,bbkgmf,bbkgac,bbstt,bbpgmde,BBEXDATE,BBKODPGM,BBEMPNO,BBOPUSER,BBMACH from bbn.bbn100p where bbdate between " + DTPicker_From.Value.ToString("yyMMdd") + " and " + DTPicker_To.Value.ToString("yyMMdd") + " " +
            //         "UNION Select bbloc,bbprod,bbserial,bbsize,bbdate,bbkgmf,bbkgac,bbstt,bbpgmde,BBEXDATE,BBKODPGM,BBEMPNO,BBOPUSER,BBMACH from bbn.bbn200p where bbdate between " + DTPicker_From.Value.ToString("yyMMdd") + " and " + DTPicker_To.Value.ToString("yyMMdd") + " "+
            //         "order by bbdate,bbprod,bbserial";
            StrSql = "Select BBLOC,BBPROD,BBSERIAL,BBSIZE,A.LACNDT,BBKGMF,BBKGAC,A.LSTT,value(TPGMDE,' '),BBEXDATE,substr(A.LERRDS,1,3),BBEMPNO,A.LUSER,BBMACH,B.LLBLNA " +
                     "from tapiali.labelgl70 A join BBN.BBN100P on A.LPROD=bbprod and A.llblno=bbserial left join bbn.tpgmbld on substr(A.LERRDS,1,3)=tpgmcod " +
                          "join TAPIALI.LABELL1 B on A.LPROD=B.LPROD and A.LLBLNO=B.LLBLNO " +
                     "where lacndt between " + DTPicker_From.Value.ToString("yyyyMMdd") + " and " + DTPicker_To.Value.ToString("yyyyMMdd") + " " +
               "union Select BBLOC,BBPROD,BBSERIAL,BBSIZE,A.LACNDT,BBKGMF,BBKGAC,A.LSTT,value(TPGMDE,' '),BBEXDATE,substr(A.LERRDS,1,3),BBEMPNO,A.LUSER,BBMACH,B.LLBLNA " +
                     "from tapiali.labelgl70 A join BBN.BBN200P on A.LPROD=bbprod and A.llblno=bbserial left join bbn.tpgmbld on substr(A.LERRDS,1,3)=tpgmcod " +
                          "join TAPIALI.LABELL1 B on A.LPROD=B.LPROD and A.LLBLNO=B.LLBLNO " +
                     "where lacndt between " + DTPicker_From.Value.ToString("yyyyMMdd") + " and " + DTPicker_To.Value.ToString("yyyyMMdd") + " " +
                     "order by BBPROD,BBSERIAL";
                     //"order by lprod,llblno,LSTTTM";
            //MessageBox.Show(StrSql);
            
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[i, 1] = DBC.Rs1.GetString(0);
                xlWorkSheet.Cells[i, 2] = DBC.Rs1.GetString(12).Trim();
                xlWorkSheet.Cells[i, 3] = DBC.Rs1.GetString(1);
                xlWorkSheet.Cells[i, 4] = DBC.Rs1.GetDecimal(2);
                xlWorkSheet.Cells[i, 5] = DBC.Rs1.GetDecimal(14);

                xlWorkSheet.Cells[i, 6] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[i, 7] = "1";                
                xlWorkSheet.Cells[i, 8] = DBC.Rs1.GetDecimal(4).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(4).ToString().Substring(4, 2) + "/20" + DBC.Rs1.GetDecimal(4).ToString().Substring(0, 4);
                xlWorkSheet.Cells[i, 9] = DBC.Rs1.GetDecimal(4).ToString().Substring(0, 4);
                xlWorkSheet.Cells[i, 10] = DBC.Rs1.GetDecimal(4).ToString().Substring(4, 2);
                xlWorkSheet.Cells[i, 11] = DBC.Rs1.GetDecimal(4).ToString().Substring(6, 2);
                xlWorkSheet.Cells[i, 12] = DBC.Rs1.GetDecimal(5);
                xlWorkSheet.Cells[i, 13] = DBC.Rs1.GetDecimal(6);

                if (DBC.Rs1.GetString(1).Trim().Substring(8, 2) == "-0")
                    xlWorkSheet.Cells[i, 22] = "Green";
                else
                    xlWorkSheet.Cells[i, 22] = "Carcass";

                if (DBC.Rs1.GetString(7) == "T") xlWorkSheet.Cells[i, 14] = "OK";
                else if (DBC.Rs1.GetString(7) == "P") xlWorkSheet.Cells[i, 14] = "SCRAP";
                else if (DBC.Rs1.GetString(7) == "S") xlWorkSheet.Cells[i, 14] = "STOP";
                xlWorkSheet.Cells[i, 15] = DBC.Rs1.GetString(10);
                xlWorkSheet.Cells[i, 16] = DBC.Rs1.GetString(8);
                if (DBC.Rs1.GetDecimal(9) == 0)
                    xlWorkSheet.Cells[i, 17] = "0";
                else
                {
                    TmpDT = DBC.Rs1.GetDecimal(9).ToString();
                    xlWorkSheet.Cells[i, 17] = TmpDT.Substring(4, 2) + "/" + TmpDT.Substring(2, 2) + "/20" + TmpDT.Substring(0, 2);
                }
                xlWorkSheet.Cells[i, 18] = DBC.Rs1.GetString(11).Trim();

                if (DBC.Rs1.GetString(13).Trim() == "")
                    xlWorkSheet.Cells[i, 19] = DBC.Rs1.GetString(0).Trim();
                else
                    xlWorkSheet.Cells[i, 19] = DBC.Rs1.GetString(13).Trim();

                // שליפת נתוני בנאי ומכונה שלב א                
                if (DBC.Rs1.GetString(1).Trim().Substring(8, 2) == "-0")
                {
                    StrSql = "Select KPROD1,KSERIAL1 from TAPIALI.LABKSRP " +
                             "where KPROD2='" + DBC.Rs1.GetString(1).Trim() + "' and KSERIAL2=" + DBC.Rs1.GetDecimal(2) + "";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        StrSql = "Select MACHINE,EMPLOYEE from STWIND.PRRP " +
                                 "where PRODUCT='" + DBC.Rs2.GetString(0).Trim() + "' and PRODUCTID= '" + DBC.Rs2.GetValue(1).ToString() + "'";
                        DBC.Rs2.Close();
                        DBC.Q_Run2(StrSql);
                        if (DBC.Rs2.Read())
                        {                            
                            xlWorkSheet.Cells[i, 20] = DBC.Rs2.GetString(1).Trim().Substring(2, 5);
                            xlWorkSheet.Cells[i, 21] = DBC.Rs2.GetString(0).Trim();
                        }
                        DBC.Rs2.Close();
                    }
                    DBC.Rs2.Close();
                }

                i++;
            }
            DBC.Rs1.Close();

            // לא בטוח שצאריכם אז ביטלתי
            //// תיקון מתאריך 30/07/2013
            //// גיליון חדש : הצגת צמיגים בסטטוס תקין אשר היה להם דיווח פגום לפני - לבקשת מיכאל אסייג מאבטחת איכות
            //i = 3;
            //xlWorkSheet2.Name = "QaStop All Repaired Tires";
            //xlWorkSheet2.Cells[1, 1] = "QaStop All Repaired Tires Report Between :  " + DTPicker_From.Value.ToString() + " and " + DTPicker_To.Value.ToString();

            //xlWorkSheet2.Cells[2, 1] = "Loc";
            //xlWorkSheet2.Cells[2, 2] = "Reporter";
            //xlWorkSheet2.Cells[2, 3] = "Catalog";
            //xlWorkSheet2.Cells[2, 4] = "Serial";
            //xlWorkSheet2.Cells[2, 5] = "Size";
            //xlWorkSheet2.Cells[2, 6] = "Qty";
            //xlWorkSheet2.Cells[2, 7] = "Date";
            //xlWorkSheet2.Cells[2, 8] = "Year";
            //xlWorkSheet2.Cells[2, 9] = "Month";
            //xlWorkSheet2.Cells[2, 10] = "Day";
            //xlWorkSheet2.Cells[2, 11] = "Spec Weight";
            //xlWorkSheet2.Cells[2, 12] = "Act Weight";
            //xlWorkSheet2.Cells[2, 13] = "Status";
            //xlWorkSheet2.Cells[2, 14] = "Emp No.";

            //// מירכוז + מיזוג + פונט מודגש לתחום            
            //chartRange = xlWorkSheet2.get_Range("A1", "D1");
            //chartRange.MergeCells = true;
            //chartRange.HorizontalAlignment = 3;
            //chartRange.VerticalAlignment = 3;
            //chartRange.Font.Bold = true;

            ////// Freeze rows            
            ////xlWnd3 = xlApp.ActiveWindow;
            ////chartRange = xlWorkSheet2.get_Range("A2", "A2").get_Offset(1, 0).EntireRow;
            ////chartRange.Select();
            ////xlWnd3.FreezePanes = true;

            //// AutoFilter            
            //chartRange = xlWorkSheet2.get_Range("A2", "N2");
            //chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            //// הגדרת רוחב עמודה של תחום            
            //chartRange = xlWorkSheet2.get_Range("A1", "A1");
            //chartRange.ColumnWidth = 6;
            //chartRange = xlWorkSheet2.get_Range("B1", "B1");
            //chartRange.ColumnWidth = 11;
            //chartRange = xlWorkSheet2.get_Range("C1", "C1");
            //chartRange.ColumnWidth = 14;
            //chartRange = xlWorkSheet2.get_Range("D1", "D1");
            //chartRange.ColumnWidth = 7;
            //chartRange = xlWorkSheet2.get_Range("E1", "E1");
            //chartRange.ColumnWidth = 20;
            //chartRange = xlWorkSheet2.get_Range("F1", "F1");
            //chartRange.ColumnWidth = 4;
            //chartRange = xlWorkSheet2.get_Range("G1", "G1");
            //chartRange.ColumnWidth = 11;
            //chartRange = xlWorkSheet2.get_Range("H1", "H1");
            //chartRange.ColumnWidth = 5;
            //chartRange = xlWorkSheet2.get_Range("I1", "I1");
            //chartRange.ColumnWidth = 7;
            //chartRange = xlWorkSheet2.get_Range("J1", "J1");
            //chartRange.ColumnWidth = 5;
            //chartRange = xlWorkSheet2.get_Range("K1", "K1");
            //chartRange.ColumnWidth = 11;
            //chartRange = xlWorkSheet2.get_Range("L1", "L1");
            //chartRange.ColumnWidth = 10;
            //chartRange = xlWorkSheet2.get_Range("M1", "M1");
            //chartRange.ColumnWidth = 7;
            //chartRange = xlWorkSheet2.get_Range("N1", "N1");
            //chartRange.ColumnWidth = 8;
            //// TAPIALI.LABELGL70  --> Qastop קובץ לוגי המכיל נתוני לוג צמיגים במערכת 
            //StrSql = "select LPROD||LLBLNO,LERRDS,LPROD,LLBLNO,INSIZ,LACNDT,LKGMF,LKGAC,LSTT,LOVED,LUSER " +
            //         "from TAPIALI.LABELGL70 join BPCSFALI.IIMN on substr(LPROD,1,8)=INPROD " +
            //         "where LACNDT between " + DTPicker_From.Value.ToString("yyyyMMdd") + " and " + DTPicker_To.Value.ToString("yyyyMMdd") + " and LSTT='T' and " +
            //               "substring(LUSER,1,3) in ('BBK','BBE') and LPROD||LLBLNO in (select LPROD||LLBLNO " +
            //                                 "from TAPIALI.LABELGL70 " +
            //                                 "where LSTT in ('P','S') and " +
            //                                       "LACNDT between " + DTPicker_From.Value.AddMonths(-1).ToString("yyyyMMdd") + " and " +
            //                                                      "" + DTPicker_To.Value.ToString("yyyyMMdd") + " " +
            //                                 "group by LPROD||LLBLNO)";           
            //DBC.Q_Run1(StrSql);
            //while (DBC.Rs1.Read())
            //{                
            //    if (DBC.Rs1.IsDBNull(1) || DBC.Rs1.GetString(1).Trim() == "")
            //        xlWorkSheet2.Cells[i, 1] = "";                
            //    else
            //        xlWorkSheet2.Cells[i, 1] = DBC.Rs1.GetString(1).Substring(4, 3).Trim();                
                
            //    xlWorkSheet2.Cells[i, 2] = DBC.Rs1.GetString(10).Trim();
            //    xlWorkSheet2.Cells[i, 3] = DBC.Rs1.GetString(2);
            //    xlWorkSheet2.Cells[i, 4] = DBC.Rs1.GetDecimal(3);
            //    xlWorkSheet2.Cells[i, 5] = DBC.Rs1.GetString(4);
            //    xlWorkSheet2.Cells[i, 6] = "1";
            //    xlWorkSheet2.Cells[i, 7] = DBC.Rs1.GetDecimal(5).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(5).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(5).ToString().Substring(0, 4);
            //    xlWorkSheet2.Cells[i, 8] = DBC.Rs1.GetDecimal(5).ToString().Substring(0, 4);
            //    xlWorkSheet2.Cells[i, 9] = DBC.Rs1.GetDecimal(5).ToString().Substring(4, 2);
            //    xlWorkSheet2.Cells[i, 10] = DBC.Rs1.GetDecimal(5).ToString().Substring(6, 2);
            //    xlWorkSheet2.Cells[i, 11] = DBC.Rs1.GetDecimal(6);
            //    xlWorkSheet2.Cells[i, 12] = DBC.Rs1.GetDecimal(7);
            //    if (DBC.Rs1.GetString(8) == "T") xlWorkSheet2.Cells[i, 13] = "OK";
            //    else if (DBC.Rs1.GetString(8) == "P") xlWorkSheet2.Cells[i, 13] = "SCRAP";
            //    else if (DBC.Rs1.GetString(8) == "S") xlWorkSheet2.Cells[i, 13] = "STOP";               
            //    xlWorkSheet2.Cells[i, 14] = DBC.Rs1.GetString(9).Trim();
            //    i++;
            //}
            //DBC.Rs1.Close();



            DBC.Close_Conn();
            Cursor.Current = Cursors.PanEast;
            xlApp.Visible = true;

            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
