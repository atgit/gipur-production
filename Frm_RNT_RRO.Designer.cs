﻿namespace Gipur_Production
{
    partial class Frm_RNT_RRO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_RNT_RRO));
            this.label1 = new System.Windows.Forms.Label();
            this.ComboB_RNTRRO = new System.Windows.Forms.ComboBox();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.DTPicker_To = new System.Windows.Forms.DateTimePicker();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.DTPicker_From = new System.Windows.Forms.DateTimePicker();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CheckB_Checked = new System.Windows.Forms.CheckBox();
            this.CheckB_WaitingList = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.SlateGray;
            this.label1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(36, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 24);
            this.label1.TabIndex = 36;
            this.label1.Text = "Filtering";
            // 
            // ComboB_RNTRRO
            // 
            this.ComboB_RNTRRO.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.ComboB_RNTRRO.FormattingEnabled = true;
            this.ComboB_RNTRRO.Items.AddRange(new object[] {
            "All",
            "RNT",
            "RRO",
            "RNT&RRO"});
            this.ComboB_RNTRRO.Location = new System.Drawing.Point(125, 123);
            this.ComboB_RNTRRO.Name = "ComboB_RNTRRO";
            this.ComboB_RNTRRO.Size = new System.Drawing.Size(114, 26);
            this.ComboB_RNTRRO.TabIndex = 33;
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(80, 220);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(123, 28);
            this.Btn_OK.TabIndex = 36;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // DTPicker_To
            // 
            this.DTPicker_To.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_To.Location = new System.Drawing.Point(125, 90);
            this.DTPicker_To.Name = "DTPicker_To";
            this.DTPicker_To.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_To.TabIndex = 32;
            // 
            // Lbl_To
            // 
            this.Lbl_To.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_To.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(36, 92);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.Size = new System.Drawing.Size(82, 24);
            this.Lbl_To.TabIndex = 35;
            this.Lbl_To.Text = "To";
            // 
            // DTPicker_From
            // 
            this.DTPicker_From.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_From.Location = new System.Drawing.Point(125, 56);
            this.DTPicker_From.Name = "DTPicker_From";
            this.DTPicker_From.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_From.TabIndex = 31;
            // 
            // Lbl_From
            // 
            this.Lbl_From.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_From.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(36, 57);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.Size = new System.Drawing.Size(82, 24);
            this.Lbl_From.TabIndex = 30;
            this.Lbl_From.Text = "From";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(49, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 29);
            this.label2.TabIndex = 29;
            this.label2.Text = "RNT / RRO";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CheckB_Checked
            // 
            this.CheckB_Checked.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CheckB_Checked.BackColor = System.Drawing.Color.SlateGray;
            this.CheckB_Checked.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CheckB_Checked.Location = new System.Drawing.Point(68, 191);
            this.CheckB_Checked.Name = "CheckB_Checked";
            this.CheckB_Checked.Size = new System.Drawing.Size(147, 27);
            this.CheckB_Checked.TabIndex = 35;
            this.CheckB_Checked.Text = "Checked";
            this.CheckB_Checked.UseVisualStyleBackColor = false;
            // 
            // CheckB_WaitingList
            // 
            this.CheckB_WaitingList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CheckB_WaitingList.BackColor = System.Drawing.Color.SlateGray;
            this.CheckB_WaitingList.Checked = true;
            this.CheckB_WaitingList.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckB_WaitingList.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CheckB_WaitingList.Location = new System.Drawing.Point(68, 163);
            this.CheckB_WaitingList.Name = "CheckB_WaitingList";
            this.CheckB_WaitingList.Size = new System.Drawing.Size(147, 27);
            this.CheckB_WaitingList.TabIndex = 34;
            this.CheckB_WaitingList.Text = "Waiting List";
            this.CheckB_WaitingList.UseVisualStyleBackColor = false;
            // 
            // Frm_RNT_RRO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.CheckB_Checked);
            this.Controls.Add(this.CheckB_WaitingList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboB_RNTRRO);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.DTPicker_To);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DTPicker_From);
            this.Controls.Add(this.Lbl_From);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_RNT_RRO";
            this.Text = "RNT / RRO";
            this.Load += new System.EventHandler(this.Frm_RNT_RRO_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboB_RNTRRO;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.DateTimePicker DTPicker_To;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.DateTimePicker DTPicker_From;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox CheckB_Checked;
        private System.Windows.Forms.CheckBox CheckB_WaitingList;
    }
}