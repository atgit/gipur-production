﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_RNT_RRO : Form
    {
        // תיקון מתאריך 09/02/2014
        // RNT OR RRO הוספת דוח צמיגים הממתינים  או שעברו בדיקת 
        // לבקשת דיווה ההודי

        // יצירת קובץ אקסל
        // Add reference -> com -> Microsoft Excel 12.0 Object Library            
        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet1, xlWorkSheet2;
        object misValue = System.Reflection.Missing.Value;
        Excel.Range chartRange;

        DBConn DBC;
        string Con_Str;
        string StrSql = "";

        long I = 0;

        public Frm_RNT_RRO()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {            
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("Error : Begining Date Bigger Than End Date", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }

            if (ComboB_RNTRRO.Text.Trim() == "")
            {
                MessageBox.Show("Error : Please Use Filtering.", "Curing / Building Production");
                ComboB_RNTRRO.Focus();
                return;
            }

            if (!CheckB_WaitingList.Checked && !CheckB_Checked.Checked)
            {
                MessageBox.Show("Error : Must Choose Waiting List Or Checked Or Both.", "Curing / Building Production");
                CheckB_WaitingList.Focus(); 
                return;
            }

            RntRro_Report();
        }

        private void RntRro_Report()
        {
            Cursor.Current = Cursors.WaitCursor;            
            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);            

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            if (CheckB_WaitingList.Checked && CheckB_Checked.Checked)
            {
                // Office 2013 עקב שדרוג ל 
                //xlWorkSheet1 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                //xlWorkSheet2 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);                
                xlWorkSheet2 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                xlWorkSheet1 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                RntRro_WaitingList(xlWorkSheet1);
                RntRro_Checked(xlWorkSheet2);
            }
            else 
            {
                xlWorkSheet1 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);                
                if (CheckB_WaitingList.Checked)
                    RntRro_WaitingList(xlWorkSheet1);
                else
                    RntRro_Checked(xlWorkSheet1);
            }

            DBC.Close_Conn();
            xlApp.Visible = true;

            if (CheckB_WaitingList.Checked && CheckB_Checked.Checked)
            {
                releaseObject(xlWorkSheet1);
                releaseObject(xlWorkSheet2);
            }
            else
                releaseObject(xlWorkSheet1);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void RntRro_WaitingList(Excel.Worksheet WS)
        {
            double Tire_RNT = 0;
            double Tire_RRO = 0;
            double Tire_RNTRRO = 0;
            string DtTO = "";
            string DtFrom = "";
            int y = 0;

            DtFrom = DTPicker_From.Value.Year.ToString("0000") + DTPicker_From.Value.Month.ToString("00") + DTPicker_From.Value.Day.ToString("00");
            DtTO = DTPicker_To.Value.Year.ToString("0000") + DTPicker_To.Value.Month.ToString("00") + DTPicker_To.Value.Day.ToString("00");            
            I = 4;

            WS.Name = "RNT,RRO Waiting List";
            WS.Cells[1, 1] = "RNT,RRO Waiting List Between : " + DTPicker_From.Value.ToString().Substring(0, 10) + " And " + DTPicker_To.Value.ToString().Substring(0, 10);

            WS.Cells[3, 1] = "Date";
            WS.Cells[3, 2] = "Shift";
            WS.Cells[3, 3] = "Time";
            WS.Cells[3, 4] = "Size";
            WS.Cells[3, 5] = "Catalog";
            WS.Cells[3, 6] = "Serial";
            WS.Cells[3, 7] = "RNT/RRO";

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            WS.DisplayRightToLeft = false;

            // AutoFilter
            chartRange = WS.get_Range("A3", "G3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // Freeze rows            
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = WS.get_Range("A3", "A3").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd1.FreezePanes = true;

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = WS.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + פונט מודגש לתחום            
            chartRange = WS.get_Range("A3", "G3");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = WS.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            // הגדרת רוחב עמודה של תחום            
            chartRange = WS.get_Range("A1", "C1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;            

            
            // תיקון מתאריך 10/03/2014
            // עקב הופעת מק"ט + סידורי כמה פעמים, הדוח אמור להציג בסופו של דבר
            // רק רשומה אחת למק"ט + סידורי מאחר וההתייחסות בדוח לכמות צמיגים
            StrSql = "select max(LACNDT),max(LSHIFT),max(LACNTM),max(INSIZ),max(LPROD),max(LLBLNO),max(substr(EFIL3,2,1)),max(substr(EFIL3,3,1)),LPROD||LLBLNO " +
                     "from TAPIALI.LABELGP join " +
                          "BPCSFALI.IIMNL01 on substr(LPROD,1,8)=INPROD join " +
                          "BPCSFALI.IIMNEL01 on substr(LPROD,1,8)=substr(EMKT15,1,8) " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and " +
                     "(substr(EFIL3,2,1)='Y' and LPROD||LLBLNO not in (select LPROD||LLBLNO from TAPIALI.LABELGP where LACTAN=46) or " +
                      "substr(EFIL3,3,1)='Y' and LPROD||LLBLNO not in (select LPROD||LLBLNO from TAPIALI.LABELGP where LACTAN=49)) " +
                     "group by LPROD||LLBLNO " +
                     "order by max(LSHIFT),max(LACNTM)";
            //StrSql = "select LACNDT,LSHIFT,LACNTM,INSIZ,LPROD,LLBLNO,substr(EFIL3,2,1),substr(EFIL3,3,1) " +
            //         "from TAPIALI.LABELGP join " +
            //              "BPCSFALI.IIMN on substr(LPROD,1,8)=INPROD join " +
            //              "BPCSFALI.IIMNEL01 on substr(LPROD,1,8)=substr(EMKT15,1,8) " +
            //         "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and " +
            //         "(substr(EFIL3,2,1)='Y' and LPROD||LLBLNO not in (select LPROD||LLBLNO from TAPIALI.LABELGP where LACTAN=46) or " +
            //          "substr(EFIL3,3,1)='Y' and LPROD||LLBLNO not in (select LPROD||LLBLNO from TAPIALI.LABELGP where LACTAN=49)) " +
            //         "order by LSHIFT,LACNTM";            

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                WS.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                WS.Cells[I, 2] = DBC.Rs1.GetString(1);
                if (DBC.Rs1.GetString(2).Trim().Length == 1)
                    WS.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 2)
                    WS.Cells[I, 3] = "00:00:" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 3)
                    WS.Cells[I, 3] = "00:0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 4)
                    WS.Cells[I, 3] = "00:" + DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 5)
                    WS.Cells[I, 3] = "0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(3, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 6)
                    WS.Cells[I, 3] = DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(4, 2);

                WS.Cells[I, 4] = DBC.Rs1.GetString(3);
                WS.Cells[I, 5] = DBC.Rs1.GetString(4).Trim();
                WS.Cells[I, 6] = DBC.Rs1.GetString(5);
                
                if (ComboB_RNTRRO.Text.Trim().ToUpper() == "ALL")
                {
                    if (DBC.Rs1.GetString(6) == "Y" && DBC.Rs1.GetString(7) == "Y")
                    {
                        WS.Cells[I, 7] = "RNT&RRO";
                        Tire_RNTRRO++;
                    }
                    else if (DBC.Rs1.GetString(6) == "Y")
                    {
                        WS.Cells[I, 7] = "RNT";
                        Tire_RNT++;
                    }
                    else if (DBC.Rs1.GetString(7) == "Y")
                    {
                        WS.Cells[I, 7] = "RRO";
                        Tire_RRO++;
                    }
                    I++;
                }
                else if (ComboB_RNTRRO.Text.Trim().ToUpper() == "RNT&RRO")
                {
                    if (DBC.Rs1.GetString(6) == "Y" && DBC.Rs1.GetString(7) == "Y")
                    {
                        WS.Cells[I, 7] = "RNT&RRO";
                        Tire_RNTRRO++;
                        I++;
                    }
                    else
                    {
                        for (y = 1; y <= 6; y++)
                        {
                            WS.Cells[I, y] = "";
                        }
                    }
                }
                else if (ComboB_RNTRRO.Text.Trim().ToUpper() == "RNT")
                {
                    if (DBC.Rs1.GetString(6) == "Y" && DBC.Rs1.GetString(7) != "Y")
                    {
                        WS.Cells[I, 7] = "RNT";
                        Tire_RNT++;
                        I++;
                    }
                    else
                    {
                        for (y = 1; y <= 6; y++)
                        {
                            WS.Cells[I, y] = "";
                        }
                    }
                }
                else if (ComboB_RNTRRO.Text.Trim().ToUpper() == "RRO")
                {
                    if (DBC.Rs1.GetString(7) == "Y" && DBC.Rs1.GetString(6) != "Y")
                    {
                        WS.Cells[I, 7] = "RRO";
                        Tire_RRO++;
                        I++;
                    }
                    else
                    {
                        for (y = 1; y <= 6; y++)
                        {
                            WS.Cells[I, y] = "";
                        }
                    }
                }                
            }
            DBC.Rs1.Close();

            // מירכוז + פונט מודגש לתחום            
            chartRange = WS.get_Range("I1", "M3");
            chartRange.ColumnWidth = 7;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = WS.get_Range("K1", "K1");
            chartRange.ColumnWidth = 10;

            WS.Cells[1, 9] = "RNT";
            WS.Cells[1, 10] = "RRO";
            WS.Cells[1, 11] = "RNT&RRO";
            WS.Cells[1, 12] = "Total";
            WS.Cells[2, 9] = Tire_RNT;
            WS.Cells[2, 10] = Tire_RRO;
            WS.Cells[2, 11] = Tire_RNTRRO;
            WS.Cells[2, 12] = "=I2+J2+K2";
            WS.Cells[3, 9] = Math.Round((Tire_RNT / (Tire_RNT + Tire_RRO + Tire_RNTRRO)) * 100, 1);
            WS.Cells[3, 10] = Math.Round((Tire_RRO / (Tire_RNT + Tire_RRO + Tire_RNTRRO)) * 100, 1);
            WS.Cells[3, 11] = Math.Round((Tire_RNTRRO / (Tire_RNT + Tire_RRO + Tire_RNTRRO)) * 100, 1);
            WS.Cells[3, 12] = "=I3+J3+K3";
            WS.Cells[3, 13] = "(%)";            
        }

        private void RntRro_Checked(Excel.Worksheet WS)
        {
            double Tire_RNT = 0;
            double Tire_RRO = 0;
            double Tire_RNTRRO = 0;
            string DtTO = "";
            string DtFrom = "";
            int y = 0;

            DtFrom = DTPicker_From.Value.Year.ToString("0000") + DTPicker_From.Value.Month.ToString("00") + DTPicker_From.Value.Day.ToString("00");
            DtTO = DTPicker_To.Value.Year.ToString("0000") + DTPicker_To.Value.Month.ToString("00") + DTPicker_To.Value.Day.ToString("00");
            I = 4;

            WS.Name = "RNT,RRO Checked";
            WS.Cells[1, 1] = "RNT,RRO Checked Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " And " + DTPicker_To.Value.ToString().Substring(0, 10);

            WS.Cells[3, 1] = "Date";
            WS.Cells[3, 2] = "Shift";
            WS.Cells[3, 3] = "Time";
            WS.Cells[3, 4] = "Size";
            WS.Cells[3, 5] = "Catalog";
            WS.Cells[3, 6] = "Serial";
            WS.Cells[3, 7] = "RNT/RRO";            

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            WS.DisplayRightToLeft = false;

            // AutoFilter
            chartRange = WS.get_Range("A3", "G3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            if (CheckB_WaitingList.Checked && CheckB_Checked.Checked)
            {
            }
            else
            {
                // Freeze rows            
                Excel.Window xlWnd1 = xlApp.ActiveWindow;
                chartRange = WS.get_Range("A3", "A3").get_Offset(1, 0).EntireRow;
                chartRange.Select();
                xlWnd1.FreezePanes = true;
            }

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = WS.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + פונט מודגש לתחום            
            chartRange = WS.get_Range("A3", "G3");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = WS.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            // הגדרת רוחב עמודה של תחום            
            chartRange = WS.get_Range("A1", "C1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;

            // תיקון מתאריך 10/03/2014
            // עקב הופעת מק"ט + סידורי כמה פעמים, הדוח אמור להציג בסופו של דבר
            // רק רשומה אחת למק"ט + סידורי מאחר וההתייחסות בדוח לכמות צמיגים            
            StrSql = "select max(LACNDT),max(LSHIFT),max(LACNTM),max(INSIZ),max(LPROD),max(LLBLNO),max(substr(EFIL3,2,1)),max(substr(EFIL3,3,1)),LPROD||LLBLNO " +
                     "from TAPIALI.LABELGP join " +
                          "BPCSFALI.IIMNL01 on substr(LPROD,1,8)=INPROD join " +
                          "BPCSFALI.IIMNEL01 on substr(LPROD,1,8)=substr(EMKT15,1,8) " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and " +
                      "(substr(EFIL3,2,1)='Y' and LPROD||LLBLNO in (select LPROD||LLBLNO from TAPIALI.LABELGP where LACTAN=46) or " +
                       "substr(EFIL3,3,1)='Y' and LPROD||LLBLNO in (select LPROD||LLBLNO from TAPIALI.LABELGP where LACTAN=49)) " +
                     "group by LPROD||LLBLNO " +
                     "order by max(LSHIFT),max(LACNTM)";

            //StrSql = "select LACNDT,LSHIFT,LACNTM,INSIZ,LPROD,LLBLNO,substr(EFIL3,2,1),substr(EFIL3,3,1) " +
            //         "from TAPIALI.LABELGP join " +
            //              "BPCSFALI.IIMN on substr(LPROD,1,8)=INPROD join " +
            //              "BPCSFALI.IIMNEL01 on substr(LPROD,1,8)=substr(EMKT15,1,8) " +
            //         "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and " +
            //         "(substr(EFIL3,2,1)='Y' and LPROD||LLBLNO in (select LPROD||LLBLNO from TAPIALI.LABELGP where LACTAN=46) or " +
            //          "substr(EFIL3,3,1)='Y' and LPROD||LLBLNO in (select LPROD||LLBLNO from TAPIALI.LABELGP where LACTAN=49)) " +
            //         "order by LSHIFT,LACNTM";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                WS.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                WS.Cells[I, 2] = DBC.Rs1.GetString(1);
                if (DBC.Rs1.GetString(2).Trim().Length == 1)
                    WS.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 2)
                    WS.Cells[I, 3] = "00:00:" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 3)
                    WS.Cells[I, 3] = "00:0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 4)
                    WS.Cells[I, 3] = "00:" + DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 5)
                    WS.Cells[I, 3] = "0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(3, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 6)
                    WS.Cells[I, 3] = DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(4, 2);

                WS.Cells[I, 4] = DBC.Rs1.GetString(3);
                WS.Cells[I, 5] = DBC.Rs1.GetString(4).Trim();
                WS.Cells[I, 6] = DBC.Rs1.GetString(5);

                if (ComboB_RNTRRO.Text.Trim().ToUpper() == "ALL")
                {
                    if (DBC.Rs1.GetString(6) == "Y" && DBC.Rs1.GetString(7) == "Y")
                    {
                        WS.Cells[I, 7] = "RNT&RRO";
                        Tire_RNTRRO++;
                    }
                    else if (DBC.Rs1.GetString(6) == "Y")
                    {
                        WS.Cells[I, 7] = "RNT";
                        Tire_RNT++;
                    }
                    else if (DBC.Rs1.GetString(7) == "Y")
                    {
                        WS.Cells[I, 7] = "RRO";
                        Tire_RRO++;
                    }
                    I++;
                }
                else if (ComboB_RNTRRO.Text.Trim().ToUpper() == "RNT&RRO")
                {
                    if (DBC.Rs1.GetString(6) == "Y" && DBC.Rs1.GetString(7) == "Y")
                    {
                        WS.Cells[I, 7] = "RNT&RRO";
                        Tire_RNTRRO++;
                        I++;
                    }
                    else
                    {
                        for (y = 1; y <= 6; y++)
                        {
                            WS.Cells[I, y] = "";
                        }
                    }
                }
                else if (ComboB_RNTRRO.Text.Trim().ToUpper() == "RNT")
                {
                    if (DBC.Rs1.GetString(6) == "Y" && DBC.Rs1.GetString(7) != "Y")
                    {
                        WS.Cells[I, 7] = "RNT";
                        Tire_RNT++;
                        I++;
                    }
                    else
                    {
                        for (y = 1; y <= 6; y++)
                        {
                            WS.Cells[I, y] = "";
                        }
                    }
                }
                else if (ComboB_RNTRRO.Text.Trim().ToUpper() == "RRO")
                {
                    if (DBC.Rs1.GetString(7) == "Y" && DBC.Rs1.GetString(6) != "Y")
                    {
                        WS.Cells[I, 7] = "RRO";
                        Tire_RRO++;
                        I++;
                    }
                    else
                    {
                        for (y = 1; y <= 6; y++)
                        {
                            WS.Cells[I, y] = "";
                        }
                    }
                }                
            }
            DBC.Rs1.Close();

            // מירכוז + פונט מודגש לתחום            
            chartRange = WS.get_Range("I1", "M3");
            chartRange.ColumnWidth = 7;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = WS.get_Range("K1", "K1");
            chartRange.ColumnWidth = 10;

            WS.Cells[1, 9] = "RNT";
            WS.Cells[1, 10] = "RRO";
            WS.Cells[1, 11] = "RNT&RRO";
            WS.Cells[1, 12] = "Total";
            WS.Cells[2, 9] = Tire_RNT;
            WS.Cells[2, 10] = Tire_RRO;
            WS.Cells[2, 11] = Tire_RNTRRO;
            WS.Cells[2, 12] = "=I2+J2+K2";
            WS.Cells[3, 9] = Math.Round((Tire_RNT / (Tire_RNT + Tire_RRO + Tire_RNTRRO)) * 100, 1);
            WS.Cells[3, 10] = Math.Round((Tire_RRO / (Tire_RNT + Tire_RRO + Tire_RNTRRO)) * 100, 1);
            WS.Cells[3, 11] = Math.Round((Tire_RNTRRO / (Tire_RNT + Tire_RRO + Tire_RNTRRO)) * 100, 1);
            WS.Cells[3, 12] = "=I3+J3+K3";
            WS.Cells[3, 13] = "(%)";            
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Frm_RNT_RRO_Load(object sender, EventArgs e)
        {            
            //DTPicker_From.MinDate = DateTime.Now.AddMonths(-1);
            DTPicker_From.MaxDate = DateTime.Now;
            //DTPicker_To.MinDate = DateTime.Now.AddMonths(-1);
            DTPicker_To.MaxDate = DateTime.Now;   
        }
    }
}
