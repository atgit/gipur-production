﻿namespace Gipur_Production
{
    partial class Frm_RNT_RRO_TK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_RNT_RRO_TK));
            this.CheckB_RRO = new System.Windows.Forms.CheckBox();
            this.CheckB_RNT = new System.Windows.Forms.CheckBox();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.DTPicker_To = new System.Windows.Forms.DateTimePicker();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.DTPicker_From = new System.Windows.Forms.DateTimePicker();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CheckB_TK = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // CheckB_RRO
            // 
            this.CheckB_RRO.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CheckB_RRO.BackColor = System.Drawing.Color.SlateGray;
            this.CheckB_RRO.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CheckB_RRO.Location = new System.Drawing.Point(45, 151);
            this.CheckB_RRO.Name = "CheckB_RRO";
            this.CheckB_RRO.Size = new System.Drawing.Size(147, 27);
            this.CheckB_RRO.TabIndex = 43;
            this.CheckB_RRO.Text = "RRO";
            this.CheckB_RRO.UseVisualStyleBackColor = false;
            // 
            // CheckB_RNT
            // 
            this.CheckB_RNT.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CheckB_RNT.BackColor = System.Drawing.Color.SlateGray;
            this.CheckB_RNT.Checked = true;
            this.CheckB_RNT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckB_RNT.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CheckB_RNT.Location = new System.Drawing.Point(45, 123);
            this.CheckB_RNT.Name = "CheckB_RNT";
            this.CheckB_RNT.Size = new System.Drawing.Size(147, 27);
            this.CheckB_RNT.TabIndex = 42;
            this.CheckB_RNT.Text = "RNT";
            this.CheckB_RNT.UseVisualStyleBackColor = false;
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(85, 219);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(123, 28);
            this.Btn_OK.TabIndex = 46;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // DTPicker_To
            // 
            this.DTPicker_To.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_To.Location = new System.Drawing.Point(130, 89);
            this.DTPicker_To.Name = "DTPicker_To";
            this.DTPicker_To.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_To.TabIndex = 40;
            // 
            // Lbl_To
            // 
            this.Lbl_To.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_To.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(41, 91);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.Size = new System.Drawing.Size(82, 24);
            this.Lbl_To.TabIndex = 44;
            this.Lbl_To.Text = "To";
            // 
            // DTPicker_From
            // 
            this.DTPicker_From.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_From.Location = new System.Drawing.Point(130, 55);
            this.DTPicker_From.Name = "DTPicker_From";
            this.DTPicker_From.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_From.TabIndex = 39;
            // 
            // Lbl_From
            // 
            this.Lbl_From.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_From.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(41, 56);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.Size = new System.Drawing.Size(82, 24);
            this.Lbl_From.TabIndex = 38;
            this.Lbl_From.Text = "From";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(49, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 29);
            this.label2.TabIndex = 37;
            this.label2.Text = "RNT / RRO / TK";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CheckB_TK
            // 
            this.CheckB_TK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CheckB_TK.BackColor = System.Drawing.Color.SlateGray;
            this.CheckB_TK.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CheckB_TK.Location = new System.Drawing.Point(45, 178);
            this.CheckB_TK.Name = "CheckB_TK";
            this.CheckB_TK.Size = new System.Drawing.Size(147, 27);
            this.CheckB_TK.TabIndex = 47;
            this.CheckB_TK.Text = "TK";
            this.CheckB_TK.UseVisualStyleBackColor = false;
            // 
            // Frm_RNT_RRO_TK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.CheckB_TK);
            this.Controls.Add(this.CheckB_RRO);
            this.Controls.Add(this.CheckB_RNT);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.DTPicker_To);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DTPicker_From);
            this.Controls.Add(this.Lbl_From);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Frm_RNT_RRO_TK";
            this.Text = "RNT / RRO / TK";
            this.Load += new System.EventHandler(this.Frm_RNT_RRO_TK_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox CheckB_RRO;
        private System.Windows.Forms.CheckBox CheckB_RNT;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.DateTimePicker DTPicker_To;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.DateTimePicker DTPicker_From;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox CheckB_TK;
    }
}