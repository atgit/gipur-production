﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_RNT_RRO_TK : Form
    {

        // תיקון מתאריך 11/04/2016
        // כניסה יציאה RNT OR RRO הוספת דוח צמיגים  
        // לבקשת קובי מתפ"י ובוריס עוזר מנכ"ל

        // יצירת קובץ אקסל
        // Add reference -> com -> Microsoft Excel 12.0 Object Library            
        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet1, xlWorkSheet2, xlWorkSheet3,
                        xlWorkSheet4, xlWorkSheet5, xlWorkSheet6,
                        xlWorkSheet7, xlWorkSheet8, xlWorkSheet9, xlWorkSheet10;
        object misValue = System.Reflection.Missing.Value;
        Excel.Range chartRange;

        DBConn DBC;
        string Con_Str;
        string StrSql = "";

        long I = 0;
        string DtTO = "";
        string DtFrom = "";

        public Frm_RNT_RRO_TK()
        {
            InitializeComponent();
        }

        private void Frm_RNT_RRO_TK_Load(object sender, EventArgs e)
        {
            DTPicker_From.MaxDate = DateTime.Now;
            DTPicker_To.MaxDate = DateTime.Now;
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("Error : Begining Date Bigger Than End Date", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }
            else if (!CheckB_RNT.Checked && !CheckB_RRO.Checked && !CheckB_TK.Checked)
            {
                MessageBox.Show("Error : Must Choose at Least one of the options RNT / RRO / TK.", "Curing / Building Production");
                CheckB_RNT.Focus();
                return;
            }

            RNT_RRO_TK_Report();
        }

        private void RNT_RRO_TK_Report()
        {
            Cursor.Current = Cursors.WaitCursor;
            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);            

            DtFrom = DTPicker_From.Value.Year.ToString("0000") + DTPicker_From.Value.Month.ToString("00") + DTPicker_From.Value.Day.ToString("00");
            DtTO = DTPicker_To.Value.Year.ToString("0000") + DTPicker_To.Value.Month.ToString("00") + DTPicker_To.Value.Day.ToString("00");            

            if (CheckB_RNT.Checked)                                            
                RNT_Report();
            if (CheckB_RRO.Checked)
                RRO_Report();
            if (CheckB_TK.Checked)
                TK_Report();


            xlApp.Visible = true;

            if (CheckB_RNT.Checked)
            {
                releaseObject(xlWorkSheet1);
                releaseObject(xlWorkSheet2);
                releaseObject(xlWorkSheet3);
            }
            if (CheckB_RRO.Checked)
            {
                releaseObject(xlWorkSheet4);
                releaseObject(xlWorkSheet5);
                releaseObject(xlWorkSheet6);
            }
            if (CheckB_TK.Checked)
            {
                releaseObject(xlWorkSheet7);
                releaseObject(xlWorkSheet8);
                releaseObject(xlWorkSheet9);
            }

            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void RNT_Report()
        {
            // דוח רנטגן
            string LastDt = "";
            int RecCount = 0;
            int I_Ar = 0;
            bool Flg_DateInArr = false;

            string[,] Arr = new string[3, 400];
            // Arr[0, 0] --> תאריך
            // Arr[1, 0] --> כניסות
            // Arr[2, 0] --> יציאות

            for (I = 0; I < 400; I++)
            {
                Arr[0, I] = "";
                Arr[1, I] = "";
                Arr[2, I] = "";
            }

            xlWorkSheet1 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet2 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet3 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            // יציאות
            I = 2;
            xlWorkSheet3.Cells[1, 1] = "Date";
            xlWorkSheet3.Cells[1, 2] = "Shift";
            xlWorkSheet3.Cells[1, 3] = "Time";
            xlWorkSheet3.Cells[1, 4] = "Size";
            xlWorkSheet3.Cells[1, 5] = "Catalog";
            xlWorkSheet3.Cells[1, 6] = "Serial";
            xlWorkSheet3.Cells[1, 7] = "Emp";

            // תוספת מתאריך 24/07/2016
            // הוספת נתונים לדוח לבקשת אייל קרן
            // הוספת הנתון חוזק צמיג אשר נמדד בתחנות רנטגן או רנאווט + קוד תערוסת
            xlWorkSheet3.Cells[1, 8] = "Shore Value";
            xlWorkSheet3.Cells[1, 9] = "Compound ID";

            xlWorkSheet3.Name = "RNT Out";
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet3.get_Range("A1", "I1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet3.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            chartRange = xlWorkSheet3.get_Range("H1", "I1");
            chartRange.ColumnWidth = 13;

            chartRange = xlWorkSheet3.get_Range("A2", "A10000");
            chartRange.NumberFormat = "@";

            StrSql = "select max(LACNDT),max(LSHIFT),max(LACNTM),max(INSIZ),max(LPROD),max(LLBLNO),LPROD||LLBLNO,max(LOVED),max(LERRDS) " +
                     "from TAPIALI.LABELGP join " +
                          "BPCSFALI.IIMNL01 on substr(LPROD,1,8)=INPROD " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=46 " +
                     "group by LPROD||LLBLNO " +
                     "order by max(LACNDT),max(LSHIFT),max(LACNTM)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (LastDt.Trim() == "")
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);

                if (LastDt == DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4))
                {
                    RecCount++;
                }
                else
                {
                    Arr[0, I_Ar] = LastDt;
                    Arr[1, I_Ar] = "0";
                    Arr[2, I_Ar] = RecCount.ToString();
                    I_Ar++;
                    RecCount = 1;
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                }

                xlWorkSheet3.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet3.Cells[I, 2] = DBC.Rs1.GetString(1);
                if (DBC.Rs1.GetString(2).Trim().Length == 1)
                    xlWorkSheet3.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 2)
                    xlWorkSheet3.Cells[I, 3] = "00:00:" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 3)
                    xlWorkSheet3.Cells[I, 3] = "00:0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 4)
                    xlWorkSheet3.Cells[I, 3] = "00:" + DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 5)
                    xlWorkSheet3.Cells[I, 3] = "0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(3, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 6)
                    xlWorkSheet3.Cells[I, 3] = DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(4, 2);

                xlWorkSheet3.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet3.Cells[I, 5] = DBC.Rs1.GetString(4).Trim();
                xlWorkSheet3.Cells[I, 6] = DBC.Rs1.GetString(5);
                xlWorkSheet3.Cells[I, 7] = DBC.Rs1.GetString(7).Trim();

                // תוספת מתאריך 24/07/2016
                // הוספת נתונים לדוח לבקשת אייל קרן
                // הוספת הנתון חוזק צמיג אשר נמדד בתחנות רנטגן או רנאווט + קוד תערוסת
                if (DBC.Rs1.GetString(8).Length >= 13)
                    xlWorkSheet3.Cells[I, 8] = DBC.Rs1.GetString(8).Substring(10, 3);
                
                // שליפת מפרט מאותה מדבקה שדווח עליה פריקה ממכבש
                string SMixture = "";
                StrSql = "select LSPEC,LMACH from TAPIALI.LABELGP where LPROD='" + DBC.Rs1.GetString(4).Trim() + "' and LLBLNO=" + DBC.Rs1.GetString(5) + " and LACTAN=42";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {                                        
                    // ID -ליפוף, קודם שליפתID הוספת תערובת של                
                    if (DBC.Rs2.GetString(0).Trim() != "")
                    {
                        SMixture = DBC.Rs2.GetString(0).Trim().Substring(DBC.Rs2.GetString(0).Trim().Length - 9, 9);
                        StrSql = "select MRKOD from  mfrt.MFRTVP " +
                                 "where MRSZ='" + SMixture.Substring(0, 2) + "' and MRNO=" + SMixture.Substring(3, 3) + " and " +
                                       "MRVRN=" + SMixture.Substring(7, 2) + " and MRSUG in ('CAP','SUO','BAS','SUL')";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            // שליפת תערובת סוליה
                            StrSql = "select XTMIXD from MFRT.XTRUP where XTCODE='" + DBC.Rs3.GetString(0).Trim() + "'";
                            DBC.Rs3.Close();
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                                xlWorkSheet3.Cells[I, 9] = DBC.Rs3.GetString(0).Trim();                            
                        }
                        DBC.Rs3.Close();
                        SMixture = "";
                    }
                }
                DBC.Rs2.Close();

                I++;
            }
            DBC.Rs1.Close();
            
            // עדכון רשומה אחרונה
            Arr[0, I_Ar] = LastDt;
            Arr[1, I_Ar] = "0";
            Arr[2, I_Ar] = RecCount.ToString();

            // כניסות
            I = 2;
            RecCount = 0;
            I_Ar = 0;
            LastDt = "";

            xlWorkSheet2.Cells[1, 1] = "Date";
            xlWorkSheet2.Cells[1, 2] = "Shift";
            xlWorkSheet2.Cells[1, 3] = "Time";
            xlWorkSheet2.Cells[1, 4] = "Size";
            xlWorkSheet2.Cells[1, 5] = "Catalog";
            xlWorkSheet2.Cells[1, 6] = "Serial";
            xlWorkSheet2.Name = "RNT In";
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet2.get_Range("A1", "G1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet2.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            chartRange = xlWorkSheet2.get_Range("A2", "A10000");
            chartRange.NumberFormat = "@";

            StrSql = "select max(LACNDT),max(LSHIFT),max(LACNTM),max(INSIZ),max(LPROD),max(LLBLNO),LPROD||LLBLNO " +
                     "from TAPIALI.LABELGP join " +
                          "BPCSFALI.IIMNL01 on substr(LPROD,1,8)=INPROD join " +
                          "BPCSFALI.IIMNEL01 on substr(LPROD,1,8)=substr(EMKT15,1,8) " +     //        צאריך רנאאוט           צריך רנטגן  
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and substr(EFIL3,2,1)='Y' " + // or substr(EFIL3,3,1)='Y') " +
                     "group by LPROD||LLBLNO " +
                     "order by max(LACNDT),max(LSHIFT),max(LACNTM)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                Flg_DateInArr = false;
                if (LastDt.Trim() == "")
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);

                if (LastDt == DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4))
                {
                    RecCount++;
                }
                else
                {
                    for (int j = 0; j < 400; j++)
                    {
                        if (Arr[0, j] == LastDt)
                        {
                            Arr[1, j] = RecCount.ToString();
                            Flg_DateInArr = true;
                            j = 400;
                        }
                    }
                    if (!Flg_DateInArr) // אם רשומת תאריך לא נמצאת
                    {
                        for (int j = 0; j < 400; j++)
                        {
                            if (Arr[0, j] == "")
                            {
                                Arr[0, j] = LastDt;
                                Arr[1, j] = RecCount.ToString();
                                Arr[2, j] = "0";
                                j = 400;
                            }
                        }
                    }

                    RecCount = 1;
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                }
          
                xlWorkSheet2.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet2.Cells[I, 2] = DBC.Rs1.GetString(1);
                if (DBC.Rs1.GetString(2).Trim().Length == 1)
                    xlWorkSheet2.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 2)
                    xlWorkSheet2.Cells[I, 3] = "00:00:" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 3)
                    xlWorkSheet2.Cells[I, 3] = "00:0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 4)
                    xlWorkSheet2.Cells[I, 3] = "00:" + DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 5)
                    xlWorkSheet2.Cells[I, 3] = "0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(3, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 6)
                    xlWorkSheet2.Cells[I, 3] = DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(4, 2);

                xlWorkSheet2.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet2.Cells[I, 5] = DBC.Rs1.GetString(4).Trim();
                xlWorkSheet2.Cells[I, 6] = DBC.Rs1.GetString(5);
                I++;
            }
            DBC.Rs1.Close();
            
            // עדכון רשומה אחרונה
            for (int j = 0; j < 400; j++)
            {
                if (Arr[0, j] == LastDt)
                {
                    Arr[1, j] = RecCount.ToString();
                    Flg_DateInArr = true;
                    j = 400;
                }
            }
            if (!Flg_DateInArr) // אם רשומת תאריך לא נמצאת
            {
                for (int j = 0; j < 400; j++)
                {
                    if (Arr[0, j] == "")
                    {
                        Arr[0, j] = LastDt;
                        Arr[1, j] = RecCount.ToString();
                        Arr[2, j] = "0";
                        j = 400;
                    }
                }
            }           

            // סה"כ
            I = 0;
            xlWorkSheet1.Cells[1, 1] = "Date";
            xlWorkSheet1.Cells[1, 2] = "IN";
            xlWorkSheet1.Cells[1, 3] = "OUT";
            xlWorkSheet1.Cells[1, 4] = "Diff.";
            xlWorkSheet1.Cells[1, 5] = "Acc.";
            xlWorkSheet1.Name = "RNT Total";
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A1", "E1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet1.get_Range("A2", "A10000");
            chartRange.NumberFormat = "@";

            for (int j = 0; j < 400; j++)
            {
                if (Arr[0, j] != "")
                {
                    xlWorkSheet1.Cells[j + 2, 1] = Arr[0, j];
                    xlWorkSheet1.Cells[j + 2, 2] = Arr[1, j];
                    xlWorkSheet1.Cells[j + 2, 3] = Arr[2, j];
                    xlWorkSheet1.Cells[j + 2, 4] = int.Parse(Arr[1, j]) - int.Parse(Arr[2, j]);
                }
                else
                    j = 400;
            }
            
            // Sort column
            chartRange = xlWorkSheet1.get_Range("A2", "E400" );
            chartRange.Sort(chartRange.Columns[1, Type.Missing], Excel.XlSortOrder.xlAscending, // the first sort key Column 1 for Range
            Type.Missing, Type.Missing, Excel.XlSortOrder.xlAscending,// second sort key Column 6 of the range
            Type.Missing, Excel.XlSortOrder.xlAscending,  // third sort key nothing, but it wants one
            Excel.XlYesNoGuess.xlGuess, Type.Missing, Type.Missing,
            Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            Excel.XlSortDataOption.xlSortNormal,
            Excel.XlSortDataOption.xlSortNormal,
            Excel.XlSortDataOption.xlSortNormal);

            // ערך מצטבר בסוף אחרי המיון 
            for (int j = 2; j < 400; j++)
            {
                if ((xlWorkSheet1.Cells[j, 1] as Excel.Range).Value != null)
                {
                    I += Convert.ToInt64((xlWorkSheet1.Cells[j, 2] as Excel.Range).Value) - Convert.ToInt64((xlWorkSheet1.Cells[j, 3] as Excel.Range).Value);
                    xlWorkSheet1.Cells[j, 5] = I;
                }
                else

                    j = 400;
            }
            
        }

        private void RRO_Report()
        {            
            // דוח רנאווט
            string LastDt = "";
            int RecCount = 0;
            int I_Ar = 0;
            bool Flg_DateInArr = false;

            string[,] Arr = new string[3, 400];
            // Arr[0, 0] --> תאריך
            // Arr[1, 0] --> כניסות
            // Arr[2, 0] --> יציאות

            for (I = 0; I < 400; I++)
            {
                Arr[0, I] = "";
                Arr[1, I] = "";
                Arr[2, I] = "";
            }
            
            xlWorkSheet4 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet5 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet6 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            // יציאות
            I = 2;
            xlWorkSheet6.Cells[1, 1] = "Date";
            xlWorkSheet6.Cells[1, 2] = "Shift";
            xlWorkSheet6.Cells[1, 3] = "Time";
            xlWorkSheet6.Cells[1, 4] = "Size";
            xlWorkSheet6.Cells[1, 5] = "Catalog";
            xlWorkSheet6.Cells[1, 6] = "Serial";
            xlWorkSheet6.Cells[1, 7] = "Emp";

            // תוספת מתאריך 24/07/2016
            // הוספת נתונים לדוח לבקשת אייל קרן
            // הוספת הנתון חוזק צמיג אשר נמדד בתחנות רנטגן או רנאווט + קוד תערוסת
            xlWorkSheet6.Cells[1, 8] = "Shore Value";
            xlWorkSheet6.Cells[1, 9] = "Compound ID";

            xlWorkSheet6.Name = "RRO Out";
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet6.get_Range("A1", "I1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet6.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            chartRange = xlWorkSheet6.get_Range("H1", "I1");
            chartRange.ColumnWidth = 13;

            chartRange = xlWorkSheet6.get_Range("A2", "A10000");
            chartRange.NumberFormat = "@";
            
            StrSql = "select max(LACNDT),max(LSHIFT),max(LACNTM),max(INSIZ),max(LPROD),max(LLBLNO),LPROD||LLBLNO,max(LOVED),max(LERRDS) " +
                     "from TAPIALI.LABELGP join " +
                          "BPCSFALI.IIMNL01 on substr(LPROD,1,8)=INPROD " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=49 " +
                     "group by LPROD||LLBLNO " +
                     "order by max(LACNDT),max(LSHIFT),max(LACNTM)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (LastDt.Trim() == "")
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);

                if (LastDt == DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4))
                {
                    RecCount++;
                }
                else
                {
                    Arr[0, I_Ar] = LastDt;
                    Arr[1, I_Ar] = "0";
                    Arr[2, I_Ar] = RecCount.ToString();
                    I_Ar++;
                    RecCount = 1;
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                }

                xlWorkSheet6.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet6.Cells[I, 2] = DBC.Rs1.GetString(1);
                if (DBC.Rs1.GetString(2).Trim().Length == 1)
                    xlWorkSheet6.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 2)
                    xlWorkSheet6.Cells[I, 3] = "00:00:" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 3)
                    xlWorkSheet6.Cells[I, 3] = "00:0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 4)
                    xlWorkSheet6.Cells[I, 3] = "00:" + DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 5)
                    xlWorkSheet6.Cells[I, 3] = "0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(3, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 6)
                    xlWorkSheet6.Cells[I, 3] = DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(4, 2);

                xlWorkSheet6.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet6.Cells[I, 5] = DBC.Rs1.GetString(4).Trim();
                xlWorkSheet6.Cells[I, 6] = DBC.Rs1.GetString(5);
                xlWorkSheet6.Cells[I, 7] = DBC.Rs1.GetString(7).Trim();

                // תוספת מתאריך 24/07/2016
                // הוספת נתונים לדוח לבקשת אייל קרן
                // הוספת הנתון חוזק צמיג אשר נמדד בתחנות רנטגן או רנאווט + קוד תערוסת
                if (DBC.Rs1.GetString(8).Length >= 13)
                    xlWorkSheet6.Cells[I, 8] = DBC.Rs1.GetString(8).Substring(10, 3);

                // שליפת מפרט מאותה מדבקה שדווח עליה פריקה ממכבש
                string SMixture = "";
                StrSql = "select LSPEC,LMACH from TAPIALI.LABELGP where LPROD='" + DBC.Rs1.GetString(4).Trim() + "' and LLBLNO=" + DBC.Rs1.GetString(5) + " and LACTAN=42";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    // ID -ליפוף, קודם שליפתID הוספת תערובת של                
                    if (DBC.Rs2.GetString(0).Trim() != "")
                    {
                        SMixture = DBC.Rs2.GetString(0).Trim().Substring(DBC.Rs2.GetString(0).Trim().Length - 9, 9);
                        StrSql = "select MRKOD from  mfrt.MFRTVP " +
                                 "where MRSZ='" + SMixture.Substring(0, 2) + "' and MRNO=" + SMixture.Substring(3, 3) + " and " +
                                       "MRVRN=" + SMixture.Substring(7, 2) + " and MRSUG in ('CAP','SUO','BAS','SUL')";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            // שליפת תערובת סוליה
                            StrSql = "select XTMIXD from MFRT.XTRUP where XTCODE='" + DBC.Rs3.GetString(0).Trim() + "'";
                            DBC.Rs3.Close();
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                                xlWorkSheet6.Cells[I, 9] = DBC.Rs3.GetString(0).Trim();
                        }
                        DBC.Rs3.Close();
                        SMixture = "";
                    }
                }
                DBC.Rs2.Close();

                I++;
            }
            DBC.Rs1.Close();

            // עדכון רשומה אחרונה
            Arr[0, I_Ar] = LastDt;
            Arr[1, I_Ar] = "0";
            Arr[2, I_Ar] = RecCount.ToString();

            // כניסות
            I = 2;
            RecCount = 0;
            I_Ar = 0;
            LastDt = "";

            xlWorkSheet5.Cells[1, 1] = "Date";
            xlWorkSheet5.Cells[1, 2] = "Shift";
            xlWorkSheet5.Cells[1, 3] = "Time";
            xlWorkSheet5.Cells[1, 4] = "Size";
            xlWorkSheet5.Cells[1, 5] = "Catalog";
            xlWorkSheet5.Cells[1, 6] = "Serial";
            xlWorkSheet5.Name = "RRO In";
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet5.get_Range("A1", "G1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet5.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            chartRange = xlWorkSheet5.get_Range("A2", "A10000");
            chartRange.NumberFormat = "@";

            StrSql = "select max(LACNDT),max(LSHIFT),max(LACNTM),max(INSIZ),max(LPROD),max(LLBLNO),LPROD||LLBLNO " +
                     "from TAPIALI.LABELGP join " +
                          "BPCSFALI.IIMNL01 on substr(LPROD,1,8)=INPROD join " +
                          "BPCSFALI.IIMNEL01 on substr(LPROD,1,8)=substr(EMKT15,1,8) " +     //     צריך רנאוט  
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and substr(EFIL3,3,1)='Y' " +
                     "group by LPROD||LLBLNO " +
                     "order by max(LACNDT),max(LSHIFT),max(LACNTM)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                Flg_DateInArr = false;
                if (LastDt.Trim() == "")
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);

                if (LastDt == DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4))
                {
                    RecCount++;
                }
                else
                {
                    for (int j = 0; j < 400; j++)
                    {
                        if (Arr[0, j] == LastDt)
                        {
                            Arr[1, j] = RecCount.ToString();
                            Flg_DateInArr = true;
                            j = 400;
                        }
                    }
                    if (!Flg_DateInArr) // אם רשומת תאריך לא נמצאת
                    {
                        for (int j = 0; j < 400; j++)
                        {
                            if (Arr[0, j] == "")
                            {
                                Arr[0, j] = LastDt;
                                Arr[1, j] = RecCount.ToString();
                                Arr[2, j] = "0";
                                j = 400;
                            }
                        }
                    }

                    RecCount = 1;
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                }

                xlWorkSheet5.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet5.Cells[I, 2] = DBC.Rs1.GetString(1);
                if (DBC.Rs1.GetString(2).Trim().Length == 1)
                    xlWorkSheet5.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 2)
                    xlWorkSheet5.Cells[I, 3] = "00:00:" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 3)
                    xlWorkSheet5.Cells[I, 3] = "00:0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 4)
                    xlWorkSheet5.Cells[I, 3] = "00:" + DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 5)
                    xlWorkSheet5.Cells[I, 3] = "0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(3, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 6)
                    xlWorkSheet5.Cells[I, 3] = DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(4, 2);

                xlWorkSheet5.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet5.Cells[I, 5] = DBC.Rs1.GetString(4).Trim();
                
                xlWorkSheet5.Cells[I, 6] = DBC.Rs1.GetString(5);
                I++;
            }
            DBC.Rs1.Close();

            // עדכון רשומה אחרונה
            for (int j = 0; j < 400; j++)
            {
                if (Arr[0, j] == LastDt)
                {
                    Arr[1, j] = RecCount.ToString();
                    Flg_DateInArr = true;
                    j = 400;
                }
            }
            if (!Flg_DateInArr) // אם רשומת תאריך לא נמצאת
            {
                for (int j = 0; j < 400; j++)
                {
                    if (Arr[0, j] == "")
                    {
                        Arr[0, j] = LastDt;
                        Arr[1, j] = RecCount.ToString();
                        Arr[2, j] = "0";
                        j = 400;
                    }
                }
            }

            // סה"כ
            I = 0;
            xlWorkSheet4.Cells[1, 1] = "Date";
            xlWorkSheet4.Cells[1, 2] = "IN";
            xlWorkSheet4.Cells[1, 3] = "OUT";
            xlWorkSheet4.Cells[1, 4] = "Diff.";
            xlWorkSheet4.Cells[1, 5] = "Acc.";
            xlWorkSheet4.Name = "RRO Total";
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet4.get_Range("A1", "E1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet4.get_Range("A2", "A10000");
            chartRange.NumberFormat = "@";

            for (int j = 0; j < 400; j++)
            {
                if (Arr[0, j] != "")
                {
                    xlWorkSheet4.Cells[j + 2, 1] = Arr[0, j];
                    xlWorkSheet4.Cells[j + 2, 2] = Arr[1, j];
                    xlWorkSheet4.Cells[j + 2, 3] = Arr[2, j];
                    xlWorkSheet4.Cells[j + 2, 4] = int.Parse(Arr[1, j]) - int.Parse(Arr[2, j]);                    
                }
                else
                    j = 400;
            }

            // Sort column
            chartRange = xlWorkSheet4.get_Range("A2", "E400");
            chartRange.Sort(chartRange.Columns[1, Type.Missing], Excel.XlSortOrder.xlAscending, // the first sort key Column 1 for Range
            Type.Missing, Type.Missing, Excel.XlSortOrder.xlAscending,// second sort key Column 6 of the range
            Type.Missing, Excel.XlSortOrder.xlAscending,  // third sort key nothing, but it wants one
            Excel.XlYesNoGuess.xlGuess, Type.Missing, Type.Missing,
            Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            Excel.XlSortDataOption.xlSortNormal,
            Excel.XlSortDataOption.xlSortNormal,
            Excel.XlSortDataOption.xlSortNormal);

            // ערך מצטבר בסוף אחרי המיון 
            for (int j = 2; j < 400; j++)
            {
                if ((xlWorkSheet4.Cells[j, 1] as Excel.Range).Value != null)
                {
                    I += Convert.ToInt64((xlWorkSheet4.Cells[j, 2] as Excel.Range).Value) - Convert.ToInt64((xlWorkSheet4.Cells[j, 3] as Excel.Range).Value);
                    xlWorkSheet4.Cells[j, 5] = I;
                }
                else

                    j = 400;
            }
        }

        private void TK_Report()
        {
            // דוח רנאווט
            string LastDt = "";
            int RecCount = 0;
            int I_Ar = 0;
            bool Flg_DateInArr = false;

            string[,] Arr = new string[3, 400];
            // Arr[0, 0] --> תאריך
            // Arr[1, 0] --> כניסות
            // Arr[2, 0] --> יציאות

            for (I = 0; I < 400; I++)
            {
                Arr[0, I] = "";
                Arr[1, I] = "";
                Arr[2, I] = "";
            }

            xlWorkSheet7 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet8 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet9 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet10 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            
            // יציאות
            I = 2;
            xlWorkSheet9.Cells[1, 1] = "Date";
            xlWorkSheet9.Cells[1, 2] = "Shift";
            xlWorkSheet9.Cells[1, 3] = "Time";
            xlWorkSheet9.Cells[1, 4] = "Size";
            xlWorkSheet9.Cells[1, 5] = "Catalog";
            xlWorkSheet9.Cells[1, 6] = "Serial";
            xlWorkSheet9.Name = "TK Out";
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet9.get_Range("A1", "G1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet9.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            chartRange = xlWorkSheet9.get_Range("A2", "A10000");
            chartRange.NumberFormat = "@";

            StrSql = "select max(LACNDT),max(LSHIFT),max(LACNTM),max(INSIZ),max(LPROD),max(LLBLNO),LPROD||LLBLNO " +
                     "from TAPIALI.LABELGP join " +
                          "BPCSFALI.IIMNL01 on substr(LPROD,1,8)=INPROD " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=47 and LSTT='T' " +
                     "group by LPROD||LLBLNO " +
                     "order by max(LACNDT),max(LSHIFT),max(LACNTM)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (LastDt.Trim() == "")
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);

                if (LastDt == DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4))
                {
                    RecCount++;
                }
                else
                {
                    Arr[0, I_Ar] = LastDt;
                    Arr[1, I_Ar] = "0";
                    Arr[2, I_Ar] = RecCount.ToString();
                    I_Ar++;
                    RecCount = 1;
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                }

                xlWorkSheet9.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet9.Cells[I, 2] = DBC.Rs1.GetString(1);
                if (DBC.Rs1.GetString(2).Trim().Length == 1)
                    xlWorkSheet9.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 2)
                    xlWorkSheet9.Cells[I, 3] = "00:00:" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 3)
                    xlWorkSheet9.Cells[I, 3] = "00:0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 4)
                    xlWorkSheet9.Cells[I, 3] = "00:" + DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 5)
                    xlWorkSheet9.Cells[I, 3] = "0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(3, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 6)
                    xlWorkSheet9.Cells[I, 3] = DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(4, 2);

                xlWorkSheet9.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet9.Cells[I, 5] = DBC.Rs1.GetString(4).Trim();
                xlWorkSheet9.Cells[I, 6] = DBC.Rs1.GetString(5);
                I++;
            }
            DBC.Rs1.Close();

            // עדכון רשומה אחרונה
            Arr[0, I_Ar] = LastDt;
            Arr[1, I_Ar] = "0";
            Arr[2, I_Ar] = RecCount.ToString();

            // כניסות
            I = 2;
            RecCount = 0;
            I_Ar = 0;
            LastDt = "";

            xlWorkSheet8.Cells[1, 1] = "Date";
            xlWorkSheet8.Cells[1, 2] = "Shift";
            xlWorkSheet8.Cells[1, 3] = "Time";
            xlWorkSheet8.Cells[1, 4] = "Size";
            xlWorkSheet8.Cells[1, 5] = "Catalog";
            xlWorkSheet8.Cells[1, 6] = "Serial";
            xlWorkSheet8.Name = "TK In";
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet8.get_Range("A1", "G1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet8.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            chartRange = xlWorkSheet8.get_Range("A2", "A10000");
            chartRange.NumberFormat = "@";

            StrSql = "select max(LACNDT),max(LSHIFT),max(LACNTM),max(INSIZ),max(LPROD),max(LLBLNO),LPROD||LLBLNO " +
                     "from TAPIALI.LABELGP join " +
                          "BPCSFALI.IIMNL01 on substr(LPROD,1,8)=INPROD " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='R' " +
                     "group by LPROD||LLBLNO " +
                     "order by max(LACNDT),max(LSHIFT),max(LACNTM)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                Flg_DateInArr = false;
                if (LastDt.Trim() == "")
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);

                if (LastDt == DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4))
                {
                    RecCount++;
                }
                else
                {
                    for (int j = 0; j < 400; j++)
                    {
                        if (Arr[0, j] == LastDt)
                        {
                            Arr[1, j] = RecCount.ToString();
                            Flg_DateInArr = true;
                            j = 400;
                        }
                    }
                    if (!Flg_DateInArr) // אם רשומת תאריך לא נמצאת
                    {
                        for (int j = 0; j < 400; j++)
                        {
                            if (Arr[0, j] == "")
                            {
                                Arr[0, j] = LastDt;
                                Arr[1, j] = RecCount.ToString();
                                Arr[2, j] = "0";
                                j = 400;
                            }
                        }
                    }

                    RecCount = 1;
                    LastDt = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                }

                xlWorkSheet8.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet8.Cells[I, 2] = DBC.Rs1.GetString(1);
                if (DBC.Rs1.GetString(2).Trim().Length == 1)
                    xlWorkSheet8.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 2)
                    xlWorkSheet8.Cells[I, 3] = "00:00:" + DBC.Rs1.GetString(2).Trim();
                else if (DBC.Rs1.GetString(2).Trim().Length == 3)
                    xlWorkSheet8.Cells[I, 3] = "00:0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 4)
                    xlWorkSheet8.Cells[I, 3] = "00:" + DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 5)
                    xlWorkSheet8.Cells[I, 3] = "0" + DBC.Rs1.GetString(2).Trim().Substring(0, 1) + ":" + DBC.Rs1.GetString(2).Trim().Substring(1, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(3, 2);
                else if (DBC.Rs1.GetString(2).Trim().Length == 6)
                    xlWorkSheet8.Cells[I, 3] = DBC.Rs1.GetString(2).Trim().Substring(0, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(2, 2) + ":" + DBC.Rs1.GetString(2).Trim().Substring(4, 2);

                xlWorkSheet8.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet8.Cells[I, 5] = DBC.Rs1.GetString(4).Trim();
                xlWorkSheet8.Cells[I, 6] = DBC.Rs1.GetString(5);
                I++;
            }
            DBC.Rs1.Close();

            // עדכון רשומה אחרונה
            for (int j = 0; j < 400; j++)
            {
                if (Arr[0, j] == LastDt)
                {
                    Arr[1, j] = RecCount.ToString();
                    Flg_DateInArr = true;
                    j = 400;
                }
            }
            if (!Flg_DateInArr) // אם רשומת תאריך לא נמצאת
            {
                for (int j = 0; j < 400; j++)
                {
                    if (Arr[0, j] == "")
                    {
                        Arr[0, j] = LastDt;
                        Arr[1, j] = RecCount.ToString();
                        Arr[2, j] = "0";
                        j = 400;
                    }
                }
            }            

            // סה"כ
            I = 0;
            xlWorkSheet7.Cells[1, 1] = "Date";
            xlWorkSheet7.Cells[1, 2] = "IN";
            xlWorkSheet7.Cells[1, 3] = "OUT";
            xlWorkSheet7.Cells[1, 4] = "Diff.";
            xlWorkSheet7.Cells[1, 5] = "Acc.";
            xlWorkSheet7.Cells[1, 6] = "Total Stock";
            xlWorkSheet7.Name = "TK Total";
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet7.get_Range("A1", "F1");
            chartRange.ColumnWidth = 10;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            //chartRange = xlWorkSheet7.get_Range("A2", "A10000");
            //chartRange.NumberFormat = "@";

            for (int j = 0; j < 400; j++)
            {
                if (Arr[0, j] != "")
                {
                    xlWorkSheet7.Cells[j + 2, 1] = Arr[0, j];
                    xlWorkSheet7.Cells[j + 2, 2] = Arr[1, j];
                    xlWorkSheet7.Cells[j + 2, 3] = Arr[2, j];
                    xlWorkSheet7.Cells[j + 2, 4] = int.Parse(Arr[1, j]) - int.Parse(Arr[2, j]);                    
                }
                else
                    j = 400;
            }

            // Sort column
            chartRange = xlWorkSheet7.get_Range("A2", "D400");
            chartRange.Sort(chartRange.Columns[1, Type.Missing], Excel.XlSortOrder.xlAscending, // the first sort key Column 1 for Range
            Type.Missing, Type.Missing, Excel.XlSortOrder.xlAscending,// second sort key Column 6 of the range
            Type.Missing, Excel.XlSortOrder.xlAscending,  // third sort key nothing, but it wants one
            Excel.XlYesNoGuess.xlGuess, Type.Missing, Type.Missing,
            Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            Excel.XlSortDataOption.xlSortNormal,
            Excel.XlSortDataOption.xlSortNormal,
            Excel.XlSortDataOption.xlSortNormal);


            // תוספת מתאריך 19/04/2017
            // שליפת סה"כ מלאי לפי ספירת מלאי
            // תיקון זה בהנחיית בוריס קזקיבץ            
            chartRange = xlWorkSheet7.get_Range("A2", "A4000");
            chartRange.NumberFormat = "dd/mm/yyyy";            

            long[,] TK_Stock = new long[2, 30];
            long TK_Counter = 0;            
            long Total_Stock = 0;
            int TK_Arr_Counter = 0;            
            string TmpDT = "";
            string Tmp = "";

            I = 0;
            StrSql = "select * from TAPIALI.TKSTOCK order by TKDATE";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                TK_Stock[0, I] = DBC.Rs1.GetInt64(0);
                TK_Stock[1, I] = DBC.Rs1.GetInt64(1);
                I++;
                TK_Counter++;
            }
            DBC.Rs1.Close();

            for (long T = 0; T < TK_Counter; T++)
            {
                if (Convert.ToInt64(DTPicker_From.Value.ToString("yyyyMMdd")) > TK_Stock[0, TK_Arr_Counter])
                    if (Convert.ToInt64(DTPicker_From.Value.ToString("yyyyMMdd")) < TK_Stock[0, TK_Arr_Counter+1])
                        T = TK_Counter;
                    else if (TK_Stock[0, T + 1] == 0)
                        T = TK_Counter;
                    else
                        TK_Arr_Counter++;
                else
                    T = TK_Counter;
            }
            I = 0;
            Total_Stock = TK_Stock[1, TK_Arr_Counter];
            // בדיקה אם ת. תחלית הדוח אחרי ת. ספירה, במקרה זה צריך לקזז צמיגים מתאריך הספירה עד תאריך תחילת הדוח 
            bool FlgReportAfterStocktaking = false;
            if (Convert.ToInt64(DTPicker_From.Value.ToString("yyyyMMdd")) > TK_Stock[0, TK_Arr_Counter])
            {
                FlgReportAfterStocktaking = true;
                long TK_In = 0;
                long TK_Out = 0;

                // שליפת סה"כ כניסות לתיקונים מתאריך ספירת מלאי אחרונה עד יום לפני תאריך תחילת הדוח
                StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGP " +
                         "where LACNDT  between " + TK_Stock[0, TK_Arr_Counter] + " and " + DTPicker_From.Value.AddDays(-1).ToString("yyyyMMdd") + " and LACTAN=45 and LSTT='R' group by LPROD||LLBLNO";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    TK_In += DBC.Rs1.GetInt64(0);
                }
                DBC.Rs1.Close();
                // שליפת סה"כ יציאות מהתיקונים מתאריך ספירת מלאי אחרונה עד יום לפני תאריך תחילת הדוח
                StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGP " +                    
                         "where LACNDT  between " + TK_Stock[0, TK_Arr_Counter] + " and " + DTPicker_From.Value.AddDays(-1).ToString("yyyyMMdd") + " and LACTAN=47 and LSTT='T' group by LPROD||LLBLNO";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    TK_Out += DBC.Rs1.GetInt64(0);
                }
                DBC.Rs1.Close();                

                // צריך לחשוב על מנגנון יותר יעיל
                //DateTime Tmp_DT = Convert.ToDateTime(TK_Stock[0, TK_Arr_Counter].ToString().Substring(6, 2) + "/" + TK_Stock[0, TK_Arr_Counter].ToString().Substring(4, 2) + "/" + TK_Stock[0, TK_Arr_Counter].ToString().Substring(0, 4));
                //for (; Tmp_DT < DTPicker_From.Value; )
                //{
                //    // שליפת סה"כ כניסות לתיקונים מתאריך ספירת מלאי אחרונה עד יום לפני תאריך תחילת הדוח
                //    StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGP " +                            
                //             "where LACNDT  =" + Tmp_DT.ToString("yyyyMMdd") + " and LACTAN=45 and LSTT='R' group by LPROD||LLBLNO";
                //             //"where LACNDT  between " + TK_Stock[0, TK_Arr_Counter] + " and " + DTPicker_From.Value.AddDays(-1).ToString("yyyyMMdd") + " and LACTAN=45 and LSTT='R' group by LPROD||LLBLNO";
                //    DBC.Q_Run1(StrSql);
                //    while (DBC.Rs1.Read())
                //    {
                //        TK_In += 1;//DBC.Rs1.GetInt64(0);
                //    }
                //    DBC.Rs1.Close();
                //    // שליפת סה"כ יציאות מהתיקונים מתאריך ספירת מלאי אחרונה עד יום לפני תאריך תחילת הדוח
                //    StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGP " +
                //             //"select max(LACNDT) from TAPIALI.LABELGP " +
                //             "where LACNDT  = " + Tmp_DT.ToString("yyyyMMdd") + " and LACTAN=47 and LSTT='T' group by LPROD||LLBLNO";    
                //             //"where LACNDT  between " + TK_Stock[0, TK_Arr_Counter] + " and " + DTPicker_From.Value.AddDays(-1).ToString("yyyyMMdd") + " and LACTAN=47 and LSTT='T' group by LPROD||LLBLNO";
                //    DBC.Q_Run1(StrSql);
                //    while (DBC.Rs1.Read())
                //    {
                //        TK_Out += 1;//DBC.Rs1.GetInt64(0);
                //    }
                //    DBC.Rs1.Close();
                //    Tmp_DT = Tmp_DT.AddDays(1);
                //}
                Total_Stock = Total_Stock + TK_In - TK_Out;
            }

            // ערך מצטבר בסוף אחרי המיון 
            for (int j = 2; j < 400; j++)
            {
                if ((xlWorkSheet7.Cells[j, 1] as Excel.Range).Value != null)
                {                    
                    I += Convert.ToInt64((xlWorkSheet7.Cells[j, 2] as Excel.Range).Value) - Convert.ToInt64((xlWorkSheet7.Cells[j, 3] as Excel.Range).Value);
                    xlWorkSheet7.Cells[j, 5] = I;


                    // תוספת מתאריך 19/04/2017
                    // שליפת סה"כ מלאי לפי ספירת מלאי
                    // תיקון זה בהנחיית בוריס קזקיבץ            
                    TmpDT = TK_Stock[0, TK_Arr_Counter].ToString().Substring(6, 2) + "/" + TK_Stock[0, TK_Arr_Counter].ToString().Substring(4, 2) + "/" + TK_Stock[0, TK_Arr_Counter].ToString().Substring(0, 4);                    
                    if (Convert.ToDateTime((xlWorkSheet7.Cells[j, 1] as Excel.Range).Value) == Convert.ToDateTime(TmpDT))
                    {
                        chartRange = xlWorkSheet7.get_Range("F" + j.ToString(), "F" + j.ToString());
                        chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);
                        chartRange.AddComment("Stocktaking QTY : " + TK_Stock[1, TK_Arr_Counter].ToString());
                    }

                    if (Convert.ToDateTime((xlWorkSheet7.Cells[j, 1] as Excel.Range).Value) < Convert.ToDateTime(TmpDT)) // בדיקה אם ת. ספירה גדול מת. תיקון
                    {
                        Total_Stock = 0;
                    }
                    else
                    {
                        if (j == 2 && FlgReportAfterStocktaking) //  אם ת. תחלית הדוח אחרי ת. ספירה, הוספת הערה לגבי תאריך וכמות הספירה 
                        {                            
                            chartRange = xlWorkSheet7.get_Range("F" + j.ToString(), "F" + j.ToString());
                            chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);
                            chartRange.AddComment("Stocktaking Date : " + TK_Stock[0, TK_Arr_Counter].ToString().Substring(6, 2) + "/" + TK_Stock[0, TK_Arr_Counter].ToString().Substring(4, 2) + "/" + TK_Stock[0, TK_Arr_Counter].ToString().Substring(0, 4) + Environment.NewLine + "Stocktaking QTY : " + TK_Stock[1, TK_Arr_Counter].ToString());
                        }
                        if (Total_Stock == 0) 
                                Total_Stock = TK_Stock[1, TK_Arr_Counter];
                        if (TK_Stock[0, TK_Arr_Counter + 1] != 0)
                        {                            
                            Tmp = TK_Stock[0, TK_Arr_Counter + 1].ToString().Substring(6, 2) + "/" + TK_Stock[0, TK_Arr_Counter + 1].ToString().Substring(4, 2) + "/" + TK_Stock[0, TK_Arr_Counter + 1].ToString().Substring(0, 4);
                            if (Convert.ToDateTime((xlWorkSheet7.Cells[j, 1] as Excel.Range).Value) >= Convert.ToDateTime(Tmp)) // בדיקה אם ת. תיקון גדול או שווה לת. ספירה
                            {
                                TK_Arr_Counter++;
                                Total_Stock = TK_Stock[1, TK_Arr_Counter];
                                Total_Stock = Total_Stock + Convert.ToInt64((xlWorkSheet7.Cells[j, 4] as Excel.Range).Value);
                                try
                                {
                                    chartRange = xlWorkSheet7.get_Range("F" + j.ToString(), "F" + j.ToString());
                                    chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);
                                    chartRange.AddComment("Stocktaking QTY : " + TK_Stock[1, TK_Arr_Counter].ToString());
                                }
                                catch
                                {

                                }
                            }
                            else
                            {
                                Total_Stock = Total_Stock + Convert.ToInt64((xlWorkSheet7.Cells[j, 4] as Excel.Range).Value);
                            }
                        }
                        else
                            Total_Stock = Total_Stock + Convert.ToInt64((xlWorkSheet7.Cells[j, 4] as Excel.Range).Value);
                    }
                    xlWorkSheet7.Cells[j, 6] = Total_Stock; 
                }
                else

                    j = 400;
            }

            // תיקון מתאריך 21/02/2017
            // TK  הוספת גיליון חדש רשימת צמיגים לא תקינים בסדנת תיקונים  
            // לבקשת חיים משקאוצן            
            I = 2;            
            xlWorkSheet10.Name = "TK Failed";

            xlWorkSheet10.Cells[1, 1] = "Date";
            xlWorkSheet10.Cells[1, 2] = "Shift";
            xlWorkSheet10.Cells[1, 3] = "Time";
            xlWorkSheet10.Cells[1, 4] = "Size";
            xlWorkSheet10.Cells[1, 5] = "Catalog";
            xlWorkSheet10.Cells[1, 6] = "Serial";
            xlWorkSheet10.Cells[1, 7] = "Defect Code";
            xlWorkSheet10.Cells[1, 8] = "Name";

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet10.get_Range("A1", "H1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.ColumnWidth = 13;

            chartRange = xlWorkSheet10.get_Range("D1", "D1");
            chartRange.ColumnWidth = 20;
            chartRange = xlWorkSheet10.get_Range("H1", "H1");
            chartRange.ColumnWidth = 20;

            // התאמת תאריך                               
            StrSql = "select LSHIFT,LACNTM,INSIZ,LPROD,LLBLNO,LERRDS,FAMILY||' '||PRATI,LACNDT " +
                     "from TAPIALI.LABELGP join BPCSFALI.IIMN on substr(LPROD,1,8)=INPROD left join ISUFKV.ISAVL10 on '00'||substring(LUSER,4,5)= OVED " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=47 and LSTT = 'P' " +
                     "order by LACNDT,LACNTM";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet10.Cells[I, 1] = DBC.Rs1.GetDecimal(7).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(7).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 4);                
                xlWorkSheet10.Cells[I, 2] = DBC.Rs1.GetString(0);
                //MessageBox.Show(DBC.Rs1.GetValue(1).ToString());
                if (DBC.Rs1.GetDecimal(1).ToString().Trim().Length == 1)
                    xlWorkSheet10.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetDecimal(1).ToString().Trim();
                else if (DBC.Rs1.GetDecimal(1).ToString().Trim().Length == 2)
                    xlWorkSheet10.Cells[I, 3] = "00:00:" + DBC.Rs1.GetDecimal(1).ToString().Trim();
                else if (DBC.Rs1.GetDecimal(1).ToString().Trim().Length == 3)
                    xlWorkSheet10.Cells[I, 3] = "00:0" + DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(1, 2);
                else if (DBC.Rs1.GetDecimal(1).ToString().Trim().Length == 4)
                    xlWorkSheet10.Cells[I, 3] = "00:" + DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(2, 2);
                else if (DBC.Rs1.GetDecimal(1).ToString().Trim().Length == 5)
                    xlWorkSheet10.Cells[I, 3] = "0" + DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(1, 2) + ":" + DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(3, 2);
                else if (DBC.Rs1.GetDecimal(1).ToString().Trim().Length == 6)
                    xlWorkSheet10.Cells[I, 3] = DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(2, 2) + ":" + DBC.Rs1.GetDecimal(1).ToString().Trim().Substring(4, 2);

                xlWorkSheet10.Cells[I, 4] = DBC.Rs1.GetString(2);
                xlWorkSheet10.Cells[I, 5] = DBC.Rs1.GetString(3);
                xlWorkSheet10.Cells[I, 6] = DBC.Rs1.GetDecimal(4).ToString();
                xlWorkSheet10.Cells[I, 7] = DBC.Rs1.GetString(5).Trim();
                if (!DBC.Rs1.IsDBNull(6))
                    xlWorkSheet10.Cells[I, 8] = DBC.Rs1.GetString(6).Trim();
                I += 1;
            }
            DBC.Rs1.Close();

        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }        
    }
}
