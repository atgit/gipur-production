﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_ScrapHistory : Form
    {
        string StrSql;
        string Con_Str;
        
        public Frm_ScrapHistory()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            string Str_S_Date;        // תאריך התחלה לצורך השאילתא
            string Str_E_Date;        // תאריך סיום לצורך השאילתא
            DateTime S_Date;          // תאריך התחלה
            DateTime E_Date;          // תאריך סיום                                    

            int I;
            I = 4;

            /////
            // אנגלית OFFEICE הרצת דוח בגרסת 
            System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
            System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;
            thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            /////

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            
            // Freeze rows            
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A3", "A3").get_Offset(1, 0).EntireRow;
            chartRange.Select();            
            xlWnd1.FreezePanes = true;            

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;
            
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "F1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;            
            xlWorkSheet.Cells[1, 1] = "Scrap History Report Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);            

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AK3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;            

            // מירכוז + מיזוג + פונט מודגש לתחום סיווג
            chartRange = xlWorkSheet.get_Range("A2", "G2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;            
            // יצירת תא עם גבולות
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 1] = "CLASS";
            // הגדרת צבע רקע
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightPink.ToArgb();            

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Size";
            xlWorkSheet.Cells[3, 3] = "Catalog";
            xlWorkSheet.Cells[3, 4] = "Serial";
            xlWorkSheet.Cells[3, 5] = "Class";
            xlWorkSheet.Cells[3, 6] = "D. Code1";
            xlWorkSheet.Cells[3, 7] = "No. Class";            

            // הגדרת רוחב עמודה של תחום סיווג
            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 5;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 9;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 8;            

            // מירכוז + מיזוג + פונט מודגש לתחום ביקורת חמה
            chartRange = xlWorkSheet.get_Range("H2", "L2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            // יצירת תא עם גבולות
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 8] = "HOT INSPECTION";
            // הגדרת צבע רקע
            chartRange.Cells.Interior.Color = System.Drawing.Color.YellowGreen.ToArgb();            

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 8] = "Date";
            xlWorkSheet.Cells[3, 9] = "Time";
            xlWorkSheet.Cells[3, 10] = "D. Code";
            xlWorkSheet.Cells[3, 11] = "Emp No.";
            xlWorkSheet.Cells[3, 12] = "Name";
            
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("K1", "K1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("M2", "S2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            // יצירת תא עם גבולות
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 13] = "Curing - Finished goods";
            // הגדרת צבע רקע
            chartRange.Cells.Interior.Color = System.Drawing.Color.NavajoWhite.ToArgb();

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 13] = "Date";
            xlWorkSheet.Cells[3, 14] = "Time";
            xlWorkSheet.Cells[3, 15] = "Press";
            xlWorkSheet.Cells[3, 16] = "Emp No. Out";
            xlWorkSheet.Cells[3, 17] = "Name Out";

            // תוספת מתאריך 18/06/2015          
            // שליפת מס' גפר + שם גפר שהכניס את הצמיג לגיפור
            xlWorkSheet.Cells[3, 18] = "Emp No. In";
            xlWorkSheet.Cells[3, 19] = "Name In";
            
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 12;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 12;
            chartRange = xlWorkSheet.get_Range("S1", "S1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("T2", "AB2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            // יצירת תא עם גבולות
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 20] = "Building Machine Stage 2";
            // הגדרת צבע רקע
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 20] = "Date";            
            xlWorkSheet.Cells[3, 21] = "Time";
            xlWorkSheet.Cells[3, 22] = "Catalog";
            xlWorkSheet.Cells[3, 23] = "Serial";
            xlWorkSheet.Cells[3, 24] = "B. Machine";
            xlWorkSheet.Cells[3, 25] = "Spc. Weight";
            xlWorkSheet.Cells[3, 26] = "Act. Weight";
            xlWorkSheet.Cells[3, 27] = "Emp No.";
            xlWorkSheet.Cells[3, 28] = "Name";
            
            chartRange = xlWorkSheet.get_Range("T1", "T1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("U1", "U1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("V1", "V1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("W1", "W1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("X1", "X1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("Y1", "Y1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("Z1", "Z1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("AA1", "AA1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AB1", "AB1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("AC2", "AK2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            // יצירת תא עם גבולות
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;                        
            xlWorkSheet.Cells[2, 29] = "Building Machine Stage 1";
            // הגדרת צבע רקע
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 29] = "Date";
            xlWorkSheet.Cells[3, 30] = "Time";
            xlWorkSheet.Cells[3, 31] = "Catalog";
            xlWorkSheet.Cells[3, 32] = "Serial";
            xlWorkSheet.Cells[3, 33] = "B. Machine";
            xlWorkSheet.Cells[3, 34] = "Spc. Weight";
            xlWorkSheet.Cells[3, 35] = "Act. Weight";
            xlWorkSheet.Cells[3, 36] = "Emp No.";
            xlWorkSheet.Cells[3, 37] = "Name";                        
            
            chartRange = xlWorkSheet.get_Range("AC1", "AC1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("AD1", "AD1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AE1", "AE1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("AF1", "AF1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("AG1", "AG1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("AH1", "AH1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("AI1", "AI1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("AJ1", "AJ1");            
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AK1", "AK1");
            chartRange.ColumnWidth = 15;

            chartRange = xlWorkSheet.get_Range("A3", "A10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("B3", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("H3", "H10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("M3", "M10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("T3", "T10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("AC3", "AC10000");
            chartRange.NumberFormat = "@";

            // התאמת תאריך
            S_Date = DTPicker_From .Value;
            E_Date = DTPicker_To.Value;
            Str_S_Date = (S_Date.Year - 1928).ToString("00") + S_Date.Month.ToString("00") + S_Date.Day.ToString("00");
            Str_E_Date = (E_Date.Year - 1928).ToString("00") + E_Date.Month.ToString("00") + E_Date.Day.ToString("00");

            // AS400 התקשרות ל   
            DBConn DBC;                        
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            // שליפת פסולים לתקופה מבוקשת            
                        
            StrSql = "select SSVGDT,SPROD,SMIFRT,SSIVUG,SPGAM1,SMSVEG,INSIZ " +
                     "from ALIQUAL.CTSVGP join BPCSFALI.IIMN on SUBSTRING(SPROD,1,8)=INPROD " +
                     "where SSVGDT between " + Str_S_Date + " and " + Str_E_Date + " and SSIVUG in ('פ','ב')";

            if (Txt_Prod.Text.Trim() != "")
                StrSql += " and SPROD = '" + Txt_Prod.Text + "'";

            if (Txt_DefectCode.Text.Trim() != "")
                StrSql += " and SPGAM1 = "+Txt_DefectCode.Text+"";
            
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (DBC.Rs1.GetString(2).Trim().Length >= 6) // בדיקה רק במצב שזה סידורי חוקי - יכול להיות שמשרשרים לסידורי ערכים בקובץ סיווג לטענת מיכאל רדת
                {
                    // נתוני סיווג
                    xlWorkSheet.Cells[I, 1] = (Convert.ToInt64(DBC.Rs1.GetDecimal(0).ToString().Substring(0, 2)) + 1928).ToString() + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(2, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2);
                    xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(1);
                    xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(2).Substring(0, 6);
                    if (DBC.Rs1.GetString(3) == "פ")
                        xlWorkSheet.Cells[I, 5] = "S";
                    else
                        xlWorkSheet.Cells[I, 5] = "DA";
                    xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetDecimal(4);
                    xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetDecimal(5);
                    xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetString(6);                                       
                    
                    // נתוני ביקורת חמה
                    StrSql = "select LSTTDT,LSTTTM,LERRDS,LOVED,LSPEC from TAPIALI.LABELGL3 where LPROD='" + DBC.Rs1.GetString(1) + "' and LLBLNO=" + DBC.Rs1.GetString(2).Substring(0, 6) + " and LACTAN=45";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        xlWorkSheet.Cells[I, 8] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                        xlWorkSheet.Cells[I, 9] = FixTime(DBC.Rs2.GetString(1));
                        xlWorkSheet.Cells[I, 10] = DBC.Rs2.GetString(2);
                        xlWorkSheet.Cells[I, 11] = DBC.Rs2.GetDecimal(3);
                        xlWorkSheet.Cells[I, 12] = GetUserName(DBC.Rs2.GetDecimal(3).ToString());
                        

                        // תוספת מתאריך 16/12/2015
                        // שליפת הנתון משקל מפרט, חזי רוצה שאציג לו נתון זה בכל מקרה גם אם אין עקיבות
                        if (DBC.Rs2.GetString(4).Trim().Length >= 9)
                            xlWorkSheet.Cells[I, 25] = DBC.Rs2.GetString(4).Trim().Substring(DBC.Rs2.GetString(4).Trim().Length - 9, 9);
                        else
                        {
                            // יש מקרים שמפרט לא מופיע ברשומת מדבקה
                            StrSql = "select ICSCP1*2.2046 from BPCSFV30.CICL01 where ICFAC ='F1' and ICPROD='" + DBC.Rs1.GetString(1) + "'";
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                                xlWorkSheet.Cells[I, 25] = Math.Round(DBC.Rs3.GetDouble(0),2);
                            DBC.Rs3.Close();
                        }
                    }
                    DBC.Rs2.Close();

                    // נתוני גיפור
                    // תוספת מתאריך 18/06/2015          
                    // שליפת מס' גפר + שם גפר שהכניס את הצמיג לגיפור
                    //StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED from TAPIALI.LABELGL3 where LPROD='" + DBC.Rs1.GetString(1) + "' and LLBLNO=" + DBC.Rs1.GetString(2).Substring(0, 6) + " and LACTAN=42";
                    StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED,KPROD1,KSERIAL1 " +
                             "from TAPIALI.LABELGL3 left join TAPIALI.LABKSRP on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                             "where LPROD='" + DBC.Rs1.GetString(1) + "' and LLBLNO=" + DBC.Rs1.GetString(2).Substring(0, 6) + " and LACTAN=42";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        xlWorkSheet.Cells[I, 13] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                        xlWorkSheet.Cells[I, 14] = FixTime(DBC.Rs2.GetString(1));
                        xlWorkSheet.Cells[I, 15] = DBC.Rs2.GetString(2);
                        xlWorkSheet.Cells[I, 16] = DBC.Rs2.GetString(3);
                        xlWorkSheet.Cells[I, 17] = GetUserName(DBC.Rs2.GetString(3));

                        // תוספת מתאריך 18/06/2015          
                        // שליפת מס' גפר + שם גפר שהכניס את הצמיג לגיפור
                        if (!DBC.Rs2.IsDBNull(3) && !DBC.Rs2.IsDBNull(5))
                        {                            
                            StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                                     "where LPROD='" + DBC.Rs2.GetString(4) + "' and LLBLNO=" + DBC.Rs2.GetDecimal(5).ToString() + " and LACTAN=41";                            
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                            {
                                xlWorkSheet.Cells[I, 18] = DBC.Rs3.GetString(0);
                                xlWorkSheet.Cells[I, 19] = GetUserName(DBC.Rs3.GetString(0));
                            }
                            DBC.Rs3.Close();
                        }
                    }
                    DBC.Rs2.Close();
 
                    // נתוני בניה שלב ב
                    StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1 " +
                             "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                             "where KPROD2='" + DBC.Rs1.GetString(1) + "' and KSERIAL2=" + DBC.Rs1.GetString(2).Substring(0, 6) + " and LACTAN=103";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        xlWorkSheet.Cells[I, 20] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                        xlWorkSheet.Cells[I, 21] = FixTime(DBC.Rs2.GetString(1));
                        xlWorkSheet.Cells[I, 22] = DBC.Rs2.GetString(6);
                        xlWorkSheet.Cells[I, 23] = DBC.Rs2.GetString(7);
                        xlWorkSheet.Cells[I, 24] = DBC.Rs2.GetString(2);
                        xlWorkSheet.Cells[I, 25] = Math.Round(DBC.Rs2.GetDecimal(4),2);
                        xlWorkSheet.Cells[I, 26] = Math.Round(DBC.Rs2.GetDecimal(5),2);
                        xlWorkSheet.Cells[I, 27] = DBC.Rs2.GetString(3);
                        xlWorkSheet.Cells[I, 28] = GetUserName(DBC.Rs2.GetString(3));

                        // נתוני בניה שלב א
                        StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1 " +
                                 "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                                 "where KPROD2='" + DBC.Rs2.GetString(6) + "' and KSERIAL2=" + DBC.Rs2.GetString(7) + " and LACTAN=3";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet.Cells[I, 29] = DBC.Rs3.GetString(0).Substring(6, 2) + "/" + DBC.Rs3.GetString(0).Substring(4, 2) + "/" + DBC.Rs3.GetString(0).Substring(0, 4);
                            xlWorkSheet.Cells[I, 30] = FixTime(DBC.Rs3.GetString(1));
                            xlWorkSheet.Cells[I, 31] = DBC.Rs3.GetString(6);
                            xlWorkSheet.Cells[I, 32] = DBC.Rs3.GetString(7);
                            xlWorkSheet.Cells[I, 33] = DBC.Rs3.GetString(2);
                            xlWorkSheet.Cells[I, 34] = Math.Round(DBC.Rs3.GetDecimal(4),2);
                            xlWorkSheet.Cells[I, 35] = Math.Round(DBC.Rs3.GetDecimal(5),2);
                            xlWorkSheet.Cells[I, 36] = DBC.Rs3.GetString(3);
                            xlWorkSheet.Cells[I, 37] = GetUserName(DBC.Rs3.GetString(3));
                        }
                        DBC.Rs3.Close(); 
                    }
                    DBC.Rs2.Close();
                    I += 1;
                }
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            
            //chartRange = xlWorkSheet.get_Range("A1", "AL"+(I-1).ToString());          
            //chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlDashDot;

            chartRange = xlWorkSheet.get_Range("G2", "G" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;

            chartRange = xlWorkSheet.get_Range("L2", "L" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;

            chartRange = xlWorkSheet.get_Range("S2", "S" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;

            chartRange = xlWorkSheet.get_Range("AB2", "AB" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;

            chartRange = xlWorkSheet.get_Range("AK2", "AK" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;

            chartRange = xlWorkSheet.get_Range("A" + (I - 1).ToString(), "AK" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A3", "AK3");
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 3d;

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "AK3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            Cursor.Current = Cursors.PanEast;
            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private string GetUserName(string EmpNo) 
        {
            string TmpSt = "";
            EmpNo= "00" + EmpNo.Trim();

            // AS400 התקשרות ל   
            DBConn DBC2;                        

            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC2 = new DBConn();
            DBC2.Initialize_Conn(Con_Str);
            
            // שליפת פסולים לתקופה מבוקשת            
            StrSql = "select NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 from ISUFKV.ISAV where OVED='" + EmpNo + "' and MIFAL='01'";
            DBC2.Q_Run1(StrSql);
            if (DBC2.Rs1.Read())
            {
                //TmpSt = strReverse(DBC2.Rs1.GetString(0).Trim()) + " " + strReverse(DBC2.Rs1.GetString(1).Trim());
                TmpSt = DBC2.Rs1.GetString(0).Trim() + " " + DBC2.Rs1.GetString(1).Trim();
                DBC2.Rs1.Close();
                DBC2.Close_Conn();
                return EmpNo = TmpSt;
            }
            else
            {
                DBC2.Rs1.Close();
                DBC2.Close_Conn();
                return "";
            }
        }
        
        private string strReverse(string s)
        {
            int j = 0;
            char[] c = new char[s.Length];
            for (int i = s.Length - 1; i >= 0; i--) c[j++] = s[i];
            return new string(c);
        }

        private string FixTime(string S)
        {
            if (S.Length == 1)
                S = "00:00:0" + S;
            else 
                if (S.Length == 2)
                    S = "00:00:" + S;
                else
                    if (S.Length == 3)
                        S = "00:0" + S.Substring(0, 1) + ":" + S.Substring(0, 2);
                    else
                        if (S.Length == 4)
                            S = "00:" + S.Substring(0, 2) + ":" + S.Substring(2, 2);
                        else
                            if (S.Length == 5)
                                S = "0" + S.Substring(0, 1) + ":" + S.Substring(1, 2) + ":" + S.Substring(3, 2);
                            else
                                S =  S.Substring(0, 2) + ":" + S.Substring(2, 2) + ":" + S.Substring(4, 2);
            return S;
        }

        private void Txt_Prod_Leave(object sender, EventArgs e)
        {
            Txt_Prod.Text = Txt_Prod.Text.ToUpper().Trim();   
        }

        private void Frm_ScrapHistory_Load(object sender, EventArgs e)
        {

        }

        private void Txt_DefectCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<-----     Delete     ----->| 
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete) e.Handled = false;
            else e.Handled = true;
        }
    }
}
