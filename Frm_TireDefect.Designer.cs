﻿namespace Gipur_Production
{
    partial class Frm_TireDefect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_TireDefect));
            this.Btn_OK = new System.Windows.Forms.Button();
            this.DTPicker_To = new System.Windows.Forms.DateTimePicker();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.DTPicker_From = new System.Windows.Forms.DateTimePicker();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboB_TireDefect = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_Pattern = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Catalog = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_DefectCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(80, 221);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(123, 28);
            this.Btn_OK.TabIndex = 30;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // DTPicker_To
            // 
            this.DTPicker_To.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_To.Location = new System.Drawing.Point(117, 75);
            this.DTPicker_To.Name = "DTPicker_To";
            this.DTPicker_To.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_To.TabIndex = 25;
            // 
            // Lbl_To
            // 
            this.Lbl_To.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_To.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(37, 77);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.Size = new System.Drawing.Size(73, 24);
            this.Lbl_To.TabIndex = 27;
            this.Lbl_To.Text = "To";
            // 
            // DTPicker_From
            // 
            this.DTPicker_From.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_From.Location = new System.Drawing.Point(117, 47);
            this.DTPicker_From.Name = "DTPicker_From";
            this.DTPicker_From.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_From.TabIndex = 24;
            // 
            // Lbl_From
            // 
            this.Lbl_From.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_From.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(37, 48);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.Size = new System.Drawing.Size(73, 24);
            this.Lbl_From.TabIndex = 23;
            this.Lbl_From.Text = "From";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(49, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 29);
            this.label2.TabIndex = 22;
            this.label2.Text = "Tire Defect";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboB_TireDefect
            // 
            this.comboB_TireDefect.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_TireDefect.FormattingEnabled = true;
            this.comboB_TireDefect.Items.AddRange(new object[] {
            "All",
            "OK",
            "Failed",
            "Repair"});
            this.comboB_TireDefect.Location = new System.Drawing.Point(117, 104);
            this.comboB_TireDefect.Name = "comboB_TireDefect";
            this.comboB_TireDefect.Size = new System.Drawing.Size(114, 26);
            this.comboB_TireDefect.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.SlateGray;
            this.label1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(37, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 24);
            this.label1.TabIndex = 28;
            this.label1.Text = "Status";
            // 
            // Txt_Pattern
            // 
            this.Txt_Pattern.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Pattern.Location = new System.Drawing.Point(117, 133);
            this.Txt_Pattern.MaxLength = 3;
            this.Txt_Pattern.Name = "Txt_Pattern";
            this.Txt_Pattern.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Pattern.Size = new System.Drawing.Size(114, 25);
            this.Txt_Pattern.TabIndex = 27;
            this.Txt_Pattern.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_Pattern.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Pattern_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.SlateGray;
            this.label3.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(37, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 24);
            this.label3.TabIndex = 41;
            this.label3.Text = "Pattern";
            // 
            // Txt_Catalog
            // 
            this.Txt_Catalog.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Catalog.Location = new System.Drawing.Point(117, 161);
            this.Txt_Catalog.MaxLength = 8;
            this.Txt_Catalog.Name = "Txt_Catalog";
            this.Txt_Catalog.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Catalog.Size = new System.Drawing.Size(114, 25);
            this.Txt_Catalog.TabIndex = 28;
            this.Txt_Catalog.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_Catalog.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Prod_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.SlateGray;
            this.label4.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(37, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 24);
            this.label4.TabIndex = 40;
            this.label4.Text = "Catalog";
            // 
            // Txt_DefectCode
            // 
            this.Txt_DefectCode.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_DefectCode.Location = new System.Drawing.Point(117, 190);
            this.Txt_DefectCode.MaxLength = 11;
            this.Txt_DefectCode.Name = "Txt_DefectCode";
            this.Txt_DefectCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_DefectCode.Size = new System.Drawing.Size(114, 25);
            this.Txt_DefectCode.TabIndex = 29;
            this.Txt_DefectCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_DefectCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_DefectCode_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.SlateGray;
            this.label5.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(37, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 24);
            this.label5.TabIndex = 43;
            this.label5.Text = "D. Code";
            // 
            // Frm_TireDefect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.Txt_DefectCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Txt_Pattern);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_Catalog);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboB_TireDefect);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.DTPicker_To);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DTPicker_From);
            this.Controls.Add(this.Lbl_From);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_TireDefect";
            this.Text = "Tire Defect";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.DateTimePicker DTPicker_To;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.DateTimePicker DTPicker_From;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboB_TireDefect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_Pattern;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Catalog;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_DefectCode;
        private System.Windows.Forms.Label label5;
    }
}