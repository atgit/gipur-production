﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_TireDefect : Form
    {
        string StrCodeList;

        public Frm_TireDefect()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            // תיקון מתאריך 22/07/2013
            // דוח חדש עבור דיווה נתן ההודי, הצגת נתוני צמיגים שעברו ביקורת חמה
            // 1. ולא היה להם תיקון קל או כבד בחודש האחרון OK - כמה צמיגים היו
            // 2. FAILED - כמה צמיגים היו
            // 3. REPAIR - כמה צמיגים היו
            
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("Error : Begining Date Bigger Than End Date", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }

            if (comboB_TireDefect.Text.Trim() == "")
            {
                MessageBox.Show("Error : Please Select Tire Status.","Curing / Building Production"); 
                comboB_TireDefect.Focus();
                return;
            }

            if (Txt_Pattern.Text.Trim() != "")
            {
                if (Txt_Pattern.Text.Length != 3)
                {
                    MessageBox.Show("Error : Wrong Pattern.", "Curing / Building Production");
                    Txt_Pattern.Focus();
                    return;
                }
            }

            if (Txt_Catalog.Text.Trim() != "")
            {
                if (Txt_Catalog.Text.Length != 8)
                {
                    MessageBox.Show("Error : Wrong Catalog.", "Curing / Building Production");
                    Txt_Catalog.Focus();
                    return;
                }
            }

            TireDefect(); 
        }

        private void TireDefect()
        {
            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            StrCodeList = "";
            int A, B, C;
            B = 0;
            C = 0;
            // בדיקה אם המשתמש ביקש חיתוך לפי דגם מסויים
            //             אפשרתי למשתמש לבחור עד 3 דגמים            
            if (Txt_DefectCode.Text.Trim() != "")
            {
                for (A = 0; A <= Txt_DefectCode.Text.Trim().Length - 1; A++)
                {
                    if (Txt_DefectCode.Text.Substring(A, 1) == ".")
                    {                        
                        StrCodeList += "'"+Txt_DefectCode.Text.Substring(B, 3)+"',";
                        B = A + 1;
                        C += 1;
                    }
                }
                // תפיסת רשומה אחרונה                
                StrCodeList += "'" + Txt_DefectCode.Text.Substring(B, 3) + "'";
            }

            if (comboB_TireDefect.Text.Trim() == "All")
            {
                Opt_All();
            }
            else if (comboB_TireDefect.Text.Trim() == "OK")
            {
                Opt_OK();
            }
            else if (comboB_TireDefect.Text.Trim() == "Failed")
            {
                Opt_Failed();                   
            }
            else if (comboB_TireDefect.Text.Trim() == "Repair")
            {
                Opt_Repair(); 
            }            
        }

        private void Opt_All()
        {
            Cursor.Current = Cursors.WaitCursor;
            string DtTO = "";
            string DtFrom = "";
            string DtFromFix = ""; // משמש בהתייחסות לצמיגים תקינים, בדיקה שלא היה דיווח תיקון כמה ימים לפני תקופת הדוח            

            DBConn DBC;
            string Con_Str;
            string StrSql = "";

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            int I = 3;
            double Tire_OK = 0;
            double Tire_Failed = 0;
            double Tire_Repair = 0;
            double Tire_Total = 0;
            string SMold = "";

            DtFrom = DTPicker_From.Value.ToString("yyyyMMdd");
            DtTO = DTPicker_To.Value.ToString("yyyyMMdd");
            DtFromFix = DTPicker_From.Value.AddDays(-3).ToString("yyyyMMdd");                                    

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet1, xlWorkSheet2;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            // Office 2013 עקב שדרוג ל 
            //xlWorkSheet1 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //xlWorkSheet2 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);            
            xlWorkSheet2 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet1 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet1.DisplayRightToLeft = false;
            xlWorkSheet2.DisplayRightToLeft = false;

            xlWorkSheet1.Name = "Tire Defect";
            xlWorkSheet1.Cells[1, 1] = "Tire Defect Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            // מירכוז + מיזוג + פונט מודגש לתחום                        
            chartRange = xlWorkSheet1.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet1.Cells[3, 1] = "OK";
            xlWorkSheet1.Cells[3, 2] = "Failed";
            xlWorkSheet1.Cells[3, 3] = "Repair";
            xlWorkSheet1.Cells[3, 4] = "Total";
            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("A1", "D1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A3", "E5");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // שליפת כמות צמיגים תקינים שעברו בביקורת חמה במחלקת גיפור
            // ולא היה להם דיווח תיקון קל או תיקון כבד בשלושת הימים האחרונים                        
            StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGL90 " +
                        "exception join " +
                        "(select LPROD||LLBLNO as x from TAPIALI.LABELGL90 where LACNDT between " + DtFromFix + " and " + DtTO + " and LACTAN=45 and LSTT in ('P','R')) " +
                        "as y on LPROD||LLBLNO=x " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='T' ";

            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,8)='" + Txt_Catalog.Text + "' ";            

            DBC.Q_Run1(StrSql);
            if (DBC.Rs1.HasRows)
            {
                xlWorkSheet1.Cells[4, 1] = DBC.Rs1.GetValue(0);
                Tire_OK = Convert.ToDouble(DBC.Rs1.GetValue(0));
            }
            DBC.Rs1.Close();

            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            if (StrCodeList.Trim() == "")
                // שליפת כמות צמיגים תקינים - שהיה להם דיווח על תיקון קל בלבד ולא היה להם דיווח על תיקון כבד                 
                StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGL90 " +
                         "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='T' and LPROD||LLBLNO " +
                              "in (select distinct LPROD||LLBLNO from TAPIALI.LABELGL90 where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='P' and LPROD||LLBLNO " +
                              "not in (select distinct LPROD||LLBLNO from TAPIALI.LABELGL90 where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='R')) ";
            else
                // שליפת כמות צמיגים תקינים - שהיה להם דיווח על תיקון קל בלבד ולא היה להם דיווח על תיקון כבד                 
                StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGL90 " +
                         "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='T' and LPROD||LLBLNO " +
                              "in (select distinct LPROD||LLBLNO from TAPIALI.LABELGL90 where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='P' and LERRDS in (" + StrCodeList + ") and LPROD||LLBLNO " +
                              "not in (select distinct LPROD||LLBLNO from TAPIALI.LABELGL90 where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='R')) ";

            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            if (StrCodeList.Trim() != "")
                if (Txt_DefectCode.Text.Trim() != "")
                    StrSql += "and LERRDS in (" + StrCodeList + ") ";

            DBC.Q_Run1(StrSql);
            if (DBC.Rs1.HasRows)
            {
                xlWorkSheet1.Cells[4, 2] = DBC.Rs1.GetValue(0);
                Tire_Failed = Convert.ToDouble(DBC.Rs1.GetValue(0));
            }
            DBC.Rs1.Close();

            //  שליפת כמות צמיגים שדווחו כפגומים --> שמועברים לתיקון אצל חזן
            StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGL90 " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='R' ";

            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            if (StrCodeList.Trim() != "")
                if (Txt_DefectCode.Text.Trim() != "")
                    StrSql += "and LERRDS in (" + StrCodeList + ") ";

            DBC.Q_Run1(StrSql);
            if (DBC.Rs1.HasRows)
            {
                xlWorkSheet1.Cells[4, 3] = DBC.Rs1.GetValue(0);
                Tire_Repair = Convert.ToDouble(DBC.Rs1.GetValue(0));
            }
            DBC.Rs1.Close();

            xlWorkSheet1.Cells[4, 4] = "=A4+B4+C4";
            Tire_Total = Tire_OK + Tire_Failed + Tire_Repair;

            xlWorkSheet1.Cells[5, 1] = Math.Round((Tire_OK / Tire_Total) * 100, 1);
            xlWorkSheet1.Cells[5, 2] = Math.Round((Tire_Failed / Tire_Total) * 100, 1);
            xlWorkSheet1.Cells[5, 3] = Math.Round((Tire_Repair / Tire_Total) * 100, 1);
            xlWorkSheet1.Cells[5, 4] = "=A5+B5+C5";
            xlWorkSheet1.Cells[5, 5] = "(%)";

            xlWorkSheet2.Name = "Tire Defect Detail";
            xlWorkSheet2.Cells[1, 1] = "Tire Defect Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            xlWorkSheet2.Cells[2, 1] = "Date";
            xlWorkSheet2.Cells[2, 2] = "Shift";
            xlWorkSheet2.Cells[2, 3] = "Time";
            xlWorkSheet2.Cells[2, 4] = "Size";
            xlWorkSheet2.Cells[2, 5] = "Catalog";
            xlWorkSheet2.Cells[2, 6] = "Serial";
            
            xlWorkSheet2.Cells[2, 7] = "DNA";
            
            xlWorkSheet2.Cells[2, 8] = "Status";
            xlWorkSheet2.Cells[2, 9] = "Defect Code";
            xlWorkSheet2.Cells[2, 10] = "Spec";
            xlWorkSheet2.Cells[2, 11] = "Press";
            xlWorkSheet2.Cells[2, 12] = "User";            
            xlWorkSheet2.Cells[2, 13] = "Mold No.";

            // תוספת מתאריך 07/06/2015          
            // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות                                
            xlWorkSheet2.Cells[1, 14] = "Press Open";
            xlWorkSheet2.Cells[2, 14] = "Emp No.";
            xlWorkSheet2.Cells[2, 15] = "Emp Name";                        

            // תוספת מתאריך 18/06/2015
            // הוספת מס' גפר + שם גפר שהכניס את הצמיג לבקשת נדב מאבטחת איכות                                
            xlWorkSheet2.Cells[1, 16] = "Press Close";
            xlWorkSheet2.Cells[2, 16] = "Emp No.";
            xlWorkSheet2.Cells[2, 17] = "Emp Name";

            // תוספת מתאריך 18/06/2015
            // SMO הוספת הנתון אם תבנית
            xlWorkSheet2.Cells[2, 18] = "SMO Mold";
            xlWorkSheet2.Cells[2, 19] = "Michelin";            

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet2.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet2.get_Range("A1", "S1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;

            chartRange = xlWorkSheet2.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet2.get_Range("A2", "S2");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet2.get_Range("A3", "A10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet2.get_Range("D3", "D10000");
            chartRange.NumberFormat = "@";

            // שליפת פירוט צמיגים תקינים שעברו בביקורת חמה במחלקת גיפור
            // ולא היה להם דיווח תיקון קל או תיקון כבד בשלושת הימים האחרונים            
            StrSql = "select a.LPROD||a.LLBLNO,min(a.LSHIFT),min(a.LPROD),min(a.LLBLNO),min(a.LSTT),min(a.LERRDS),min(a.LUSER),min(a.LACNTM),min(INSIZ),min(c.LSPEC),min(c.LMACH),min(a.LACNDT),max(c.LLBLNA) " +
                     "from TAPIALI.LABELGL22 a join " +
                          "BPCSFALI.IIMNL01 on substr(a.LPROD,1,8)=INPROD left join " +
                          "TAPIALI.LABELGL22 c on a.LPROD||a.LLBLNO=c.LPROD||c.LLBLNO and c.LACTAN=42 and c.LACNDT between " + DtFromFix + " and " + DtTO + " " +
                     "where a.LACNDT between " + DtFrom + " and " + DtTO + " and a.LACTAN=45 and a.LSTT='T' and " +
                           "a.LPROD||a.LLBLNO not in (select b.LPROD||b.LLBLNO from TAPIALI.LABELGL22 b " +
                           "where b.LACNDT between " + DtFromFix + " and " + DtTO + " and b.LACTAN=45 and b.LSTT in ('P','R')) ";

            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            StrSql+= "group by a.LPROD||a.LLBLNO " +
                     "order by a.LPROD||a.LLBLNO"; 

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet2.Cells[I, 1] = DBC.Rs1.GetDecimal(11).ToString().Substring(6,2)+"/"+DBC.Rs1.GetDecimal(11).ToString().Substring(4,2)+"/"+DBC.Rs1.GetDecimal(11).ToString().Substring(0,4);
                xlWorkSheet2.Cells[I, 2] = DBC.Rs1.GetString(1);

                if (DBC.Rs1.GetDecimal(7).ToString().Length == 1)
                    xlWorkSheet2.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 2)
                    xlWorkSheet2.Cells[I, 3] = "00:00:" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 3)
                    xlWorkSheet2.Cells[I, 3] = "00:0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 4)
                    xlWorkSheet2.Cells[I, 3] = "00:" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 5)
                    xlWorkSheet2.Cells[I, 3] = "0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(3, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 6)
                    xlWorkSheet2.Cells[I, 3] = DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(4, 2);
                
                xlWorkSheet2.Cells[I, 4] = DBC.Rs1.GetString(8);
                xlWorkSheet2.Cells[I, 5] = DBC.Rs1.GetString(2);
                xlWorkSheet2.Cells[I, 6] = DBC.Rs1.GetDecimal(3).ToString();//GetString(3);                

                xlWorkSheet2.Cells[I, 7] = DBC.Rs1.GetValue(12);

                xlWorkSheet2.Cells[I, 8] = "OK";
                xlWorkSheet2.Cells[I, 9] = DBC.Rs1.GetString(5).Trim();
                
                // תוספת מתאריך 19/04/2017
                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                if (DBC.Rs1.GetString(2).Substring(0, 3) == "771" || DBC.Rs1.GetString(2).Substring(0, 3) == "781" || DBC.Rs1.GetString(2).Substring(0, 3) == "791" || DBC.Rs1.GetString(2).Substring(0, 3) == "772")
                    xlWorkSheet2.Cells[I, 19] = "Michelin";

                if (!DBC.Rs1.IsDBNull(9))                
                {
                    xlWorkSheet2.Cells[I, 10] = DBC.Rs1.GetString(9).Trim();
                    xlWorkSheet2.Cells[I, 11] = DBC.Rs1.GetString(10).Trim();

                    // 23/07/2014 תוספת מתאריך 
                    // הוספת מס' תבנית לבקשת ג'וגאן ההודי                    
                    if (DBC.Rs1.GetString(9).Trim() != "")
                    {
                        SMold = DBC.Rs1.GetString(9).Trim().Substring(DBC.Rs1.GetString(9).Trim().Length - 9, 9);

                        // תוספת מתאריך 18/06/2015
                        // SMO הוספת הנתון אם תבנית                                                                        
                        //StrSql = "select MFTVDG,MFTVN,MFTVNT from  MFRT.MFRTP " +
                        //         "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";
                        StrSql = "select MFTVDG,MFTVN,MFTVNT,TASMO " +
                                 "from  MFRT.MFRTP join MFRT.TVNMHAP ON MFTVDG=TATVDG and MFTVN=TATVNT and MFTVNT=TATVNO  " +
                                 "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";                                                                                                  
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet2.Cells[I, 13] = DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString();
                            xlWorkSheet2.Cells[I, 18] = DBC.Rs3.GetString(3).Trim();
                            //MessageBox.Show(DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString());
                        }
                        DBC.Rs3.Close();
                        SMold = "";
                    }

                    // תוספת מתאריך 07/06/2015          
                    // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות                                        
                    // נתוני גפר                                        
                    // תוספת מתאריך 18/06/2015          
                    //StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                    //         "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    StrSql = "select LOVED,KPROD1,KSERIAL1 " +
                             "from TAPIALI.LABELGL3 left join TAPIALI.LABKSRP on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                             "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";

                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet2.Cells[I, 14] = DBC.Rs3.GetString(0);
                        xlWorkSheet2.Cells[I, 15] = GetUserName(DBC.Rs3.GetString(0));

                        // תוספת מתאריך 18/06/2015          
                        // שליפת מס' גפר + שם גפר שהכניס את הצמיג לגיפור
                        if (!DBC.Rs3.IsDBNull(1))
                        {
                            StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                                     "where LPROD='" + DBC.Rs3.GetString(1) + "' and LLBLNO=" + DBC.Rs3.GetDecimal(2).ToString() + " and LACTAN=41";
                            DBC.Rs3.Close();
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                            {
                                xlWorkSheet2.Cells[I, 16] = DBC.Rs3.GetString(0);
                                xlWorkSheet2.Cells[I, 17] = GetUserName(DBC.Rs3.GetString(0));
                            }
                        }
                    }
                    DBC.Rs3.Close();
                }                
                xlWorkSheet2.Cells[I, 12] = DBC.Rs1.GetString(6);                               
                I += 1;
            }
            DBC.Rs1.Close();

            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            if (StrCodeList.Trim() =="")
                // שליפת פירוט צמיגים תקינים - שהיה להם דיווח על תיקון קל בלבד ולא היה להם דיווח על תיקון כבד
                StrSql = "select a.LPROD||a.LLBLNO,min(a.LSHIFT),min(a.LPROD),min(a.LLBLNO),min(a.LSTT),min(a.LERRDS),min(a.LUSER),min(a.LACNTM),min(INSIZ),min(d.LSPEC),min(d.LMACH),min(a.LACNDT),max(d.LLBLNA) " +
                         "from TAPIALI.LABELGL22 a join BPCSFALI.IIMNL01 on substr(a.LPROD,1,8)=INPROD left join " +
                         "TAPIALI.LABELGL22 d on a.LPROD||a.LLBLNO=d.LPROD||d.LLBLNO and d.LACTAN=42 and d.LACNDT between " + DtFromFix + " and " + DtTO + " " +
                         "where a.LACNDT between " + DtFrom + " and " + DtTO + " and a.LACTAN=45 and a.LSTT='P' and a.LPROD||a.LLBLNO " +
                              "in (select distinct b.LPROD||b.LLBLNO from TAPIALI.LABELGL22  b where b.LACNDT between " + DtFrom + " and " + DtTO + " and b.LACTAN=45 and b.LSTT='T' and b.LPROD||b.LLBLNO " +
                              "not in (select distinct c.LPROD||c.LLBLNO from TAPIALI.LABELGL22 c where c.LACNDT between " + DtFrom + " and " + DtTO + " and c.LACTAN=45 and c.LSTT='R')) ";
            else
                // שליפת פירוט צמיגים תקינים - שהיה להם דיווח על תיקון קל בלבד ולא היה להם דיווח על תיקון כבד
                StrSql = "select a.LPROD||a.LLBLNO,min(a.LSHIFT),min(a.LPROD),min(a.LLBLNO),min(a.LSTT),min(a.LERRDS),min(a.LUSER),min(a.LACNTM),min(INSIZ),min(d.LSPEC),min(d.LMACH),min(a.LACNDT),max(d.LLBLNA) " +
                         "from TAPIALI.LABELGL22 a join BPCSFALI.IIMNL01 on substr(a.LPROD,1,8)=INPROD left join " +
                         "TAPIALI.LABELGL22 d on a.LPROD||a.LLBLNO=d.LPROD||d.LLBLNO and d.LACTAN=42 and d.LACNDT between " + DtFromFix + " and " + DtTO + " " +
                         "where a.LACNDT between " + DtFrom + " and " + DtTO + " and a.LACTAN=45 and a.LSTT='P' and a.LERRDS in (" + StrCodeList + ") and a.LPROD||a.LLBLNO " +
                              "in (select distinct b.LPROD||b.LLBLNO from TAPIALI.LABELGL22  b where b.LACNDT between " + DtFrom + " and " + DtTO + " and b.LACTAN=45 and b.LSTT='T' and b.LPROD||b.LLBLNO " +
                              "not in (select distinct c.LPROD||c.LLBLNO from TAPIALI.LABELGL22 c where c.LACNDT between " + DtFrom + " and " + DtTO + " and c.LACTAN=45 and c.LSTT='R')) ";


            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            StrSql+= "group by a.LPROD||a.LLBLNO " +
                     "order by a.LPROD||a.LLBLNO";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet2.Cells[I, 1] = DBC.Rs1.GetDecimal(11).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(0, 4);
                xlWorkSheet2.Cells[I, 2] = DBC.Rs1.GetString(1);

                if (DBC.Rs1.GetDecimal(7).ToString().Length == 1)
                    xlWorkSheet2.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 2)
                    xlWorkSheet2.Cells[I, 3] = "00:00:" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 3)
                    xlWorkSheet2.Cells[I, 3] = "00:0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 4)
                    xlWorkSheet2.Cells[I, 3] = "00:" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 5)
                    xlWorkSheet2.Cells[I, 3] = "0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(3, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 6)
                    xlWorkSheet2.Cells[I, 3] = DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(4, 2);
  
                xlWorkSheet2.Cells[I, 4] = DBC.Rs1.GetString(8);
                xlWorkSheet2.Cells[I, 5] = DBC.Rs1.GetString(2);
                xlWorkSheet2.Cells[I, 6] = DBC.Rs1.GetDecimal(3).ToString();//GetString(3);                

                xlWorkSheet2.Cells[I, 7] = DBC.Rs1.GetValue(12);

                xlWorkSheet2.Cells[I, 8] = "Failed";                
                xlWorkSheet2.Cells[I, 9] = DBC.Rs1.GetString(5).Trim();

                // תוספת מתאריך 19/04/2017
                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                if (DBC.Rs1.GetString(2).Substring(0, 3) == "771" || DBC.Rs1.GetString(2).Substring(0, 3) == "781" || DBC.Rs1.GetString(2).Substring(0, 3) == "791" || DBC.Rs1.GetString(2).Substring(0, 3) == "772")
                    xlWorkSheet2.Cells[I, 19] = "Michelin";

                if (!DBC.Rs1.IsDBNull(9))
                {
                    xlWorkSheet2.Cells[I, 10] = DBC.Rs1.GetString(9).Trim();
                    xlWorkSheet2.Cells[I, 11] = DBC.Rs1.GetString(10).Trim();

                    // 23/07/2014 תוספת מתאריך 
                    // הוספת מס' תבנית לבקשת ג'וגאן ההודי
                    if (DBC.Rs1.GetString(9).Trim() != "")
                    {
                        SMold = DBC.Rs1.GetString(9).Trim().Substring(DBC.Rs1.GetString(9).Trim().Length - 9, 9);
                        
                        // תוספת מתאריך 18/06/2015
                        // SMO הוספת הנתון אם תבנית                                                                        
                        //StrSql = "select MFTVDG,MFTVN,MFTVNT from  MFRT.MFRTP " +
                        //         "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";
                        StrSql = "select MFTVDG,MFTVN,MFTVNT,TASMO " +
                                 "from  MFRT.MFRTP join MFRT.TVNMHAP ON MFTVDG=TATVDG and MFTVN=TATVNT and MFTVNT=TATVNO  " +
                                 "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";                                                                          
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet2.Cells[I, 13] = DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString();
                            xlWorkSheet2.Cells[I, 18] = DBC.Rs3.GetString(3).Trim();
                            //MessageBox.Show(DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString());
                        }
                        DBC.Rs3.Close();
                        SMold = "";
                    }

                    // תוספת מתאריך 07/06/2015          
                    // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות

                    // תוספת מתאריך 07/06/2015          
                    // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות                                        
                    // נתוני גפר
                    // תוספת מתאריך 18/06/2015          
                    //StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                    //         "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    StrSql = "select LOVED,KPROD1,KSERIAL1 " +
                             "from TAPIALI.LABELGL3 left join TAPIALI.LABKSRP on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                             "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";

                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet2.Cells[I, 14] = DBC.Rs3.GetString(0);
                        xlWorkSheet2.Cells[I, 15] = GetUserName(DBC.Rs3.GetString(0));

                        // תוספת מתאריך 18/06/2015          
                        // שליפת מס' גפר + שם גפר שהכניס את הצמיג לגיפור
                        if (!DBC.Rs3.IsDBNull(1))
                        {
                            StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                                     "where LPROD='" + DBC.Rs3.GetString(1) + "' and LLBLNO=" + DBC.Rs3.GetDecimal(2).ToString() + " and LACTAN=41";
                            DBC.Rs3.Close();
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                            {
                                xlWorkSheet2.Cells[I, 16] = DBC.Rs3.GetString(0);
                                xlWorkSheet2.Cells[I, 17] = GetUserName(DBC.Rs3.GetString(0));
                            }
                        }
                    }
                    DBC.Rs3.Close();
                }
                xlWorkSheet2.Cells[I, 12] = DBC.Rs1.GetString(6);
                I += 1;
            }
            DBC.Rs1.Close();

            //  שליפת פירוט צמיגים שדווחו כפגומים --> שמועברים לתיקון אצל חזן
            StrSql = "select a.LPROD||a.LLBLNO,min(a.LSHIFT),min(a.LPROD),min(a.LLBLNO),min(a.LSTT),min(a.LERRDS),min(a.LUSER),min(a.LACNTM),min(INSIZ),min(b.LSPEC),min(b.LMACH),min(a.LACNDT),max(b.LLBLNA) " +
                     "from TAPIALI.LABELGL22 a join " +
                          "BPCSFALI.IIMNL01 on substr(a.LPROD,1,8)=INPROD left join " +
                          "TAPIALI.LABELGL22 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO and b.LACTAN=42 and b.LACNDT between " + DtFromFix + " and " + DtTO + " " +
                     "where a.LACNDT between " + DtFrom + " and " + DtTO + " and a.LACTAN=45 and a.LSTT='R' ";

            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            if (StrCodeList.Trim() != "")
                if (Txt_DefectCode.Text.Trim() != "")
                    StrSql += "and a.LERRDS in (" + StrCodeList + ") ";

            StrSql+= "group by a.LPROD||a.LLBLNO " +
                     "order by a.LPROD||a.LLBLNO";                       
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet2.Cells[I, 1] = DBC.Rs1.GetDecimal(11).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(0, 4);
                xlWorkSheet2.Cells[I, 2] = DBC.Rs1.GetString(1);

                if (DBC.Rs1.GetDecimal(7).ToString().Length == 1)
                    xlWorkSheet2.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 2)
                    xlWorkSheet2.Cells[I, 3] = "00:00:" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 3)
                    xlWorkSheet2.Cells[I, 3] = "00:0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 4)
                    xlWorkSheet2.Cells[I, 3] = "00:" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 5)
                    xlWorkSheet2.Cells[I, 3] = "0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(3, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 6)
                    xlWorkSheet2.Cells[I, 3] = DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(4, 2);
                
                xlWorkSheet2.Cells[I, 4] = DBC.Rs1.GetString(8);
                xlWorkSheet2.Cells[I, 5] = DBC.Rs1.GetString(2);
                xlWorkSheet2.Cells[I, 6] = DBC.Rs1.GetDecimal(3).ToString();//GetString(3);

                xlWorkSheet2.Cells[I, 7] = DBC.Rs1.GetValue(12);

                xlWorkSheet2.Cells[I, 8] = "Repair";
                xlWorkSheet2.Cells[I, 9] = DBC.Rs1.GetString(5).Trim();

                // תוספת מתאריך 19/04/2017
                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                if (DBC.Rs1.GetString(2).Substring(0, 3) == "771" || DBC.Rs1.GetString(2).Substring(0, 3) == "781" || DBC.Rs1.GetString(2).Substring(0, 3) == "791" || DBC.Rs1.GetString(2).Substring(0, 3) == "772")
                    xlWorkSheet2.Cells[I, 19] = "Michelin";

                if (!DBC.Rs1.IsDBNull(9))
                {
                    xlWorkSheet2.Cells[I, 10] = DBC.Rs1.GetString(9).Trim();
                    xlWorkSheet2.Cells[I, 11] = DBC.Rs1.GetString(10).Trim();

                    // 23/07/2014 תוספת מתאריך 
                    // הוספת מס' תבנית לבקשת ג'וגאן ההודי
                    if (DBC.Rs1.GetString(9).Trim() != "")
                    {
                        SMold = DBC.Rs1.GetString(9).Trim().Substring(DBC.Rs1.GetString(9).Trim().Length - 9, 9);

                        // תוספת מתאריך 18/06/2015
                        // SMO הוספת הנתון אם תבנית                                                                        
                        //StrSql = "select MFTVDG,MFTVN,MFTVNT from  MFRT.MFRTP " +
                        //         "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";
                        StrSql = "select MFTVDG,MFTVN,MFTVNT,TASMO " +
                                 "from  MFRT.MFRTP join MFRT.TVNMHAP ON MFTVDG=TATVDG and MFTVN=TATVNT and MFTVNT=TATVNO  " +
                                 "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";                                                
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet2.Cells[I, 13] = DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString();
                            xlWorkSheet2.Cells[I, 18] = DBC.Rs3.GetString(3).Trim();
                            //MessageBox.Show(DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString());
                        }
                        DBC.Rs3.Close();
                        SMold = "";
                    }

                    // תוספת מתאריך 07/06/2015          
                    // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות
                    // נתוני גפר
                    // תוספת מתאריך 18/06/2015          
                    //StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                    //         "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    StrSql = "select LOVED,KPROD1,KSERIAL1 " +
                             "from TAPIALI.LABELGL3 left join TAPIALI.LABKSRP on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                             "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet2.Cells[I, 14] = DBC.Rs3.GetString(0);
                        xlWorkSheet2.Cells[I, 15] = GetUserName(DBC.Rs3.GetString(0));

                        // תוספת מתאריך 18/06/2015          
                        // שליפת מס' גפר + שם גפר שהכניס את הצמיג לגיפור
                        if (!DBC.Rs3.IsDBNull(1))
                        {
                            StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                                     "where LPROD='" + DBC.Rs3.GetString(1) + "' and LLBLNO=" + DBC.Rs3.GetDecimal(2).ToString() + " and LACTAN=41";
                            DBC.Rs3.Close();
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                            {
                                xlWorkSheet2.Cells[I, 16] = DBC.Rs3.GetString(0);
                                xlWorkSheet2.Cells[I, 17] = GetUserName(DBC.Rs3.GetString(0));
                            }
                        }
                    }
                    DBC.Rs3.Close();
                }
                xlWorkSheet2.Cells[I, 12] = DBC.Rs1.GetString(6);
                I += 1;
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            xlApp.Visible = true;
            releaseObject(xlWorkSheet1);
            releaseObject(xlWorkSheet2);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void Opt_OK()
        {

            Cursor.Current = Cursors.WaitCursor;
            string DtTO = "";
            string DtFrom = "";
            string DtFromFix = ""; // משמש בהתייחסות לצמיגים תקינים, בדיקה שלא היה דיווח תיקון חודש לפני תקופת הדוח            
            string SMold = "";

            DBConn DBC;
            string Con_Str;
            string StrSql = "";

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            int I = 3;
            DtFrom = DTPicker_From.Value.ToString("yyyyMMdd");
            DtTO = DTPicker_To.Value.ToString("yyyyMMdd");
            DtFromFix = DTPicker_From.Value.AddDays(-3).ToString("yyyyMMdd");

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet, xlWorkSheet1;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            // Office 2013 עקב שדרוג ל 
            //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //xlWorkSheet1 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
            xlWorkSheet1 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;            
            xlWorkSheet1.DisplayRightToLeft = false;

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "F1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "F5");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            xlWorkSheet.Name = "Tire Defect";
            xlWorkSheet.Cells[1, 1] = "Tire Defect Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            xlWorkSheet.Cells[3, 1] = "OK";                        

            // שליפת כמות צמיגים תקינים, שלא היה להם דיווח תיקון קל או תיקון אצל חזן בחודש האחרון                
            StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGL90 " +
                        "exception join " +
                        "(select LPROD||LLBLNO as x from TAPIALI.LABELGL90 where LACNDT between " + DtFromFix + " and " + DtTO + " and LACTAN=45 and LSTT in ('P','R')) " +
                        "as y on LPROD||LLBLNO=x " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='T' ";

            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            DBC.Q_Run1(StrSql);
            if (DBC.Rs1.HasRows)            
                xlWorkSheet.Cells[4, 1] = DBC.Rs1.GetValue(0);                            
            DBC.Rs1.Close();

            xlWorkSheet1.Name = "Tire Defect Detail";
            xlWorkSheet1.Cells[1, 1] = "Tire Defect Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            xlWorkSheet1.Cells[2, 1] = "Date";
            xlWorkSheet1.Cells[2, 2] = "Shift";
            xlWorkSheet1.Cells[2, 3] = "Time";
            xlWorkSheet1.Cells[2, 4] = "Size";
            xlWorkSheet1.Cells[2, 5] = "Catalog";
            xlWorkSheet1.Cells[2, 6] = "Serial";

            xlWorkSheet1.Cells[2, 7] = "DNA";

            xlWorkSheet1.Cells[2, 8] = "Status";
            xlWorkSheet1.Cells[2, 9] = "Defect Code";
            xlWorkSheet1.Cells[2, 10] = "Spec";
            xlWorkSheet1.Cells[2, 11] = "Press";
            xlWorkSheet1.Cells[2, 12] = "User";
            xlWorkSheet1.Cells[2, 13] = "Mold No.";

            // תוספת מתאריך 07/06/2015          
            // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות                    
            xlWorkSheet1.Cells[1, 14] = "Press Open";
            xlWorkSheet1.Cells[2, 14] = "Emp No.";
            xlWorkSheet1.Cells[2, 15] = "Emp Name";

            // תוספת מתאריך 18/06/2015
            // הוספת מס' גפר + שם גפר שהכניס את הצמיג לבקשת נדב מאבטחת איכות                                
            xlWorkSheet1.Cells[1, 16] = "Press Close";
            xlWorkSheet1.Cells[2, 16] = "Emp No.";
            xlWorkSheet1.Cells[2, 17] = "Emp Name";

            // תוספת מתאריך 18/06/2015
            // SMO הוספת הנתון אם תבנית
            xlWorkSheet1.Cells[2, 18] = "SMO Mold";
            xlWorkSheet1.Cells[2, 19] = "Michelin";            

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("A1", "S1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;

            chartRange = xlWorkSheet1.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A2", "S2");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet1.get_Range("A3", "A10000");
            chartRange.NumberFormat = "@";

            chartRange = xlWorkSheet1.get_Range("D3", "D10000");
            chartRange.NumberFormat = "@";

            // שליפת פירוט צמיגים תקינים, שלא היה להם דיווח תיקון קל או תיקון אצל חזן בחודש האחרון                
            StrSql = "select a.LPROD||a.LLBLNO,min(a.LSHIFT),min(a.LPROD),min(a.LLBLNO),min(a.LSTT),min(a.LERRDS),min(a.LUSER),min(a.LACNTM),min(INSIZ),min(c.LSPEC),min(c.LMACH),min(a.LACNDT),max(c.LLBLNA) " +
                     "from TAPIALI.LABELGL22 a join BPCSFALI.IIMNL01 on substr(a.LPROD,1,8)=INPROD left join " +
                     "TAPIALI.LABELGL22 c on a.LPROD||a.LLBLNO=c.LPROD||c.LLBLNO and c.LACTAN=42 and c.LACNDT between " + DtFromFix + " and " + DtTO + " " +
                     "where a.LACNDT between " + DtFrom + " and " + DtTO + " and a.LACTAN=45 and a.LSTT='T' and " +
                           "a.LPROD||a.LLBLNO not in (select b.LPROD||b.LLBLNO from TAPIALI.LABELGL22 b " +
                           "where b.LACNDT between " + DtFromFix + " and " + DtTO + " and b.LACTAN=45 and b.LSTT in ('P','R')) ";

            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            StrSql+= "group by a.LPROD||a.LLBLNO " +
                     "order by a.LPROD||a.LLBLNO"; 
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet1.Cells[I, 1] = DBC.Rs1.GetDecimal(11).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(0, 4);
                xlWorkSheet1.Cells[I, 2] = DBC.Rs1.GetString(1);

                if (DBC.Rs1.GetDecimal(7).ToString().Length == 1)
                    xlWorkSheet1.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 2)
                    xlWorkSheet1.Cells[I, 3] = "00:00:" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 3)
                    xlWorkSheet1.Cells[I, 3] = "00:0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 4)
                    xlWorkSheet1.Cells[I, 3] = "00:" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 5)
                    xlWorkSheet1.Cells[I, 3] = "0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(3, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 6)
                    xlWorkSheet1.Cells[I, 3] = DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(4, 2);
                
                xlWorkSheet1.Cells[I, 4] = DBC.Rs1.GetString(8);
                xlWorkSheet1.Cells[I, 5] = DBC.Rs1.GetString(2);
                xlWorkSheet1.Cells[I, 6] = DBC.Rs1.GetDecimal(3).ToString();

                xlWorkSheet1.Cells[I, 7] = DBC.Rs1.GetValue(12);

                xlWorkSheet1.Cells[I, 8] = "OK";
                xlWorkSheet1.Cells[I, 9] = DBC.Rs1.GetString(5).Trim();

                // תוספת מתאריך 19/04/2017
                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                if (DBC.Rs1.GetString(2).Substring(0, 3) == "771" || DBC.Rs1.GetString(2).Substring(0, 3) == "781" || DBC.Rs1.GetString(2).Substring(0, 3) == "791" || DBC.Rs1.GetString(2).Substring(0, 3) == "772")
                    xlWorkSheet1.Cells[I, 19] = "Michelin";

                if (!DBC.Rs1.IsDBNull(9))
                {
                    xlWorkSheet1.Cells[I, 10] = DBC.Rs1.GetString(9).Trim();
                    xlWorkSheet1.Cells[I, 11] = DBC.Rs1.GetString(10).Trim();

                    // 23/07/2014 תוספת מתאריך 
                    // הוספת מס' תבנית לבקשת ג'וגאן ההודי
                    if (DBC.Rs1.GetString(9).Trim() != "")
                    {
                        SMold = DBC.Rs1.GetString(9).Trim().Substring(DBC.Rs1.GetString(9).Trim().Length - 9, 9);

                        // תוספת מתאריך 18/06/2015
                        // SMO הוספת הנתון אם תבנית                                                                        
                        //StrSql = "select MFTVDG,MFTVN,MFTVNT from  MFRT.MFRTP " +
                        //         "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";
                        StrSql = "select MFTVDG,MFTVN,MFTVNT,TASMO " +
                                 "from  MFRT.MFRTP join MFRT.TVNMHAP ON MFTVDG=TATVDG and MFTVN=TATVNT and MFTVNT=TATVNO  " +
                                 "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";                        
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet1.Cells[I, 13] = DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString();
                            xlWorkSheet1.Cells[I, 18] = DBC.Rs3.GetString(3).Trim();
                            //MessageBox.Show(DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString());
                        }
                        DBC.Rs3.Close();
                        SMold = "";
                    }

                    // תוספת מתאריך 07/06/2015          
                    // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות
                    // נתוני גפר
                    // תוספת מתאריך 18/06/2015                                                                  
                    //StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                    //         "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    StrSql = "select LOVED,KPROD1,KSERIAL1 " +
                             "from TAPIALI.LABELGL3 left join TAPIALI.LABKSRP on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                             "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet1.Cells[I, 14] = DBC.Rs3.GetString(0);
                        xlWorkSheet1.Cells[I, 15] = GetUserName(DBC.Rs3.GetString(0));

                        // תוספת מתאריך 18/06/2015          
                        // שליפת מס' גפר + שם גפר שהכניס את הצמיג לגיפור
                        if (!DBC.Rs3.IsDBNull(1))
                        {
                            StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                                     "where LPROD='" + DBC.Rs3.GetString(1) + "' and LLBLNO=" + DBC.Rs3.GetDecimal(2).ToString() + " and LACTAN=41";
                            DBC.Rs3.Close();
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                            {
                                xlWorkSheet1.Cells[I, 16] = DBC.Rs3.GetString(0);
                                xlWorkSheet1.Cells[I, 17] = GetUserName(DBC.Rs3.GetString(0));
                            }
                        }
                    }
                    DBC.Rs3.Close();
                }
                xlWorkSheet1.Cells[I, 12] = DBC.Rs1.GetString(6);
                I += 1;
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkSheet1);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void Opt_Failed()
        {
            Cursor.Current = Cursors.WaitCursor;
            string DtTO = "";
            string DtFrom = "";
            string DtFromFix = ""; // משמש בהתייחסות לצמיגים תקינים, בדיקה שלא היה דיווח תיקון כמה ימים לפני תקופת הדוח            
            string SMold = "";

            DBConn DBC;
            string Con_Str;
            string StrSql = "";

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            int I = 3;
            DtFrom = DTPicker_From.Value.ToString("yyyyMMdd");
            DtTO = DTPicker_To.Value.ToString("yyyyMMdd");
            DtFromFix = DTPicker_From.Value.AddDays(-3).ToString("yyyyMMdd");                                    

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet, xlWorkSheet1;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            // Office 2013 עקב שדרוג ל 
            //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //xlWorkSheet1 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
            xlWorkSheet1 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;
            xlWorkSheet1.DisplayRightToLeft = false;

            xlWorkSheet.Name = "Tire Defect";
            xlWorkSheet.Cells[1, 1] = "Tire Defect Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            xlWorkSheet.Cells[3, 1] = "Failed";

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "F1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "F5");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            if (StrCodeList.Trim() =="")
                // שליפת כמות צמיגים תקינים - שהיה להם דיווח על תיקון קל בלבד ולא היה להם דיווח על תיקון כבד     
                StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGL90 " +
                         "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='T' and LPROD||LLBLNO " +
                              "in (select distinct LPROD||LLBLNO from TAPIALI.LABELGL90 where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='P' and LPROD||LLBLNO " +
                              "not in (select distinct LPROD||LLBLNO from TAPIALI.LABELGL90 where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='R')) ";
            else
                // שליפת כמות צמיגים תקינים - שהיה להם דיווח על תיקון קל בלבד ולא היה להם דיווח על תיקון כבד     
                StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGL90 " +
                         "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='T' and LPROD||LLBLNO " +
                              "in (select distinct LPROD||LLBLNO from TAPIALI.LABELGL90 where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='P' and LERRDS in (" + StrCodeList + ") and LPROD||LLBLNO " +
                              "not in (select distinct LPROD||LLBLNO from TAPIALI.LABELGL90 where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='R')) ";

            
            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(LPROD,1,8)='" + Txt_Catalog.Text + "' ";
            
            DBC.Q_Run1(StrSql);
            if (DBC.Rs1.HasRows)
                xlWorkSheet.Cells[4, 1] = DBC.Rs1.GetValue(0);
            DBC.Rs1.Close();

            xlWorkSheet1.Name = "Tire Defect Detail";
            xlWorkSheet1.Cells[1, 1] = "Tire Defect Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            xlWorkSheet1.Cells[2, 1] = "Date";
            xlWorkSheet1.Cells[2, 2] = "Shift";
            xlWorkSheet1.Cells[2, 3] = "Time";
            xlWorkSheet1.Cells[2, 4] = "Size";
            xlWorkSheet1.Cells[2, 5] = "Catalog";
            xlWorkSheet1.Cells[2, 6] = "Serial";

            xlWorkSheet1.Cells[2, 7] = "DNA";

            xlWorkSheet1.Cells[2, 8] = "Status";
            xlWorkSheet1.Cells[2, 9] = "Defect Code";
            xlWorkSheet1.Cells[2, 10] = "Spec";
            xlWorkSheet1.Cells[2, 11] = "Press";
            xlWorkSheet1.Cells[2, 12] = "User";
            xlWorkSheet1.Cells[2, 13] = "Mold No.";

            // תוספת מתאריך 07/06/2015          
            // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות                    
            xlWorkSheet1.Cells[1, 14] = "Press Open";
            xlWorkSheet1.Cells[2, 14] = "Emp No.";
            xlWorkSheet1.Cells[2, 15] = "Emp Name";

            // תוספת מתאריך 18/06/2015
            // הוספת מס' גפר + שם גפר שהכניס את הצמיג לבקשת נדב מאבטחת איכות                                
            xlWorkSheet1.Cells[1, 16] = "Press Close";
            xlWorkSheet1.Cells[2, 16] = "Emp No.";
            xlWorkSheet1.Cells[2, 17] = "Emp Name";

            // תוספת מתאריך 18/06/2015
            // SMO הוספת הנתון אם תבנית
            xlWorkSheet1.Cells[2, 18] = "SMO Mold";
            xlWorkSheet1.Cells[2, 19] = "Michelin";            

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("A1", "S1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;

            chartRange = xlWorkSheet1.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A2", "S2");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet1.get_Range("A3", "A10000");
            chartRange.NumberFormat = "@";

            chartRange = xlWorkSheet1.get_Range("D3", "D10000");
            chartRange.NumberFormat = "@";

            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            if (StrCodeList.Trim() == "")
                // שליפת פירוט צמיגים תקינים - שהיה להם דיווח על תיקון קל בלבד ולא היה להם דיווח על תיקון כבד
                StrSql = "select a.LPROD||a.LLBLNO,min(a.LSHIFT),min(a.LPROD),min(a.LLBLNO),min(a.LSTT),min(a.LERRDS),min(a.LUSER),min(a.LACNTM),min(INSIZ),min(d.LSPEC),min(d.LMACH),min(a.LACNDT),max(d.LLBLNA) " +
                         "from TAPIALI.LABELGL22 a join BPCSFALI.IIMNL01 on substr(a.LPROD,1,8)=INPROD left join " +
                         "TAPIALI.LABELGL22 d on a.LPROD||a.LLBLNO=d.LPROD||d.LLBLNO and d.LACTAN=42 and d.LACNDT between " + DtFromFix + " and " + DtTO + " " +
                         "where a.LACNDT between " + DtFrom + " and " + DtTO + " and a.LACTAN=45 and a.LSTT='P' and a.LPROD||a.LLBLNO " +
                              "in (select distinct b.LPROD||b.LLBLNO from TAPIALI.LABELGL22  b where b.LACNDT between " + DtFrom + " and " + DtTO + " and b.LACTAN=45 and b.LSTT='T' and b.LPROD||b.LLBLNO " +
                              "not in (select distinct c.LPROD||c.LLBLNO from TAPIALI.LABELGL22 c where c.LACNDT between " + DtFrom + " and " + DtTO + " and c.LACTAN=45 and c.LSTT='R')) ";
            else
                // שליפת פירוט צמיגים תקינים - שהיה להם דיווח על תיקון קל בלבד ולא היה להם דיווח על תיקון כבד
                StrSql = "select a.LPROD||a.LLBLNO,min(a.LSHIFT),min(a.LPROD),min(a.LLBLNO),min(a.LSTT),min(a.LERRDS),min(a.LUSER),min(a.LACNTM),min(INSIZ),min(d.LSPEC),min(d.LMACH),min(a.LACNDT),max(d.LLBLNA) " +
                         "from TAPIALI.LABELGL22 a join BPCSFALI.IIMNL01 on substr(a.LPROD,1,8)=INPROD left join " +
                         "TAPIALI.LABELGL22 d on a.LPROD||a.LLBLNO=d.LPROD||d.LLBLNO and d.LACTAN=42 and d.LACNDT between " + DtFromFix + " and " + DtTO + " " +
                         "where a.LACNDT between " + DtFrom + " and " + DtTO + " and a.LACTAN=45 and a.LSTT='P' and a.LERRDS in (" + StrCodeList + ") and a.LPROD||a.LLBLNO " +
                              "in (select distinct b.LPROD||b.LLBLNO from TAPIALI.LABELGL22  b where b.LACNDT between " + DtFrom + " and " + DtTO + " and b.LACTAN=45 and b.LSTT='T' and b.LPROD||b.LLBLNO " +
                              "not in (select distinct c.LPROD||c.LLBLNO from TAPIALI.LABELGL22 c where c.LACNDT between " + DtFrom + " and " + DtTO + " and c.LACTAN=45 and c.LSTT='R')) ";

            
            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,3)='" + Txt_Pattern.Text + "' ";

            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            StrSql+= "group by a.LPROD||a.LLBLNO " +
                     "order by a.LPROD||a.LLBLNO";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet1.Cells[I, 1] = DBC.Rs1.GetDecimal(11).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(0, 4);
                xlWorkSheet1.Cells[I, 2] = DBC.Rs1.GetString(1);

                if (DBC.Rs1.GetDecimal(7).ToString().Length == 1)
                    xlWorkSheet1.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 2)
                    xlWorkSheet1.Cells[I, 3] = "00:00:" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 3)
                    xlWorkSheet1.Cells[I, 3] = "00:0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 4)
                    xlWorkSheet1.Cells[I, 3] = "00:" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 5)
                    xlWorkSheet1.Cells[I, 3] = "0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(3, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 6)
                    xlWorkSheet1.Cells[I, 3] = DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(4, 2);
                
                xlWorkSheet1.Cells[I, 4] = DBC.Rs1.GetString(8);
                xlWorkSheet1.Cells[I, 5] = DBC.Rs1.GetString(2);
                xlWorkSheet1.Cells[I, 6] = DBC.Rs1.GetDecimal(3).ToString();

                xlWorkSheet1.Cells[I, 7] = DBC.Rs1.GetValue(12);

                xlWorkSheet1.Cells[I, 8] = "Failed";
                xlWorkSheet1.Cells[I, 9] = DBC.Rs1.GetString(5).Trim();

                // תוספת מתאריך 19/04/2017
                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                if (DBC.Rs1.GetString(2).Substring(0, 3) == "771" || DBC.Rs1.GetString(2).Substring(0, 3) == "781" || DBC.Rs1.GetString(2).Substring(0, 3) == "791" || DBC.Rs1.GetString(2).Substring(0, 3) == "772")
                    xlWorkSheet1.Cells[I, 19] = "Michelin";

                if (!DBC.Rs1.IsDBNull(9))
                {
                    xlWorkSheet1.Cells[I, 10] = DBC.Rs1.GetString(9).Trim();
                    xlWorkSheet1.Cells[I, 11] = DBC.Rs1.GetString(10).Trim();

                    // 23/07/2014 תוספת מתאריך 
                    // הוספת מס' תבנית לבקשת ג'וגאן ההודי
                    if (DBC.Rs1.GetString(9).Trim() != "")
                    {
                        SMold = DBC.Rs1.GetString(9).Trim().Substring(DBC.Rs1.GetString(9).Trim().Length - 9, 9);

                        // תוספת מתאריך 18/06/2015
                        // SMO הוספת הנתון אם תבנית                        
                        //StrSql = "select MFTVDG,MFTVN,MFTVNT from  MFRT.MFRTP " +
                        //         "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";                        
                        StrSql = "select MFTVDG,MFTVN,MFTVNT,TASMO " +
                                 "from  MFRT.MFRTP join MFRT.TVNMHAP ON MFTVDG=TATVDG and MFTVN=TATVNT and MFTVNT=TATVNO  " +
                                 "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet1.Cells[I, 13] = DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString();
                            xlWorkSheet1.Cells[I, 18] = DBC.Rs3.GetString(3).Trim();
                            //MessageBox.Show(DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString());
                        }
                        DBC.Rs3.Close();
                        SMold = "";
                    }

                    // תוספת מתאריך 07/06/2015          
                    // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות
                    // נתוני גפר

                    // תוספת מתאריך 18/06/2015                                                                                                          
                    //StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                    //         "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    StrSql = "select LOVED,KPROD1,KSERIAL1 " +
                             "from TAPIALI.LABELGL3 left join TAPIALI.LABKSRP on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                             "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet1.Cells[I, 14] = DBC.Rs3.GetString(0);
                        xlWorkSheet1.Cells[I, 15] = GetUserName(DBC.Rs3.GetString(0));

                        // תוספת מתאריך 18/06/2015          
                        // שליפת מס' גפר + שם גפר שהכניס את הצמיג לגיפור
                        if (!DBC.Rs3.IsDBNull(1))
                        {
                            StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                                     "where LPROD='" + DBC.Rs3.GetString(1) + "' and LLBLNO=" + DBC.Rs3.GetDecimal(2).ToString() + " and LACTAN=41";
                            DBC.Rs3.Close();
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                            {
                                xlWorkSheet1.Cells[I, 16] = DBC.Rs3.GetString(0);
                                xlWorkSheet1.Cells[I, 17] = GetUserName(DBC.Rs3.GetString(0));
                            }
                        }
                    }
                    DBC.Rs3.Close();
                }
                xlWorkSheet1.Cells[I, 12] = DBC.Rs1.GetString(6);
                I += 1;
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkSheet1);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void Opt_Repair()
        {
            Cursor.Current = Cursors.WaitCursor;
            string DtTO = "";
            string DtFrom = "";
            string DtFromFix = ""; // משמש בהתייחסות לצמיגים תקינים, בדיקה שלא היה דיווח תיקון כמה ימים לפני תקופת הדוח            
            string SMold = "";

            DBConn DBC;
            string Con_Str;
            string StrSql = "";

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            int I = 3;
            DtFrom = DTPicker_From.Value.ToString("yyyyMMdd");
            DtTO = DTPicker_To.Value.ToString("yyyyMMdd");
            DtFromFix = DTPicker_From.Value.AddDays(-3).ToString("yyyyMMdd");                                    

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet, xlWorkSheet1;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            // Office 2013 עקב שדרוג ל 
            //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //xlWorkSheet1 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
            xlWorkSheet1 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;
            xlWorkSheet1.DisplayRightToLeft = false;

            xlWorkSheet.Name = "Tire Defect";
            xlWorkSheet.Cells[1, 1] = "Tire Defect Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            xlWorkSheet.Cells[3, 1] = "Repair";

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "F1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "F5");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            
            //  שליפת כמות צמיגים שדווחו כפגומים --> שמועברים לתיקון אצל חזן
            StrSql = "select count(distinct LPROD||LLBLNO) from TAPIALI.LABELGL90 " +
                     "where LACNDT between " + DtFrom + " and " + DtTO + " and LACTAN=45 and LSTT='R'";
            if (Txt_Pattern.Text.Trim() != "")
                StrSql += " and substring(LPROD,1,3)='"+Txt_Pattern.Text+"' ";
            
            if (Txt_Catalog.Text.Trim() != "")
                StrSql += " and substring(LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            if (StrCodeList.Trim() != "")
                if (Txt_DefectCode.Text.Trim() != "")
                    StrSql += "and LERRDS in (" + StrCodeList + ") ";

            DBC.Q_Run1(StrSql);
            if (DBC.Rs1.HasRows)
                xlWorkSheet.Cells[4, 1] = DBC.Rs1.GetValue(0);
            DBC.Rs1.Close();

            xlWorkSheet1.Name = "Tire Defect Detail";
            xlWorkSheet1.Cells[1, 1] = "Tire Defect Report Between  :  " + DTPicker_From.Value.ToString().Substring(0, 10) + " and " + DTPicker_To.Value.ToString().Substring(0, 10);
            xlWorkSheet1.Cells[2, 1] = "Date";
            xlWorkSheet1.Cells[2, 2] = "Shift";
            xlWorkSheet1.Cells[2, 3] = "Time";
            xlWorkSheet1.Cells[2, 4] = "Size";
            xlWorkSheet1.Cells[2, 5] = "Catalog";
            xlWorkSheet1.Cells[2, 6] = "Serial";
            
            xlWorkSheet1.Cells[2, 7] = "DNA";

            xlWorkSheet1.Cells[2, 8] = "Status";
            xlWorkSheet1.Cells[2, 9] = "Defect Code";
            xlWorkSheet1.Cells[2, 10] = "Spec";
            xlWorkSheet1.Cells[2, 11] = "Press";
            xlWorkSheet1.Cells[2, 12] = "User";
            xlWorkSheet1.Cells[2, 13] = "Mold No.";

            // תוספת מתאריך 07/06/2015          
            // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות                    
            xlWorkSheet1.Cells[1, 14] = "Press Open";
            xlWorkSheet1.Cells[2, 14] = "Emp No.";
            xlWorkSheet1.Cells[2, 15] = "Emp Name";

            // תוספת מתאריך 18/06/2015
            // הוספת מס' גפר + שם גפר שהכניס את הצמיג לבקשת נדב מאבטחת איכות                                
            xlWorkSheet1.Cells[1, 16] = "Press Close";
            xlWorkSheet1.Cells[2, 16] = "Emp No.";
            xlWorkSheet1.Cells[2, 17] = "Emp Name";

            // תוספת מתאריך 18/06/2015
            // SMO הוספת הנתון אם תבנית
            xlWorkSheet1.Cells[2, 18] = "SMO Mold";
            xlWorkSheet1.Cells[2, 19] = "Michelin";            

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A1", "D1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet1.get_Range("A1", "S1");
            chartRange.ColumnWidth = 13;
            chartRange.HorizontalAlignment = 1;

            chartRange = xlWorkSheet1.get_Range("D1", "D1");
            chartRange.ColumnWidth = 25;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet1.get_Range("A2", "S2");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet1.get_Range("A3", "A10000");
            chartRange.NumberFormat = "@";

            chartRange = xlWorkSheet1.get_Range("D3", "D10000");
            chartRange.NumberFormat = "@";

            //  שליפת פירוט צמיגים שדווחו כפגומים --> שמועברים לתיקון אצל חזן
            StrSql = "select a.LPROD||a.LLBLNO,min(a.LSHIFT),min(a.LPROD),min(a.LLBLNO),min(a.LSTT),min(a.LERRDS),min(a.LUSER),min(a.LACNTM),min(INSIZ),min(b.LSPEC),min(b.LMACH),min(a.LACNDT),max(b.LLBLNA) " +
                     "from TAPIALI.LABELGL22 a join BPCSFALI.IIMNL01 on substr(a.LPROD,1,8)=INPROD left join " +
                     "TAPIALI.LABELGL22 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO and b.LACTAN=42 and b.LACNDT between " + DtFromFix + " and " + DtTO + " " +
                     "where a.LACNDT between " + DtFrom + " and " + DtTO + " and a.LACTAN=45 and a.LSTT='R' ";
                     
            if (Txt_Pattern.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,3)='"+Txt_Pattern.Text+"' ";
            
            if (Txt_Catalog.Text.Trim() != "")
                StrSql += "and substring(a.LPROD,1,8)='" + Txt_Catalog.Text + "' ";

            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            if (StrCodeList.Trim() != "")
                if (Txt_DefectCode.Text.Trim() != "")
                    StrSql += "and a.LERRDS in (" + StrCodeList + ") ";

            StrSql += "group by a.LPROD||a.LLBLNO " +
                      "order by a.LPROD||a.LLBLNO";                       

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet1.Cells[I, 1] = DBC.Rs1.GetDecimal(11).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(11).ToString().Substring(0, 4);
                xlWorkSheet1.Cells[I, 2] = DBC.Rs1.GetString(1);

                if (DBC.Rs1.GetDecimal(7).ToString().Length == 1)
                    xlWorkSheet1.Cells[I, 3] = "00:00:0" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 2)
                    xlWorkSheet1.Cells[I, 3] = "00:00:" + DBC.Rs1.GetDecimal(7).ToString();
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 3)
                    xlWorkSheet1.Cells[I, 3] = "00:0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 4)
                    xlWorkSheet1.Cells[I, 3] = "00:" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 5)
                    xlWorkSheet1.Cells[I, 3] = "0" + DBC.Rs1.GetDecimal(7).ToString().Substring(0, 1) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(1, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(3, 2);
                else if (DBC.Rs1.GetDecimal(7).ToString().Length == 6)
                    xlWorkSheet1.Cells[I, 3] = DBC.Rs1.GetDecimal(7).ToString().Substring(0, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(2, 2) + ":" + DBC.Rs1.GetDecimal(7).ToString().Substring(4, 2);
                
                xlWorkSheet1.Cells[I, 4] = DBC.Rs1.GetString(8);
                xlWorkSheet1.Cells[I, 5] = DBC.Rs1.GetString(2);
                xlWorkSheet1.Cells[I, 6] = DBC.Rs1.GetDecimal(3).ToString();

                xlWorkSheet1.Cells[I, 7] = DBC.Rs1.GetValue(12);
                
                xlWorkSheet1.Cells[I, 8] = "Repair";
                xlWorkSheet1.Cells[I, 9] = DBC.Rs1.GetString(5).Trim();

                // תוספת מתאריך 19/04/2017
                // הוספת עמודה חדשה צמיגי מישלן לבקשת בוריס קזקיבץ
                if (DBC.Rs1.GetString(2).Substring(0, 3) == "771" || DBC.Rs1.GetString(2).Substring(0, 3) == "781" || DBC.Rs1.GetString(2).Substring(0, 3) == "791" || DBC.Rs1.GetString(2).Substring(0, 3) == "772")
                    xlWorkSheet1.Cells[I, 19] = "Michelin";

                if (!DBC.Rs1.IsDBNull(9))
                {
                    xlWorkSheet1.Cells[I, 10] = DBC.Rs1.GetString(9).Trim();
                    xlWorkSheet1.Cells[I, 11] = DBC.Rs1.GetString(10).Trim();

                    // 23/07/2014 תוספת מתאריך 
                    // הוספת מס' תבנית לבקשת ג'וגאן ההודי
                    if (DBC.Rs1.GetString(9).Trim() != "")
                    {
                        SMold = DBC.Rs1.GetString(9).Trim().Substring(DBC.Rs1.GetString(9).Trim().Length - 9, 9);

                        // תוספת מתאריך 18/06/2015
                        // SMO הוספת הנתון אם תבנית
                        //StrSql = "select MFTVDG,MFTVN,MFTVNT from  MFRT.MFRTP " +
                        //         "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";
                        StrSql = "select MFTVDG,MFTVN,MFTVNT,TASMO " +
                                 "from  MFRT.MFRTP join MFRT.TVNMHAP ON MFTVDG=TATVDG and MFTVN=TATVNT and MFTVNT=TATVNO  " +
                                 "where MFSZ='" + SMold.Substring(0, 2) + "' and MFNO=" + SMold.Substring(3, 3) + " and MFVRN=" + SMold.Substring(7, 2) + "";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet1.Cells[I, 13] = DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString();
                            xlWorkSheet1.Cells[I, 18] = DBC.Rs3.GetString(3).Trim();
                            //MessageBox.Show(DBC.Rs3.GetString(0) + "-" + DBC.Rs3.GetValue(1).ToString() + "-" + DBC.Rs3.GetValue(2).ToString());
                        }
                        DBC.Rs3.Close();
                        SMold = "";
                    }

                    // תוספת מתאריך 07/06/2015          
                    // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות
                    // נתוני גפר

                    // תוספת מתאריך 18/06/2015                                                                                                                              
                    //StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                    //         "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    StrSql = "select LOVED,KPROD1,KSERIAL1 " +
                             "from TAPIALI.LABELGL3 left join TAPIALI.LABKSRP on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                             "where LPROD='" + DBC.Rs1.GetString(2) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(3).ToString() + " and LACTAN=42";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet1.Cells[I, 14] = DBC.Rs3.GetString(0);
                        xlWorkSheet1.Cells[I, 15] = GetUserName(DBC.Rs3.GetString(0));

                        // תוספת מתאריך 18/06/2015          
                        // שליפת מס' גפר + שם גפר שהכניס את הצמיג לגיפור
                        if (!DBC.Rs3.IsDBNull(1))
                        {
                            StrSql = "select LOVED from TAPIALI.LABELGL3 " +
                                     "where LPROD='" + DBC.Rs3.GetString(1) + "' and LLBLNO=" + DBC.Rs3.GetDecimal(2).ToString() + " and LACTAN=41";
                            DBC.Rs3.Close();
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                            {
                                xlWorkSheet1.Cells[I, 16] = DBC.Rs3.GetString(0);
                                xlWorkSheet1.Cells[I, 17] = GetUserName(DBC.Rs3.GetString(0));
                            }
                        }
                    }
                    DBC.Rs3.Close();
                }
                xlWorkSheet1.Cells[I, 12] = DBC.Rs1.GetString(6);
                I += 1;
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkSheet1);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Cursor.Current = Cursors.PanEast;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Txt_Pattern_KeyPress(object sender, KeyPressEventArgs e)
        {
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<-----     Delete     ----->| 
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete) e.Handled = false;
            else e.Handled = true;
        }

        private void Txt_Prod_KeyPress(object sender, KeyPressEventArgs e)
        {
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<-----     Delete     ----->| 
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete) e.Handled = false;
            else e.Handled = true;
        }

        private string GetUserName(string EmpNo)
        {   // תוספת מתאריך 07/06/2015          
            // הוספת מס' גפר + שם גפר לבקשת מיכאל אסייג מאבטחת איכות
            string Con_Str;
            string StrSql = "";
            string TmpSt = "";
            EmpNo = "00" + EmpNo.Trim();

            // AS400 התקשרות ל   
            DBConn DBC2;

            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC2 = new DBConn();
            DBC2.Initialize_Conn(Con_Str);

            // שליפת פסולים לתקופה מבוקשת            
            StrSql = "select NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 from ISUFKV.ISAV where OVED='" + EmpNo + "' and MIFAL='01'";
            DBC2.Q_Run1(StrSql);
            if (DBC2.Rs1.Read())
            {
                //TmpSt = strReverse(DBC2.Rs1.GetString(0).Trim()) + " " + strReverse(DBC2.Rs1.GetString(1).Trim());
                TmpSt = DBC2.Rs1.GetString(0).Trim() + " " + DBC2.Rs1.GetString(1).Trim();
                DBC2.Rs1.Close();
                DBC2.Close_Conn();
                return EmpNo = TmpSt;
            }
            else
            {
                DBC2.Rs1.Close();
                DBC2.Close_Conn();
                return "";
            }
        }

        private void Txt_DefectCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            // תוספת מתאריך 09/09/2015
            // הוספת רמת סינון לפי קוד פגם לבקשת חזי לוי הנדסת תהליך
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<-----     Delete     ----->|    |<----- . ----->|
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete || e.KeyChar == '.')
            {
                if (e.KeyChar == '.')
                {
                    // אסור להקליד נקודה בהתחלה
                    if (Txt_DefectCode.Text.Length <3)
                        e.Handled = true;    
                    // אין להקליד ברצף שתי נקודות או יותר
                    else if (Txt_DefectCode.Text.Substring(Txt_DefectCode.Text.Trim().Length - 1, 1) == ".")
                        e.Handled = true;                        
                }
                else
                    e.Handled = false;
            }
            else 
                e.Handled = true;
        }
    }
}
