﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_TireTraceability : Form
    {
        string StrSql;
        string Con_Str;

        public Frm_TireTraceability()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            string Str_S_Date;        // תאריך התחלה לצורך השאילתא
            string Str_E_Date;        // תאריך סיום לצורך השאילתא
            DateTime S_Date;          // תאריך התחלה
            DateTime E_Date;          // תאריך סיום                                    

            int I;
            I = 4;

            /////
            // אנגלית OFFEICE הרצת דוח בגרסת 
            System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
            System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;
            thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            /////

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            // Freeze rows            
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A3", "A3").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd1.FreezePanes = true;

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "H1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Tire Traceability - Hot Inspection To B. M. Stage 1 Report Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AE3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            
            // מירכוז + מיזוג + פונט מודגש לתחום ביקורת חמה
            chartRange = xlWorkSheet.get_Range("A2", "H2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "HOT INSPECTION";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "D. Code";
            xlWorkSheet.Cells[3, 7] = "Emp No.";
            xlWorkSheet.Cells[3, 8] = "Name";

            chartRange = xlWorkSheet.get_Range("A1", "A5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C5000");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D5000");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("E1", "E5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("F1", "F5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("G1", "G5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("H1", "H5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("I2", "M2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 9] = "Curing - Finished goods";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 9] = "Date";
            xlWorkSheet.Cells[3, 10] = "Time";
            xlWorkSheet.Cells[3, 11] = "Press";
            xlWorkSheet.Cells[3, 12] = "Emp No.";
            xlWorkSheet.Cells[3, 13] = "Name";

            chartRange = xlWorkSheet.get_Range("I1", "I5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("J1", "J5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("K1", "K5000");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("L1", "L5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("M1", "M5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("N2", "V2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 14] = "Building Machine Stage 2";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 14] = "Date";
            xlWorkSheet.Cells[3, 15] = "Time";
            xlWorkSheet.Cells[3, 16] = "Catalog";
            xlWorkSheet.Cells[3, 17] = "Serial";
            xlWorkSheet.Cells[3, 18] = "B. Machine";
            xlWorkSheet.Cells[3, 19] = "Spc. Weight";
            xlWorkSheet.Cells[3, 20] = "Act. Weight";
            xlWorkSheet.Cells[3, 21] = "Emp No.";
            xlWorkSheet.Cells[3, 22] = "Name";

            chartRange = xlWorkSheet.get_Range("N1", "N5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("O1", "O5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("P1", "P5000");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("Q1", "Q5000");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("R1", "R5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("S1", "S5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("T1", "T5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("U1", "U5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("V1", "V5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("W2", "AE2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 23] = "Building Machine Stage 1";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 23] = "Date";
            xlWorkSheet.Cells[3, 24] = "Time";
            xlWorkSheet.Cells[3, 25] = "Catalog";
            xlWorkSheet.Cells[3, 26] = "Serial";
            xlWorkSheet.Cells[3, 27] = "B. Machine";
            xlWorkSheet.Cells[3, 28] = "Spc. Weight";
            xlWorkSheet.Cells[3, 29] = "Act. Weight";
            xlWorkSheet.Cells[3, 30] = "Emp No.";
            xlWorkSheet.Cells[3, 31] = "Name";            

            chartRange = xlWorkSheet.get_Range("W1", "W5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("X1", "X5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("Y1", "Y5000");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("Z1", "Z5000");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("AA1", "AA5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("AB1", "AB5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("AC1", "AC5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("AD1", "AD5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AE1", "AE5000");
            chartRange.ColumnWidth = 15;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("I4", "J10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("N4", "O10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("W4", "X10000");
            chartRange.NumberFormat = "@";

            // התאמת תאריך
            S_Date = DTPicker_From.Value;
            E_Date = DTPicker_To.Value;            
            Str_S_Date = S_Date.ToString("yyyyMMdd");
            Str_E_Date = E_Date.ToString("yyyyMMdd");

            // AS400 התקשרות ל   
            DBConn DBC;
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            // שליפת צמיגים שעברו בביקורת חמה בתקופה מבוקשת                        
            StrSql = "select LSTTDT,LSTTTM,INSIZ,LPROD,LLBLNO,LERRDS,LOVED, " +
                     "NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 " + 
                     "from TAPIALI.LABELGL22 join BPCSFALI.IIMN on SUBSTRING(LPROD,1,8)=INPROD " +
                     "join ISUFKV.ISAV on OVED=00||trim(LOVED)  and MIFAL='01' " +
                     "where LSTTDT between " + Str_S_Date + " and " + Str_E_Date + " and LACTAN=45";   
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(0).Substring(6, 2) + "/" + DBC.Rs1.GetString(0).Substring(4, 2) + "/" + DBC.Rs1.GetString(0).Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetString(1));
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(4);                
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(5).Trim();                
                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetDecimal(6);
                //xlWorkSheet.Cells[I, 8] = strReverse(DBC.Rs1.GetString(7).Trim()+" "+DBC.Rs1.GetString(8).Trim()); //GetUserName(DBC.Rs1.GetDecimal(6).ToString());
                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim() + " " + DBC.Rs1.GetString(8).Trim();

                //MessageBox.Show(DBC.Rs1.GetDecimal(6).ToString());
                // נתוני גיפור

                StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED " +                         
                         "from TAPIALI.LABELGL3 " +                
                         "where LPROD='" + DBC.Rs1.GetString(3) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=42"; 

                //StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED, " +
                //         "NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 " + 
                //         "from TAPIALI.LABELGL22 " +
                //         "join ISUFKV.ISAV on OVED=00||trim(LOVED) " +
                //         "where LPROD='" + DBC.Rs1.GetString(3) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=42";                
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    //MessageBox.Show(DBC.Rs2.GetString(3));
                    xlWorkSheet.Cells[I, 9] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 10] = FixTime(DBC.Rs2.GetString(1));
                    xlWorkSheet.Cells[I, 11] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 13] = GetUserName(DBC.Rs2.GetString(3));
                    //xlWorkSheet.Cells[I, 13] = strReverse(DBC.Rs2.GetString(4).Trim() + " " + DBC.Rs2.GetString(5).Trim());
                }
                DBC.Rs2.Close();

                // נתוני בניה שלב ב
                StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1 " +                         
                         "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +                         
                         "where KPROD2='" + DBC.Rs1.GetString(3) + "' and KSERIAL2=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=3";
                //StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1, " +
                //         "NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 " +
                //         "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                //         "join ISUFKV.ISAV on OVED=00||trim(LOVED) " +
                //         "where KPROD2='" + DBC.Rs1.GetString(3) + "' and KSERIAL2=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=3";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 15] = FixTime(DBC.Rs2.GetString(1));
                    xlWorkSheet.Cells[I, 16] = DBC.Rs2.GetString(6);
                    xlWorkSheet.Cells[I, 17] = DBC.Rs2.GetString(7);
                    xlWorkSheet.Cells[I, 18] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 19] = Math.Round(DBC.Rs2.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 20] = Math.Round(DBC.Rs2.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 21] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 22] = GetUserName(DBC.Rs2.GetString(3));
                    //xlWorkSheet.Cells[I, 22] = strReverse(DBC.Rs2.GetString(8).Trim() + " " + DBC.Rs2.GetString(9).Trim());

                    // נתוני בניה שלב א
                    StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1 " +                             
                             "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +                             
                             "where KPROD2='" + DBC.Rs2.GetString(6) + "' and KSERIAL2=" + DBC.Rs2.GetString(7) + " and LACTAN=3";
                    //StrSql = "select LSTTDT,LSTTTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1, " +
                    //         "NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 " +
                    //         "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                    //         "join ISUFKV.ISAV on OVED=00||trim(LOVED) " +
                    //         "where KPROD2='" + DBC.Rs2.GetString(6) + "' and KSERIAL2=" + DBC.Rs2.GetString(7) + " and LACTAN=3";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet.Cells[I, 23] = DBC.Rs3.GetString(0).Substring(6, 2) + "/" + DBC.Rs3.GetString(0).Substring(4, 2) + "/" + DBC.Rs3.GetString(0).Substring(0, 4);
                        xlWorkSheet.Cells[I, 24] = FixTime(DBC.Rs3.GetString(1));
                        xlWorkSheet.Cells[I, 25] = DBC.Rs3.GetString(6);
                        xlWorkSheet.Cells[I, 26] = DBC.Rs3.GetString(7);
                        xlWorkSheet.Cells[I, 27] = DBC.Rs3.GetString(2);
                        xlWorkSheet.Cells[I, 28] = Math.Round(DBC.Rs3.GetDecimal(4), 2);
                        xlWorkSheet.Cells[I, 29] = Math.Round(DBC.Rs3.GetDecimal(5), 2);
                        xlWorkSheet.Cells[I, 30] = DBC.Rs3.GetString(3);
                        xlWorkSheet.Cells[I, 31] = GetUserName(DBC.Rs3.GetString(3));
                        //xlWorkSheet.Cells[I, 31] = strReverse(DBC.Rs2.GetString(8).Trim() + " " + DBC.Rs2.GetString(9).Trim());
                    }
                    DBC.Rs3.Close();
                }
                DBC.Rs2.Close();
                I += 1;
            }
            DBC.Rs1.Close();
            DBC.Close_Conn(); 

            Cursor.Current = Cursors.PanEast;
            xlApp.Visible = true;  
        }
        
        private string GetUserName(string EmpNo) 
        {
            string TmpSt = "";
            EmpNo= "00" + EmpNo.Trim();

            // AS400 התקשרות ל   
            DBConn DBC2;                        

            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC2 = new DBConn();
            DBC2.Initialize_Conn(Con_Str);

            // שליפת פסולים לתקופה מבוקשת            
            StrSql = "select NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 from ISUFKV.ISAV where OVED='" + EmpNo + "' and MIFAL='01'";
            DBC2.Q_Run1(StrSql);
            if (DBC2.Rs1.Read())
            {
                //TmpSt = strReverse(DBC2.Rs1.GetString(0).Trim()) + " " + strReverse(DBC2.Rs1.GetString(1).Trim());
                TmpSt = DBC2.Rs1.GetString(0).Trim() + " " + DBC2.Rs1.GetString(1).Trim();
                DBC2.Rs1.Close();
                DBC2.Close_Conn();
                return EmpNo = TmpSt; 
            }
            else
            {
                DBC2.Rs1.Close();
                DBC2.Close_Conn();
                return "";
            }                        
        }
        
        private string strReverse(string s)
        {
            int j = 0;
            char[] c = new char[s.Length];
            for (int i = s.Length - 1; i >= 0; i--) c[j++] = s[i];
            return new string(c);
        }

        private string FixTime(string S)
        {
            if (S.Length == 1)
                S = "00:00:0" + S;
            else 
                if (S.Length == 2)
                    S = "00:00:" + S;
                else
                    if (S.Length == 3)
                        S = "00:0" + S.Substring(0, 1) + ":" + S.Substring(0, 2);
                    else
                        if (S.Length == 4)
                            S = "00:" + S.Substring(0, 2) + ":" + S.Substring(2, 2);
                        else
                            if (S.Length == 5)
                                S = "0" + S.Substring(0, 1) + ":" + S.Substring(1, 2) + ":" + S.Substring(3, 2);
                            else
                                S =  S.Substring(0, 2) + ":" + S.Substring(2, 2) + ":" + S.Substring(4, 2);
            return S;
        }
    }
}
