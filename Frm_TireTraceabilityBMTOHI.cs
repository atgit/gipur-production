﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_TireTraceabilityBMTOHI : Form
    {
        // תיקון מתאריך 14/05/2014
        // דוח עקיבות צמיג מבניית צמיג שלב א עד ביקורת חמה
        string StrSql;
        string Con_Str;

        public Frm_TireTraceabilityBMTOHI()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            string Str_S_Date;        // תאריך התחלה לצורך השאילתא
            string Str_E_Date;        // תאריך סיום לצורך השאילתא
            string Catalog;
            string Serial;
            DateTime S_Date;          // תאריך התחלה
            DateTime E_Date;          // תאריך סיום                                    

            int I;
            I = 4;

            /////
            // אנגלית OFFEICE הרצת דוח בגרסת 
            System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
            System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;
            thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            /////

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            // Freeze rows            
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A3", "A3").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd1.FreezePanes = true;

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "H1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Tire Traceability - B. M. Stage 1 To Hot Inspection Report Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            chartRange = xlWorkSheet.get_Range("A2", "A3");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Size";
            chartRange = xlWorkSheet.get_Range("A2", "A3");
            chartRange.ColumnWidth = 25;            

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("B2", "J2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 2] = "Building Machine Stage 1";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 2] = "Date";
            xlWorkSheet.Cells[3, 3] = "Time";            
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "B. Machine";
            xlWorkSheet.Cells[3, 7] = "Spc. Weight";
            xlWorkSheet.Cells[3, 8] = "Act. Weight";
            xlWorkSheet.Cells[3, 9] = "Emp No.";
            xlWorkSheet.Cells[3, 10] = "Name";
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 8;            
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("K2", "S2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 11] = "Building Machine Stage 2";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 11] = "Date";
            xlWorkSheet.Cells[3, 12] = "Time";
            xlWorkSheet.Cells[3, 13] = "Catalog";
            xlWorkSheet.Cells[3, 14] = "Serial";
            xlWorkSheet.Cells[3, 15] = "B. Machine";
            xlWorkSheet.Cells[3, 16] = "Spc. Weight";
            xlWorkSheet.Cells[3, 17] = "Act. Weight";
            xlWorkSheet.Cells[3, 18] = "Emp No.";
            xlWorkSheet.Cells[3, 19] = "Name";
            chartRange = xlWorkSheet.get_Range("k1", "K1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("S1", "S1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("T2", "Z2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 20] = "Curing - Finished goods";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 20] = "Date";
            xlWorkSheet.Cells[3, 21] = "Time";
            xlWorkSheet.Cells[3, 22] = "Catalog";
            xlWorkSheet.Cells[3, 23] = "Serial";
            xlWorkSheet.Cells[3, 24] = "Press";
            xlWorkSheet.Cells[3, 25] = "Emp No.";
            xlWorkSheet.Cells[3, 26] = "Name";
            chartRange = xlWorkSheet.get_Range("T1", "T1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("U1", "U1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("V1", "V1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("W1", "W1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("X1", "X1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("Y1", "Y1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("Z1", "Z1");
            chartRange.ColumnWidth = 15;            

            // מירכוז + מיזוג + פונט מודגש לתחום ביקורת חמה
            chartRange = xlWorkSheet.get_Range("AA2", "AE2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 27] = "HOT INSPECTION";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 27] = "Date";
            xlWorkSheet.Cells[3, 28] = "Time";                        
            xlWorkSheet.Cells[3, 29] = "D. Code";
            xlWorkSheet.Cells[3, 30] = "Emp No.";
            xlWorkSheet.Cells[3, 31] = "Name";
            chartRange = xlWorkSheet.get_Range("AA1", "AA1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("AB1", "AB1");
            chartRange.ColumnWidth = 8;            
            chartRange = xlWorkSheet.get_Range("AC1", "AC1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AD1", "AD1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AE1", "AE1");
            chartRange.ColumnWidth = 15;            

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AE3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            
            chartRange = xlWorkSheet.get_Range("B4", "C100000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("K4", "L100000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("T4", "U100000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("AA4", "AB100000");
            chartRange.NumberFormat = "@";




            // התאמת תאריך
            S_Date = DTPicker_From.Value;
            E_Date = DTPicker_To.Value;
            Str_S_Date = S_Date.ToString("yyyyMMdd");
            Str_E_Date = E_Date.ToString("yyyyMMdd");

            // AS400 התקשרות ל   
            DBConn DBC;
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,LPROD,LLBLNO,INSIZ " +
                     "from TAPIALI.LABELGL22 join BPCSFALI.IIMN on SUBSTRING(LPROD,1,8)=INPROD " +
                     "where LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and LACTAN=3 and SUBSTRING(LPROD,10,1)='1'";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())            
            {
                //MessageBox.Show(DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4)); 
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(8);
                xlWorkSheet.Cells[I, 2] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet.Cells[I, 3] = FixTime(DBC.Rs1.GetDecimal(1).ToString());
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(6);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(7);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 7] = Math.Round(DBC.Rs1.GetDecimal(4), 2);
                xlWorkSheet.Cells[I, 8] = Math.Round(DBC.Rs1.GetDecimal(5), 2);
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 10] = GetUserName(DBC.Rs1.GetString(3));


                // נתוני בניה שלב ב
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD2,KSERIAL2 " +                         
                         "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO " +                         
                         "where KPROD1='" + DBC.Rs1.GetString(6) + "' and KSERIAL1=" + DBC.Rs1.GetDecimal(7) + " and LACTAN=3";                
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 11] = DBC.Rs2.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs2.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs2.GetDecimal(0).ToString().Substring(0, 4);                      
                    xlWorkSheet.Cells[I, 12] = FixTime(DBC.Rs2.GetDecimal(1).ToString());
                    xlWorkSheet.Cells[I, 13] = DBC.Rs2.GetString(6);
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(7);
                    xlWorkSheet.Cells[I, 15] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 16] = Math.Round(DBC.Rs2.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 17] = Math.Round(DBC.Rs2.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 18] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 19] = GetUserName(DBC.Rs2.GetString(3));
                    Catalog = "";
                    Serial = "";
                    // נתוני גיפור
                    StrSql = "select LACNDT,LACNTM,LMACH,LOVED,KPROD2,KSERIAL2 " +
                            "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO and LACTAN=42 " +
                            "where KPROD1='" + DBC.Rs2.GetString(6).Trim() + "' and KSERIAL1=" + DBC.Rs2.GetString(7) + "";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {                        
                        xlWorkSheet.Cells[I, 20] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);                      
                        xlWorkSheet.Cells[I, 21] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                        xlWorkSheet.Cells[I, 22] = DBC.Rs3.GetString(4);
                        xlWorkSheet.Cells[I, 23] = DBC.Rs3.GetString(5);
                        xlWorkSheet.Cells[I, 24] = DBC.Rs3.GetString(2);
                        xlWorkSheet.Cells[I, 25] = DBC.Rs3.GetString(3);
                        xlWorkSheet.Cells[I, 26] = GetUserName(DBC.Rs3.GetString(3));
                        Catalog = DBC.Rs3.GetString(4).Trim();
                        Serial = DBC.Rs3.GetString(5);
                    }
                    DBC.Rs3.Close();

                    if (Catalog != "")
                    {
                        // נתוני ביקורת חמה
                        StrSql = "select LACNDT,LACNTM,LERRDS,LOVED " +
                                 "from TAPIALI.LABELGL22 " +
                                 "where LPROD = '" + Catalog + "' and LLBLNO = " + Serial + " and LACTAN=45";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet.Cells[I, 27] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);                      
                            xlWorkSheet.Cells[I, 28] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                            xlWorkSheet.Cells[I, 29] = DBC.Rs3.GetString(2);
                            xlWorkSheet.Cells[I, 30] = DBC.Rs3.GetString(3);
                            xlWorkSheet.Cells[I, 31] = GetUserName(DBC.Rs3.GetString(3));
                        }
                        DBC.Rs3.Close();
                    }
                }
                DBC.Rs2.Close();                
                I++;
            }
            DBC.Rs1.Close();

            // שליפת נתוני צמיגים שלמים
            StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,LPROD,LLBLNO,INSIZ,LPROD||LLBLNO " +
                     "from TAPIALI.LABELGL22 join BPCSFALI.IIMN on SUBSTRING(LPROD,1,8)=INPROD " +
                     "where LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and LACTAN=3 and SUBSTRING(LPROD,10,1)='0' and " +
                           "LUSER in ('J-59','T-22','T-25','T-26','JUMBO-12','JUMBO-16','34-3','34-4') and " +
                           "LPROD||LLBLNO not in (select KPROD2||KSERIAL2 from TAPIALI.LABKSRL2 where SUBSTRING(KPROD2,10,1)='0' and " +
                           "KOPDATE between " + Str_S_Date + " and " + Str_E_Date + ")";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] =  DBC.Rs1.GetString(8);
                xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet.Cells[I, 12] = FixTime(DBC.Rs1.GetDecimal(1).ToString());
                xlWorkSheet.Cells[I, 13] = DBC.Rs1.GetString(6);
                xlWorkSheet.Cells[I, 14] = DBC.Rs1.GetDecimal(7);
                xlWorkSheet.Cells[I, 15] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 16] = Math.Round(DBC.Rs1.GetDecimal(4), 2);
                xlWorkSheet.Cells[I, 17] = Math.Round(DBC.Rs1.GetDecimal(5), 2);
                xlWorkSheet.Cells[I, 18] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 19] = GetUserName(DBC.Rs1.GetString(3));                
                Catalog = "";
                Serial = "";
                // נתוני גיפור
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,KPROD2,KSERIAL2 " +
                        "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO and LACTAN=42 " +
                        "where KPROD1='" + DBC.Rs1.GetString(6).Trim() + "' and KSERIAL1=" + DBC.Rs1.GetDecimal(7) + "";
                DBC.Q_Run3(StrSql);
                if (DBC.Rs3.Read())
                {
                    xlWorkSheet.Cells[I, 20] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);
                    xlWorkSheet.Cells[I, 21] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                    xlWorkSheet.Cells[I, 22] = DBC.Rs3.GetString(4);
                    xlWorkSheet.Cells[I, 23] = DBC.Rs3.GetString(5);
                    xlWorkSheet.Cells[I, 24] = DBC.Rs3.GetString(2);
                    xlWorkSheet.Cells[I, 25] = DBC.Rs3.GetString(3);
                    xlWorkSheet.Cells[I, 26] = GetUserName(DBC.Rs3.GetString(3));
                    Catalog = DBC.Rs3.GetString(4).Trim();
                    Serial = DBC.Rs3.GetString(5);
                }
                DBC.Rs3.Close();
                
                if (Catalog != "")
                {
                    // נתוני ביקורת חמה
                    StrSql = "select LACNDT,LACNTM,LERRDS,LOVED " +
                             "from TAPIALI.LABELGL22 " +
                             "where LPROD = '" + Catalog + "' and LLBLNO = " + Serial + " and LACTAN=45";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet.Cells[I, 27] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);
                        xlWorkSheet.Cells[I, 28] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                        xlWorkSheet.Cells[I, 29] = DBC.Rs3.GetString(2);
                        xlWorkSheet.Cells[I, 30] = DBC.Rs3.GetString(3);
                        xlWorkSheet.Cells[I, 31] = GetUserName(DBC.Rs3.GetString(3));
                    }
                    DBC.Rs3.Close();
                }
                I++;
            }
            DBC.Rs1.Close(); 
            DBC.Close_Conn(); 

            Cursor.Current = Cursors.PanEast;
            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);            

        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private string GetUserName(string EmpNo)
        {
            string TmpSt = "";
            EmpNo = "00" + EmpNo.Trim();

            // AS400 התקשרות ל   
            DBConn DBC2;

            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC2 = new DBConn();
            DBC2.Initialize_Conn(Con_Str);

            // שליפת פסולים לתקופה מבוקשת            
            StrSql = "select NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 from ISUFKV.ISAV where OVED='" + EmpNo + "' and MIFAL='01'";
            DBC2.Q_Run1(StrSql);
            if (DBC2.Rs1.Read())
            {
                //TmpSt = strReverse(DBC2.Rs1.GetString(0).Trim()) + " " + strReverse(DBC2.Rs1.GetString(1).Trim());
                TmpSt = DBC2.Rs1.GetString(0).Trim() + " " + DBC2.Rs1.GetString(1).Trim();
                DBC2.Rs1.Close();
                DBC2.Close_Conn();
                return EmpNo = TmpSt;
            }
            else
            {
                DBC2.Rs1.Close();
                DBC2.Close_Conn();
                return "";
            }
        }

        private string strReverse(string s)
        {
            int j = 0;
            char[] c = new char[s.Length];
            for (int i = s.Length - 1; i >= 0; i--) c[j++] = s[i];
            return new string(c);
        }

        private string FixTime(string S)
        {
            if (S.Length == 1)
                S = "00:00:0" + S;
            else
                if (S.Length == 2)
                    S = "00:00:" + S;
                else
                    if (S.Length == 3)
                        S = "00:0" + S.Substring(0, 1) + ":" + S.Substring(0, 2);
                    else
                        if (S.Length == 4)
                            S = "00:" + S.Substring(0, 2) + ":" + S.Substring(2, 2);
                        else
                            if (S.Length == 5)
                                S = "0" + S.Substring(0, 1) + ":" + S.Substring(1, 2) + ":" + S.Substring(3, 2);
                            else
                                S = S.Substring(0, 2) + ":" + S.Substring(2, 2) + ":" + S.Substring(4, 2);
            return S;
        }
    }
}
