﻿namespace Gipur_Production
{
    partial class Frm_Traceability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Traceability));
            this.Btn_OK = new System.Windows.Forms.Button();
            this.DTPicker_To = new System.Windows.Forms.DateTimePicker();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.DTPicker_From = new System.Windows.Forms.DateTimePicker();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.Pnl_Start = new System.Windows.Forms.Panel();
            this.Lbl_Start = new System.Windows.Forms.Label();
            this.RB_BuildingStage_1_Start = new System.Windows.Forms.RadioButton();
            this.RB_BuildingStage_2_Start = new System.Windows.Forms.RadioButton();
            this.RB_CuringFinishedGoods_Start = new System.Windows.Forms.RadioButton();
            this.RB_HotInspection_Start = new System.Windows.Forms.RadioButton();
            this.Pnl_End = new System.Windows.Forms.Panel();
            this.Lbl_End = new System.Windows.Forms.Label();
            this.RB_BuildingStage_1_End = new System.Windows.Forms.RadioButton();
            this.RB_BuildingStage_2_End = new System.Windows.Forms.RadioButton();
            this.RB_CuringFinishedGoods_End = new System.Windows.Forms.RadioButton();
            this.RB_HotInspection_End = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Prod = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_Pattern = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_EmpNo = new System.Windows.Forms.TextBox();
            this.Pnl_Start.SuspendLayout();
            this.Pnl_End.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(199, 356);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(127, 28);
            this.Btn_OK.TabIndex = 8;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // DTPicker_To
            // 
            this.DTPicker_To.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_To.Location = new System.Drawing.Point(368, 65);
            this.DTPicker_To.Name = "DTPicker_To";
            this.DTPicker_To.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_To.TabIndex = 2;
            // 
            // Lbl_To
            // 
            this.Lbl_To.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_To.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(295, 65);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.Size = new System.Drawing.Size(73, 24);
            this.Lbl_To.TabIndex = 29;
            this.Lbl_To.Text = "To";
            // 
            // DTPicker_From
            // 
            this.DTPicker_From.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_From.Location = new System.Drawing.Point(114, 65);
            this.DTPicker_From.Name = "DTPicker_From";
            this.DTPicker_From.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_From.TabIndex = 1;
            // 
            // Lbl_From
            // 
            this.Lbl_From.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_From.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(41, 65);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.Size = new System.Drawing.Size(73, 24);
            this.Lbl_From.TabIndex = 26;
            this.Lbl_From.Text = "From";
            // 
            // Pnl_Start
            // 
            this.Pnl_Start.BackColor = System.Drawing.Color.SlateGray;
            this.Pnl_Start.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Pnl_Start.Controls.Add(this.Lbl_Start);
            this.Pnl_Start.Controls.Add(this.RB_BuildingStage_1_Start);
            this.Pnl_Start.Controls.Add(this.RB_BuildingStage_2_Start);
            this.Pnl_Start.Controls.Add(this.RB_CuringFinishedGoods_Start);
            this.Pnl_Start.Controls.Add(this.RB_HotInspection_Start);
            this.Pnl_Start.Location = new System.Drawing.Point(42, 108);
            this.Pnl_Start.Name = "Pnl_Start";
            this.Pnl_Start.Size = new System.Drawing.Size(186, 138);
            this.Pnl_Start.TabIndex = 3;
            // 
            // Lbl_Start
            // 
            this.Lbl_Start.BackColor = System.Drawing.Color.SteelBlue;
            this.Lbl_Start.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Lbl_Start.Location = new System.Drawing.Point(0, -5);
            this.Lbl_Start.Name = "Lbl_Start";
            this.Lbl_Start.Size = new System.Drawing.Size(186, 29);
            this.Lbl_Start.TabIndex = 34;
            this.Lbl_Start.Text = "Start";
            this.Lbl_Start.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RB_BuildingStage_1_Start
            // 
            this.RB_BuildingStage_1_Start.AutoSize = true;
            this.RB_BuildingStage_1_Start.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RB_BuildingStage_1_Start.Location = new System.Drawing.Point(12, 102);
            this.RB_BuildingStage_1_Start.Name = "RB_BuildingStage_1_Start";
            this.RB_BuildingStage_1_Start.Size = new System.Drawing.Size(123, 19);
            this.RB_BuildingStage_1_Start.TabIndex = 3;
            this.RB_BuildingStage_1_Start.TabStop = true;
            this.RB_BuildingStage_1_Start.Text = "Building - Stage 1";
            this.RB_BuildingStage_1_Start.UseVisualStyleBackColor = true;
            this.RB_BuildingStage_1_Start.Click += new System.EventHandler(this.RB_BuildingStage_1_Start_Click);
            // 
            // RB_BuildingStage_2_Start
            // 
            this.RB_BuildingStage_2_Start.AutoSize = true;
            this.RB_BuildingStage_2_Start.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RB_BuildingStage_2_Start.Location = new System.Drawing.Point(12, 79);
            this.RB_BuildingStage_2_Start.Name = "RB_BuildingStage_2_Start";
            this.RB_BuildingStage_2_Start.Size = new System.Drawing.Size(123, 19);
            this.RB_BuildingStage_2_Start.TabIndex = 2;
            this.RB_BuildingStage_2_Start.TabStop = true;
            this.RB_BuildingStage_2_Start.Text = "Building - Stage 2";
            this.RB_BuildingStage_2_Start.UseVisualStyleBackColor = true;
            this.RB_BuildingStage_2_Start.Click += new System.EventHandler(this.RB_BuildingStage_2_Start_Click);
            // 
            // RB_CuringFinishedGoods_Start
            // 
            this.RB_CuringFinishedGoods_Start.AutoSize = true;
            this.RB_CuringFinishedGoods_Start.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RB_CuringFinishedGoods_Start.Location = new System.Drawing.Point(12, 56);
            this.RB_CuringFinishedGoods_Start.Name = "RB_CuringFinishedGoods_Start";
            this.RB_CuringFinishedGoods_Start.Size = new System.Drawing.Size(158, 19);
            this.RB_CuringFinishedGoods_Start.TabIndex = 1;
            this.RB_CuringFinishedGoods_Start.TabStop = true;
            this.RB_CuringFinishedGoods_Start.Text = "Curing - Finished Goods";
            this.RB_CuringFinishedGoods_Start.UseVisualStyleBackColor = true;
            this.RB_CuringFinishedGoods_Start.Click += new System.EventHandler(this.RB_CuringFinishedGoods_Start_Click);
            // 
            // RB_HotInspection_Start
            // 
            this.RB_HotInspection_Start.AutoSize = true;
            this.RB_HotInspection_Start.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RB_HotInspection_Start.Location = new System.Drawing.Point(12, 33);
            this.RB_HotInspection_Start.Name = "RB_HotInspection_Start";
            this.RB_HotInspection_Start.Size = new System.Drawing.Size(106, 19);
            this.RB_HotInspection_Start.TabIndex = 0;
            this.RB_HotInspection_Start.TabStop = true;
            this.RB_HotInspection_Start.Text = "Hot Inspection";
            this.RB_HotInspection_Start.UseVisualStyleBackColor = true;
            this.RB_HotInspection_Start.Click += new System.EventHandler(this.RB_HotInspection_Start_Click);
            // 
            // Pnl_End
            // 
            this.Pnl_End.BackColor = System.Drawing.Color.SlateGray;
            this.Pnl_End.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Pnl_End.Controls.Add(this.Lbl_End);
            this.Pnl_End.Controls.Add(this.RB_BuildingStage_1_End);
            this.Pnl_End.Controls.Add(this.RB_BuildingStage_2_End);
            this.Pnl_End.Controls.Add(this.RB_CuringFinishedGoods_End);
            this.Pnl_End.Controls.Add(this.RB_HotInspection_End);
            this.Pnl_End.Location = new System.Drawing.Point(297, 108);
            this.Pnl_End.Name = "Pnl_End";
            this.Pnl_End.Size = new System.Drawing.Size(186, 138);
            this.Pnl_End.TabIndex = 4;
            // 
            // Lbl_End
            // 
            this.Lbl_End.BackColor = System.Drawing.Color.SteelBlue;
            this.Lbl_End.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Lbl_End.Location = new System.Drawing.Point(0, -5);
            this.Lbl_End.Name = "Lbl_End";
            this.Lbl_End.Size = new System.Drawing.Size(186, 29);
            this.Lbl_End.TabIndex = 35;
            this.Lbl_End.Text = "End";
            this.Lbl_End.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RB_BuildingStage_1_End
            // 
            this.RB_BuildingStage_1_End.AutoSize = true;
            this.RB_BuildingStage_1_End.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RB_BuildingStage_1_End.Location = new System.Drawing.Point(12, 102);
            this.RB_BuildingStage_1_End.Name = "RB_BuildingStage_1_End";
            this.RB_BuildingStage_1_End.Size = new System.Drawing.Size(123, 19);
            this.RB_BuildingStage_1_End.TabIndex = 3;
            this.RB_BuildingStage_1_End.TabStop = true;
            this.RB_BuildingStage_1_End.Text = "Building - Stage 1";
            this.RB_BuildingStage_1_End.UseVisualStyleBackColor = true;
            // 
            // RB_BuildingStage_2_End
            // 
            this.RB_BuildingStage_2_End.AutoSize = true;
            this.RB_BuildingStage_2_End.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RB_BuildingStage_2_End.Location = new System.Drawing.Point(12, 79);
            this.RB_BuildingStage_2_End.Name = "RB_BuildingStage_2_End";
            this.RB_BuildingStage_2_End.Size = new System.Drawing.Size(123, 19);
            this.RB_BuildingStage_2_End.TabIndex = 2;
            this.RB_BuildingStage_2_End.TabStop = true;
            this.RB_BuildingStage_2_End.Text = "Building - Stage 2";
            this.RB_BuildingStage_2_End.UseVisualStyleBackColor = true;
            // 
            // RB_CuringFinishedGoods_End
            // 
            this.RB_CuringFinishedGoods_End.AutoSize = true;
            this.RB_CuringFinishedGoods_End.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RB_CuringFinishedGoods_End.Location = new System.Drawing.Point(12, 56);
            this.RB_CuringFinishedGoods_End.Name = "RB_CuringFinishedGoods_End";
            this.RB_CuringFinishedGoods_End.Size = new System.Drawing.Size(158, 19);
            this.RB_CuringFinishedGoods_End.TabIndex = 1;
            this.RB_CuringFinishedGoods_End.TabStop = true;
            this.RB_CuringFinishedGoods_End.Text = "Curing - Finished Goods";
            this.RB_CuringFinishedGoods_End.UseVisualStyleBackColor = true;
            // 
            // RB_HotInspection_End
            // 
            this.RB_HotInspection_End.AutoSize = true;
            this.RB_HotInspection_End.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RB_HotInspection_End.Location = new System.Drawing.Point(12, 33);
            this.RB_HotInspection_End.Name = "RB_HotInspection_End";
            this.RB_HotInspection_End.Size = new System.Drawing.Size(106, 19);
            this.RB_HotInspection_End.TabIndex = 0;
            this.RB_HotInspection_End.TabStop = true;
            this.RB_HotInspection_End.Text = "Hot Inspection";
            this.RB_HotInspection_End.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(180, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 32);
            this.label2.TabIndex = 33;
            this.label2.Text = "Traceability";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_Prod
            // 
            this.Txt_Prod.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Prod.Location = new System.Drawing.Point(117, 290);
            this.Txt_Prod.MaxLength = 8;
            this.Txt_Prod.Name = "Txt_Prod";
            this.Txt_Prod.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Prod.Size = new System.Drawing.Size(110, 25);
            this.Txt_Prod.TabIndex = 6;
            this.Txt_Prod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_Prod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Prod_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.SlateGray;
            this.label4.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(37, 290);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 24);
            this.label4.TabIndex = 35;
            this.label4.Text = "Catalog";
            // 
            // Txt_Pattern
            // 
            this.Txt_Pattern.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Pattern.Location = new System.Drawing.Point(117, 259);
            this.Txt_Pattern.MaxLength = 3;
            this.Txt_Pattern.Name = "Txt_Pattern";
            this.Txt_Pattern.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Pattern.Size = new System.Drawing.Size(110, 25);
            this.Txt_Pattern.TabIndex = 5;
            this.Txt_Pattern.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_Pattern.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Pattern_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.SlateGray;
            this.label1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(37, 259);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 24);
            this.label1.TabIndex = 37;
            this.label1.Text = "Pattern";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.SlateGray;
            this.label3.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(37, 322);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 24);
            this.label3.TabIndex = 38;
            this.label3.Text = "Emp No";
            // 
            // Txt_EmpNo
            // 
            this.Txt_EmpNo.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_EmpNo.Location = new System.Drawing.Point(117, 321);
            this.Txt_EmpNo.MaxLength = 5;
            this.Txt_EmpNo.Name = "Txt_EmpNo";
            this.Txt_EmpNo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_EmpNo.Size = new System.Drawing.Size(110, 25);
            this.Txt_EmpNo.TabIndex = 7;
            this.Txt_EmpNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_EmpNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_EmpNo_KeyPress);
            // 
            // Frm_Traceability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(525, 395);
            this.Controls.Add(this.Txt_EmpNo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_Pattern);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Txt_Prod);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Pnl_End);
            this.Controls.Add(this.Pnl_Start);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.DTPicker_To);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DTPicker_From);
            this.Controls.Add(this.Lbl_From);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Frm_Traceability";
            this.Text = "Traceability";
            this.Load += new System.EventHandler(this.Frm_Traceability_Load);
            this.Pnl_Start.ResumeLayout(false);
            this.Pnl_Start.PerformLayout();
            this.Pnl_End.ResumeLayout(false);
            this.Pnl_End.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.DateTimePicker DTPicker_To;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.DateTimePicker DTPicker_From;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.Panel Pnl_Start;
        private System.Windows.Forms.RadioButton RB_BuildingStage_1_Start;
        private System.Windows.Forms.RadioButton RB_BuildingStage_2_Start;
        private System.Windows.Forms.RadioButton RB_CuringFinishedGoods_Start;
        private System.Windows.Forms.RadioButton RB_HotInspection_Start;
        private System.Windows.Forms.Panel Pnl_End;
        private System.Windows.Forms.RadioButton RB_BuildingStage_1_End;
        private System.Windows.Forms.RadioButton RB_BuildingStage_2_End;
        private System.Windows.Forms.RadioButton RB_CuringFinishedGoods_End;
        private System.Windows.Forms.RadioButton RB_HotInspection_End;
        private System.Windows.Forms.Label Lbl_Start;
        private System.Windows.Forms.Label Lbl_End;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_Prod;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_Pattern;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_EmpNo;
    }
}