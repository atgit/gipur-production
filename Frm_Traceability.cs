﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_Traceability : Form
    {
        // תיקון מתאריך 10/08/2014
        // דוח עקיבות צמיג - חיתוך הנתונים לפי בחירת המשתמש

        string StrSql;
        string Con_Str;        

        string Str_S_Date;        // תאריך התחלה לצורך השאילתא
        string Str_E_Date;        // תאריך סיום לצורך השאילתא
        DateTime S_Date;          // תאריך התחלה
        DateTime E_Date;          // תאריך סיום                                    
        DBConn DBC;
        int I;                        

        // יצירת קובץ אקסל
        // Add reference -> com -> Microsoft Excel 12.0 Object Library                        
        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        object misValue = System.Reflection.Missing.Value;
        Excel.Range chartRange;
            

        public Frm_Traceability()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {            
            if (DTPicker_From.Value.Date > DTPicker_To.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }

            if (!RB_HotInspection_End.Checked && !RB_CuringFinishedGoods_End.Checked && !RB_BuildingStage_2_End.Checked && !RB_BuildingStage_1_End.Checked)
            {
                MessageBox.Show("שגיאה, נא לבחור תחנת סיום", "Curing / Building Production");
                DTPicker_From.Focus();
                return;
            }

            if (Txt_Pattern.Text != "" && Txt_Pattern.Text.Length < 3)
            {
                MessageBox.Show("שגיאה, מספר דגם שגוי", "Curing / Building Production");
                Txt_Pattern.Focus();
                return;
            }

            if (Txt_Prod.Text != "" && Txt_Prod.Text.Length < 8)
            {
                MessageBox.Show("שגיאה, מספר מק''ט שגוי", "Curing / Building Production");
                Txt_Prod.Focus();
                return;
            }
            
            Cursor.Current = Cursors.WaitCursor;

            // אנגלית OFFEICE הרצת דוח בגרסת 
            System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
            System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;
            thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            // Freeze rows            
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A3", "A3").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd1.FreezePanes = true;

            // הצגת גיליון משמאל לימין
            xlApp.DefaultSheetDirection = (int)Excel.Constants.xlLTR;
            xlWorkSheet.DisplayRightToLeft = false;

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);
            I = 4;
            // התאמת תאריך
            S_Date = DTPicker_From.Value;
            E_Date = DTPicker_To.Value;
            Str_S_Date = S_Date.ToString("yyyyMMdd");
            Str_E_Date = E_Date.ToString("yyyyMMdd");

            if (RB_HotInspection_Start.Checked && RB_BuildingStage_1_End.Checked)
                HI_CU_B2_B1();
            else if (RB_HotInspection_Start.Checked && RB_BuildingStage_2_End.Checked)
                HI_CU_B2();
            else if (RB_HotInspection_Start.Checked && RB_CuringFinishedGoods_End.Checked)
                HI_CU();
            
            else if (RB_CuringFinishedGoods_Start.Checked && RB_HotInspection_End.Checked)
                CU_HI();            
            else if (RB_CuringFinishedGoods_Start.Checked && RB_BuildingStage_2_End.Checked)
                CU_B2();
            else if (RB_CuringFinishedGoods_Start.Checked && RB_BuildingStage_1_End.Checked)
                CU_B2_B1();            
            
            else if (RB_BuildingStage_2_Start.Checked && RB_HotInspection_End.Checked)
                B2_CU_HI();
            else if (RB_BuildingStage_2_Start.Checked && RB_CuringFinishedGoods_End.Checked)
                B2_CU();
            else if (RB_BuildingStage_2_Start.Checked && RB_BuildingStage_1_End.Checked)
                B2_B1();

            else if (RB_BuildingStage_1_Start.Checked && RB_HotInspection_End.Checked)
                B1_B2_CU_HI();
            else if (RB_BuildingStage_1_Start.Checked && RB_CuringFinishedGoods_End.Checked)
                B1_B2_CU();
            else if (RB_BuildingStage_1_Start.Checked && RB_BuildingStage_2_End.Checked)
                B1_B2();            

            DBC.Close_Conn();
            Cursor.Current = Cursors.PanEast;
            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp); 
        }


        private void HI_CU_B2_B1() // HOT INSPECTION --> Curing - Finished goods --> Building Stage 2 --> Building Stage 1.
        {
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "H1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability  -  Hot Inspection  To  Building Stage 1 Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + מיזוג + פונט מודגש לתחום ביקורת חמה
            chartRange = xlWorkSheet.get_Range("A2", "I2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "HOT INSPECTION";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "D. Code";
            xlWorkSheet.Cells[3, 7] = "Emp No.";
            xlWorkSheet.Cells[3, 8] = "Name";
            xlWorkSheet.Cells[3, 9] = "ID";

            chartRange = xlWorkSheet.get_Range("A1", "A5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C5000");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D5000");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("E1", "E5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("F1", "F5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("G1", "G5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("H1", "H5000");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("I1", "I5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("J2", "N2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 10] = "Curing - Finished goods";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 10] = "Date";
            xlWorkSheet.Cells[3, 11] = "Time";
            xlWorkSheet.Cells[3, 12] = "Press";
            xlWorkSheet.Cells[3, 13] = "Emp No.";
            xlWorkSheet.Cells[3, 14] = "Name";

            chartRange = xlWorkSheet.get_Range("J1", "J5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("K1", "K5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("L1", "L5000");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("M1", "M5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("O2", "W2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 15] = "Building Stage 2";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 15] = "Date";
            xlWorkSheet.Cells[3, 16] = "Time";
            xlWorkSheet.Cells[3, 17] = "Catalog";
            xlWorkSheet.Cells[3, 18] = "Serial";
            xlWorkSheet.Cells[3, 19] = "B. Machine";
            xlWorkSheet.Cells[3, 20] = "Spc. Weight";
            xlWorkSheet.Cells[3, 21] = "Act. Weight";
            xlWorkSheet.Cells[3, 22] = "Emp No.";
            xlWorkSheet.Cells[3, 23] = "Name";

            chartRange = xlWorkSheet.get_Range("O1", "O5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("P1", "P5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("Q1", "Q5000");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("R1", "R5000");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("S1", "S5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("T1", "T5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("U1", "U5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("V1", "V5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("W1", "W5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("X2", "AF2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 24] = "Building Stage 1";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 24] = "Date";
            xlWorkSheet.Cells[3, 25] = "Time";
            xlWorkSheet.Cells[3, 26] = "Catalog";
            xlWorkSheet.Cells[3, 27] = "Serial";
            xlWorkSheet.Cells[3, 28] = "B. Machine";
            xlWorkSheet.Cells[3, 29] = "Spc. Weight";
            xlWorkSheet.Cells[3, 30] = "Act. Weight";
            xlWorkSheet.Cells[3, 31] = "Emp No.";
            xlWorkSheet.Cells[3, 32] = "Name";

            chartRange = xlWorkSheet.get_Range("X1", "X5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("Y1", "Y5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("Z1", "Z5000");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("AA1", "AA5000");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("AB1", "AB5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("AC1", "AC5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("AD1", "AD5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("AE1", "AE5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AF1", "AF5000");
            chartRange.ColumnWidth = 15;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("J4", "K10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("O4", "P10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("X4", "Y10000");
            chartRange.NumberFormat = "@";            

            // שליפת צמיגים שעברו בביקורת חמה בתקופה מבוקשת                        
            StrSql = "select a.LACNDT,a.LACNTM,INSIZ,a.LPROD,a.LLBLNO,a.LERRDS,a.LOVED, " +
                     "value(PRATI,' '),value(FAMILY,' '),b.LLBLNA " +
                     "from TAPIALI.LABELGL22 a " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) and MIFAL='01' " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=45";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";

            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(0).Substring(6, 2) + "/" + DBC.Rs1.GetString(0).Substring(4, 2) + "/" + DBC.Rs1.GetString(0).Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetString(1));
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(4);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(5).Trim();
                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetDecimal(6);
                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim() + " " + DBC.Rs1.GetString(8).Trim();

                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetValue(9);

                // נתוני גיפור
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABELGL3 left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where LPROD='" + DBC.Rs1.GetString(3) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=42";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 10] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 11] = FixTime(DBC.Rs2.GetString(1));
                    xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 13] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(4).Trim() + " " + DBC.Rs2.GetString(5).Trim();
                }
                DBC.Rs2.Close();

                // נתוני בניה שלב ב
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1,value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                                          "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where KPROD2='" + DBC.Rs1.GetString(3) + "' and KSERIAL2=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=103";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 15] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 16] = FixTime(DBC.Rs2.GetString(1));
                    xlWorkSheet.Cells[I, 17] = DBC.Rs2.GetString(6);
                    xlWorkSheet.Cells[I, 18] = DBC.Rs2.GetString(7);
                    xlWorkSheet.Cells[I, 19] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 20] = Math.Round(DBC.Rs2.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 21] = Math.Round(DBC.Rs2.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 22] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 23] = DBC.Rs2.GetString(8).Trim() + " " + DBC.Rs2.GetString(9).Trim();

                    // נתוני בניה שלב א
                    StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1,value(PRATI,' '),value(FAMILY,' ') " +
                             "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                                              "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                             "where KPROD2='" + DBC.Rs2.GetString(6) + "' and KSERIAL2=" + DBC.Rs2.GetString(7) + " and LACTAN=3";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet.Cells[I, 24] = DBC.Rs3.GetString(0).Substring(6, 2) + "/" + DBC.Rs3.GetString(0).Substring(4, 2) + "/" + DBC.Rs3.GetString(0).Substring(0, 4);
                        xlWorkSheet.Cells[I, 25] = FixTime(DBC.Rs3.GetString(1));
                        xlWorkSheet.Cells[I, 26] = DBC.Rs3.GetString(6);
                        xlWorkSheet.Cells[I, 27] = DBC.Rs3.GetString(7);
                        xlWorkSheet.Cells[I, 28] = DBC.Rs3.GetString(2);
                        xlWorkSheet.Cells[I, 29] = Math.Round(DBC.Rs3.GetDecimal(4), 2);
                        xlWorkSheet.Cells[I, 30] = Math.Round(DBC.Rs3.GetDecimal(5), 2);
                        xlWorkSheet.Cells[I, 31] = DBC.Rs3.GetString(3);
                        xlWorkSheet.Cells[I, 32] = DBC.Rs3.GetString(8).Trim() + " " + DBC.Rs3.GetString(9).Trim();
                    }
                    DBC.Rs3.Close();
                }
                DBC.Rs2.Close();
                I += 1;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("I2", "I" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "I3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("N2", "N" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("J2", "N3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("W2", "W" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("O2", "W3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.NavajoWhite.ToArgb();            
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("AF2", "AF" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("X2", "AF3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.YellowGreen.ToArgb();            
        }

        private void HI_CU_B2()    // HOT INSPECTION --> Curing - Finished goods --> Building Stage 2.
        {
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "I1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability  -  Hot Inspection  To  Building Stage 2,  Report Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + מיזוג + פונט מודגש לתחום ביקורת חמה
            chartRange = xlWorkSheet.get_Range("A2", "I2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "HOT INSPECTION";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "D. Code";
            xlWorkSheet.Cells[3, 7] = "Emp No.";
            xlWorkSheet.Cells[3, 8] = "Name";
            xlWorkSheet.Cells[3, 9] = "ID";

            chartRange = xlWorkSheet.get_Range("A1", "A5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C5000");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D5000");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("E1", "E5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("F1", "F5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("G1", "G5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("H1", "H5000");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("I1", "I5000");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("J2", "N2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 10] = "Curing - Finished goods";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 10] = "Date";
            xlWorkSheet.Cells[3, 11] = "Time";
            xlWorkSheet.Cells[3, 12] = "Press";
            xlWorkSheet.Cells[3, 13] = "Emp No.";
            xlWorkSheet.Cells[3, 14] = "Name";

            chartRange = xlWorkSheet.get_Range("J1", "J5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("K1", "K5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("L1", "L5000");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("M1", "M5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N5000");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("O2", "W2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 15] = "Building Stage 2";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 15] = "Date";
            xlWorkSheet.Cells[3, 16] = "Time";
            xlWorkSheet.Cells[3, 17] = "Catalog";
            xlWorkSheet.Cells[3, 18] = "Serial";
            xlWorkSheet.Cells[3, 19] = "B. Machine";
            xlWorkSheet.Cells[3, 20] = "Spc. Weight";
            xlWorkSheet.Cells[3, 21] = "Act. Weight";
            xlWorkSheet.Cells[3, 22] = "Emp No.";
            xlWorkSheet.Cells[3, 23] = "Name";

            chartRange = xlWorkSheet.get_Range("O1", "O5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("P1", "P5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("Q1", "Q5000");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("R1", "R5000");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("S1", "S5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("T1", "T5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("U1", "U5000");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("V1", "V5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("W1", "W5000");
            chartRange.ColumnWidth = 15;
            
            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("J4", "K10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("O4", "P10000");
            chartRange.NumberFormat = "@";            

            // שליפת צמיגים שעברו בביקורת חמה בתקופה מבוקשת                        
            StrSql = "select a.LACNDT,a.LACNTM,INSIZ,a.LPROD,a.LLBLNO,a.LERRDS,a.LOVED,value(PRATI,' '),value(FAMILY,' '),b.LLBLNA " +
                     "from TAPIALI.LABELGL22 a " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=45";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(0).Substring(6, 2) + "/" + DBC.Rs1.GetString(0).Substring(4, 2) + "/" + DBC.Rs1.GetString(0).Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetString(1));
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(4);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(5).Trim();
                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetDecimal(6);
                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim() + " " + DBC.Rs1.GetString(8).Trim();
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetValue(9);

                // נתוני גיפור
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABELGL3 left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where LPROD='" + DBC.Rs1.GetString(3) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=42";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 10] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 11] = FixTime(DBC.Rs2.GetString(1));
                    xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 13] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(4).Trim() + " " + DBC.Rs2.GetString(5).Trim();
                }
                DBC.Rs2.Close();

                // נתוני בניה שלב ב
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1,value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                                          "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where KPROD2='" + DBC.Rs1.GetString(3) + "' and KSERIAL2=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=103";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 15] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 16] = FixTime(DBC.Rs2.GetString(1));
                    xlWorkSheet.Cells[I, 17] = DBC.Rs2.GetString(6);
                    xlWorkSheet.Cells[I, 18] = DBC.Rs2.GetString(7);
                    xlWorkSheet.Cells[I, 19] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 20] = Math.Round(DBC.Rs2.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 21] = Math.Round(DBC.Rs2.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 22] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 23] = DBC.Rs2.GetString(8).Trim() + " " + DBC.Rs2.GetString(9).Trim();
                }
                DBC.Rs2.Close();
                I += 1;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "W3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("I2", "I" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "I3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("N2", "N" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("J2", "N3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("W2", "W" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("O2", "W3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.NavajoWhite.ToArgb();            
        }

        private void HI_CU()       // HOT INSPECTION --> Curing - Finished goods.
        {
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "I1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability  -  Hot Inspection To Curing - Finished goods Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + מיזוג + פונט מודגש לתחום ביקורת חמה
            chartRange = xlWorkSheet.get_Range("A2", "I2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "HOT INSPECTION";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "D. Code";
            xlWorkSheet.Cells[3, 7] = "Emp No.";
            xlWorkSheet.Cells[3, 8] = "Name";
            xlWorkSheet.Cells[3, 9] = "ID";

            chartRange = xlWorkSheet.get_Range("A1", "A5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C5000");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D5000");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("E1", "E5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("F1", "F5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("G1", "G5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("H1", "H5000");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("I1", "I5000");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("J2", "N2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 10] = "Curing - Finished goods";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 10] = "Date";
            xlWorkSheet.Cells[3, 11] = "Time";
            xlWorkSheet.Cells[3, 12] = "Press";
            xlWorkSheet.Cells[3, 13] = "Emp No.";
            xlWorkSheet.Cells[3, 14] = "Name";            

            chartRange = xlWorkSheet.get_Range("J1", "J5000");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("K1", "K5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("L1", "L5000");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("M1", "M5000");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N5000");
            chartRange.ColumnWidth = 15;            

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("J4", "K10000");
            chartRange.NumberFormat = "@";            

            // שליפת צמיגים שעברו בביקורת חמה בתקופה מבוקשת                        
            StrSql = "select a.LACNDT,a.LACNTM,INSIZ,a.LPROD,a.LLBLNO,a.LERRDS,a.LOVED, " +
                     "value(PRATI,' '),value(FAMILY,' '),b.LLBLNA " +
                     "from TAPIALI.LABELGL22 a " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +                     
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=45";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(0).Substring(6, 2) + "/" + DBC.Rs1.GetString(0).Substring(4, 2) + "/" + DBC.Rs1.GetString(0).Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetString(1));
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(4);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(5).Trim();
                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetDecimal(6);                
                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim() + " " + DBC.Rs1.GetString(8).Trim();
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetValue(9); 

                // נתוני גיפור                 
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED," +
                         "value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABELGL3 left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where LPROD='" + DBC.Rs1.GetString(3) + "' and LLBLNO=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=42";

                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 10] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 11] = FixTime(DBC.Rs2.GetString(1));
                    xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 13] = DBC.Rs2.GetString(3);                                        
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(4).Trim() + " " + DBC.Rs2.GetString(5).Trim();
                }
                DBC.Rs2.Close();                
                I += 1;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "N3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("I2", "I"+(I-1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "I3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("N2", "N" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("J2", "N3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();            
        }        


        private void CU_B2_B1()    // Curing - Finished goods --> Building Stage 2 --> Building Stage 1.
        {
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "I1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability  -  Curing - Finished goods  To  Building Stage 1 Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("A2", "I2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Curing - Finished goods";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "Press";
            xlWorkSheet.Cells[3, 7] = "Emp No.";
            xlWorkSheet.Cells[3, 8] = "Name";
            xlWorkSheet.Cells[3, 9] = "ID";

            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("J2", "R2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 10] = "Building Stage 2";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 10] = "Date";
            xlWorkSheet.Cells[3, 11] = "Time";
            xlWorkSheet.Cells[3, 12] = "Catalog";
            xlWorkSheet.Cells[3, 13] = "Serial";
            xlWorkSheet.Cells[3, 14] = "B. Machine";
            xlWorkSheet.Cells[3, 15] = "Spc. Weight";
            xlWorkSheet.Cells[3, 16] = "Act. Weight";
            xlWorkSheet.Cells[3, 17] = "Emp No.";
            xlWorkSheet.Cells[3, 18] = "Name";

            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("K1", "K1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("S2", "AA2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 19] = "Building Stage 1";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 19] = "Date";
            xlWorkSheet.Cells[3, 20] = "Time";
            xlWorkSheet.Cells[3, 21] = "Catalog";
            xlWorkSheet.Cells[3, 22] = "Serial";
            xlWorkSheet.Cells[3, 23] = "B. Machine";
            xlWorkSheet.Cells[3, 24] = "Spc. Weight";
            xlWorkSheet.Cells[3, 25] = "Act. Weight";
            xlWorkSheet.Cells[3, 26] = "Emp No.";
            xlWorkSheet.Cells[3, 27] = "Name";

            chartRange = xlWorkSheet.get_Range("S1", "S1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("T1", "T1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("U1", "U1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("V1", "V1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("W1", "W1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("X1", "X1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("Y1", "Y1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("Z1", "Z1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AA1", "AA1");
            chartRange.ColumnWidth = 15;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("J4", "K10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("S4", "T10000");
            chartRange.NumberFormat = "@";

            // שליפת צמיגים מגופרים לתקופה מבוקשת                                    
            StrSql = "select a.LACNDT,a.LACNTM,INSIZ,a.LPROD,a.LLBLNO,a.LMACH,a.LOVED,value(PRATI,' '),value(FAMILY,' '),b.LLBLNA " +
                     "from TAPIALI.LABELGL22 a " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=42";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(0).Substring(6, 2) + "/" + DBC.Rs1.GetString(0).Substring(4, 2) + "/" + DBC.Rs1.GetString(0).Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetString(1));
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(4);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(5).Trim();
                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetDecimal(6);
                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim() + " " + DBC.Rs1.GetString(8).Trim();
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetValue(9);
                // נתוני בניה שלב ב
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1,value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                                          "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where KPROD2='" + DBC.Rs1.GetString(3) + "' and KSERIAL2=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=103";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 10] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 11] = FixTime(DBC.Rs2.GetString(1));
                    xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetString(6);
                    xlWorkSheet.Cells[I, 13] = DBC.Rs2.GetString(7);
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 15] = Math.Round(DBC.Rs2.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 16] = Math.Round(DBC.Rs2.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 17] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 18] = DBC.Rs2.GetString(8).Trim() + " " + DBC.Rs2.GetString(9).Trim();

                    // נתוני בניה שלב א
                    StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1,value(PRATI,' '),value(FAMILY,' ') " +
                             "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                                              "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                             "where KPROD2='" + DBC.Rs2.GetString(6) + "' and KSERIAL2=" + DBC.Rs2.GetString(7) + " and LACTAN=3";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet.Cells[I, 19] = DBC.Rs3.GetString(0).Substring(6, 2) + "/" + DBC.Rs3.GetString(0).Substring(4, 2) + "/" + DBC.Rs3.GetString(0).Substring(0, 4);
                        xlWorkSheet.Cells[I, 20] = FixTime(DBC.Rs3.GetString(1));
                        xlWorkSheet.Cells[I, 21] = DBC.Rs3.GetString(6);
                        xlWorkSheet.Cells[I, 22] = DBC.Rs3.GetString(7);
                        xlWorkSheet.Cells[I, 23] = DBC.Rs3.GetString(2);
                        xlWorkSheet.Cells[I, 24] = Math.Round(DBC.Rs3.GetDecimal(4), 2);
                        xlWorkSheet.Cells[I, 25] = Math.Round(DBC.Rs3.GetDecimal(5), 2);
                        xlWorkSheet.Cells[I, 26] = DBC.Rs3.GetString(3);
                        xlWorkSheet.Cells[I, 27] = DBC.Rs3.GetString(8).Trim() + " " + DBC.Rs3.GetString(9).Trim();
                    }
                    DBC.Rs3.Close();
                }
                DBC.Rs2.Close();
                I += 1;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "AA3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("I2", "I" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "I3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("R2", "R" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("J2", "R3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("AA2", "AA" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("S2", "AA3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.NavajoWhite.ToArgb();            
        }
        
        private void CU_B2()       // Curing - Finished goods --> Building Stage 2.
        {
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "H1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability - Curing - Finished goods  To  Building Stage 2 Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AE3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;            

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("A2", "I2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Curing - Finished goods";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "Press";
            xlWorkSheet.Cells[3, 7] = "Emp No.";
            xlWorkSheet.Cells[3, 8] = "Name";
            xlWorkSheet.Cells[3, 9] = "ID";

            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("J2", "R2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 10] = "Building Stage 2";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 10] = "Date";
            xlWorkSheet.Cells[3, 11] = "Time";
            xlWorkSheet.Cells[3, 12] = "Catalog";
            xlWorkSheet.Cells[3, 13] = "Serial";
            xlWorkSheet.Cells[3, 14] = "B. Machine";
            xlWorkSheet.Cells[3, 15] = "Spc. Weight";
            xlWorkSheet.Cells[3, 16] = "Act. Weight";
            xlWorkSheet.Cells[3, 17] = "Emp No.";
            xlWorkSheet.Cells[3, 18] = "Name";

            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("K1", "K1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 15;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("J4", "K10000");
            chartRange.NumberFormat = "@";            

            // שליפת צמיגים מגופרים לתקופה מבוקשת                                    
            StrSql = "select a.LACNDT,a.LACNTM,INSIZ,a.LPROD,a.LLBLNO,a.LMACH,a.LOVED,value(PRATI,' '),value(FAMILY,' '),b.LLBLNO " +
                     "from TAPIALI.LABELGL22 A " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=42";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(0).Substring(6, 2) + "/" + DBC.Rs1.GetString(0).Substring(4, 2) + "/" + DBC.Rs1.GetString(0).Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetString(1));
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(4);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(5).Trim();
                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetDecimal(6);
                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim() + " " + DBC.Rs1.GetString(8).Trim();
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetValue(9);

                // נתוני בניה שלב ב
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1,value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                                          "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where KPROD2='" + DBC.Rs1.GetString(3) + "' and KSERIAL2=" + DBC.Rs1.GetDecimal(4) + " and LACTAN=103";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 10] = DBC.Rs2.GetString(0).Substring(6, 2) + "/" + DBC.Rs2.GetString(0).Substring(4, 2) + "/" + DBC.Rs2.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 11] = FixTime(DBC.Rs2.GetString(1));
                    xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetString(6);
                    xlWorkSheet.Cells[I, 13] = DBC.Rs2.GetString(7);
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 15] = Math.Round(DBC.Rs2.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 16] = Math.Round(DBC.Rs2.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 17] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 18] = DBC.Rs2.GetString(8).Trim() + " " + DBC.Rs2.GetString(9).Trim();
                }
                DBC.Rs2.Close();
                I += 1;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "R3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("I2", "I" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "I3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("R2", "R" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("J2", "R3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();            
        }

        private void CU_HI()       // Curing - Finished goods --> HOT INSPECTION.
        {
            string Catalog = "";
            string Serial = "";

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "H1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability - Curing - Finished goods  To  HOT INSPECTION Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AE3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("A2", "I2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Curing - Finished goods";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "Press";
            xlWorkSheet.Cells[3, 7] = "Emp No.";
            xlWorkSheet.Cells[3, 8] = "Name";
            xlWorkSheet.Cells[3, 9] = "ID";

            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("J2", "N2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 10] = "Building Stage 2";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 10] = "Date";
            xlWorkSheet.Cells[3, 11] = "Time";
            xlWorkSheet.Cells[3, 12] = "D. Code";
            xlWorkSheet.Cells[3, 13] = "Emp No.";
            xlWorkSheet.Cells[3, 14] = "Name";

            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("K1", "K1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 10;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("J4", "K10000");
            chartRange.NumberFormat = "@";

            // שליפת צמיגים מגופרים לתקופה מבוקשת                                    
            StrSql = "select a.LACNDT,a.LACNTM,INSIZ,a.LPROD,a.LLBLNO,a.LMACH,a.LOVED,value(PRATI,' '),value(FAMILY,' '),b.LLBLNO " +
                     "from TAPIALI.LABELGL22 A " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=42";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(0).Substring(6, 2) + "/" + DBC.Rs1.GetString(0).Substring(4, 2) + "/" + DBC.Rs1.GetString(0).Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetString(1));
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(4);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(5).Trim();
                xlWorkSheet.Cells[I, 7] = DBC.Rs1.GetDecimal(6);
                xlWorkSheet.Cells[I, 8] = DBC.Rs1.GetString(7).Trim() + " " + DBC.Rs1.GetString(8).Trim();
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetValue(9);
                Catalog = DBC.Rs1.GetString(3).Trim();
                Serial = DBC.Rs1.GetDecimal(4).ToString();


                if (Catalog != "")
                {
                    // נתוני ביקורת חמה
                    StrSql = "select LACNDT,LACNTM,LERRDS,LOVED,value(PRATI,' '),value(FAMILY,' ') " +
                             "from TAPIALI.LABELGL22 left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                             "where LPROD = '" + Catalog + "' and LLBLNO = " + Serial + " and LACTAN=45";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet.Cells[I, 10] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);
                        xlWorkSheet.Cells[I, 11] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                        xlWorkSheet.Cells[I, 12] = DBC.Rs3.GetString(2);
                        xlWorkSheet.Cells[I, 13] = DBC.Rs3.GetString(3);
                        xlWorkSheet.Cells[I, 14] = DBC.Rs3.GetString(4).Trim() + " " + DBC.Rs3.GetString(5).Trim();
                    }
                    else
                    {
                        // בדיקה אם תוצ''ג עצור או נפסל ע"י ביקורת איכות
                        DBC.Rs3.Close();
                        StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                                 "where LPROD = '" + Catalog + "' and LLBLNO = " + Serial + " " +
                                 "order by LLOGNO desc";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "S")
                                xlWorkSheet.Cells[I, 10] = "Stop By QA Engineer";
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "P")
                                xlWorkSheet.Cells[I, 10] = "Scrap By QA Engineer";
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs3.GetString(0) == "S")
                                xlWorkSheet.Cells[I, 10] = "Stop By QA Building";
                            chartRange = xlWorkSheet.get_Range("J" + I.ToString(), "N" + I.ToString());
                            chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                        }
                    }
                    DBC.Rs3.Close();
                }

                I += 1;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "AA3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("I2", "I" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "I3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("N2", "N" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("J2", "N3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();            
        }


        private void B2_CU_HI()    // Building Stage 2--> Curing - Finished goods --> HOT INSPECTION.      
        {
            string Catalog;
            string Serial;
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "I1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability - Building Stage 2  To  Hot Inspection Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("A2", "K2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Building Machine Stage 2";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "B. Machine";
            xlWorkSheet.Cells[3, 7] = "Spc. Weight";
            xlWorkSheet.Cells[3, 8] = "Act. Weight";
            xlWorkSheet.Cells[3, 9] = "Emp No.";
            xlWorkSheet.Cells[3, 10] = "Name";
            xlWorkSheet.Cells[3, 11] = "ID";
            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("k1", "k1");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("L2", "R2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 12] = "Curing - Finished goods";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 12] = "Date";
            xlWorkSheet.Cells[3, 13] = "Time";
            xlWorkSheet.Cells[3, 14] = "Catalog";
            xlWorkSheet.Cells[3, 15] = "Serial";
            xlWorkSheet.Cells[3, 16] = "Press";
            xlWorkSheet.Cells[3, 17] = "Emp No.";
            xlWorkSheet.Cells[3, 18] = "Name";
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום ביקורת חמה
            chartRange = xlWorkSheet.get_Range("S2", "W2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 19] = "HOT INSPECTION";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 19] = "Date";
            xlWorkSheet.Cells[3, 20] = "Time";
            xlWorkSheet.Cells[3, 21] = "D. Code";
            xlWorkSheet.Cells[3, 22] = "Emp No.";
            xlWorkSheet.Cells[3, 23] = "Name";
            chartRange = xlWorkSheet.get_Range("S1", "S1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("T1", "T1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("U1", "U1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("V1", "V1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("W1", "W1");
            chartRange.ColumnWidth = 15;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("L4", "M10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("S4", "T10000");
            chartRange.NumberFormat = "@";

            StrSql = "select a.LACNDT,a.LACNTM,a.LMACH,a.LOVED,a.LKGMF*2.2046,a.LKGAC*2.2046,a.LPROD,a.LLBLNO,INSIZ,a.LPROD||a.LLBLNO,b.LLBLNA,value(PRATI,' '),value(FAMILY,' ') " +
                     "from TAPIALI.LABELGL22 a " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=103 and SUBSTRING(a.LPROD,10,1)='0' and " +
                           "a.LUSER in ('J-59','T-22','T-25','T-26','JUMBO-12','JUMBO-16','34-3','34-4') and " +
                           "a.LPROD||a.LLBLNO not in (select KPROD2||KSERIAL2 from TAPIALI.LABKSRL2 where SUBSTRING(KPROD2,10,1)='0' and " +
                           "KOPDATE between " + Str_S_Date + " and " + Str_E_Date + ")";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetDecimal(1).ToString());
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(8);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(6);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(7);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 7] = Math.Round(DBC.Rs1.GetDecimal(4), 2);
                xlWorkSheet.Cells[I, 8] = Math.Round(DBC.Rs1.GetDecimal(5), 2);
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 10] = DBC.Rs1.GetString(11).Trim() + " " + DBC.Rs1.GetString(12).Trim();
                xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetValue(10);
                Catalog = "";
                Serial = "";
                // נתוני גיפור
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,KPROD2,KSERIAL2,value(PRATI,' '),value(FAMILY,' ') " +
                        "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO and LACTAN=42 " +
                                         "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                        "where KPROD1='" + DBC.Rs1.GetString(6).Trim() + "' and KSERIAL1=" + DBC.Rs1.GetDecimal(7) + "";
                DBC.Q_Run3(StrSql);
                if (DBC.Rs3.Read())
                {
                    xlWorkSheet.Cells[I, 12] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);
                    xlWorkSheet.Cells[I, 13] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                    xlWorkSheet.Cells[I, 14] = DBC.Rs3.GetString(4);
                    xlWorkSheet.Cells[I, 15] = DBC.Rs3.GetString(5);
                    xlWorkSheet.Cells[I, 16] = DBC.Rs3.GetString(2);
                    xlWorkSheet.Cells[I, 17] = DBC.Rs3.GetString(3);
                    xlWorkSheet.Cells[I, 18] = DBC.Rs3.GetString(6).Trim() + " " + DBC.Rs3.GetString(7).Trim();
                    Catalog = DBC.Rs3.GetString(4).Trim();
                    Serial = DBC.Rs3.GetString(5);
                }
                else
                {
                    // בדיקה אם ירוק עצור או נפסל ע"י ביקורת איכות
                    DBC.Rs3.Close();
                    StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                             "where LPROD = '" + DBC.Rs1.GetString(6) + "' and LLBLNO = " + DBC.Rs1.GetDecimal(7) + " " +
                             "order by LLOGNO desc";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Engineer";
                        if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "P")
                            xlWorkSheet.Cells[I, 12] = "Scrap By QA Engineer";
                        if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs3.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Building";
                        chartRange = xlWorkSheet.get_Range("L" + I.ToString(), "R" + I.ToString());
                        chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                    }
                }
                DBC.Rs3.Close();

                if (Catalog != "")
                {
                    // נתוני ביקורת חמה
                    StrSql = "select LACNDT,LACNTM,LERRDS,LOVED,value(PRATI,' '),value(FAMILY,' ') " +
                             "from TAPIALI.LABELGL22 left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                             "where LPROD = '" + Catalog + "' and LLBLNO = " + Serial + " and LACTAN=45";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet.Cells[I, 19] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);
                        xlWorkSheet.Cells[I, 20] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                        xlWorkSheet.Cells[I, 21] = DBC.Rs3.GetString(2);
                        xlWorkSheet.Cells[I, 22] = DBC.Rs3.GetString(3);
                        xlWorkSheet.Cells[I, 23] = DBC.Rs3.GetString(4).Trim() + " " + DBC.Rs3.GetString(5).Trim();
                    }
                    else
                    {
                        // בדיקה אם תוצ''ג עצור או נפסל ע"י ביקורת איכות
                        DBC.Rs3.Close();
                        StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                                 "where LPROD = '" + Catalog + "' and LLBLNO = " + Serial + " " +
                                 "order by LLOGNO desc";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "S")
                                xlWorkSheet.Cells[I, 19] = "Stop By QA Engineer";
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "P")
                                xlWorkSheet.Cells[I, 19] = "Scrap By QA Engineer";
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs3.GetString(0) == "S")
                                xlWorkSheet.Cells[I, 19] = "Stop By QA Building";
                            chartRange = xlWorkSheet.get_Range("S" + I.ToString(), "W" + I.ToString());
                            chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                        }
                    }
                    DBC.Rs3.Close();
                }
                I++;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "W3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("K2", "K" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "K3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("R2", "R" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("L2", "R3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("W2", "W" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("S2", "W3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.NavajoWhite.ToArgb();  
        }

        private void B2_CU()       // Building Stage 2--> Curing - Finished goods.      
        {
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "I1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability - Building Stage 2  To  Curing  -  Finished goods Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("A2", "K2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Building Machine Stage 2";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "B. Machine";
            xlWorkSheet.Cells[3, 7] = "Spc. Weight";
            xlWorkSheet.Cells[3, 8] = "Act. Weight";
            xlWorkSheet.Cells[3, 9] = "Emp No.";
            xlWorkSheet.Cells[3, 10] = "Name";
            xlWorkSheet.Cells[3, 11] = "ID";
            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("k1", "k1");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("L2", "R2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 12] = "Curing - Finished goods";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 12] = "Date";
            xlWorkSheet.Cells[3, 13] = "Time";
            xlWorkSheet.Cells[3, 14] = "Catalog";
            xlWorkSheet.Cells[3, 15] = "Serial";
            xlWorkSheet.Cells[3, 16] = "Press";
            xlWorkSheet.Cells[3, 17] = "Emp No.";
            xlWorkSheet.Cells[3, 18] = "Name";
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 15;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("L4", "M10000");
            chartRange.NumberFormat = "@";

            StrSql = "select a.LACNDT,a.LACNTM,a.LMACH,a.LOVED,a.LKGMF*2.2046,a.LKGAC*2.2046,a.LPROD,a.LLBLNO,INSIZ,a.LPROD||a.LLBLNO,b.LLBLNA,value(PRATI,' '),value(FAMILY,' ') " +
                     "from TAPIALI.LABELGL22 a " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=103 and SUBSTRING(a.LPROD,10,1)='0' and " +
                           "a.LUSER in ('J-59','T-22','T-25','T-26','JUMBO-12','JUMBO-16','34-3','34-4') and " +
                           "a.LPROD||a.LLBLNO not in (select KPROD2||KSERIAL2 from TAPIALI.LABKSRL2 where SUBSTRING(KPROD2,10,1)='0' and " +
                           "KOPDATE between " + Str_S_Date + " and " + Str_E_Date + ")";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetDecimal(1).ToString());
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(8);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(6);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(7);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 7] = Math.Round(DBC.Rs1.GetDecimal(4), 2);
                xlWorkSheet.Cells[I, 8] = Math.Round(DBC.Rs1.GetDecimal(5), 2);
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 10] = DBC.Rs1.GetString(11).Trim() + " " + DBC.Rs1.GetString(12).Trim();
                xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetValue(10);

                // נתוני גיפור
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,KPROD2,KSERIAL2,value(PRATI,' '),value(FAMILY,' ') " +
                        "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO and LACTAN=42 " +
                                         "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                        "where KPROD1='" + DBC.Rs1.GetString(6).Trim() + "' and KSERIAL1=" + DBC.Rs1.GetDecimal(7) + "";
                DBC.Q_Run3(StrSql);
                if (DBC.Rs3.Read())
                {
                    xlWorkSheet.Cells[I, 12] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);
                    xlWorkSheet.Cells[I, 13] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                    xlWorkSheet.Cells[I, 14] = DBC.Rs3.GetString(4);
                    xlWorkSheet.Cells[I, 15] = DBC.Rs3.GetString(5);
                    xlWorkSheet.Cells[I, 16] = DBC.Rs3.GetString(2);
                    xlWorkSheet.Cells[I, 17] = DBC.Rs3.GetString(3);
                    xlWorkSheet.Cells[I, 18] = DBC.Rs3.GetString(6).Trim() + " " + DBC.Rs3.GetString(7).Trim();
                }
                else
                {
                    // בדיקה אם ירוק עצור או נפסל ע"י ביקורת איכות
                    DBC.Rs3.Close();
                    StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                             "where LPROD = '" + DBC.Rs1.GetString(6) + "' and LLBLNO = " + DBC.Rs1.GetDecimal(7) + " " +
                             "order by LLOGNO desc";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Engineer";
                        if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "P")
                            xlWorkSheet.Cells[I, 12] = "Scrap By QA Engineer";
                        if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs3.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Building";
                        chartRange = xlWorkSheet.get_Range("L" + I.ToString(), "R" + I.ToString());
                        chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                    }
                }
                DBC.Rs3.Close();
                I++;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "R3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("K2", "K" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "K3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("R2", "R" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("L2", "R3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
        }        
        
        private void B2_B1()       // Building Stage 2 --> Building Stage 1.
        {
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "K1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability  -  Building Stage 2 Report  To  Building Stage 1 Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("A2", "K2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Building Stage 2";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "B. Machine";
            xlWorkSheet.Cells[3, 7] = "Spc. Weight";
            xlWorkSheet.Cells[3, 8] = "Act. Weight";
            xlWorkSheet.Cells[3, 9] = "Emp No.";
            xlWorkSheet.Cells[3, 10] = "Name";
            xlWorkSheet.Cells[3, 11] = "ID";


            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("K1", "K1");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("L2", "T2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 12] = "Building Stage 1";

            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 12] = "Date";
            xlWorkSheet.Cells[3, 13] = "Time";
            xlWorkSheet.Cells[3, 14] = "Catalog";
            xlWorkSheet.Cells[3, 15] = "Serial";
            xlWorkSheet.Cells[3, 16] = "B. Machine";
            xlWorkSheet.Cells[3, 17] = "Spc. Weight";
            xlWorkSheet.Cells[3, 18] = "Act. Weight";
            xlWorkSheet.Cells[3, 19] = "Emp No.";
            xlWorkSheet.Cells[3, 20] = "Name";

            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("S1", "S1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("T1", "T1");
            chartRange.ColumnWidth = 15;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("L4", "M10000");            
            chartRange.NumberFormat = "@";
                
            // נתוני בניה שלב ב            
            StrSql = "select a.LACNDT,a.LACNTM,INSIZ,a.LPROD,a.LLBLNO,a.LMACH,a.LKGMF*2.2046,a.LKGAC*2.2046,a.LOVED,b.LLBLNA,value(PRATI,' '),value(FAMILY,' ') " +
                        "from TAPIALI.LABELGL22 a " +
                        "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                        "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                        "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                        "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=103 and  SUBSTRING(a.LPROD,10,1)='0'";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetString(0).Substring(6, 2) + "/" + DBC.Rs1.GetString(0).Substring(4, 2) + "/" + DBC.Rs1.GetString(0).Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetString(1));
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetValue(4);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetValue(5);
                xlWorkSheet.Cells[I, 7] = Math.Round(DBC.Rs1.GetDecimal(6), 2);
                xlWorkSheet.Cells[I, 8] = Math.Round(DBC.Rs1.GetDecimal(6), 2);
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(8);
                xlWorkSheet.Cells[I, 10] = DBC.Rs1.GetString(10).Trim() + " " + DBC.Rs1.GetString(11).Trim();
                xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetValue(9);
                // נתוני בניה שלב א
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD1,KSERIAL1,value(PRATI,' '),value(FAMILY,' ') " +
                            "from TAPIALI.LABKSRL2 join TAPIALI.LABELGL3 on KPROD1=LPROD and KSERIAL1=LLBLNO " +
                                             "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                            "where KPROD2='" + DBC.Rs1.GetString(3) + "' and KSERIAL2=" + DBC.Rs1.GetValue(4) + " and LACTAN=3";
                DBC.Q_Run3(StrSql);
                if (DBC.Rs3.Read())
                {
                    xlWorkSheet.Cells[I, 12] = DBC.Rs3.GetString(0).Substring(6, 2) + "/" + DBC.Rs3.GetString(0).Substring(4, 2) + "/" + DBC.Rs3.GetString(0).Substring(0, 4);
                    xlWorkSheet.Cells[I, 13] = FixTime(DBC.Rs3.GetString(1));
                    xlWorkSheet.Cells[I, 14] = DBC.Rs3.GetString(6);
                    xlWorkSheet.Cells[I, 15] = DBC.Rs3.GetString(7);
                    xlWorkSheet.Cells[I, 16] = DBC.Rs3.GetString(2);
                    xlWorkSheet.Cells[I, 17] = Math.Round(DBC.Rs3.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 18] = Math.Round(DBC.Rs3.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 19] = DBC.Rs3.GetString(3);
                    xlWorkSheet.Cells[I, 20] = DBC.Rs3.GetString(8).Trim() + " " + DBC.Rs3.GetString(9).Trim();
                }
                DBC.Rs3.Close();
                I += 1;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "T3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("K2", "K" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "K3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("T2", "T" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("L2", "T3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
        }


        private void B1_B2_CU_HI() // Building Stage 1 --> Building Stage 2--> Curing - Finished goods --> HOT INSPECTION.
        {
            string Catalog;
            string Serial;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "I1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability - Building Stage 1  To  Hot Inspection Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);            

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("A2", "K2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Building Machine Stage 1";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "B. Machine";
            xlWorkSheet.Cells[3, 7] = "Spc. Weight";
            xlWorkSheet.Cells[3, 8] = "Act. Weight";
            xlWorkSheet.Cells[3, 9] = "Emp No.";
            xlWorkSheet.Cells[3, 10] = "Name";
            xlWorkSheet.Cells[3, 11] = "ID";

            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("K1", "K1");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("L2", "T2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 12] = "Building Machine Stage 2";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 12] = "Date";
            xlWorkSheet.Cells[3, 13] = "Time";
            xlWorkSheet.Cells[3, 14] = "Catalog";
            xlWorkSheet.Cells[3, 15] = "Serial";
            xlWorkSheet.Cells[3, 16] = "B. Machine";
            xlWorkSheet.Cells[3, 17] = "Spc. Weight";
            xlWorkSheet.Cells[3, 18] = "Act. Weight";
            xlWorkSheet.Cells[3, 19] = "Emp No.";
            xlWorkSheet.Cells[3, 20] = "Name";
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("S1", "S1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("T1", "T1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("U2", "AA2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 21] = "Curing - Finished goods";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 21] = "Date";
            xlWorkSheet.Cells[3, 22] = "Time";
            xlWorkSheet.Cells[3, 23] = "Catalog";
            xlWorkSheet.Cells[3, 24] = "Serial";
            xlWorkSheet.Cells[3, 25] = "Press";
            xlWorkSheet.Cells[3, 26] = "Emp No.";
            xlWorkSheet.Cells[3, 27] = "Name";
            chartRange = xlWorkSheet.get_Range("U1", "U1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("V1", "V1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("W1", "W1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("X1", "X1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("Y1", "Y1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("Z1", "Z1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AA1", "AA1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום ביקורת חמה
            chartRange = xlWorkSheet.get_Range("AB2", "AF2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 28] = "HOT INSPECTION";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 28] = "Date";
            xlWorkSheet.Cells[3, 29] = "Time";
            xlWorkSheet.Cells[3, 30] = "D. Code";
            xlWorkSheet.Cells[3, 31] = "Emp No.";
            xlWorkSheet.Cells[3, 32] = "Name";
            chartRange = xlWorkSheet.get_Range("AB1", "AB1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("AC1", "AC1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AD1", "AD1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AE1", "AE1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AF1", "AF1");
            chartRange.ColumnWidth = 15;

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("L4", "M10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("U4", "V10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("AB4", "AC10000");
            chartRange.NumberFormat = "@";

            StrSql = "select a.LACNDT,a.LACNTM,a.LMACH,a.LOVED,a.LKGMF*2.2046,a.LKGAC*2.2046,a.LPROD,a.LLBLNO,INSIZ,b.LLBLNA,value(PRATI,' '),value(FAMILY,' ') " +
                     "from TAPIALI.LABELGL22 a " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=3 and SUBSTRING(a.LPROD,10,1)='1'";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                //MessageBox.Show(DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4)); 
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetDecimal(1).ToString());
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(8); 
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(6);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(7);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 7] = Math.Round(DBC.Rs1.GetDecimal(4), 2);
                xlWorkSheet.Cells[I, 8] = Math.Round(DBC.Rs1.GetDecimal(5), 2);
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 10] = DBC.Rs1.GetString(10).Trim() + " " + DBC.Rs1.GetString(11).Trim();
                xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetValue(9);

                // נתוני בניה שלב ב
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD2,KSERIAL2,value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                                          "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where KPROD1='" + DBC.Rs1.GetString(6) + "' and KSERIAL1=" + DBC.Rs1.GetDecimal(7) + " and LACTAN=103";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs2.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs2.GetDecimal(0).ToString().Substring(0, 4);
                    xlWorkSheet.Cells[I, 13] = FixTime(DBC.Rs2.GetDecimal(1).ToString());
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(6);
                    xlWorkSheet.Cells[I, 15] = DBC.Rs2.GetString(7);
                    xlWorkSheet.Cells[I, 16] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 17] = Math.Round(DBC.Rs2.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 18] = Math.Round(DBC.Rs2.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 19] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 20] = DBC.Rs2.GetString(8).Trim() + " " + DBC.Rs2.GetString(9).Trim();
                    Catalog = "";
                    Serial = "";

                    // נתוני גיפור
                    StrSql = "select LACNDT,LACNTM,LMACH,LOVED,KPROD2,KSERIAL2,value(PRATI,' '),value(FAMILY,' ') " +
                             "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO and LACTAN=42 " +
                                              "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                             "where KPROD1='" + DBC.Rs2.GetString(6).Trim() + "' and KSERIAL1=" + DBC.Rs2.GetString(7) + "";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet.Cells[I, 21] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);
                        xlWorkSheet.Cells[I, 22] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                        xlWorkSheet.Cells[I, 23] = DBC.Rs3.GetString(4);
                        xlWorkSheet.Cells[I, 24] = DBC.Rs3.GetString(5);
                        xlWorkSheet.Cells[I, 25] = DBC.Rs3.GetString(2);
                        xlWorkSheet.Cells[I, 26] = DBC.Rs3.GetString(3);
                        xlWorkSheet.Cells[I, 27] = DBC.Rs3.GetString(6).Trim() + " " + DBC.Rs3.GetString(7).Trim();
                        Catalog = DBC.Rs3.GetString(4).Trim();
                        Serial = DBC.Rs3.GetString(5);
                    }
                    else
                    {
                        // בדיקה אם ירוק עצור או נפסל ע"י ביקורת איכות
                        DBC.Rs3.Close();
                        StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                                 "where LPROD = '" + DBC.Rs2.GetString(6) + "' and LLBLNO = " + DBC.Rs2.GetString(7) + " " +
                                 "order by LLOGNO desc";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "S")
                                xlWorkSheet.Cells[I, 21] = "Stop By QA Engineer";
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "P")
                                xlWorkSheet.Cells[I, 21] = "Scrap By QA Engineer";
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs3.GetString(0) == "S")
                                xlWorkSheet.Cells[I, 21] = "Stop By QA Building";
                            chartRange = xlWorkSheet.get_Range("U" + I.ToString(), "AA" + I.ToString());
                            chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                        }
                    }
                    DBC.Rs3.Close();

                    if (Catalog != "")
                    {
                        // נתוני ביקורת חמה
                        StrSql = "select LACNDT,LACNTM,LERRDS,LOVED,value(PRATI,' '),value(FAMILY,' ') " +
                                 "from TAPIALI.LABELGL22 left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                                 "where LPROD = '" + Catalog + "' and LLBLNO = " + Serial + " and LACTAN=45";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            xlWorkSheet.Cells[I, 28] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);
                            xlWorkSheet.Cells[I, 29] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                            xlWorkSheet.Cells[I, 30] = DBC.Rs3.GetString(2);
                            xlWorkSheet.Cells[I, 31] = DBC.Rs3.GetString(3);
                            xlWorkSheet.Cells[I, 32] = DBC.Rs3.GetString(4).Trim() + " " + DBC.Rs3.GetString(5).Trim();
                        }
                        else
                        {
                            // בדיקה אם ירוק עצור או נפסל ע"י ביקורת איכות
                            DBC.Rs3.Close();
                            StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                                     "where LPROD = '" + Catalog + "' and LLBLNO = " + Serial + " " +
                                     "order by LLOGNO desc";
                            DBC.Q_Run3(StrSql);
                            if (DBC.Rs3.Read())
                            {
                                if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "S")
                                    xlWorkSheet.Cells[I, 28] = "Stop By QA Engineer";
                                if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "P")
                                    xlWorkSheet.Cells[I, 28] = "Scrap By QA Engineer";
                                if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs3.GetString(0) == "S")
                                    xlWorkSheet.Cells[I, 28] = "Stop By QA Building";
                                chartRange = xlWorkSheet.get_Range("AB" + I.ToString(), "AF" + I.ToString());
                                chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                            }
                        }
                        DBC.Rs3.Close();
                    }
                }
                else
                {
                    // בדיקה אם קרקס עצור או נפסל ע"י ביקורת איכות
                    DBC.Rs2.Close();
                    StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                             "where LPROD = '" + DBC.Rs1.GetString(6) + "' and LLBLNO = " + DBC.Rs1.GetDecimal(7) + " " +
                             "order by LLOGNO desc";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        if (DBC.Rs2.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs2.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Engineer";
                        if (DBC.Rs2.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs2.GetString(0) == "P")
                            xlWorkSheet.Cells[I, 12] = "Scrap By QA Engineer";
                        if (DBC.Rs2.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs2.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Building";
                        chartRange = xlWorkSheet.get_Range("L" + I.ToString(), "T" + I.ToString());
                        chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                    }
                }
                DBC.Rs2.Close();
                I++;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("K2", "K" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "K3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("T2", "T" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("L2", "T3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("AA2", "AA" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("U2", "AA3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.NavajoWhite.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("AF2", "AF" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("AB2", "AF3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.YellowGreen.ToArgb();  
        }

        private void B1_B2_CU()    // Building Stage 1 --> Building Stage 2--> Curing - Finished goods.
        {            
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "I1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability - Building Stage 1  To  Curing - Finished goods Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("A2", "K2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Building Machine Stage 1";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "B. Machine";
            xlWorkSheet.Cells[3, 7] = "Spc. Weight";
            xlWorkSheet.Cells[3, 8] = "Act. Weight";
            xlWorkSheet.Cells[3, 9] = "Emp No.";
            xlWorkSheet.Cells[3, 10] = "Name";
            xlWorkSheet.Cells[3, 11] = "ID";

            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("K1", "K1");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("L2", "T2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 12] = "Building Machine Stage 2";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 12] = "Date";
            xlWorkSheet.Cells[3, 13] = "Time";
            xlWorkSheet.Cells[3, 14] = "Catalog";
            xlWorkSheet.Cells[3, 15] = "Serial";
            xlWorkSheet.Cells[3, 16] = "B. Machine";
            xlWorkSheet.Cells[3, 17] = "Spc. Weight";
            xlWorkSheet.Cells[3, 18] = "Act. Weight";
            xlWorkSheet.Cells[3, 19] = "Emp No.";
            xlWorkSheet.Cells[3, 20] = "Name";
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("S1", "S1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("T1", "T1");
            chartRange.ColumnWidth = 15;

            // מירכוז + מיזוג + פונט מודגש לתחום תוצ"ג - גיפור
            chartRange = xlWorkSheet.get_Range("U2", "AA2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 21] = "Curing - Finished goods";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 21] = "Date";
            xlWorkSheet.Cells[3, 22] = "Time";
            xlWorkSheet.Cells[3, 23] = "Catalog";
            xlWorkSheet.Cells[3, 24] = "Serial";
            xlWorkSheet.Cells[3, 25] = "Press";
            xlWorkSheet.Cells[3, 26] = "Emp No.";
            xlWorkSheet.Cells[3, 27] = "Name";
            chartRange = xlWorkSheet.get_Range("U1", "U1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("V1", "V1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("W1", "W1");
            chartRange.ColumnWidth = 13;
            chartRange = xlWorkSheet.get_Range("X1", "X1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("Y1", "Y1");
            chartRange.ColumnWidth = 6;
            chartRange = xlWorkSheet.get_Range("Z1", "Z1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("AA1", "AA1");
            chartRange.ColumnWidth = 15;            

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("L4", "M10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("U4", "V10000");
            chartRange.NumberFormat = "@";

            StrSql = "select a.LACNDT,a.LACNTM,a.LMACH,a.LOVED,a.LKGMF*2.2046,a.LKGAC*2.2046,a.LPROD,a.LLBLNO,INSIZ,b.LLBLNA,value(PRATI,' '),value(FAMILY,' ') " +
                     "from TAPIALI.LABELGL22 a " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=3 and SUBSTRING(a.LPROD,10,1)='1'";

            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {                
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetDecimal(1).ToString());
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(8);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(6);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(7);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 7] = Math.Round(DBC.Rs1.GetDecimal(4), 2);
                xlWorkSheet.Cells[I, 8] = Math.Round(DBC.Rs1.GetDecimal(5), 2);
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 10] = DBC.Rs1.GetString(10).Trim() + " " + DBC.Rs1.GetString(11).Trim();
                xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetValue(9);

                // נתוני בניה שלב ב
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD2,KSERIAL2,value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                                          "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where KPROD1='" + DBC.Rs1.GetString(6) + "' and KSERIAL1=" + DBC.Rs1.GetDecimal(7) + " and LACTAN=103";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs2.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs2.GetDecimal(0).ToString().Substring(0, 4);
                    xlWorkSheet.Cells[I, 13] = FixTime(DBC.Rs2.GetDecimal(1).ToString());
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(6);
                    xlWorkSheet.Cells[I, 15] = DBC.Rs2.GetString(7);
                    xlWorkSheet.Cells[I, 16] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 17] = Math.Round(DBC.Rs2.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 18] = Math.Round(DBC.Rs2.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 19] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 20] = DBC.Rs2.GetString(8).Trim() + " " + DBC.Rs2.GetString(9).Trim();

                    // נתוני גיפור
                    StrSql = "select LACNDT,LACNTM,LMACH,LOVED,KPROD2,KSERIAL2,value(PRATI,' '),value(FAMILY,' ') " +
                             "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO and LACTAN=42 " +
                                              "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                             "where KPROD1='" + DBC.Rs2.GetString(6).Trim() + "' and KSERIAL1=" + DBC.Rs2.GetString(7) + "";
                    DBC.Q_Run3(StrSql);
                    if (DBC.Rs3.Read())
                    {
                        xlWorkSheet.Cells[I, 21] = DBC.Rs3.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs3.GetDecimal(0).ToString().Substring(0, 4);
                        xlWorkSheet.Cells[I, 22] = FixTime(DBC.Rs3.GetDecimal(1).ToString());
                        xlWorkSheet.Cells[I, 23] = DBC.Rs3.GetString(4);
                        xlWorkSheet.Cells[I, 24] = DBC.Rs3.GetString(5);
                        xlWorkSheet.Cells[I, 25] = DBC.Rs3.GetString(2);
                        xlWorkSheet.Cells[I, 26] = DBC.Rs3.GetString(3);
                        xlWorkSheet.Cells[I, 27] = DBC.Rs3.GetString(6).Trim() + " " + DBC.Rs3.GetString(7).Trim();               
                    }
                    else
                    {
                        // בדיקה אם ירוק עצור או נפסל ע"י ביקורת איכות
                        DBC.Rs3.Close();
                        StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                                 "where LPROD = '" + DBC.Rs2.GetString(6) + "' and LLBLNO = " + DBC.Rs2.GetValue(7) + " " +
                                 "order by LLOGNO desc";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read())
                        {
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "S")
                                xlWorkSheet.Cells[I, 21] = "Stop By QA Engineer";
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs3.GetString(0) == "P")
                                xlWorkSheet.Cells[I, 21] = "Scrap By QA Engineer";
                            if (DBC.Rs3.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs3.GetString(0) == "S")
                                xlWorkSheet.Cells[I, 21] = "Stop By QA Building";
                            chartRange = xlWorkSheet.get_Range("U" + I.ToString(), "AA" + I.ToString());
                            chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                        }
                    }
                    DBC.Rs3.Close();                    
                }
                else
                {
                    // בדיקה אם קרקס עצור או נפסל ע"י ביקורת איכות
                    DBC.Rs2.Close();
                    StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                             "where LPROD = '" + DBC.Rs1.GetString(6) + "' and LLBLNO = " + DBC.Rs1.GetDecimal(7) + " " +
                             "order by LLOGNO desc";
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        if (DBC.Rs2.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs2.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Engineer";
                        if (DBC.Rs2.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs2.GetString(0) == "P")
                            xlWorkSheet.Cells[I, 12] = "Scrap By QA Engineer";
                        if (DBC.Rs2.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs2.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Building";
                        chartRange = xlWorkSheet.get_Range("L" + I.ToString(), "T" + I.ToString());
                        chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                    }
                }
                DBC.Rs2.Close();
                I++;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "AA3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("K2", "K" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "K3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("T2", "T" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("L2", "T3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("AA2", "AA" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("U2", "AA3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.NavajoWhite.ToArgb();
        }

        private void B1_B2()       // Building Stage 1 --> Building Stage 2.
        {            
            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "I1");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "Traceability - Building Stage 1  To  Building Stage 2 Report,  Between : " + DTPicker_From.Value.ToString("dd/MM/yyyy").Substring(0, 10) + " And  " + DTPicker_To.Value.ToString("dd/MM/yyyy").Substring(0, 10);

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב א
            chartRange = xlWorkSheet.get_Range("A2", "K2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 1] = "Building Machine Stage 1";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 1] = "Date";
            xlWorkSheet.Cells[3, 2] = "Time";
            xlWorkSheet.Cells[3, 3] = "Size";
            xlWorkSheet.Cells[3, 4] = "Catalog";
            xlWorkSheet.Cells[3, 5] = "Serial";
            xlWorkSheet.Cells[3, 6] = "B. Machine";
            xlWorkSheet.Cells[3, 7] = "Spc. Weight";
            xlWorkSheet.Cells[3, 8] = "Act. Weight";
            xlWorkSheet.Cells[3, 9] = "Emp No.";
            xlWorkSheet.Cells[3, 10] = "Name";
            xlWorkSheet.Cells[3, 11] = "ID";

            chartRange = xlWorkSheet.get_Range("A1", "A1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("G1", "G1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("I1", "I1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("J1", "J1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("K1", "K1");
            chartRange.ColumnWidth = 8;

            // מירכוז + מיזוג + פונט מודגש לתחום בניה שלב ב
            chartRange = xlWorkSheet.get_Range("L2", "T2");
            chartRange.MergeCells = true;
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
            xlWorkSheet.Cells[2, 12] = "Building Machine Stage 2";
            // הגדרת שדות הדוח                
            xlWorkSheet.Cells[3, 12] = "Date";
            xlWorkSheet.Cells[3, 13] = "Time";
            xlWorkSheet.Cells[3, 14] = "Catalog";
            xlWorkSheet.Cells[3, 15] = "Serial";
            xlWorkSheet.Cells[3, 16] = "B. Machine";
            xlWorkSheet.Cells[3, 17] = "Spc. Weight";
            xlWorkSheet.Cells[3, 18] = "Act. Weight";
            xlWorkSheet.Cells[3, 19] = "Emp No.";
            xlWorkSheet.Cells[3, 20] = "Name";
            chartRange = xlWorkSheet.get_Range("L1", "L1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("M1", "M1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("N1", "N1");
            chartRange.ColumnWidth = 14;
            chartRange = xlWorkSheet.get_Range("O1", "O1");
            chartRange.ColumnWidth = 7;
            chartRange = xlWorkSheet.get_Range("P1", "P1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("Q1", "Q1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("R1", "R1");
            chartRange.ColumnWidth = 11;
            chartRange = xlWorkSheet.get_Range("S1", "S1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("T1", "T1");
            chartRange.ColumnWidth = 15;            

            // מירכוז + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A3", "AF3");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            chartRange = xlWorkSheet.get_Range("A4", "B10000");
            chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("L4", "M10000");

            StrSql = "select a.LACNDT,a.LACNTM,a.LMACH,a.LOVED,a.LKGMF*2.2046,a.LKGAC*2.2046,a.LPROD,a.LLBLNO,INSIZ,b.LLBLNA,value(PRATI,' '),value(FAMILY,' ') " +
                     "from TAPIALI.LABELGL22 a " +
                     "join BPCSFALI.IIMN on SUBSTRING(a.LPROD,1,8)=INPROD " +
                     "left join ISUFKV.ISAVL10 on OVED=00||trim(a.LOVED) " +
                     "join TAPIALI.LABELL1 b on a.LPROD||a.LLBLNO=b.LPROD||b.LLBLNO " +
                     "where a.LACNDT between " + Str_S_Date + " and " + Str_E_Date + " and a.LACTAN=3 and SUBSTRING(a.LPROD,10,1)='1'";
            
            if (Txt_Pattern.Text != "")
                StrSql += " and left(a.LPROD,3) = '" + Txt_Pattern.Text + "'";
            if (Txt_Prod.Text != "")
                StrSql += " and left(a.LPROD,8) = '" + Txt_Prod.Text + "'";
            if (Txt_EmpNo.Text != "")
                StrSql += " and trim(a.LOVED) = '" + Txt_EmpNo.Text + "' ";

            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                xlWorkSheet.Cells[I, 1] = DBC.Rs1.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs1.GetDecimal(0).ToString().Substring(0, 4);
                xlWorkSheet.Cells[I, 2] = FixTime(DBC.Rs1.GetDecimal(1).ToString());
                xlWorkSheet.Cells[I, 3] = DBC.Rs1.GetString(8);
                xlWorkSheet.Cells[I, 4] = DBC.Rs1.GetString(6);
                xlWorkSheet.Cells[I, 5] = DBC.Rs1.GetDecimal(7);
                xlWorkSheet.Cells[I, 6] = DBC.Rs1.GetString(2);
                xlWorkSheet.Cells[I, 7] = Math.Round(DBC.Rs1.GetDecimal(4), 2);
                xlWorkSheet.Cells[I, 8] = Math.Round(DBC.Rs1.GetDecimal(5), 2);
                xlWorkSheet.Cells[I, 9] = DBC.Rs1.GetString(3);
                xlWorkSheet.Cells[I, 10] = DBC.Rs1.GetString(10).Trim() + " " + DBC.Rs1.GetString(11).Trim();
                xlWorkSheet.Cells[I, 11] = DBC.Rs1.GetValue(9);

                // נתוני בניה שלב ב
                StrSql = "select LACNDT,LACNTM,LMACH,LOVED,LKGMF*2.2046,LKGAC*2.2046,KPROD2,KSERIAL2,value(PRATI,' '),value(FAMILY,' ') " +
                         "from TAPIALI.LABKSRL1 join TAPIALI.LABELGL3 on KPROD2=LPROD and KSERIAL2=LLBLNO " +
                                          "left join ISUFKV.ISAVL10 on right(LOVED,5)=right(OVED,5) " +
                         "where KPROD1='" + DBC.Rs1.GetString(6) + "' and KSERIAL1=" + DBC.Rs1.GetDecimal(7) + " and LACTAN=103";
                DBC.Q_Run2(StrSql);
                if (DBC.Rs2.Read())
                {
                    xlWorkSheet.Cells[I, 12] = DBC.Rs2.GetDecimal(0).ToString().Substring(6, 2) + "/" + DBC.Rs2.GetDecimal(0).ToString().Substring(4, 2) + "/" + DBC.Rs2.GetDecimal(0).ToString().Substring(0, 4);
                    xlWorkSheet.Cells[I, 13] = FixTime(DBC.Rs2.GetDecimal(1).ToString());
                    xlWorkSheet.Cells[I, 14] = DBC.Rs2.GetString(6);
                    xlWorkSheet.Cells[I, 15] = DBC.Rs2.GetString(7);
                    xlWorkSheet.Cells[I, 16] = DBC.Rs2.GetString(2);
                    xlWorkSheet.Cells[I, 17] = Math.Round(DBC.Rs2.GetDecimal(4), 2);
                    xlWorkSheet.Cells[I, 18] = Math.Round(DBC.Rs2.GetDecimal(5), 2);
                    xlWorkSheet.Cells[I, 19] = DBC.Rs2.GetString(3);
                    xlWorkSheet.Cells[I, 20] = DBC.Rs2.GetString(8).Trim() + " " + DBC.Rs2.GetString(9).Trim();
                }
                else
                {
                    // בדיקה אם קרקס עצור או נפסל ע"י ביקורת איכות
                    DBC.Rs2.Close();
                    StrSql = "select LSTT,LUSER from TAPIALI.LABELGL70 " +
                             "where LPROD = '" + DBC.Rs1.GetString(6) + "' and LLBLNO = " + DBC.Rs1.GetDecimal(7) + " " +
                             "order by LLOGNO desc";                    
                    DBC.Q_Run2(StrSql);
                    if (DBC.Rs2.Read())
                    {
                        if (DBC.Rs2.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs2.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Engineer";
                        if (DBC.Rs2.GetString(1).Trim().Substring(0, 3) == "BBE" && DBC.Rs2.GetString(0) == "P")
                            xlWorkSheet.Cells[I, 12] = "Scrap By QA Engineer";
                        if (DBC.Rs2.GetString(1).Trim().Substring(0, 3) == "BBK" && DBC.Rs2.GetString(0) == "S")
                            xlWorkSheet.Cells[I, 12] = "Stop By QA Building";                        
                        chartRange = xlWorkSheet.get_Range("L" + I.ToString(), "T" + I.ToString());
                        chartRange.Cells.Interior.Color = System.Drawing.Color.LightCoral.ToArgb();                        
                    }

                }
                DBC.Rs2.Close();
                I++;
            }
            DBC.Rs1.Close();

            // AutoFilter
            chartRange = xlWorkSheet.get_Range("A3", "T3");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("K2", "K" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("A2", "K3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGoldenrodYellow.ToArgb();
            // Thick Border + Color
            chartRange = xlWorkSheet.get_Range("T2", "T" + (I - 1).ToString());
            chartRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 3d;
            chartRange = xlWorkSheet.get_Range("L2", "T3");
            chartRange.Cells.Interior.Color = System.Drawing.Color.LightGray.ToArgb();
        }                


        private string strReverse(string s)
        {
            int j = 0;
            char[] c = new char[s.Length];
            for (int i = s.Length - 1; i >= 0; i--) c[j++] = s[i];
            return new string(c);
        }

        private string FixTime(string S)
        {
            if (S.Length == 1)
                S = "00:00:0" + S;
            else
                if (S.Length == 2)
                    S = "00:00:" + S;
                else
                    if (S.Length == 3)
                        S = "00:0" + S.Substring(0, 1) + ":" + S.Substring(0, 2);
                    else
                        if (S.Length == 4)
                            S = "00:" + S.Substring(0, 2) + ":" + S.Substring(2, 2);
                        else
                            if (S.Length == 5)
                                S = "0" + S.Substring(0, 1) + ":" + S.Substring(1, 2) + ":" + S.Substring(3, 2);
                            else
                                S = S.Substring(0, 2) + ":" + S.Substring(2, 2) + ":" + S.Substring(4, 2);
            return S;
        }

        private void Frm_Traceability_Load(object sender, EventArgs e)
        {
            RB_HotInspection_Start.Checked = true;
            //RB_HotInspection_End.Checked = true;
            Fix_Destination_Options();
        }

        private void Fix_Destination_Options()
        {
            RB_HotInspection_End.Visible = true;
            RB_HotInspection_End.Checked = false;
            RB_CuringFinishedGoods_End.Visible = true;
            RB_CuringFinishedGoods_End.Checked = false;
            RB_BuildingStage_2_End.Visible = true;
            RB_BuildingStage_2_End.Checked = false;
            RB_BuildingStage_1_End.Visible = true;
            RB_BuildingStage_1_End.Checked = false;

            if (RB_HotInspection_Start.Checked)
            {
                RB_HotInspection_End.Visible = false;

                RB_CuringFinishedGoods_End.Location = new System.Drawing.Point(12,38);
                RB_BuildingStage_2_End.Location = new System.Drawing.Point(12, 61);
                RB_BuildingStage_1_End.Location = new System.Drawing.Point(12, 84);
            }
            else if (RB_CuringFinishedGoods_Start.Checked)
            {
                RB_CuringFinishedGoods_End.Visible = false;

                RB_HotInspection_End.Location = new System.Drawing.Point(12, 38);
                RB_BuildingStage_2_End.Location = new System.Drawing.Point(12, 61);
                RB_BuildingStage_1_End.Location = new System.Drawing.Point(12, 84);
            }
            else if (RB_BuildingStage_2_Start.Checked)
            {
                RB_BuildingStage_2_End.Visible = false;

                RB_HotInspection_End.Location = new System.Drawing.Point(12, 38);
                RB_CuringFinishedGoods_End.Location = new System.Drawing.Point(12, 61);
                RB_BuildingStage_1_End.Location = new System.Drawing.Point(12, 84);
            }
            else if (RB_BuildingStage_1_Start.Checked)
            {
                RB_BuildingStage_1_End.Visible = false;

                RB_HotInspection_End.Location = new System.Drawing.Point(12, 38);
                RB_CuringFinishedGoods_End.Location = new System.Drawing.Point(12, 61);
                RB_BuildingStage_2_End.Location = new System.Drawing.Point(12, 84);
            }
        }
            
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void RB_HotInspection_Start_Click(object sender, EventArgs e)
        {
            Fix_Destination_Options();
        }

        private void RB_CuringFinishedGoods_Start_Click(object sender, EventArgs e)
        {
            Fix_Destination_Options();
        }

        private void RB_BuildingStage_2_Start_Click(object sender, EventArgs e)
        {
            Fix_Destination_Options();
        }

        private void RB_BuildingStage_1_Start_Click(object sender, EventArgs e)
        {
            Fix_Destination_Options();
        }

        private void Txt_Pattern_KeyPress(object sender, KeyPressEventArgs e)
        {
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<-----     Delete     ----->| 
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete) e.Handled = false;
            else e.Handled = true;
        }

        private void Txt_Prod_KeyPress(object sender, KeyPressEventArgs e)
        {
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<-----     Delete     ----->| 
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete) e.Handled = false;
            else e.Handled = true;
        }

        private void Txt_EmpNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //   |<------       Numbers     ------>|    |<-----   BackSpace  ----->|   |<-----     Delete     ----->| 
            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete) e.Handled = false;
            else e.Handled = true;
        }


        //private string GetUserName(string EmpNo)
        //{
        //    string TmpSt = "";
        //    EmpNo = "00" + EmpNo.Trim();

        //    // AS400 התקשרות ל   
        //    DBConn DBC2;

        //    Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
        //    DBC2 = new DBConn();
        //    DBC2.Initialize_Conn(Con_Str);

        //    // שליפת פסולים לתקופה מבוקשת            
        //    StrSql = "select NAME1||NAME2||NAME3||NAME4||NAME5||NAME6,FMLY1||FMLY2||FMLY3||FMLY4||FMLY5||FMLY6||FMLY7||FMLY8||FMLY9||FMLY10 from ISUFKV.ISAV where OVED='" + EmpNo + "'";
        //    DBC2.Q_Run1(StrSql);
        //    if (DBC2.Rs1.Read())
        //    {                
        //        TmpSt = DBC2.Rs1.GetString(0).Trim() + " " + DBC2.Rs1.GetString(1).Trim();
        //        DBC2.Rs1.Close();
        //        DBC2.Close_Conn();
        //        return EmpNo = TmpSt;
        //    }
        //    else
        //    {
        //        DBC2.Rs1.Close();
        //        DBC2.Close_Conn();
        //        return "";
        //    }
        //}
    }
}
