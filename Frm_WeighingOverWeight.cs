﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_WeighingOverWeight : Form
    {
        // תוספת מתאריך 04/08/2016
        // הוספת דוח עודף משקל בשקילות, לבקשת פאדי
        // פיתוח של רן

        public Frm_WeighingOverWeight()
        {
            InitializeComponent();
        }

        //  Click
        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (DTPicker_FromW.Value.Date > DTPicker_ToW.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_FromW.Focus();
                return;
            }
            OverWeightReport();
        }

        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            //checkB_Carcass.Checked = false;
            //checkB_Green.Checked = false;
            DTPicker_FromW.Value = DateTime.Now.Date;
            DTPicker_ToW.Value = DateTime.Now.Date;
            PrepareFilterDatasBetweenDates();
        }        
                

        // //  Leave
        private void comboB_Machine_Leave(object sender, EventArgs e)
        {
            if (comboB_Machine.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";

                comboB_Size.Text = "";
                comboB_Size.Items.Clear();
                comboB_Emp.Text = "";
                comboB_Emp.Items.Clear();
                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);
                // שליפת רשימת גדלים לתקופה מבוקשת
                StrSql = "select distinct DESC from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                //else if (checkB_Carcass.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                //else if (checkB_Green.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by DESC";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_Size.Items.Add(DBC.Rs1.GetString(0).Trim()); // Catalog
                }
                DBC.Rs1.Close();
                // שליפת רשימת עובדים לתקופה מבוקשת
                StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                         "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                //else if (checkB_Carcass.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                //else if (checkB_Green.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by EMPLOYEE";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                    else
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
                }
                DBC.Rs1.Close();
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and ";
                StrSql += "substring(DATA12,1,3)='ID=' and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                //else if (checkB_Carcass.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                //else if (checkB_Green.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        private void comboB_Size_Leave(object sender, EventArgs e)
        {
            if (comboB_Size.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";

                comboB_Emp.Text = "";
                comboB_Emp.Items.Clear();
                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);
                // שליפת רשימת עובדים לתקופה מבוקשת
                StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                         "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";

                if (comboB_Machine.Text.Trim() != "")
                    StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                //else if (checkB_Carcass.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                //else if (checkB_Green.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by EMPLOYEE";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                    else
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
                }
                DBC.Rs1.Close();
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                               "substring(DATA12,1,3)='ID=' ";

                if (comboB_Machine.Text.Trim() != "")
                    StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                //else if (checkB_Carcass.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                //else if (checkB_Green.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        private void comboB_Emp_Leave(object sender, EventArgs e)
        {
            if (comboB_Emp.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";

                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                               "substring(DATA12,1,3)='ID=' ";

                if (comboB_Machine.Text.Trim() != "")
                    StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
                if (comboB_Size.Text.Trim() != "")
                    StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";

                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Substring(0, 5) + "0' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                //else if (checkB_Carcass.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                //else if (checkB_Green.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        private void DTPicker_ToW_Leave(object sender, EventArgs e)
        {
            if (DTPicker_FromW.Value.Date > DTPicker_ToW.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_FromW.Focus();
                return;
            }
            PrepareFilterDatasBetweenDates();
        }


        //  Load
        private void Frm_WeighingOverWeight_Load(object sender, EventArgs e)
        {
            //checkB_Carcass.Checked = false;
            //checkB_Green.Checked = false;

            PrepareFilterDatasBetweenDates();
        }


        // Routines
        private void PrepareFilterDatasBetweenDates()
        {
            Cursor.Current = Cursors.WaitCursor;

            string Con_Str;
            string StrSql = "";

            comboB_Machine.Text = "";
            comboB_Machine.Items.Clear();
            comboB_Size.Text = "";
            comboB_Size.Items.Clear();
            comboB_Emp.Text = "";
            comboB_Emp.Items.Clear();
            comboB_ID.Text = "";
            comboB_ID.Items.Clear();

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBC;
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);
            // שליפת רשימת מכונת לתקופה מבוקשת
            StrSql = "select distinct MACHINE from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            //else if (checkB_Carcass.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            //else if (checkB_Green.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by MACHINE";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_Machine.Items.Add(DBC.Rs1.GetString(0).Trim()); // Machine
            }
            DBC.Rs1.Close();
            // שליפת רשימת גדלים לתקופה מבוקשת
            StrSql = "select distinct DESC from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            //else if (checkB_Carcass.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            //else if (checkB_Green.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by DESC";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_Size.Items.Add(DBC.Rs1.GetString(0).Trim()); // Size
            }
            DBC.Rs1.Close();
            // שליפת רשימת עובדים לתקופה מבוקשת
            StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                     "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            //else if (checkB_Carcass.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            //else if (checkB_Green.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by EMPLOYEE";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                    comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                else
                    comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
            }
            DBC.Rs1.Close();
            // ליפוף ID שליפת רשימת             
            StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                           "substring(DATA12,1,3)='ID=' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            //else if (checkB_Carcass.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            //else if (checkB_Green.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            Cursor.Current = Cursors.PanEast;
        }       

        private void OverWeightReport()
        {
            string Con_Str;
            string StrSql = "";
            int i = 2, j = 10, k = 2;
            int ID_Position = 0;
            int TireSpecPosition = 0;
            int ORIGTSPosition = 0;
            int ACTUALTSPosition = 0;
            Int64 RecordNo = 0;
            int carcaspositivediv = 0;
            int greenpositivediv = 0;
            int stage2positivediv = 0;
            int carcasnegetivediv = 0;
            int greennegativediv = 0;
            int stage2negativediv = 0;
            decimal greenlbdiv = 0;
            decimal carcaslbdiv = 0;
            decimal stage2lbdiv = 0;
            int abovezero = 0;
            int greenabovezero = 0;
            Int64 greenrecord = 0;
            Int64 carcasrecord = 0;
            int stage2abovezero = 0;
            Int64 stage2record = 0;
            string[,] Emp;
            int counter = 0;
            int pointer = 0;
            
            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBCW;
            DBCW = new DBConn();
            DBCW.Initialize_Conn(Con_Str);

            //// שליפת רשימת פריטים לתקופה מבוקשת
            //StrSql = "select distinct PRODUCT from STWIND.PRRP " +
            //         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            //StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel._Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet, xlWorkSheet2, xlWorkSheet3, xlWorkSheet4, xlWorkSheet5;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;
            Excel.Range chartRangeEmp;
            Excel.Range chartRangeCatalog;
            Excel.Range chartRangeDT;
            Excel.Range chartRangeMin, chartRangeMin1;

            xlApp = new Excel.Application();
            //Excel._Application xlApp = new Excel.ApplicationClass();

            xlWorkBook = xlApp.Workbooks.Add(misValue);            

            // מחיקת גיליונות אקסל מיותרים
            int WN = 0;
            foreach (Excel.Worksheet xlworksheet in xlWorkBook.Worksheets)
            {
                if (WN != 0) // על מנת לא למחוק את הגיליון הראשון - חייבים להשאיר גיליון 1 
                    xlworksheet.Delete();
                WN++;
            }

            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet2 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet3 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet4 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet5 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            xlWorkSheet.Name = "Weighing Report";
            xlWorkSheet2.Name = "ElaboratedData";
            xlWorkSheet3.Name = "Diviation graph by size";
            xlWorkSheet4.Name = "Diviation graph by machine";
            xlWorkSheet5.Name = "Diviation graph by employee";

            xlWorkSheet2.Activate();

            xlWorkSheet.Cells[1, 1] = "Machine";
            xlWorkSheet.Cells[1, 2] = "Size";
            xlWorkSheet.Cells[1, 3] = "Catalog";
            xlWorkSheet.Cells[1, 4] = "Serial";
            xlWorkSheet.Cells[1, 5] = "ID (Small Label)";
            xlWorkSheet.Cells[1, 6] = "Date";
            xlWorkSheet.Cells[1, 7] = "Emp. No.";
            xlWorkSheet.Cells[1, 8] = "Emp. Name";
            xlWorkSheet.Cells[1, 9] = "Spec Tire Wt.";
            xlWorkSheet.Cells[1, 10] = "Actual Tire Wt.";
            xlWorkSheet.Cells[1, 11] = "Tire Wt. Deviation";
            xlWorkSheet.Cells[1, 12] = "Spec Carcas Wt.";
            xlWorkSheet.Cells[1, 13] = "Actual Carcas Wt.";
            xlWorkSheet.Cells[1, 14] = "Carcas Wt. Deviation";
            xlWorkSheet.Cells[1, 15] = "Stage II Spec Wt.";
            xlWorkSheet.Cells[1, 16] = "Stage II Actual Wt.";
            xlWorkSheet.Cells[1, 17] = "Stage II Wt. Deviation";
            xlWorkSheet.Cells[1, 18] = "Building Time [min]";
            xlWorkSheet.Cells[1, 19] = "Malfunction";
            xlWorkSheet.Cells[1, 20] = "ID No. Of Rotations";
            xlWorkSheet.Cells[1, 21] = "Actual No. Of Rotations";
            xlWorkSheet.Cells[1, 22] = "ID";
            xlWorkSheet.Cells[1, 23] = "Tire Spec";
            xlWorkSheet.Cells[1, 24] = "ID ToStart";
            xlWorkSheet.Cells[1, 25] = "Actual ToStart";
            xlWorkSheet.Cells[1, 26] = "Spec Carcas Perimeter";
            xlWorkSheet.Cells[1, 27] = "Actual Carcas Perimeter";

            xlWorkSheet2.Cells[9, 1] = "Machine";
            xlWorkSheet2.Cells[9, 2] = "Size";
            xlWorkSheet2.Cells[9, 3] = "Catalog";
            xlWorkSheet2.Cells[9, 4] = "Serial";
            xlWorkSheet2.Cells[9, 5] = "ID (Small Label)";
            xlWorkSheet2.Cells[9, 6] = "Employee No.";
            xlWorkSheet2.Cells[9, 7] = "Date";
            xlWorkSheet2.Cells[9, 8] = "Spec Tire Wt.";
            xlWorkSheet2.Cells[9, 9] = "Actual Tire Wt.";
            //xlWorkSheet2.Cells[1, 13] = "Tire Wt.";
            xlWorkSheet2.Cells[9, 10] = " Deviation";

           


            xlWorkSheet3.Cells[1, 1] = "Size";
            xlWorkSheet3.Cells[1, 2] = "Ammount";

        
            xlWorkSheet4.Cells[1, 1] = "Machine";
            xlWorkSheet4.Cells[1, 2] = "Ammount";


            xlWorkSheet5.Cells[1, 1] = "Employee";
            xlWorkSheet5.Cells[1, 2] = "Ammount";

            //הרחבת תאים
            chartRange = xlWorkSheet3.get_Range("A:A");
            chartRange.ColumnWidth = 20;
            chartRange = xlWorkSheet4.get_Range("A:A");
            chartRange.ColumnWidth = 20;
            chartRange = xlWorkSheet5.get_Range("A:A");
            chartRange.ColumnWidth = 20;

            //עיצוב טבלה - גבולות תאים
            chartRange = xlWorkSheet2.get_Range("L1", "P1");
            chartRange.Merge();
            chartRange = xlWorkSheet2.get_Range("L2", "M2");
            chartRange.Merge();
            chartRange = xlWorkSheet2.get_Range("L2", "P2" );
            chartRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            chartRange = xlWorkSheet2.get_Range("L3", "L4");
            chartRange.Merge();
            chartRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            chartRange = xlWorkSheet2.get_Range("L5", "M5");
            chartRange.Merge();
            chartRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            chartRange = xlWorkSheet2.get_Range("L5", "P6");
            chartRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            chartRange = xlWorkSheet2.get_Range("M3", "P4");
            chartRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            chartRange = xlWorkSheet2.get_Range("N7", "P7");
            chartRange.Merge();
            chartRange = xlWorkSheet2.get_Range("L6", "M6");
            chartRange.Merge();
            chartRange = xlWorkSheet2.get_Range("L7", "M7");
            chartRange.Merge();
            chartRange = xlWorkSheet2.get_Range("L7", "P7");
            chartRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            chartRange = xlWorkSheet2.get_Range("L2", "P7");
            chartRange.VerticalAlignment =  Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter ;


            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "AA1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRangeEmp = xlWorkSheet2.get_Range("A9", "J9");
            chartRangeEmp.HorizontalAlignment = 3;
            chartRangeEmp.VerticalAlignment = 3;
            chartRangeEmp.Font.Bold = true;

            // Freeze rows            
            //Excel.Window xlWnd3 = xlApp.ActiveWindow;
            //chartRange = xlWorkSheet.get_Range("A1", "A1").get_Offset(1, 0).EntireRow;
            //chartRange.Select();
            //xlWnd3.FreezePanes = true;

            StrSql = "select count(*) from STWIND.PRRP " +
                 "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                RecordNo = DBCW.Rs1.GetInt64(0);
            }
            DBCW.Rs1.Close();

            //שליפת מספר ירוקים 
            StrSql = "select count(*) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and substring(PRODUCT, 10, 1) = '0' and DATA4 = 0 ";
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                greenrecord = DBCW.Rs1.GetInt64(0);
            }
            DBCW.Rs1.Close();

            // שליפת מספר קרקסים 
            StrSql = "select count(*) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and substring(PRODUCT, 10, 1) = '1' ";
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                carcasrecord = DBCW.Rs1.GetInt64(0);
            }
            DBCW.Rs1.Close();
            stage2record = RecordNo - (greenrecord + carcasrecord);

            // שליפה טבלת צמיגים חורגים מאותה מידה
            StrSql = "select DESC, count(*) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "((substring(PRODUCT, 10, 1) = '1' and ABS(DATA4/DATA3 - 1) * 100 > 3) or " + // קרקס
                       "(substring(PRODUCT, 10, 1) = '0' and DATA4=0 and ABS(DATA2/DATA1 - 1) * 100 > 3) or " + // ירוק
                       "(substring(PRODUCT, 10, 1) = '0' and DATA4<>0 and ABS((DATA2-DATA4)/(DATA1-DATA3) - 1) * 100 > 3))";   // שלב ב
            //
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";
            
            StrSql += " group by DESC";
            StrSql += " order by 2 desc";
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet3.Cells[k, 1] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet3.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                k++;
            }
            DBCW.Rs1.Close();

            var charts = xlWorkSheet3.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            var chartObject = charts.Add(350, 50, 550, 350) as Microsoft.Office.Interop.Excel.ChartObject;
            var chart = chartObject.Chart;
            // Set chart range.            
            var range = xlWorkSheet3.get_Range("A2", "B" + (k - 1).ToString());
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Tire diviation analysis by size",
                              CategoryTitle: "Size",
                              ValueTitle: "Ammount");

            //שליפת טבלת צמיגים חורגים מאותה מכונה
            StrSql = "select MACHINE, count(*) from STWIND.PRRP " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                     "((substring(PRODUCT, 10, 1) = '1' and ABS(DATA4/DATA3 - 1) * 100 > 3) or " + // קרקס
                       "(substring(PRODUCT, 10, 1) = '0' and DATA4=0 and ABS(DATA2/DATA1 - 1) * 100 > 3) or " + // ירוק
                       "(substring(PRODUCT, 10, 1) = '0' and DATA4<>0 and ABS((DATA2-DATA4)/(DATA1-DATA3) - 1) * 100 > 3))";   // שלב ב

            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by MACHINE ";
            StrSql += " order by 2 desc";
            DBCW.Q_Run1(StrSql);
            k = 2;
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet4.Cells[k, 1] = "M " + DBCW.Rs1.GetString(0).Trim();
                xlWorkSheet4.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim();
                k++;
            }
            DBCW.Rs1.Close();

            charts = xlWorkSheet4.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(350, 50, 550, 350) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            range = xlWorkSheet4.get_Range("A2", "B" + (k - 1).ToString());
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Tire diviation analysis by machine",
                              CategoryTitle: "Machine",
                              ValueTitle: "Ammount");


            // שליפת טבלת צמיגים חורגים מאותו עובד
            // בדיקת כמות עובדים - כרכס
            StrSql = "select count(*) from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
              "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                    "(substring(PRODUCT, 10, 1) = '1' and ABS(DATA4/DATA3 - 1) * 100 > 3) "; // קרקס
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            DBCW.Q_Run1(StrSql);
            k = 2;
            while (DBCW.Rs1.Read())
            {
                counter = int.Parse(DBCW.Rs1.GetString(0));
            }
            DBCW.Rs1.Close();

            //בדיקת כמות עובדים - לא כרכסים 
            StrSql = "select count(*) from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
              "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                    "((substring(PRODUCT, 10, 1) = '0' and DATA4=0 and ABS(DATA2/DATA1 - 1) * 100 > 3) or " + // ירוק
                       "(substring(PRODUCT, 10, 1) = '0' and DATA4<>0 and ABS((DATA2-DATA4)/(DATA1-DATA3) - 1) * 100 > 3)) ";   // שלב ב
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            DBCW.Q_Run1(StrSql);
            k = 2;
            while (DBCW.Rs1.Read())
            {
                counter += int.Parse(DBCW.Rs1.GetString(0));
            }
            DBCW.Rs1.Close();
            Emp = new string[counter, 2];

            StrSql = "select substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*'), count(*) from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
              "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                    "(substring(PRODUCT, 10, 1) = '1' and ABS(DATA4/DATA3 - 1) * 100 > 3) "; // קרקס
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*') ";
            StrSql += " order by 4 desc";
            DBCW.Q_Run1(StrSql);
            k = 0;
            while (DBCW.Rs1.Read())
            {
               
                    Emp[k, 0] = DBCW.Rs1.GetString(1).Trim() + " " + DBCW.Rs1.GetString(2).Trim();
                    Emp[k, 1] = DBCW.Rs1.GetString(3).Trim();
                    k++;
                    pointer++;
                
                //xlWorkSheet5.Cells[k, 1] = DBCW.Rs1.GetString(0).Trim() + " " + DBCW.Rs1.GetString(1).Trim() + " " + DBCW.Rs1.GetString(2).Trim();
                //xlWorkSheet5.Cells[k, 2] = DBCW.Rs1.GetString(3).Trim();
                //k++;
            }
            DBCW.Rs1.Close();

            StrSql = "select substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*'), count(*) from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                    "((substring(PRODUCT, 10, 1) = '0' and DATA4=0 and ABS(DATA2/DATA1 - 1) * 100 > 3) or " + // ירוק
                       "(substring(PRODUCT, 10, 1) = '0' and DATA4<>0 and ABS((DATA2-DATA4)/(DATA1-DATA3) - 1) * 100 > 3)) ";   // שלב ב
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*') ";
            StrSql += " order by 4 desc";
            DBCW.Q_Run1(StrSql);
            k = 0;
            while (DBCW.Rs1.Read())
            {
              
                    if (Emp[k,0] ==  DBCW.Rs1.GetString(1).Trim() + " " + DBCW.Rs1.GetString(2).Trim())
                    {
                        Emp[k, 1] += int.Parse(DBCW.Rs1.GetString(3).Trim());
                    }
                    else
                    {
                        Emp[pointer, 0] =  DBCW.Rs1.GetString(1).Trim() + " " + DBCW.Rs1.GetString(2).Trim();
                        Emp[pointer, 1] = DBCW.Rs1.GetString(3).Trim();
                        pointer++;
                    }
                //DBCW.Rs1.GetString(0).Trim() + " " +
            }
            i = 2;
            for (int r = 0; r < Emp.GetLength(0); r++)
            {
                if (Emp[r, 0] != null)
                {
                    xlWorkSheet5.Cells[i, 1] = Emp[r, 0];
                    xlWorkSheet5.Cells[i, 2] = Emp[r, 1];
                    i++;
                } 
            }
            charts = xlWorkSheet5.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(350, 50, 550, 350) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            range = xlWorkSheet5.get_Range("A2", "B" + (i-1).ToString());
            chart.SetSourceData(range);
            //range = xlWorkSheet5.get_Range("B:B");
             range.Sort(range.Columns[2, Type.Missing],Excel.XlSortOrder.xlDescending, // the first sort key Column 1 for Range
              range.Columns[1, Type.Missing],Type.Missing, Excel.XlSortOrder.xlDescending,// second sort key Column 6 of the range
              Type.Missing, Excel.XlSortOrder.xlDescending,  // third sort key nothing, but it wants one
              Excel.XlYesNoGuess.xlGuess, Type.Missing, Type.Missing, 
              Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,   
              Excel.XlSortDataOption.xlSortNormal,
              Excel.XlSortDataOption.xlSortNormal, 
              Excel.XlSortDataOption.xlSortNormal);
 
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Tire diviation analysis by employee",
                              CategoryTitle: "Employee",
                              ValueTitle: "Ammount");


            StrSql = "select DESC,PDATE,EMPLOYEE,PRODUCT,DATA1,DATA2,DATA3,DATA4,MACHINE,DATA5,DATA6,DATA12,PRODUCTID,DATA10,DATA9,LLBLNA,LSPEC,value(PRATI,'*'),value(FAMILY,'*') " +
                     "from STWIND.PRRP left join " +
                          "TAPIALI.LABELP on PRODUCT=LPROD and PRODUCTID=LLBLNO left join " +
                          "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " + 
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            //DBCW.Q_Run1(StrSql);
            //while (DBCW.Rs1.Read())
            //{
            //    RecordNo = DBCW.Rs1.GetInt64(0);
            //}
            //DBCW.Rs1.Close();
            DBCW.Q_Run1(StrSql);
            i = 2;
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet.Cells[i, 1] = DBCW.Rs1.GetString(8); //"Machine";
                xlWorkSheet.Cells[i, 2] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet.Cells[i, 3] = DBCW.Rs1.GetString(3).Trim(); //"Catalog";
                xlWorkSheet.Cells[i, 4] = DBCW.Rs1.GetString(12).Trim(); //"Serial";
                xlWorkSheet.Cells[i, 5] = DBCW.Rs1.GetValue(15).ToString(); //"ID (Small Label)";
                xlWorkSheet.Cells[i, 6] = DBCW.Rs1.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss"); //"Date";
                xlWorkSheet.Cells[i, 7] = DBCW.Rs1.GetString(2).Trim().Substring(2, 5); //"Emp. No.";
                if (!DBCW.Rs1.IsDBNull(17) && !DBCW.Rs1.IsDBNull(18))
                    xlWorkSheet.Cells[i, 8] = DBCW.Rs1.GetString(17).Trim() + " " + DBCW.Rs1.GetString(18).Trim(); //"Emp. Name";                                          -------------------------------
                xlWorkSheet.Cells[i, 9] = DBCW.Rs1.GetDecimal(4); //"Spec Tire Wt.";                                
                if (DBCW.Rs1.GetDecimal(5) != 0)
                {
                    xlWorkSheet.Cells[i, 10] = DBCW.Rs1.GetDecimal(5); //"Actual Tire Wt.";
                    xlWorkSheet.Cells[i, 11] = Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) + "%"; //"Tire Wt. Deviation";
                }
                // ספירת כמות הצמיגים בסטייה גדולה מ 3%
                var cellValue = (DBCW.Rs1.GetString(3).Trim().Substring(9, 1));
                decimal x = (DBCW.Rs1.GetDecimal(5)) - (DBCW.Rs1.GetDecimal(7));
                decimal y = (DBCW.Rs1.GetDecimal(4)) - (DBCW.Rs1.GetDecimal(6));
                if (cellValue == "0")
                {

                    if ((DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(4)) > 0)
                    {
                        abovezero++;
                        if ((DBCW.Rs1.GetDecimal(6)) <= 0) // בדיקה אם קיים קרקס על מנת להחליט אם ירוק או שלב ב
                        {
                            greenabovezero++;
                        }
                        else
                        {
                            stage2abovezero++;
                        }
                    }


                    if (Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) > 3 || (Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) < -3) || (Math.Round((x / y - 1) * 100, 2) > 3) || (Math.Round((x / y - 1) * 100, 2) < -3))
                    {
                        if (Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) > 3 || (Math.Round((x / y - 1) * 100, 2) > 3) )
                        {
                            if ((DBCW.Rs1.GetDecimal(7)) <= 0)
                            {
                                greenpositivediv++;
                            }                            
                        }
                        if ((Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) < -3) || (Math.Round((x / y - 1) * 100, 2) < -3) )
                        {
                            if ((DBCW.Rs1.GetDecimal(7)) <= 0)
                            {
                                greennegativediv++;
                            }
                          
                        }
                        if ((DBCW.Rs1.GetDecimal(6)) <= 0)
                        {

                            greenlbdiv += DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(4);
                            xlWorkSheet2.Cells[j, 1] = DBCW.Rs1.GetString(8); //"Machine";
                            xlWorkSheet2.Cells[j, 2] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                            xlWorkSheet2.Cells[j, 3] = DBCW.Rs1.GetString(3).Trim(); //"Catalog";
                            xlWorkSheet2.Cells[j, 4] = DBCW.Rs1.GetString(12).Trim(); //"Serial";
                            xlWorkSheet2.Cells[j, 5] = DBCW.Rs1.GetValue(15).ToString(); //"ID (Small Label)";
                            xlWorkSheet2.Cells[j, 6] = DBCW.Rs1.GetString(2).Trim().Substring(2, 5); //"Emp. No.";
                            xlWorkSheet2.Cells[j, 7] = DBCW.Rs1.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss"); //"Date";
                            xlWorkSheet2.Cells[j, 8] = DBCW.Rs1.GetDecimal(4); //"Spec Tire Wt.";                                
                            xlWorkSheet2.Cells[j, 9] = DBCW.Rs1.GetDecimal(5); //"Actual Tire Wt.";
                            xlWorkSheet2.Cells[j, 10] = Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) + "%"; //"Tire Wt. Deviation"; 
                            j++;
                        }
                        else
                        {
                            if (Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) > 3 || (Math.Round((x / y - 1) * 100, 2) > 3))
                            {
                                stage2positivediv++;
                            }
                            if ((Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) < -3) || (Math.Round((x / y - 1) * 100, 2) < -3))
                            {
                                stage2negativediv++;
                            }

                            decimal stage2 = DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(7);
                            stage2lbdiv += DBCW.Rs1.GetDecimal(4) - DBCW.Rs1.GetDecimal(6) - stage2;
                            xlWorkSheet2.Cells[j, 1] = DBCW.Rs1.GetString(8); //"Machine";
                            xlWorkSheet2.Cells[j, 2] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                            xlWorkSheet2.Cells[j, 3] = DBCW.Rs1.GetString(3).Trim(); //"Catalog";
                            xlWorkSheet2.Cells[j, 4] = DBCW.Rs1.GetString(12).Trim(); //"Serial";
                            xlWorkSheet2.Cells[j, 5] = DBCW.Rs1.GetValue(15).ToString(); //"ID (Small Label)";
                            xlWorkSheet2.Cells[j, 6] = DBCW.Rs1.GetString(2).Trim().Substring(2, 5); //"Emp. No.";
                            xlWorkSheet2.Cells[j, 7] = DBCW.Rs1.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss"); //"Date";
                            xlWorkSheet2.Cells[j, 8] = DBCW.Rs1.GetDecimal(4) - DBCW.Rs1.GetDecimal(6); //"Stage II Spec Wt.";
                            xlWorkSheet2.Cells[j, 9] = DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(7); //"Stage II Actual Wt.";
                            xlWorkSheet2.Cells[j, 10] = Math.Round(((DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(7)) / (DBCW.Rs1.GetDecimal(4) - DBCW.Rs1.GetDecimal(6)) - 1) * 100, 2) + "%"; //"Stage II Wt. Deviation";
                            j++;
                        }
                    }

                }
                else
                {
                    x = (DBCW.Rs1.GetDecimal(5)) - (DBCW.Rs1.GetDecimal(7));
                    y = (DBCW.Rs1.GetDecimal(4)) - (DBCW.Rs1.GetDecimal(6));
                    if (Math.Round((DBCW.Rs1.GetDecimal(7) / DBCW.Rs1.GetDecimal(6) - 1) * 100, 2) > 3 )
                    {
                        carcaspositivediv++;
                        carcaslbdiv += DBCW.Rs1.GetDecimal(7) - DBCW.Rs1.GetDecimal(6);
                        xlWorkSheet2.Cells[j, 1] = DBCW.Rs1.GetString(8); //"Machine";
                        xlWorkSheet2.Cells[j, 2] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                        xlWorkSheet2.Cells[j, 3] = DBCW.Rs1.GetString(3).Trim(); //"Catalog";
                        xlWorkSheet2.Cells[j, 4] = DBCW.Rs1.GetString(12).Trim(); //"Serial";
                        xlWorkSheet2.Cells[j, 5] = DBCW.Rs1.GetValue(15).ToString(); //"ID (Small Label)";
                        xlWorkSheet2.Cells[j, 6] = DBCW.Rs1.GetString(2).Trim().Substring(2, 5); //"Emp. No.";
                        xlWorkSheet2.Cells[j, 7] = DBCW.Rs1.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss"); //"Date";
                        xlWorkSheet2.Cells[j, 8] = DBCW.Rs1.GetDecimal(6); //"Spec Carcas Wt.";
                        xlWorkSheet2.Cells[j, 9] = DBCW.Rs1.GetDecimal(7); //"Actual Carcas Wt.";
                        xlWorkSheet2.Cells[j, 10] = Math.Round((DBCW.Rs1.GetDecimal(7) / DBCW.Rs1.GetDecimal(6) - 1) * 100, 2) + "%"; //"Carcas Wt. Deviation";
                        j++;
                    }
                    if ((Math.Round((DBCW.Rs1.GetDecimal(7) / DBCW.Rs1.GetDecimal(6) - 1) * 100, 2) < -3) )
                    {
                        carcasnegetivediv++;
                    }
                    //carcasabovezero++;
                    //if (Math.Round((DBCW.Rs1.GetDecimal(7) / DBCW.Rs1.GetDecimal(6) - 1) * 100, 2) > 3 || (Math.Round((DBCW.Rs1.GetDecimal(7) / DBCW.Rs1.GetDecimal(6) - 1) * 100, 2) < -3))
                    //{
                        
                        
                    //}


                }

                if (DBCW.Rs1.GetDecimal(6) > 0)
                {
                    xlWorkSheet.Cells[i, 12] = DBCW.Rs1.GetDecimal(6); //"Spec Carcas Wt.";
                    xlWorkSheet.Cells[i, 13] = DBCW.Rs1.GetDecimal(7); //"Actual Carcas Wt.";
                    xlWorkSheet.Cells[i, 14] = Math.Round((DBCW.Rs1.GetDecimal(7) / DBCW.Rs1.GetDecimal(6) - 1) * 100, 2) + "%"; //"Carcas Wt. Deviation";
                    if (DBCW.Rs1.GetDecimal(5) != 0)
                    {
                        xlWorkSheet.Cells[i, 15] = DBCW.Rs1.GetDecimal(4) - DBCW.Rs1.GetDecimal(6); //"Stage II Spec Wt.";
                        xlWorkSheet.Cells[i, 16] = DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(7); //"Stage II Actual Wt.";
                        xlWorkSheet.Cells[i, 17] = Math.Round(((DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(7)) / (DBCW.Rs1.GetDecimal(4) - DBCW.Rs1.GetDecimal(6)) - 1) * 100, 2) + "%"; //"Stage II Wt. Deviation";
                    }
                }

                chartRangeCatalog = xlWorkSheet.get_Range("C" + (i - 1).ToString(), "C" + (i - 1).ToString());
                chartRangeDT = xlWorkSheet.get_Range("F" + (i - 1).ToString(), "F" + (i - 1).ToString());
                chartRangeEmp = xlWorkSheet.get_Range("G" + (i - 1).ToString(), "G" + (i - 1).ToString());
                chartRangeMin = xlWorkSheet.get_Range("R" + i.ToString(), "R" + i.ToString());

                //נופל משום מה
                //if (chartRangeEmp.Cells.Value.ToString().Trim() == DBCW.Rs1.GetString(2).Trim().Substring(2, 5) && chartRangeCatalog.Cells.Value.ToString().Trim() == DBCW.Rs1.GetString(3).Trim())
                //{
                //    TotalMinutes = int.Parse(Math.Round((DBCW.Rs1.GetDateTime(1) - Convert.ToDateTime(chartRangeDT.Cells.Value.ToString())).TotalMinutes, 0).ToString());
                //    xlWorkSheet.Cells[i, 18] = TotalMinutes; //"Building Time [min]";                                        
                //    if (TotalMinutes >= 60)
                //    {
                //        chartRangeMin.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                //    }
                //}
                //else
                //    xlWorkSheet.Cells[i, 18] = "---";

                //xlWorkSheet.Cells[i, 19] = ""; //"Malfunction";
                //if (DBCW.Rs1.GetDecimal(9) > 0)
                //    xlWorkSheet.Cells[i, 20] = DBCW.Rs1.GetDecimal(9); //"ID No. Of Rotations";
                //if (DBCW.Rs1.GetDecimal(10) > 0)
                //    xlWorkSheet.Cells[i, 21] = Math.Round(DBCW.Rs1.GetDecimal(10), 2); // "Actual No. Of Rotations";

                //if (!DBCW.Rs1.IsDBNull(16))
                //    xlWorkSheet.Cells[i, 23] = DBCW.Rs1.GetString(16).Trim().Substring(DBCW.Rs1.GetString(16).Trim().Length - 9, 9); //"Tire Spec";

                // המשפט בנוי בצורה כזו
                // ID=122;Spec=8P-777-00;OrigTS=-24;TS=-24
                ID_Position = DBCW.Rs1.GetString(11).Trim().IndexOf("ID=");
                TireSpecPosition = DBCW.Rs1.GetString(11).Trim().IndexOf(";Spec=");
                ORIGTSPosition = DBCW.Rs1.GetString(11).Trim().IndexOf(";OrigTS=");
                ACTUALTSPosition = DBCW.Rs1.GetString(11).Trim().IndexOf(";TS=");
                if (ID_Position >= 0)
                {
                    xlWorkSheet.Cells[i, 22] = DBCW.Rs1.GetString(11).Trim().Substring(ID_Position + 3, TireSpecPosition - (ID_Position + 3)); //"ID";                     
                    xlWorkSheet.Cells[i, 24] = DBCW.Rs1.GetString(11).Trim().Substring(ORIGTSPosition + 8, ACTUALTSPosition - (ORIGTSPosition + 8)); //"ID ToStart";
                    xlWorkSheet.Cells[i, 25] = DBCW.Rs1.GetString(11).Trim().Substring(ACTUALTSPosition + 4, DBCW.Rs1.GetString(11).Trim().Length - (ACTUALTSPosition + 4)); //"Actual ToStart";                                         
                }
                //if (DBCW.Rs1.GetString(8).Trim() == "S1-50" || DBCW.Rs1.GetString(8).Trim() == "S1-51" ||
                //    DBCW.Rs1.GetString(8).Trim() == "S2-52" || DBCW.Rs1.GetString(8).Trim() == "S2-53" ||
                //    DBCW.Rs1.GetString(8).Trim() == "S4-54" || DBCW.Rs1.GetString(8).Trim() == "S4-55" ||
                //    DBCW.Rs1.GetString(8).Trim() == "T-4")
                //{
                if (DBCW.Rs1.GetString(8).Trim() == "50" || DBCW.Rs1.GetString(8).Trim() == "51" ||
                    DBCW.Rs1.GetString(8).Trim() == "52" || DBCW.Rs1.GetString(8).Trim() == "53" ||
                    DBCW.Rs1.GetString(8).Trim() == "54" || DBCW.Rs1.GetString(8).Trim() == "55" ||
                    DBCW.Rs1.GetString(8).Trim() == "04")
                {
                    xlWorkSheet.Cells[i, 26] = DBCW.Rs1.GetString(14).Trim(); //"Spec Carcas Perimeter"; 
                    //if (DBCW.Rs1.GetString(8).Trim() == "S4-54" || DBCW.Rs1.GetString(8).Trim() == "S4-55") // במכונה זו טעינת היקף קרקס מתבצעת ע"י תוכנה איטלקית                        
                    if (DBCW.Rs1.GetString(8).Trim() == "54" || DBCW.Rs1.GetString(8).Trim() == "55") // במכונה זו טעינת היקף קרקס מתבצעת ע"י תוכנה איטלקית                        
                        xlWorkSheet.Cells[i, 27] = Math.Round(Convert.ToDouble(DBCW.Rs1.GetString(13).Trim()) * 3.14 / 1000, 2); //"Actual Carcas Perimeter";                         
                    //else if (DBCW.Rs1.GetString(8).Trim() == "T-4")
                    else if (DBCW.Rs1.GetString(8).Trim() == "04")
                        xlWorkSheet.Cells[i, 27] = Math.Round(Convert.ToDouble(DBCW.Rs1.GetString(13).Trim()) / 1000, 2); //"Actual Carcas Perimeter"; 
                    else
                        xlWorkSheet.Cells[i, 27] = Math.Round(Convert.ToDouble(DBCW.Rs1.GetString(13).Trim()), 2); //"Actual Carcas Perimeter"; 
                }
                i++;

            }
            i = 2;
            xlWorkSheet2.Cells[1, 12] = "Weight diviations reprort";
            xlWorkSheet2.Cells[i, 14] = "Carcas";
            xlWorkSheet2.Cells[i, 15] = "Stage 2";
            xlWorkSheet2.Cells[i, 16] = "Green";
            xlWorkSheet2.Cells[++i, 12] = "Quant";
            xlWorkSheet2.Cells[i, 13] = "Above 3%: ";
            xlWorkSheet2.Cells[i, 14] = carcaspositivediv;
            xlWorkSheet2.Cells[i, 15] = stage2positivediv;
            xlWorkSheet2.Cells[i, 16] = greenpositivediv;
            xlWorkSheet2.Cells[++i, 12] = "Quant";
            xlWorkSheet2.Cells[i, 13] = "Below 3%: ";
            xlWorkSheet2.Cells[i, 14] = carcasnegetivediv;
            xlWorkSheet2.Cells[i, 15] = stage2negativediv;
            xlWorkSheet2.Cells[i, 16] = greennegativediv;
            xlWorkSheet2.Cells[++i, 12] = "Total Weight lost: ";
            xlWorkSheet2.Cells[i, 14] = carcaslbdiv +" lb";
            xlWorkSheet2.Cells[i, 15] = stage2lbdiv * -1 + " lb";
            xlWorkSheet2.Cells[i, 16] = greenlbdiv + " lb";
            xlWorkSheet2.Cells[++i, 12] = "% Above 0: ";
            if (carcasrecord > 0)
            {
                xlWorkSheet2.Cells[i, 14] = decimal.Round((decimal)carcaspositivediv / (decimal)carcasrecord, 2) * 100 + "%";
            }
            if (greenrecord > 0)
            {
                xlWorkSheet2.Cells[i, 16] = decimal.Round((decimal)greenpositivediv / (decimal)greenrecord, 2) * 100 + "%";
            }
            if (stage2record > 0)
            {
                xlWorkSheet2.Cells[i, 15] = decimal.Round((decimal)stage2positivediv/(decimal)stage2record, 2)*100 + "%";
            } 
            xlWorkSheet2.Cells[++i, 12] = "Total % Above 0: ";
            xlWorkSheet2.Cells[i, 14] = decimal.Round(abovezero / (decimal)RecordNo, 2) * 100 + "%";

            
            chartRangeMin1 = xlWorkSheet2.get_Range("L2", "P" + i.ToString());
            for (int p = 0; p < i; p++)
            {
                chartRangeMin1.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
            }
            chartRangeMin1.ColumnWidth = 14;
            //xlWorkSheet.Visible = Excel.XlSheetVisibility.xlSheetHidden;
            DBCW.Rs1.Close();
            DBCW.Close_Conn();

            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void DTPicker_FromW_ValueChanged(object sender, EventArgs e)
        {

        }

        private void DTPicker_ToW_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
