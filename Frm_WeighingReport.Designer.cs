﻿namespace Gipur_Production
{
    partial class Frm_WeighingReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_WeighingReport));
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_Reset = new System.Windows.Forms.Button();
            this.DTPicker_ToW = new System.Windows.Forms.DateTimePicker();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.DTPicker_FromW = new System.Windows.Forms.DateTimePicker();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboB_Machine = new System.Windows.Forms.ComboBox();
            this.comboB_Size = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboB_Emp = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.comboB_ID = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.checkB_Carcass = new System.Windows.Forms.CheckBox();
            this.checkB_Green = new System.Windows.Forms.CheckBox();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.SlateGray;
            this.label1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(15, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 24);
            this.label1.TabIndex = 48;
            this.label1.Text = "Machine";
            // 
            // Btn_Reset
            // 
            this.Btn_Reset.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_Reset.Location = new System.Drawing.Point(225, 328);
            this.Btn_Reset.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_Reset.Name = "Btn_Reset";
            this.Btn_Reset.Size = new System.Drawing.Size(100, 28);
            this.Btn_Reset.TabIndex = 10;
            this.Btn_Reset.Text = "Reset";
            this.Btn_Reset.UseVisualStyleBackColor = true;
            this.Btn_Reset.Click += new System.EventHandler(this.Btn_Reset_Click);
            // 
            // DTPicker_ToW
            // 
            this.DTPicker_ToW.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_ToW.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_ToW.Location = new System.Drawing.Point(269, 128);
            this.DTPicker_ToW.Name = "DTPicker_ToW";
            this.DTPicker_ToW.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_ToW.TabIndex = 4;
            this.DTPicker_ToW.Leave += new System.EventHandler(this.DTPicker_To_Leave);
            // 
            // Lbl_To
            // 
            this.Lbl_To.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_To.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(231, 130);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.Size = new System.Drawing.Size(73, 24);
            this.Lbl_To.TabIndex = 45;
            this.Lbl_To.Text = "To";
            // 
            // DTPicker_FromW
            // 
            this.DTPicker_FromW.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_FromW.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_FromW.Location = new System.Drawing.Point(77, 128);
            this.DTPicker_FromW.Name = "DTPicker_FromW";
            this.DTPicker_FromW.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_FromW.TabIndex = 3;
            // 
            // Lbl_From
            // 
            this.Lbl_From.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_From.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(15, 129);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.Size = new System.Drawing.Size(73, 24);
            this.Lbl_From.TabIndex = 40;
            this.Lbl_From.Text = "From";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(102, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 29);
            this.label2.TabIndex = 39;
            this.label2.Text = "Weighing Report";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboB_Machine
            // 
            this.comboB_Machine.DropDownHeight = 150;
            this.comboB_Machine.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_Machine.FormattingEnabled = true;
            this.comboB_Machine.IntegralHeight = false;
            this.comboB_Machine.Location = new System.Drawing.Point(109, 179);
            this.comboB_Machine.Name = "comboB_Machine";
            this.comboB_Machine.Size = new System.Drawing.Size(216, 26);
            this.comboB_Machine.TabIndex = 5;           
            this.comboB_Machine.Leave += new System.EventHandler(this.comboB_Machine_Leave);
            // 
            // comboB_Size
            // 
            this.comboB_Size.DropDownHeight = 150;
            this.comboB_Size.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_Size.FormattingEnabled = true;
            this.comboB_Size.IntegralHeight = false;
            this.comboB_Size.Location = new System.Drawing.Point(109, 214);
            this.comboB_Size.Name = "comboB_Size";
            this.comboB_Size.Size = new System.Drawing.Size(216, 26);
            this.comboB_Size.TabIndex = 6;
            this.comboB_Size.Leave += new System.EventHandler(this.comboB_Size_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.SlateGray;
            this.label3.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(15, 214);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 24);
            this.label3.TabIndex = 50;
            this.label3.Text = "Catalog";
            // 
            // comboB_Emp
            // 
            this.comboB_Emp.DropDownHeight = 150;
            this.comboB_Emp.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_Emp.FormattingEnabled = true;
            this.comboB_Emp.IntegralHeight = false;
            this.comboB_Emp.Location = new System.Drawing.Point(109, 250);
            this.comboB_Emp.Name = "comboB_Emp";
            this.comboB_Emp.Size = new System.Drawing.Size(216, 26);
            this.comboB_Emp.TabIndex = 7;
            this.comboB_Emp.Leave += new System.EventHandler(this.comboB_Emp_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.SlateGray;
            this.label4.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(15, 250);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 24);
            this.label4.TabIndex = 52;
            this.label4.Text = "Emp";
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(107, 328);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(100, 28);
            this.Btn_OK.TabIndex = 9;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // comboB_ID
            // 
            this.comboB_ID.DropDownHeight = 150;
            this.comboB_ID.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_ID.FormattingEnabled = true;
            this.comboB_ID.IntegralHeight = false;
            this.comboB_ID.Location = new System.Drawing.Point(109, 286);
            this.comboB_ID.Name = "comboB_ID";
            this.comboB_ID.Size = new System.Drawing.Size(216, 26);
            this.comboB_ID.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.SlateGray;
            this.label5.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(17, 286);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 24);
            this.label5.TabIndex = 54;
            this.label5.Text = "ID";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 381);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(414, 22);
            this.statusStrip1.TabIndex = 55;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(81, 17);
            this.toolStripStatusLabel1.Text = "     0 Records";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.BackColor = System.Drawing.Color.Lime;
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(280, 16);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(42, 14);
            this.toolStripStatusLabel2.Text = "       0";
            // 
            // checkB_Carcass
            // 
            this.checkB_Carcass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkB_Carcass.BackColor = System.Drawing.Color.PowderBlue;
            this.checkB_Carcass.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.checkB_Carcass.Location = new System.Drawing.Point(19, 62);
            this.checkB_Carcass.Name = "checkB_Carcass";
            this.checkB_Carcass.Size = new System.Drawing.Size(95, 22);
            this.checkB_Carcass.TabIndex = 1;
            this.checkB_Carcass.Text = "Carcass";
            this.checkB_Carcass.UseVisualStyleBackColor = false;
            // 
            // checkB_Green
            // 
            this.checkB_Green.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkB_Green.BackColor = System.Drawing.Color.PowderBlue;
            this.checkB_Green.Checked = true;
            this.checkB_Green.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkB_Green.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.checkB_Green.Location = new System.Drawing.Point(19, 90);
            this.checkB_Green.Name = "checkB_Green";
            this.checkB_Green.Size = new System.Drawing.Size(95, 22);
            this.checkB_Green.TabIndex = 2;
            this.checkB_Green.Text = "Green";
            this.checkB_Green.UseVisualStyleBackColor = false;
            // 
            // Frm_WeighingReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(414, 403);
            this.Controls.Add(this.checkB_Carcass);
            this.Controls.Add(this.checkB_Green);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.comboB_ID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.comboB_Emp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboB_Size);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboB_Machine);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Btn_Reset);
            this.Controls.Add(this.DTPicker_ToW);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DTPicker_FromW);
            this.Controls.Add(this.Lbl_From);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Frm_WeighingReport";
            this.Text = "Weighing Report";
            this.Load += new System.EventHandler(this.Frm_WeighingReport_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_Reset;
        private System.Windows.Forms.DateTimePicker DTPicker_ToW;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.DateTimePicker DTPicker_FromW;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboB_Machine;
        private System.Windows.Forms.ComboBox comboB_Size;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboB_Emp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.ComboBox comboB_ID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.CheckBox checkB_Carcass;
        private System.Windows.Forms.CheckBox checkB_Green;
    }
}