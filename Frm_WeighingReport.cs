﻿// תוספת מתאריך 16/12/2015
// תוספות + C# ל  VB6 המרת דוח שקילה מ 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Gipur_Production
{
    public partial class Frm_WeighingReport : Form
    {
        public Frm_WeighingReport()
        {
            InitializeComponent();
        }

        //  Click

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (DTPicker_FromW.Value.Date > DTPicker_ToW.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_FromW.Focus();
                return;
            }
            WeighingReport();
        }

        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            checkB_Carcass.Checked = false;
            checkB_Green.Checked = false;            
            DTPicker_FromW.Value = DateTime.Now.Date;
            DTPicker_ToW.Value = DateTime.Now.Date;
            PrepareFilterDatasBetweenDates();           
        }        

        //  Leave        

        private void comboB_Machine_Leave(object sender, EventArgs e)
        {

            if (comboB_Machine.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";

                comboB_Size.Text = "";
                comboB_Size.Items.Clear();
                comboB_Emp.Text = "";
                comboB_Emp.Items.Clear();
                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);
                // שליפת רשימת פריטים לתקופה מבוקשת
                StrSql = "select distinct PRODUCT from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";                                                
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                else if (checkB_Carcass.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                else if (checkB_Green.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by PRODUCT";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_Size.Items.Add(DBC.Rs1.GetString(0).Trim()); // Catalog
                }
                DBC.Rs1.Close();
                // שליפת רשימת עובדים לתקופה מבוקשת
                StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                         "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +   
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";                
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                else if (checkB_Carcass.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                else if (checkB_Green.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by EMPLOYEE";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                    else
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
                }
                DBC.Rs1.Close();
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and ";
                StrSql += "substring(DATA12,1,3)='ID=' and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                else if (checkB_Carcass.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                else if (checkB_Green.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        private void comboB_Size_Leave(object sender, EventArgs e)
        {
            if (comboB_Size.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";
                
                comboB_Emp.Text = "";
                comboB_Emp.Items.Clear();
                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);                
                // שליפת רשימת עובדים לתקופה מבוקשת
                StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                         "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +   
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
                
                if (comboB_Machine.Text.Trim() != "")
                    StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";                                

                StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                else if (checkB_Carcass.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                else if (checkB_Green.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by EMPLOYEE";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                    else
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
                }
                DBC.Rs1.Close();
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                               "substring(DATA12,1,3)='ID=' ";

                if (comboB_Machine.Text.Trim() != "")
                    StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
                
                StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                else if (checkB_Carcass.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                else if (checkB_Green.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        private void comboB_Emp_Leave(object sender, EventArgs e)
        {
            if (comboB_Emp.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";
                
                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);                
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                               "substring(DATA12,1,3)='ID=' ";

                if (comboB_Machine.Text.Trim() != "")
                    StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
                if (comboB_Size.Text.Trim() != "")
                    StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' ";                

                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Substring(0,5) + "0' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                else if (checkB_Carcass.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                else if (checkB_Green.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        private void DTPicker_To_Leave(object sender, EventArgs e)
        {
            if (DTPicker_FromW.Value.Date > DTPicker_ToW.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_FromW.Focus();
                return;
            }
            PrepareFilterDatasBetweenDates();
        }

        //  Load

        private void Frm_WeighingReport_Load(object sender, EventArgs e)
        {
            checkB_Carcass.Checked = false;
            checkB_Green.Checked = false;            

            PrepareFilterDatasBetweenDates();            
        }

        // Routines

        private void PrepareFilterDatasBetweenDates()
        {
            Cursor.Current = Cursors.WaitCursor;

            string Con_Str;
            string StrSql = "";

            comboB_Machine.Text = "";
            comboB_Machine.Items.Clear();
            comboB_Size.Text = "";
            comboB_Size.Items.Clear();
            comboB_Emp.Text = "";
            comboB_Emp.Items.Clear();
            comboB_ID.Text = "";
            comboB_ID.Items.Clear();

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBC;
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);
            // שליפת רשימת מכונת לתקופה מבוקשת
            StrSql = "select distinct MACHINE from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            
            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            if (checkB_Carcass.Checked && checkB_Green.Checked){}  
            else if (!checkB_Carcass.Checked && !checkB_Green.Checked){}
            else if (checkB_Carcass.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            else if (checkB_Green.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by MACHINE";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_Machine.Items.Add(DBC.Rs1.GetString(0).Trim()); // Machine
            }
            DBC.Rs1.Close();
            // שליפת רשימת פריטים לתקופה מבוקשת
            StrSql = "select distinct PRODUCT from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            
            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            else if (checkB_Carcass.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            else if (checkB_Green.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-0' ";         
            
            StrSql +="order by PRODUCT";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_Size.Items.Add(DBC.Rs1.GetString(0).Trim()); // Catalog
            }
            DBC.Rs1.Close();
            // שליפת רשימת עובדים לתקופה מבוקשת
            StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                     "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            else if (checkB_Carcass.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            else if (checkB_Green.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by EMPLOYEE";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                    comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                else
                    comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
            }
            DBC.Rs1.Close();
            // ליפוף ID שליפת רשימת             
            StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                           "substring(DATA12,1,3)='ID=' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            if (checkB_Carcass.Checked && checkB_Green.Checked){}  
            else if (!checkB_Carcass.Checked && !checkB_Green.Checked){}
            else if (checkB_Carcass.Checked)
                    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            else if (checkB_Green.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            Cursor.Current = Cursors.PanEast;
        }

        private void WeighingReport()
        {
            Cursor.Current = Cursors.WaitCursor;

            string Con_Str;
            string StrSql = "";
            int i = 2;
            int ID_Position = 0;
            int TireSpecPosition = 0;
            int ORIGTSPosition = 0;
            int ACTUALTSPosition = 0;
            Int64 RecordNo = 0;
            int TotalMinutes = 0;
            decimal RecordPr = 0;
            
            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBCW;
            DBCW = new DBConn();
            DBCW.Initialize_Conn(Con_Str);

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet; // xlWorkSheet2;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;
            Excel.Range chartRangeEmp;
            Excel.Range chartRangeCatalog;
            Excel.Range chartRangeDT;            
            Excel.Range chartRangeMin;

            xlApp = new Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "Weighing Report";

            xlWorkSheet.Cells[1, 1] = "Machine";
            xlWorkSheet.Cells[1, 2] = "Size";
            xlWorkSheet.Cells[1, 3] = "Catalog";
            xlWorkSheet.Cells[1, 4] = "Serial";
            xlWorkSheet.Cells[1, 5] = "DNA"; //"ID (Small Label)";
            xlWorkSheet.Cells[1, 6] = "Date";
            xlWorkSheet.Cells[1, 7] = "Emp. No.";
            xlWorkSheet.Cells[1, 8] = "Emp. Name";
            xlWorkSheet.Cells[1, 9] = "Spec Tire Wt.";
            xlWorkSheet.Cells[1, 10] = "Actual Tire Wt.";
            xlWorkSheet.Cells[1, 11] = "Tire Wt. Deviation";
            xlWorkSheet.Cells[1, 12] = "Spec Carcas Wt.";
            xlWorkSheet.Cells[1, 13] = "Actual Carcas Wt.";
            xlWorkSheet.Cells[1, 14] = "Carcas Wt. Deviation";
            xlWorkSheet.Cells[1, 15] = "Stage II Spec Wt.";
            xlWorkSheet.Cells[1, 16] = "Stage II Actual Wt.";
            xlWorkSheet.Cells[1, 17] = "Stage II Wt. Deviation";
            xlWorkSheet.Cells[1, 18] = "Building Time [min]";
            xlWorkSheet.Cells[1, 19] = "Malfunction";
            xlWorkSheet.Cells[1, 20] = "ID No. Of Rotations";
            xlWorkSheet.Cells[1, 21] = "Actual No. Of Rotations";
            xlWorkSheet.Cells[1, 22] = "ID";
            xlWorkSheet.Cells[1, 23] = "Tire Spec";
            xlWorkSheet.Cells[1, 24] = "ID ToStart";
            xlWorkSheet.Cells[1, 25] = "Actual ToStart";
            xlWorkSheet.Cells[1, 26] = "Spec Carcas Perimeter";
            xlWorkSheet.Cells[1, 27] = "Actual Carcas Perimeter";

            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "AA1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // Freeze rows            
            Excel.Window xlWnd3 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A1", "A1").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd3.FreezePanes = true;

            // AutoFilter            
            chartRange = xlWorkSheet.get_Range("A1", "AA1");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // Wrap Text
            chartRange = xlWorkSheet.get_Range("A1", "AA1");
            chartRange.Cells.WrapText = true;

            // הגדרת רוחב עמודה של תחום            
            chartRange = xlWorkSheet.get_Range("A1", "AA1");
            chartRange.ColumnWidth = 10;
            chartRange = xlWorkSheet.get_Range("B1", "B1");
            chartRange.ColumnWidth = 25;
            chartRange = xlWorkSheet.get_Range("C1", "C1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("D1", "D1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("E1", "E1");
            chartRange.ColumnWidth = 8;
            chartRange = xlWorkSheet.get_Range("F1", "F1");
            chartRange.ColumnWidth = 15;
            chartRange = xlWorkSheet.get_Range("H1", "H1");
            chartRange.ColumnWidth = 15;

            // Column Format
            //chartRange = xlWorkSheet.get_Range("E2", "E10000");
            //chartRange.NumberFormat = "@";
            chartRange = xlWorkSheet.get_Range("F2", "F10000");
            chartRange.NumberFormat = "dd/MM/yyyy HH:mm";            

            // שליפת מס' רשומות
            StrSql = "select count(*) from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0,5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            else if (checkB_Carcass.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            else if (checkB_Green.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-0'";
            
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {                
                RecordNo = DBCW.Rs1.GetInt64(0);
            }
            DBCW.Rs1.Close();
            toolStripStatusLabel1.Text = RecordNo.ToString() + " Records";
            toolStripProgressBar1.Value = 0;
            RecordPr += RecordNo / 100; // חישוב כמה זה אחוז 1 מהכמות

            StrSql = "select DESC,PDATE,EMPLOYEE,PRODUCT,DATA1,DATA2,DATA3,DATA4,MACHINE,DATA5,DATA6,DATA12,PRODUCTID,DATA10,DATA9,LLBLNA,LSPEC,value(PRATI,'*'),value(FAMILY,'*') " +
                     "from STWIND.PRRP left join " +
                          "TAPIALI.LABELP on PRODUCT=LPROD and PRODUCTID=LLBLNO left join " +
                          "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and PRODUCT='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            else if (checkB_Carcass.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            else if (checkB_Green.Checked)
                StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by MACHINE,PDATE";

            //StrSql = "select DESC,PDATE,EMPLOYEE,PRODUCT,DATA1,DATA2,DATA3,DATA4,MACHINE,DATA5,DATA6,DATA12,PRODUCTID,DATA10,DATA9,LLBLNA,LSPEC,value(PRATI,'*'),value(FAMILY,'*') " +
            //         "from STWIND.PRRP left join TAPIALI.LABELP on PRODUCT=LPROD and PRODUCTID=LLBLNO left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
            //         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and MACHINE in ('04','12','13','16','24','50','51','52','53','54','55','73','75') " +
            //  "Union select DESC,PDATE,EMPLOYEE,PRODUCT,DATA1,DATA2,DATA3,DATA4,MACHINE,DATA5,DATA6,DATA12,PRODUCTID,DATA10,DATA9,LLBLNA,LSPEC,value(PRATI,'*'),value(FAMILY,'*') " +
            //         "from bpcsfgib.PRRP left join TAPIALI.LABELP on PRODUCT=LPROD and PRODUCTID=LLBLNO left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
            //         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and MACHINE in ('04','12','13','16','24','50','51','52','53','54','55','73','75') " +
            //         "order by MACHINE,PDATE";

            //// שליפת צמיגי מישלן
            //StrSql = "select DESC,PDATE,EMPLOYEE,PRODUCT,DATA1,DATA2,DATA3,DATA4,MACHINE,DATA5,DATA6,DATA12,PRODUCTID,DATA10,DATA9,LLBLNA,LSPEC,value(PRATI,'*'),value(FAMILY,'*') from STWIND.PRRP left join TAPIALI.LABELP on PRODUCT=LPROD and PRODUCTID=LLBLNO left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' where PDATE between '2015-12-31-23.30.00.000000' and '2017-07-31-23.29.59.999999' and substring(PRODUCT,9,2)= '-0' and substring(PRODUCT,1,3) in ('771','772','781','791') " +
            //        "union all " +
            //        "select DESC,PDATE,EMPLOYEE,PRODUCT,DATA1,DATA2,DATA3,DATA4,MACHINE,DATA5,DATA6,DATA12,PRODUCTID,DATA10,DATA9,LLBLNA,LSPEC,value(PRATI,'*'),value(FAMILY,'*') from BPCSFGIB.PRRP left join TAPIALI.LABELP on PRODUCT=LPROD and PRODUCTID=LLBLNO left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' where PDATE between '2015-12-31-23.30.00.000000' and '2017-07-31-23.29.59.999999' and substring(PRODUCT,9,2)= '-0' and substring(PRODUCT,1,3) in ('771','772','781','791') order by MACHINE,PDATE ";
                   
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet.Cells[i, 1] = DBCW.Rs1.GetString(8).Trim(); //"Machine";
                xlWorkSheet.Cells[i, 2] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet.Cells[i, 3] = DBCW.Rs1.GetString(3).Trim(); //"Catalog";
                xlWorkSheet.Cells[i, 4] = DBCW.Rs1.GetString(12).Trim(); //"Serial";
                xlWorkSheet.Cells[i, 5] = DBCW.Rs1.GetValue(15).ToString(); //"ID (Small Label)";
                xlWorkSheet.Cells[i, 6] = DBCW.Rs1.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss"); //"Date";
                xlWorkSheet.Cells[i, 7] = DBCW.Rs1.GetString(2).Trim().Substring(2, 5); //"Emp. No.";
                if (!DBCW.Rs1.IsDBNull(17) && !DBCW.Rs1.IsDBNull(18))
                    xlWorkSheet.Cells[i, 8] = DBCW.Rs1.GetString(17).Trim() + " " + DBCW.Rs1.GetString(18).Trim(); //"Emp. Name";                                          -------------------------------
                xlWorkSheet.Cells[i, 9] = DBCW.Rs1.GetDecimal(4); //"Spec Tire Wt.";                                
                if (DBCW.Rs1.GetDecimal(5) != 0)
                {
                    xlWorkSheet.Cells[i, 10] = DBCW.Rs1.GetDecimal(5); //"Actual Tire Wt.";
                    xlWorkSheet.Cells[i, 11] = Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) + "%"; //"Tire Wt. Deviation";                
                }

                if (DBCW.Rs1.GetDecimal(6) > 0)
                {
                    xlWorkSheet.Cells[i, 12] = DBCW.Rs1.GetDecimal(6); //"Spec Carcas Wt.";
                    xlWorkSheet.Cells[i, 13] = DBCW.Rs1.GetDecimal(7); //"Actual Carcas Wt.";
                    xlWorkSheet.Cells[i, 14] = Math.Round((DBCW.Rs1.GetDecimal(7) / DBCW.Rs1.GetDecimal(6) - 1) * 100, 2) + "%"; //"Carcas Wt. Deviation";
                    if (DBCW.Rs1.GetDecimal(5) != 0)
                    {
                        xlWorkSheet.Cells[i, 15] = DBCW.Rs1.GetDecimal(4) - DBCW.Rs1.GetDecimal(6); //"Stage II Spec Wt.";
                        xlWorkSheet.Cells[i, 16] = DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(7); //"Stage II Actual Wt.";
                        xlWorkSheet.Cells[i, 17] = Math.Round(((DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(7)) / (DBCW.Rs1.GetDecimal(4) - DBCW.Rs1.GetDecimal(6)) - 1) * 100, 2) + "%"; //"Stage II Wt. Deviation";
                    }
                }

                chartRangeCatalog = xlWorkSheet.get_Range("C" + (i-1).ToString(), "C" + (i-1).ToString());                
                chartRangeDT = xlWorkSheet.get_Range("F" + (i - 1).ToString(), "F" + (i - 1).ToString());
                chartRangeEmp = xlWorkSheet.get_Range("G" + (i - 1).ToString(), "G" + (i - 1).ToString());
                chartRangeMin = xlWorkSheet.get_Range("R" + i.ToString(), "R" + i.ToString());
                

                if (chartRangeEmp.Cells.Value.ToString().Trim() == DBCW.Rs1.GetString(2).Trim().Substring(2, 5) && chartRangeCatalog.Cells.Value.ToString().Trim() == DBCW.Rs1.GetString(3).Trim())
                {                  
                    TotalMinutes = int.Parse(Math.Round((DBCW.Rs1.GetDateTime(1) - Convert.ToDateTime(chartRangeDT.Cells.Value.ToString())).TotalMinutes, 0).ToString());
                    xlWorkSheet.Cells[i, 18] = TotalMinutes; //"Building Time [min]";                                        
                    if (TotalMinutes >= 60)
                    {                        
                        chartRangeMin.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);                        
                    }
                }
                else
                    xlWorkSheet.Cells[i, 18] = "---";

                xlWorkSheet.Cells[i, 19] = ""; //"Malfunction";
                if (DBCW.Rs1.GetDecimal(9) > 0)
                    xlWorkSheet.Cells[i, 20] = DBCW.Rs1.GetDecimal(9); //"ID No. Of Rotations";
                if (DBCW.Rs1.GetDecimal(10) > 0)
                    xlWorkSheet.Cells[i, 21] = Math.Round(DBCW.Rs1.GetDecimal(10), 2); // "Actual No. Of Rotations";

                if (!DBCW.Rs1.IsDBNull(16))
                    xlWorkSheet.Cells[i, 23] = DBCW.Rs1.GetString(16).Trim().Substring(DBCW.Rs1.GetString(16).Trim().Length - 9, 9); //"Tire Spec";

                // המשפט בנוי בצורה כזו
                // ID=122;Spec=8P-777-00;OrigTS=-24;TS=-24
                ID_Position = DBCW.Rs1.GetString(11).Trim().IndexOf("ID=");
                TireSpecPosition = DBCW.Rs1.GetString(11).Trim().IndexOf(";Spec=");
                ORIGTSPosition = DBCW.Rs1.GetString(11).Trim().IndexOf(";OrigTS=");
                ACTUALTSPosition = DBCW.Rs1.GetString(11).Trim().IndexOf(";TS=");
                if (ID_Position >= 0)
                {
                    xlWorkSheet.Cells[i, 22] = DBCW.Rs1.GetString(11).Trim().Substring(ID_Position + 3, TireSpecPosition - (ID_Position + 3)); //"ID";                     
                    xlWorkSheet.Cells[i, 24] = DBCW.Rs1.GetString(11).Trim().Substring(ORIGTSPosition + 8, ACTUALTSPosition - (ORIGTSPosition + 8)); //"ID ToStart";
                    xlWorkSheet.Cells[i, 25] = DBCW.Rs1.GetString(11).Trim().Substring(ACTUALTSPosition + 4, DBCW.Rs1.GetString(11).Trim().Length - (ACTUALTSPosition + 4)); //"Actual ToStart";                                         
                }
                //if (DBCW.Rs1.GetString(8).Trim() == "S1-50" || DBCW.Rs1.GetString(8).Trim() == "S1-51" ||
                //    DBCW.Rs1.GetString(8).Trim() == "S2-52" || DBCW.Rs1.GetString(8).Trim() == "S2-53" ||
                //    DBCW.Rs1.GetString(8).Trim() == "S4-54" || DBCW.Rs1.GetString(8).Trim() == "S4-55" ||
                //    DBCW.Rs1.GetString(8).Trim() == "T-4")
                //{
                if (DBCW.Rs1.GetString(8).Trim() == "50" || DBCW.Rs1.GetString(8).Trim() == "51" ||
                    DBCW.Rs1.GetString(8).Trim() == "52" || DBCW.Rs1.GetString(8).Trim() == "53" ||
                    DBCW.Rs1.GetString(8).Trim() == "54" || DBCW.Rs1.GetString(8).Trim() == "55" ||
                    DBCW.Rs1.GetString(8).Trim() == "04")
                {
                    xlWorkSheet.Cells[i, 26] = DBCW.Rs1.GetString(14).Trim(); //"Spec Carcas Perimeter"; 
                    //if (DBCW.Rs1.GetString(8).Trim() == "S4-54" || DBCW.Rs1.GetString(8).Trim() == "S4-55") // במכונה זו טעינת היקף קרקס מתבצעת ע"י תוכנה איטלקית                        
                    if (DBCW.Rs1.GetString(8).Trim() == "54" || DBCW.Rs1.GetString(8).Trim() == "55") // במכונה זו טעינת היקף קרקס מתבצעת ע"י תוכנה איטלקית                        
                        xlWorkSheet.Cells[i, 27] = Math.Round(Convert.ToDouble(DBCW.Rs1.GetString(13).Trim()) * 3.14 / 1000, 2); //"Actual Carcas Perimeter";                         
                    //else if (DBCW.Rs1.GetString(8).Trim() == "T-4")
                    else if (DBCW.Rs1.GetString(8).Trim() == "04")
                        xlWorkSheet.Cells[i, 27] = Math.Round(Convert.ToDouble(DBCW.Rs1.GetString(13).Trim()) / 1000, 2); //"Actual Carcas Perimeter"; 
                    else
                        xlWorkSheet.Cells[i, 27] = Math.Round(Convert.ToDouble(DBCW.Rs1.GetString(13).Trim()), 2); //"Actual Carcas Perimeter"; 
                }
                i++;
                toolStripStatusLabel2.Text = i.ToString();
                if (RecordNo>=100)
                    if (i % RecordPr == 0 && toolStripProgressBar1.Value <100)
                        toolStripProgressBar1.Value = toolStripProgressBar1.Value + 1;                                
            }

            xlWorkSheet.Cells[i, 1] = "Total";

            xlWorkSheet.Cells[i, 9] = "=sum(I2:I" + (i - 1).ToString() + ")";
            xlWorkSheet.Cells[i, 10] = "=sum(J2:J" + (i - 1).ToString() + ")";
            xlWorkSheet.Cells[i, 11] = "=round((J" + i.ToString() + "/I" + i.ToString() + "-1)*100,2)";

            xlWorkSheet.Cells[i, 12] = "=sum(L2:L" + (i - 1).ToString() + ")";
            xlWorkSheet.Cells[i, 13] = "=sum(M2:M" + (i - 1).ToString() + ")";
            xlWorkSheet.Cells[i, 14] = "=round((M" + i.ToString() + "/L" + i.ToString() + "-1)*100,2)";

            xlWorkSheet.Cells[i, 15] = "=sum(O2:O" + (i - 1).ToString() + ")";
            xlWorkSheet.Cells[i, 16] = "=sum(P2:P" + (i - 1).ToString() + ")";
            xlWorkSheet.Cells[i, 17] = "=round((P" + i.ToString() + "/O" + i.ToString() + "-1)*100,2)";

            DBCW.Rs1.Close();
            DBCW.Close_Conn();

            xlApp.Visible = true;
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

            toolStripStatusLabel1.Text = "     0 Records";
            toolStripProgressBar1.Value = 0;
            toolStripStatusLabel2.Text = "     0";
            Cursor.Current = Cursors.PanEast;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
