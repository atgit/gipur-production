﻿namespace Gipur_Production
{
    partial class Frm_WeigingAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.comboB_ID = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.comboB_Emp = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboB_Size = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboB_Machine = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_Reset = new System.Windows.Forms.Button();
            this.DTPicker_ToW = new System.Windows.Forms.DateTimePicker();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.DTPicker_FromW = new System.Windows.Forms.DateTimePicker();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(15, -35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(271, 29);
            this.label2.TabIndex = 83;
            this.label2.Text = "Weighing Over Weight";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboB_ID
            // 
            this.comboB_ID.DropDownHeight = 150;
            this.comboB_ID.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_ID.FormattingEnabled = true;
            this.comboB_ID.IntegralHeight = false;
            this.comboB_ID.Location = new System.Drawing.Point(112, 250);
            this.comboB_ID.Name = "comboB_ID";
            this.comboB_ID.Size = new System.Drawing.Size(216, 26);
            this.comboB_ID.TabIndex = 91;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.SlateGray;
            this.label5.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(20, 250);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 24);
            this.label5.TabIndex = 100;
            this.label5.Text = "ID";
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_OK.Location = new System.Drawing.Point(110, 292);
            this.Btn_OK.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(100, 28);
            this.Btn_OK.TabIndex = 92;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // comboB_Emp
            // 
            this.comboB_Emp.DropDownHeight = 150;
            this.comboB_Emp.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_Emp.FormattingEnabled = true;
            this.comboB_Emp.IntegralHeight = false;
            this.comboB_Emp.Location = new System.Drawing.Point(112, 214);
            this.comboB_Emp.Name = "comboB_Emp";
            this.comboB_Emp.Size = new System.Drawing.Size(216, 26);
            this.comboB_Emp.TabIndex = 90;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.SlateGray;
            this.label4.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(18, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 24);
            this.label4.TabIndex = 99;
            this.label4.Text = "Emp";
            // 
            // comboB_Size
            // 
            this.comboB_Size.DropDownHeight = 150;
            this.comboB_Size.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_Size.FormattingEnabled = true;
            this.comboB_Size.IntegralHeight = false;
            this.comboB_Size.Location = new System.Drawing.Point(112, 178);
            this.comboB_Size.Name = "comboB_Size";
            this.comboB_Size.Size = new System.Drawing.Size(216, 26);
            this.comboB_Size.TabIndex = 89;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.SlateGray;
            this.label3.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(18, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 24);
            this.label3.TabIndex = 98;
            this.label3.Text = "Size";
            // 
            // comboB_Machine
            // 
            this.comboB_Machine.DropDownHeight = 150;
            this.comboB_Machine.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboB_Machine.FormattingEnabled = true;
            this.comboB_Machine.IntegralHeight = false;
            this.comboB_Machine.Location = new System.Drawing.Point(112, 143);
            this.comboB_Machine.Name = "comboB_Machine";
            this.comboB_Machine.Size = new System.Drawing.Size(216, 26);
            this.comboB_Machine.TabIndex = 88;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.SlateGray;
            this.label1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(18, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 24);
            this.label1.TabIndex = 97;
            this.label1.Text = "Machine";
            // 
            // Btn_Reset
            // 
            this.Btn_Reset.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Btn_Reset.Location = new System.Drawing.Point(228, 292);
            this.Btn_Reset.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Btn_Reset.Name = "Btn_Reset";
            this.Btn_Reset.Size = new System.Drawing.Size(100, 28);
            this.Btn_Reset.TabIndex = 93;
            this.Btn_Reset.Text = "Reset";
            this.Btn_Reset.UseVisualStyleBackColor = true;
            // 
            // DTPicker_ToW
            // 
            this.DTPicker_ToW.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_ToW.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_ToW.Location = new System.Drawing.Point(272, 92);
            this.DTPicker_ToW.Name = "DTPicker_ToW";
            this.DTPicker_ToW.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_ToW.TabIndex = 87;
            // 
            // Lbl_To
            // 
            this.Lbl_To.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_To.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(234, 94);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.Size = new System.Drawing.Size(73, 24);
            this.Lbl_To.TabIndex = 96;
            this.Lbl_To.Text = "To";
            // 
            // DTPicker_FromW
            // 
            this.DTPicker_FromW.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker_FromW.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPicker_FromW.Location = new System.Drawing.Point(80, 92);
            this.DTPicker_FromW.Name = "DTPicker_FromW";
            this.DTPicker_FromW.Size = new System.Drawing.Size(114, 25);
            this.DTPicker_FromW.TabIndex = 86;
            // 
            // Lbl_From
            // 
            this.Lbl_From.BackColor = System.Drawing.Color.SlateGray;
            this.Lbl_From.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(18, 93);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.Size = new System.Drawing.Size(73, 24);
            this.Lbl_From.TabIndex = 95;
            this.Lbl_From.Text = "From";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.SteelBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.Location = new System.Drawing.Point(80, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(271, 29);
            this.label6.TabIndex = 94;
            this.label6.Text = "Weighing Analysis";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frm_WeigingAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(414, 374);
            this.Controls.Add(this.comboB_ID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.comboB_Emp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboB_Size);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboB_Machine);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Btn_Reset);
            this.Controls.Add(this.DTPicker_ToW);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DTPicker_FromW);
            this.Controls.Add(this.Lbl_From);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Name = "Frm_WeigingAnalysis";
            this.Text = "Frm_WeigingAnalysis";
            this.Load += new System.EventHandler(this.Frm_WeigingAnalysis_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboB_ID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.ComboBox comboB_Emp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboB_Size;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboB_Machine;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_Reset;
        private System.Windows.Forms.DateTimePicker DTPicker_ToW;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.DateTimePicker DTPicker_FromW;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.Label label6;
    }
}