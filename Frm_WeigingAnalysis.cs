﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using Excel = Microsoft.Office.Interop.Excel;
namespace Gipur_Production
{
    public partial class Frm_WeigingAnalysis : Form
    {
        // תוספת מתאריך 14/08/2016
        // הוספת דוח אנליזה של חריגות משקלים 
        // פיתוח של רן
        public Frm_WeigingAnalysis()
        {
            InitializeComponent();
        }
        //  Click
        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (DTPicker_FromW.Value.Date > DTPicker_ToW.Value.Date)
            {
                MessageBox.Show("שגיאה, תאריך התחלה גדול מתאריך סיום", "Curing / Building Production");
                DTPicker_FromW.Focus();
                return;
            }
            OverWeightAnalysis();
        }
        //  Load
        private void Frm_WeigingAnalysis_Load(object sender, EventArgs e)
        {
            //checkB_Carcass.Checked = false;
            //checkB_Green.Checked = false;

            PrepareFilterDatasBetweenDates();
        }

        // Routines
        private void PrepareFilterDatasBetweenDates()
        {
            Cursor.Current = Cursors.WaitCursor;

            string Con_Str;
            string StrSql = "";

            comboB_Machine.Text = "";
            comboB_Machine.Items.Clear();
            comboB_Size.Text = "";
            comboB_Size.Items.Clear();
            comboB_Emp.Text = "";
            comboB_Emp.Items.Clear();
            comboB_ID.Text = "";
            comboB_ID.Items.Clear();

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBC;
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);
            // שליפת רשימת מכונת לתקופה מבוקשת
            StrSql = "select distinct MACHINE from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            //else if (checkB_Carcass.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            //else if (checkB_Green.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by MACHINE";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_Machine.Items.Add(DBC.Rs1.GetString(0).Trim()); // Machine
            }
            DBC.Rs1.Close();
            // שליפת רשימת גדלים לתקופה מבוקשת
            StrSql = "select distinct DESC from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            //else if (checkB_Carcass.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            //else if (checkB_Green.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by DESC";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_Size.Items.Add(DBC.Rs1.GetString(0).Trim()); // Size
            }
            DBC.Rs1.Close();
            // שליפת רשימת עובדים לתקופה מבוקשת
            StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                     "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            //else if (checkB_Carcass.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            //else if (checkB_Green.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by EMPLOYEE";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                    comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                else
                    comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
            }
            DBC.Rs1.Close();
            // ליפוף ID שליפת רשימת             
            StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                           "substring(DATA12,1,3)='ID=' ";

            // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
            //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
            //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
            //else if (checkB_Carcass.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            //else if (checkB_Green.Checked)
            //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

            StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
            DBC.Q_Run1(StrSql);
            while (DBC.Rs1.Read())
            {
                comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
            }
            DBC.Rs1.Close();
            DBC.Close_Conn();
            Cursor.Current = Cursors.PanEast;
        }  

        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            //checkB_Carcass.Checked = false;
            //checkB_Green.Checked = false;
            DTPicker_FromW.Value = DateTime.Now.Date;
            DTPicker_ToW.Value = DateTime.Now.Date;
            PrepareFilterDatasBetweenDates();
        }


        // //  Leave
        private void comboB_Machine_Leave(object sender, EventArgs e)
        {
            if (comboB_Machine.Text.Trim() != "")
            {
                Cursor.Current = Cursors.WaitCursor;

                string Con_Str;
                string StrSql = "";

                comboB_Size.Text = "";
                comboB_Size.Items.Clear();
                comboB_Emp.Text = "";
                comboB_Emp.Items.Clear();
                comboB_ID.Text = "";
                comboB_ID.Items.Clear();

                // AS400 התקשרות ל               
                Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
                DBConn DBC;
                DBC = new DBConn();
                DBC.Initialize_Conn(Con_Str);
                // שליפת רשימת גדלים לתקופה מבוקשת
                StrSql = "select distinct DESC from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                //else if (checkB_Carcass.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                //else if (checkB_Green.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by DESC";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_Size.Items.Add(DBC.Rs1.GetString(0).Trim()); // Catalog
                }
                DBC.Rs1.Close();
                // שליפת רשימת עובדים לתקופה מבוקשת
                StrSql = "select distinct EMPLOYEE,value(PRATI,'*'),value(FAMILY,'*') " +
                         "from STWIND.PRRP left join ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                //else if (checkB_Carcass.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                //else if (checkB_Green.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by EMPLOYEE";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    if (!DBC.Rs1.IsDBNull(1) && !DBC.Rs1.IsDBNull(2))
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5) + "   " + DBC.Rs1.GetString(1).Trim() + " " + DBC.Rs1.GetString(2).Trim()); // Employee
                    else
                        comboB_Emp.Items.Add(DBC.Rs1.GetString(0).Trim().Substring(2, 5)); // כשאין שם עובד
                }
                DBC.Rs1.Close();
                // ליפוף ID שליפת רשימת             
                StrSql = "select distinct substring(DATA12,4,locate(';',DATA12)-4) from STWIND.PRRP " +
                         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and ";
                StrSql += "substring(DATA12,1,3)='ID=' and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

                // אם לא בחרו סוג צמיג או בחרו שניהם זה אותו דבר
                //if (checkB_Carcass.Checked && checkB_Green.Checked) { }
                //else if (!checkB_Carcass.Checked && !checkB_Green.Checked) { }
                //else if (checkB_Carcass.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-1' ";
                //else if (checkB_Green.Checked)
                //    StrSql += "and substring(PRODUCT,9,2)= '-0' ";

                StrSql += "order by substring(DATA12,4,locate(';',DATA12)-4)";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    comboB_ID.Items.Add(DBC.Rs1.GetString(0).Trim()); // ID
                }
                DBC.Rs1.Close();
                DBC.Close_Conn();
                Cursor.Current = Cursors.PanEast;
            }
        }

        private void OverWeightAnalysis()
        {
            string Con_Str;
            string StrSql = "";
            Excel.Range range, range2;
            int i = 2, j = 2, k = 2, p = 0;
            double lb = 0.45359237;
            int pos = 0;
            string [,] tiresum; 

            // AS400 התקשרות ל               
            Con_Str = "Dsn=csharp;uid=csharp;pwd=csharp";
            DBConn DBCW;
            DBCW = new DBConn();
            DBCW.Initialize_Conn(Con_Str);

            //// שליפת רשימת פריטים לתקופה מבוקשת
            //StrSql = "select distinct PRODUCT from STWIND.PRRP " +
            //         "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            //StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel._Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet, xlWorkSheet2, xlWorkSheet3, xlWorkSheet4, xlWorkSheet5;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;
            Excel.Range chartRangeEmp;
            Excel.Range chartRangeCatalog;
            Excel.Range chartRangeDT;
            Excel.Range chartRangeMin;

            xlApp = new Excel.Application();
            //Excel._Application xlApp = new Excel.ApplicationClass();

            xlWorkBook = xlApp.Workbooks.Add(misValue);

            // מחיקת גיליונות אקסל מיותרים
            int WN = 0;
            foreach (Excel.Worksheet xlworksheet in xlWorkBook.Worksheets)
            {
                if (WN != 0) // על מנת לא למחוק את הגיליון הראשון - חייבים להשאיר גיליון 1 
                    xlworksheet.Delete();
                WN++;
            }
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet2 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet3 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet4 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet5 = (Excel.Worksheet)xlApp.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkSheet.Activate();

            xlWorkSheet.Name = " Weights Acc.";
            xlWorkSheet2.Name = "Weight deviation per size";
            xlWorkSheet3.Name = "Weight deviation per TBM";
            xlWorkSheet4.Name = "Weight deviation per employee";
            xlWorkSheet5.Name = "2015 VS. 2016";

            xlWorkSheet.Cells[1, 1] = "Machine";
            xlWorkSheet.Cells[1, 2] = "Size";
            xlWorkSheet.Cells[1, 3] = "Catalog";
            xlWorkSheet.Cells[1, 4] = "Serial";
            xlWorkSheet.Cells[1, 5] = "ID (Small Label)";
            xlWorkSheet.Cells[1, 6] = "Date";
            xlWorkSheet.Cells[1, 7] = "Emp. No.";
            xlWorkSheet.Cells[1, 8] = "Emp. Name";
            xlWorkSheet.Cells[1, 9] = "Spec Tire Wt.";
            xlWorkSheet.Cells[1, 10] = "Actual Tire Wt.";
            xlWorkSheet.Cells[1, 11] = "Tire Wt. Deviation";
            xlWorkSheet.Cells[1, 12] = "Spec Carcas Wt.";
            xlWorkSheet.Cells[1, 13] = "Actual Carcas Wt.";
            xlWorkSheet.Cells[1, 14] = "Carcas Wt. Deviation";
            xlWorkSheet.Cells[1, 15] = "Stage II Spec Wt.";
            xlWorkSheet.Cells[1, 16] = "Stage II Actual Wt.";
            xlWorkSheet.Cells[1, 17] = "Stage II Wt. Deviation";

            xlWorkSheet2.Cells[1, 1] = "Size";
            //xlWorkSheet2.Cells[1, 2] = "Tires Count";
            xlWorkSheet2.Cells[1, 2] = "Weight Dev";
            xlWorkSheet2.Cells[1, 1] = "Size";
            xlWorkSheet2.Cells[1, 2] = "% Dev";
            xlWorkSheet3.Cells[1, 1] = "Machine";
            xlWorkSheet3.Cells[1, 2] = "Weight Wasted";
            xlWorkSheet4.Cells[1, 1] = "Employee";
            xlWorkSheet4.Cells[1, 2] = "Weight Wasted";
            xlWorkSheet4.Cells[1, 3] = "Employee";
            xlWorkSheet4.Cells[1, 4] = "Percentage";
            // מירכוז + מיזוג + פונט מודגש לתחום            
            chartRange = xlWorkSheet.get_Range("A1", "AA1");
            chartRange.HorizontalAlignment = 3;
            chartRange.VerticalAlignment = 3;
            chartRange.Font.Bold = true;

            // AutoFilter            
            chartRange = xlWorkSheet.get_Range("A1", "Q1");
            chartRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            // Wrap Text
            chartRange = xlWorkSheet.get_Range("A1", "AA1");
            chartRange.Cells.WrapText = true;

            //Text font white
            chartRange = xlWorkSheet2.get_Range("A:D");
            chartRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            chartRange = xlWorkSheet3.get_Range("A:D");
            chartRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            chartRange = xlWorkSheet4.get_Range("A:D");
            chartRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            //Diviation range color
            chartRange = xlWorkSheet2.get_Range("A1:CR1000");
            chartRange.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            chartRange = xlWorkSheet2.get_Range("I1:J51");
            chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Beige);
            chartRange.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Beige);
            chartRange = xlWorkSheet3.get_Range("A1:CR1000");
            chartRange.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            chartRange = xlWorkSheet3.get_Range("I1:J51");
            chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Beige);
            chartRange.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Beige);
            chartRange = xlWorkSheet4.get_Range("A1:CR1000");
            chartRange.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            chartRange = xlWorkSheet4.get_Range("I1:J51");
            chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Beige);
            chartRange.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Beige);
            chartRange = xlWorkSheet5.get_Range("A1:CR1000");
            chartRange.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

            StrSql = "select DESC,PDATE,EMPLOYEE,PRODUCT,DATA1,DATA2,DATA3,DATA4,MACHINE,DATA5,DATA6,DATA12,PRODUCTID,DATA10,DATA9,LLBLNA,LSPEC,value(PRATI,'*'),value(FAMILY,'*') " +
                     "from STWIND.PRRP left join " +
                          "TAPIALI.LABELP on PRODUCT=LPROD and PRODUCTID=LLBLNO left join " +
                          "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";
            //שליפת ירוקים 
            StrSql += "and substring(PRODUCT,9,2)= '-0' ";
            DBCW.Q_Run1(StrSql);
            i = 2;
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet.Cells[i, 1] = DBCW.Rs1.GetString(8); //"Machine";
                xlWorkSheet.Cells[i, 2] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet.Cells[i, 3] = DBCW.Rs1.GetString(3).Trim(); //"Catalog";
                xlWorkSheet.Cells[i, 4] = DBCW.Rs1.GetString(12).Trim(); //"Serial";
                xlWorkSheet.Cells[i, 5] = DBCW.Rs1.GetValue(15).ToString(); //"ID (Small Label)";
                xlWorkSheet.Cells[i, 6] = DBCW.Rs1.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss"); //"Date";
                xlWorkSheet.Cells[i, 7] = DBCW.Rs1.GetString(2).Trim().Substring(2, 5); //"Emp. No.";
                if (!DBCW.Rs1.IsDBNull(17) && !DBCW.Rs1.IsDBNull(18))
                    xlWorkSheet.Cells[i, 8] = DBCW.Rs1.GetString(17).Trim() + " " + DBCW.Rs1.GetString(18).Trim(); //"Emp. Name";                                          -------------------------------
                xlWorkSheet.Cells[i, 9] = (double)DBCW.Rs1.GetDecimal(4) * lb; //"Spec Tire Wt.";                                
                if (DBCW.Rs1.GetDecimal(5) != 0)
                {
                    xlWorkSheet.Cells[i, 10] = (double)DBCW.Rs1.GetDecimal(5) * lb; //"Actual Tire Wt.";
                    xlWorkSheet.Cells[i, 11] = Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) + "%"; //"Tire Wt. Deviation";
                }
                if (DBCW.Rs1.GetDecimal(6) > 0)
                {
                    xlWorkSheet.Cells[i, 12] = (double)DBCW.Rs1.GetDecimal(6) * lb; //"Spec Carcas Wt.";
                    xlWorkSheet.Cells[i, 13] = (double)DBCW.Rs1.GetDecimal(7) * lb; //"Actual Carcas Wt.";
                    xlWorkSheet.Cells[i, 14] = Math.Round((DBCW.Rs1.GetDecimal(7) / DBCW.Rs1.GetDecimal(6) - 1) * 100, 2) + "%"; //"Carcas Wt. Deviation";
                    if (DBCW.Rs1.GetDecimal(5) != 0)
                    {
                        xlWorkSheet.Cells[i, 15] = ((double)DBCW.Rs1.GetDecimal(4) * lb) - ((double)DBCW.Rs1.GetDecimal(6) * lb); //"Stage II Spec Wt.";
                        xlWorkSheet.Cells[i, 16] = ((double)DBCW.Rs1.GetDecimal(5) * lb) - ((double)DBCW.Rs1.GetDecimal(7) * lb); //"Stage II Actual Wt.";
                        xlWorkSheet.Cells[i, 17] = Math.Round(((DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(7)) / (DBCW.Rs1.GetDecimal(4) - DBCW.Rs1.GetDecimal(6)) - 1) * 100, 2) + "%"; //"Stage II Wt. Deviation";
                    }
                }


                chartRangeCatalog = xlWorkSheet.get_Range("C" + (i - 1).ToString(), "C" + (i - 1).ToString());
                chartRangeDT = xlWorkSheet.get_Range("F" + (i - 1).ToString(), "F" + (i - 1).ToString());
                chartRangeEmp = xlWorkSheet.get_Range("G" + (i - 1).ToString(), "G" + (i - 1).ToString());
                chartRangeMin = xlWorkSheet.get_Range("R" + i.ToString(), "R" + i.ToString());
                i++;
            }
            // שליפת שאר הצמיגים
            StrSql = "select DESC,PDATE,EMPLOYEE,PRODUCT,DATA1,DATA2,DATA3,DATA4,MACHINE,DATA5,DATA6,DATA12,PRODUCTID,DATA10,DATA9,LLBLNA,LSPEC,value(PRATI,'*'),value(FAMILY,'*') " +
                     "from STWIND.PRRP left join " +
                          "TAPIALI.LABELP on PRODUCT=LPROD and PRODUCTID=LLBLNO left join " +
                          "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
                     "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' ";
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";
            StrSql += "and substring(PRODUCT,9,2)= '-1' ";
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet.Cells[i, 1] = DBCW.Rs1.GetString(8); //"Machine";
                xlWorkSheet.Cells[i, 2] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet.Cells[i, 3] = DBCW.Rs1.GetString(3).Trim(); //"Catalog";
                xlWorkSheet.Cells[i, 4] = DBCW.Rs1.GetString(12).Trim(); //"Serial";
                xlWorkSheet.Cells[i, 5] = DBCW.Rs1.GetValue(15).ToString(); //"ID (Small Label)";
                xlWorkSheet.Cells[i, 6] = DBCW.Rs1.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss"); //"Date";
                xlWorkSheet.Cells[i, 7] = DBCW.Rs1.GetString(2).Trim().Substring(2, 5); //"Emp. No.";
                if (!DBCW.Rs1.IsDBNull(17) && !DBCW.Rs1.IsDBNull(18))
                    xlWorkSheet.Cells[i, 8] = DBCW.Rs1.GetString(17).Trim() + " " + DBCW.Rs1.GetString(18).Trim(); //"Emp. Name";                                          -------------------------------
                xlWorkSheet.Cells[i, 9] = (double)DBCW.Rs1.GetDecimal(4) * lb; //"Spec Tire Wt.";                                
                if (DBCW.Rs1.GetDecimal(5) != 0)
                {
                    xlWorkSheet.Cells[i, 10] = (double)DBCW.Rs1.GetDecimal(5) * lb; //"Actual Tire Wt.";
                    xlWorkSheet.Cells[i, 11] = Math.Round((DBCW.Rs1.GetDecimal(5) / DBCW.Rs1.GetDecimal(4) - 1) * 100, 2) + "%"; //"Tire Wt. Deviation";
                }
                if (DBCW.Rs1.GetDecimal(6) > 0)
                {
                    xlWorkSheet.Cells[i, 12] = (double)DBCW.Rs1.GetDecimal(6) * lb; //"Spec Carcas Wt.";
                    xlWorkSheet.Cells[i, 13] = (double)DBCW.Rs1.GetDecimal(7) * lb; //"Actual Carcas Wt.";
                    xlWorkSheet.Cells[i, 14] = Math.Round((DBCW.Rs1.GetDecimal(7) / DBCW.Rs1.GetDecimal(6) - 1) * 100, 2) + "%"; //"Carcas Wt. Deviation";
                    if (DBCW.Rs1.GetDecimal(5) != 0)
                    {
                        xlWorkSheet.Cells[i, 15] = ((double)DBCW.Rs1.GetDecimal(4) * lb) - ((double)DBCW.Rs1.GetDecimal(6) * lb); //"Stage II Spec Wt.";
                        xlWorkSheet.Cells[i, 16] = ((double)DBCW.Rs1.GetDecimal(5) * lb) - ((double)DBCW.Rs1.GetDecimal(7) * lb); //"Stage II Actual Wt.";
                        xlWorkSheet.Cells[i, 17] = Math.Round(((DBCW.Rs1.GetDecimal(5) - DBCW.Rs1.GetDecimal(7)) / (DBCW.Rs1.GetDecimal(4) - DBCW.Rs1.GetDecimal(6)) - 1) * 100, 2) + "%"; //"Stage II Wt. Deviation";
                    }
                }

                chartRangeCatalog = xlWorkSheet.get_Range("C" + (i - 1).ToString(), "C" + (i - 1).ToString());
                chartRangeDT = xlWorkSheet.get_Range("F" + (i - 1).ToString(), "F" + (i - 1).ToString());
                chartRangeEmp = xlWorkSheet.get_Range("G" + (i - 1).ToString(), "G" + (i - 1).ToString());
                chartRangeMin = xlWorkSheet.get_Range("R" + i.ToString(), "R" + i.ToString());
                i++;
            }

            DBCW.Rs1.Close();

            // שליפה טבלת צמיגים ירוקים חורגים מאותה מידה
            StrSql = "select DESC, count(*),sum(DATA1),sum(DATA2) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1)) = '0' ";// ירוק

            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by DESC";
            StrSql += " order by (sum(DATA2) - sum(DATA1)) desc";
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet2.Cells[k, 1] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet2.Cells[k, 2] = Math.Round(((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb),2); //"Sum Spec";
                k++;
            }
            DBCW.Rs1.Close();
            // טבלאת צמיגים ירוקים חורגים
            var charts = xlWorkSheet2.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            var chartObject = charts.Add(0,0,450,182) as Microsoft.Office.Interop.Excel.ChartObject;
            var chart = chartObject.Chart;
            // Set chart range.  
       
            if ((k-1)<= 7)
            {
                range = xlWorkSheet2.get_Range("A2:A" + (k - 1).ToString() + ",B2:B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet2.get_Range("A2:A8,B2:B8");
            }
            
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Green Tire deviation analysis by weight",
                              CategoryTitle: "Size",
                              ValueTitle: "Weight (Kg)");
            chart.ApplyDataLabels();
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet2.get_Range("A2:B" + (k - 1).ToString());
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            StrSql = "select DESC, count(*),sum(DATA1),sum(DATA2) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1)) = '0' ";// ירוק

            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by DESC";
            StrSql += " order by (sum(DATA2)/Sum(DATA1)) desc";
            DBCW.Q_Run1(StrSql);
            tiresum = new string[k,2];
            while (DBCW.Rs1.Read())
            {
                tiresum[p, 0] = DBCW.Rs1.GetString(0).Trim();
                tiresum[p,1] = DBCW.Rs1.GetString(1); //"Count";
                xlWorkSheet2.Cells[j, 3] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet2.Cells[j, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Deviation %";
                p++;
                j++;
            }
            // טבלאת אחוזי סטייה
            charts = xlWorkSheet2.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 182, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - 1) <= 7)
            {
                range = xlWorkSheet2.get_Range("C2:C" + (k - 1).ToString() + ",D2:D" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet2.get_Range("C2:C8,D2:D8");
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Green Tire deviation percentage",
                              CategoryTitle: "Size",
                              ValueTitle: "Percentage");
             //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
            //    range = xlWorkSheet2.get_Range("D2:D" + (j - 1).ToString());
            //    ((Excel.Range)range.Columns.get_Item(1, Type.Missing))
            //      .Sort(range.Columns[1, Type.Missing],
            //      Excel.XlSortOrder.xlDescending, Type.Missing, Type.Missing,
            //      Excel.XlSortOrder.xlDescending, Type.Missing,
            //      Excel.XlSortOrder.xlDescending, Excel.XlYesNoGuess.xlGuess,
            //      Type.Missing, Type.Missing,
            //      Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            //      Excel.XlSortDataOption.xlSortNormal,
            //      Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            //}


                range = xlWorkSheet2.get_Range("C2:D" + (j - 1).ToString());
                //((Excel.Range)range.Columns.get_Item(1, Type.Missing))
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }
           
            // שליפה טבלת צמיגים ירוקים חורגים מעל 3%
            StrSql = "select DESC, count(*),sum(DATA1),sum(DATA2) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1)) = '0' and (DATA2/DATA1 - 1) * 100 > 3 ";// ירוק
            //
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by DESC";
            StrSql += " order by (sum(DATA1)/sum(DATA2)) desc";
            k++;
            xlWorkSheet2.Cells[k, 1] = "Green ";
            xlWorkSheet2.Cells[k, 3] = "Above 3%";
            k++;
            pos = k;
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet2.Cells[k, 1] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet2.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                //xlWorkSheet2.Cells[k, 3] = ((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb); //"Sum Spec";
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r,0])
                    {
                        //xlWorkSheet2.Cells[k, 3] =  Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1]))*100,3)+"%";
                        xlWorkSheet2.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                //xlWorkSheet2.Cells[k, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Diviation %";
                k++;
            }
            DBCW.Rs1.Close();
            // טבלאת סטיות מעל 3% ירוקים
            charts = xlWorkSheet2.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 364, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.     
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos +7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "% Green tires with more than 3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            range = xlWorkSheet2.get_Range("B" + (pos-1) + ":C" + (k - 1).ToString());
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet2.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }
           

            // -שליפה טבלת צמיגים ירוקים חורגים מתחת 3%
            StrSql = "select DESC, count(*),sum(DATA1),sum(DATA2) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1)) = '0'  and (DATA2/DATA1 - 1) * 100 < -3";// ירוק
            //
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by DESC";
            StrSql += " order by (sum(DATA1)/sum(DATA2)) desc";
            k++; 
            xlWorkSheet2.Cells[k, 1] = "Green ";
            xlWorkSheet2.Cells[k, 3] = "Below -3%";
            k++; 
            pos = k;
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet2.Cells[k, 1] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet2.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet2.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet2.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                k++;
                
            }
            DBCW.Rs1.Close();
            // טבלאת סטיות מתחת 3%- ירוקים
            charts = xlWorkSheet2.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 546, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.    
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",C" + pos + ":C" + (k - 1).ToString());
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "% Green tires with less than -3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            //if (k != pos + 1)
            //{
            //    range = xlWorkSheet2.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
            //    //((Excel.Range)range.Columns.get_Item(1, Type.Missing))
            //    //((Excel.Range)range.Columns.get_Item(1, Type.Missing))
            //    range.Sort(range.Columns[1, Type.Missing],
            //     Excel.XlSortOrder.xlAscending,
            //     range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlAscending,
            //     Type.Missing,
            //     Excel.XlSortOrder.xlAscending,
            //      Excel.XlYesNoGuess.xlGuess,
            //     Type.Missing, Type.Missing,
            //     Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            //     Excel.XlSortDataOption.xlSortNormal,
            //     Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            //}

            if (k != pos + 1)
            {
                range = xlWorkSheet2.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }


            // שליפה טבלת קרקסים חורגים מאותה מידה
            StrSql = "select DESC, count(*),sum(DATA3),sum(DATA4) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "((substring(PRODUCT, 10, 1)) = '1' ) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by DESC";
            StrSql += " order by (sum(DATA3)-sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            k++;
            xlWorkSheet2.Cells[k, 1] = "Carcas"; 
            xlWorkSheet2.Cells[k, 3] = "Tires"; 
            k++;
            pos = k;
            j = pos;
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet2.Cells[k, 1] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                //xlWorkSheet2.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                xlWorkSheet2.Cells[k, 2] = Math.Round(((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb), 2); //"Sum Spec";
                k++;
            }
            DBCW.Rs1.Close();

            // טבלאת קרקסים חורגים
            charts = xlWorkSheet2.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 0, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas deviation by size",
                              CategoryTitle: "Size",
                              ValueTitle: "Weight (Kg)");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":B" + (k - 1).ToString());
               // range = xlWorkSheet2.get_Range("C2:D" + (j - 1).ToString());
                //((Excel.Range)range.Columns.get_Item(1, Type.Missing))
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }


            StrSql = "select DESC, count(*),sum(DATA3),sum(DATA4) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "((substring(PRODUCT, 10, 1)) = '1' ) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by DESC";
            StrSql += " order by (sum(DATA3)/sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            tiresum = new string[k, 2];
            p = 0;
            while (DBCW.Rs1.Read())
            {
                tiresum[p, 0] = DBCW.Rs1.GetString(0).Trim();
                tiresum[p, 1] = DBCW.Rs1.GetString(1); //"Count";
                xlWorkSheet2.Cells[j, 3] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet2.Cells[j, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Deviation %";
                p++;
                j++;
            }
            // טבלאת אחוזי סטייה
            charts = xlWorkSheet2.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 182, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet2.get_Range("C" + pos + ":C" + (k - 1).ToString() + ",D" + pos + ":D" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet2.get_Range("C" + pos + ":C" + (pos + 7).ToString() + ",D" + pos + ":D" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas deviation percentage",
                              CategoryTitle: "Size",
                              ValueTitle: "Percentage");
             //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet2.get_Range("C" + pos + ":D" + (k - 1).ToString());
                //range = xlWorkSheet2.get_Range("C2:D" + (j - 1).ToString());
                //((Excel.Range)range.Columns.get_Item(1, Type.Missing))
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }
            // טבלאת קרקסים חורגים מעל 3%
            StrSql = "select DESC, count(*),sum(DATA3),sum(DATA4) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "(substring(PRODUCT, 10, 1) = '1' and (DATA4/DATA3 - 1) * 100 > 3) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by DESC";
            StrSql += " order by (sum(DATA3)/sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            k++;
            xlWorkSheet2.Cells[k, 1] = "Carcas";
            xlWorkSheet2.Cells[k, 3] = "Above 3%";
            k++;
            pos = k;
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet2.Cells[k, 1] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet2.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                //xlWorkSheet2.Cells[k, 3] = ((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb); //"Sum Spec";
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet2.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet2.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                //xlWorkSheet2.Cells[k, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Diviation %";
                k++;
            }
            DBCW.Rs1.Close();

            // טבלאת סטיות מעל 3% קרקסים
            charts = xlWorkSheet2.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 364, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",C" + pos + ":C" + (k - 1).ToString());
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas tires with more than 3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
             //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet2.get_Range("A" + (pos-1) + ":C" + (k - 1).ToString());
                //((Excel.Range)range.Columns.get_Item(1, Type.Missing))
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            // -טבלאת קרקסים חורגים מעל 3%
            StrSql = "select DESC, count(*),sum(DATA3),sum(DATA4) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "(substring(PRODUCT, 10, 1) = '1' and (DATA4/DATA3 - 1) * 100 < -3) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by DESC";
            StrSql += " order by (sum(DATA3)/sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            k++;
            xlWorkSheet2.Cells[k, 1] = "Carcas";
            xlWorkSheet2.Cells[k, 3] = "Below 3%";
            k++;
            pos = k;
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet2.Cells[k, 1] = DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet2.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet2.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet2.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                k++;

            }
            DBCW.Rs1.Close();

            // טבלאת סטיות מתחת 3%- קרקסים
            charts = xlWorkSheet2.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 546, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet2.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas tires with lessthan -3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
             //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet2.get_Range("A" + (pos-1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }
            k = 2; pos = 0; j = 2; p = 0;
            ////////////////////////////////////////////////////////
            //שליפת טבלת צמיגים ירוקים חורגים מאותה מכונה
            StrSql = "select MACHINE,count(*),sum(DATA1),sum(DATA2) from STWIND.PRRP " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1) = '0') "; // ירוק
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by MACHINE ";
            StrSql += " order by 2 desc";
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet3.Cells[k, 1] = "M" + DBCW.Rs1.GetString(0).Trim(); //"Machine";
                xlWorkSheet3.Cells[k, 2] = Math.Round(((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb), 2); //"Sum Spec";
                k++;
            }
            DBCW.Rs1.Close();
            // טבלאת צמיגים ירוקים חורגים
            charts = xlWorkSheet3.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 0, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.  

            if ((k - 1) <= 7)
            {
                range = xlWorkSheet3.get_Range("A2:A" + (k - 1).ToString() + ",B2:B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet3.get_Range("A2:A8,B2:B8");
            }

            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Green Tire deviation analysis by weight",
                              CategoryTitle: "Machine",
                              ValueTitle: "Weight(Kg)");
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet3.get_Range("A2:B" + (k - 1).ToString());
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }



            StrSql = "select MACHINE,count(*),sum(DATA1),sum(DATA2) from STWIND.PRRP " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1) = '0') "; // ירוק
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by MACHINE ";
            StrSql += " order by 2 desc";
            DBCW.Q_Run1(StrSql);
            DBCW.Q_Run1(StrSql);
            tiresum = new string[k, 2];
            while (DBCW.Rs1.Read())
            {
                tiresum[p, 0] = DBCW.Rs1.GetString(0).Trim();
                tiresum[p, 1] = DBCW.Rs1.GetString(1); //"Count";
                xlWorkSheet3.Cells[j, 3] = "M"+DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet3.Cells[j, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Deviation %";
                p++;
                j++;
            }
            // טבלאת אחוזי סטייה
            charts = xlWorkSheet3.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 182, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - 1) <= 7)
            {
                range = xlWorkSheet3.get_Range("C2:C" + (k - 1).ToString() + ",D2:D" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet3.get_Range("C2:C8,D2:D8");
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Green Tire deviation percentage",
                              CategoryTitle: "Machine",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet3.get_Range("C2:D" + (j - 1).ToString());
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }


            // שליפה טבלת צמיגים ירוקים חורגים מעל 3%
            StrSql = "select MACHINE, count(*),sum(DATA1),sum(DATA2) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1)) = '0'  and (DATA2/DATA1 - 1) * 100 > 3";// ירוק
            //
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by MACHINE";
            StrSql += " order by (sum(DATA1)/sum(DATA2)) desc";
            k++;
            xlWorkSheet3.Cells[k, 1] = "Green ";
            xlWorkSheet3.Cells[k, 3] = "Above 3%";
            k++;
            pos = k;
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet3.Cells[k, 1] = "M" + DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet3.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet3.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet3.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                k++;
            }
            DBCW.Rs1.Close();
            // טבלאת סטיות מעל 3% ירוקים
            charts = xlWorkSheet3.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 364, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.     
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "% Green tires with more than 3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            chart.ApplyDataLabels();
            //סידור נתונים בסדר עולה
            range = xlWorkSheet3.get_Range("B" + (pos - 1) + ":C" + (k - 1).ToString());
            chart.ApplyDataLabels();
            //if (k != pos + 1)
            //{
            //    range.Sort(range.Columns[2, Type.Missing],
            //       Excel.XlSortOrder.xlDescending,
            //       range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
            //       Type.Missing,
            //       Excel.XlSortOrder.xlDescending,
            //        Excel.XlYesNoGuess.xlGuess,
            //       Type.Missing, Type.Missing,
            //       Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            //       Excel.XlSortDataOption.xlSortNormal,
            //       Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            //}


            if (k != pos + 1)
            {
                range = xlWorkSheet3.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            /////////////
            // -שליפה טבלת צמיגים ירוקים חורגים מתחת 3%
            StrSql = "select MACHINE, count(*),sum(DATA1),sum(DATA2) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1)) = '0'  and (DATA2/DATA1 - 1) * 100 < -3";// ירוק
            //
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by MACHINE";
            StrSql += " order by (sum(DATA1)/sum(DATA2)) desc";
            k++;
            xlWorkSheet3.Cells[k, 1] = "Green ";
            xlWorkSheet3.Cells[k, 3] = "Below -3%";
            k++;
            pos = k;
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet3.Cells[k, 1] = "M" +DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet3.Cells[k, 2] =  DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        xlWorkSheet3.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                    }
                }
                k++;

            }
            DBCW.Rs1.Close();
            // טבלאת סטיות מתחת 3%- ירוקים
            charts = xlWorkSheet3.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 546, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.    
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",C" + pos + ":C" + (k - 1).ToString());
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "% Green tires with less than -3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            //if (k != pos + 1)
            //{
            //    range = xlWorkSheet3.get_Range("B" + (pos - 1) + ":C" + (k - 1).ToString());
            //    range.Sort(range.Columns[2, Type.Missing],
            //      Excel.XlSortOrder.xlDescending,
            //      range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
            //      Type.Missing,
            //      Excel.XlSortOrder.xlDescending,
            //       Excel.XlYesNoGuess.xlGuess,
            //      Type.Missing, Type.Missing,
            //      Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            //      Excel.XlSortDataOption.xlSortNormal,
            //      Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            //}

            if (k != pos + 1)
            {
                range = xlWorkSheet3.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }


            ////////////////////////////
            // שליפה טבלת קרקסים חורגים מאותה מידה
            StrSql = "select MACHINE, count(*),sum(DATA3),sum(DATA4) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "((substring(PRODUCT, 10, 1)) = '1' ) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by MACHINE";
            StrSql += " order by (sum(DATA3)-sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            k++;
            xlWorkSheet3.Cells[k, 1] = "Carcas";
            xlWorkSheet3.Cells[k, 3] = "Tires";
            k++;
            pos = k;
            j = pos;
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet3.Cells[k, 1] = "M" + DBCW.Rs1.GetString(0).Trim(); //"Size";
                //xlWorkSheet2.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                xlWorkSheet3.Cells[k, 2] = Math.Round(((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb), 2); //"Sum Spec";
                k++;
            }
            DBCW.Rs1.Close();

            // טבלאת קרקסים חורגים
            charts = xlWorkSheet3.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 0, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas deviation by machine",
                              CategoryTitle: "Machine",
                              ValueTitle: "Weight(Kg)");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":B" + (k - 1).ToString());
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            StrSql = "select MACHINE, count(*),sum(DATA3),sum(DATA4) from STWIND.PRRP " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                     "((substring(PRODUCT, 10, 1)) = '1' ) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by MACHINE";
            StrSql += " order by (sum(DATA3)/sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            tiresum = new string[k, 2];
            p = 0;
            while (DBCW.Rs1.Read())
            {
                tiresum[p, 0] = DBCW.Rs1.GetString(0).Trim();
                tiresum[p, 1] = DBCW.Rs1.GetString(1); //"Count";
                xlWorkSheet3.Cells[j, 3] = "M" + DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet3.Cells[j, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Deviation %";
                p++;
                j++;
            }
            // טבלאת אחוזי סטייה
            charts = xlWorkSheet3.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 182, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet3.get_Range("C" + pos + ":C" + (k - 1).ToString() + ",D" + pos + ":D" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet3.get_Range("C" + pos + ":C" + (pos + 7).ToString() + ",D" + pos + ":D" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas deviation percentage",
                              CategoryTitle: "Machine",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet3.get_Range("C" + pos + ":D" + (k - 1).ToString());
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }



            // טבלאת קרקסים חורגים מעל 3%
            StrSql = "select MACHINE, count(*),sum(DATA3),sum(DATA4) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "(substring(PRODUCT, 10, 1) = '1' and (DATA4/DATA3 - 1) * 100 > 3) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by MACHINE";
            StrSql += " order by (sum(DATA3)/sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            k++;
            xlWorkSheet3.Cells[k, 1] = "Carcas";
            xlWorkSheet3.Cells[k, 3] = "Above 3%";
            k++;
            pos = k;
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet3.Cells[k, 1] = "M" + DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet3.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                //xlWorkSheet2.Cells[k, 3] = ((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb); //"Sum Spec";
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet3.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet3.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                //xlWorkSheet2.Cells[k, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Diviation %";
                k++;
            }
            DBCW.Rs1.Close();

            // טבלאת סטיות מעל 3% קרקסים
            charts = xlWorkSheet3.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 364, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",C" + pos + ":C" + (k - 1).ToString());
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas tires with more than 3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet3.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            // -טבלאת קרקסים חורגים מעל 3%
            StrSql = "select MACHINE, count(*),sum(DATA3),sum(DATA4) from STWIND.PRRP " +
                "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "(substring(PRODUCT, 10, 1) = '1' and (DATA4/DATA3 - 1) * 100 < -3) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by MACHINE";
            StrSql += " order by (sum(DATA3)/sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            k++;
            xlWorkSheet3.Cells[k, 1] = "Carcas";
            xlWorkSheet3.Cells[k, 3] = "Below 3%";
            k++;
            pos = k;
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet3.Cells[k, 1] = "M" +DBCW.Rs1.GetString(0).Trim(); //"Size";
                xlWorkSheet3.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet3.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet3.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                k++;

            }
            DBCW.Rs1.Close();

            // טבלאת סטיות מתחת 3%- קרקסים
            charts = xlWorkSheet3.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 546, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet3.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas tires with less than -3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();

            if (k != pos + 1)
            {
                range = xlWorkSheet3.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                   Excel.XlSortOrder.xlDescending,
                   range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                   Type.Missing,
                   Excel.XlSortOrder.xlDescending,
                    Excel.XlYesNoGuess.xlGuess,
                   Type.Missing, Type.Missing,
                   Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                   Excel.XlSortDataOption.xlSortNormal,
                   Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            /////////////// By Employee /////////////

            k = 2; pos = 0; j = 2; p = 0;
            ////////////////////////////////////////////////////////
            //שליפת טבלת צמיגים ירוקים חורגים מאותה מכונה
            StrSql = "select substring(EMPLOYEE,3,5),count(*),sum(DATA1),sum(DATA2),value(PRATI,'*'),value(FAMILY,'*') from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1) = '0') "; // ירוק
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*') ";
            StrSql += " order by 2 desc";
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet4.Cells[k, 1] = DBCW.Rs1.GetString(0) + "-" + DBCW.Rs1.GetString(4).Trim();// +" " + DBCW.Rs1.GetString(5).Trim();
                xlWorkSheet4.Cells[k, 2] = Math.Round(((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb), 2); //"Sum Spec";
                k++;
            }
            DBCW.Rs1.Close();
            // טבלאת צמיגים ירוקים חורגים
            charts = xlWorkSheet4.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 0, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.  

            if ((k - 1) <= 7)
            {
                range = xlWorkSheet4.get_Range("A2:A" + (k - 1).ToString() + ",B2:B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet4.get_Range("A2:A8,B2:B8");
            }

            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Green Tire deviation analysis by weight",
                              CategoryTitle: "Employee",
                              ValueTitle: "Weight(Kg)");
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet4.get_Range("A2:B" + (k - 1).ToString());
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }



            StrSql = "select substring(EMPLOYEE,3,5),count(*),sum(DATA1),sum(DATA2),value(PRATI,'*'),value(FAMILY,'*') from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1) = '0') "; // ירוק
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*') ";
            StrSql += " order by 2 desc";
            DBCW.Q_Run1(StrSql);
            DBCW.Q_Run1(StrSql);
            tiresum = new string[k, 2];
            while (DBCW.Rs1.Read())
            {
                tiresum[p, 0] = DBCW.Rs1.GetString(0).Trim();
                tiresum[p, 1] = DBCW.Rs1.GetString(1); //"Count";
                xlWorkSheet4.Cells[j, 3] = DBCW.Rs1.GetString(0) + "-" + DBCW.Rs1.GetString(4).Trim();// +" " + DBCW.Rs1.GetString(5).Trim();
                xlWorkSheet4.Cells[j, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Deviation %";
                p++;
                j++;
            }
            // טבלאת אחוזי סטייה
            charts = xlWorkSheet4.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 182, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - 1) <= 7)
            {
                range = xlWorkSheet4.get_Range("C2:C" + (k - 1).ToString() + ",D2:D" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet4.get_Range("C2:C8,D2:D8");
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Green Tire deviation percentage",
                              CategoryTitle: "Employee",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet4.get_Range("C1:D" + (j - 1).ToString());
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            // שליפה טבלת צמיגים ירוקים חורגים מעל 3%
            StrSql = "select substring(EMPLOYEE,3,5),count(*),sum(DATA1),sum(DATA2),value(PRATI,'*'),value(FAMILY,'*') from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1)) = '0' and (DATA2/DATA1 - 1) * 100 > 3";// ירוק
            //
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*')";
            StrSql += " order by (sum(DATA1)/sum(DATA2)) desc";
            k++;
            xlWorkSheet4.Cells[k, 1] = "Green ";
            xlWorkSheet4.Cells[k, 3] = "Above 3%";
            k++;
            pos = k;
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                
                xlWorkSheet4.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                xlWorkSheet4.Cells[k, 1] = DBCW.Rs1.GetString(0) + "-" + DBCW.Rs1.GetString(4).Trim();// +" " + DBCW.Rs1.GetString(5).Trim();
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet4.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet4.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                k++;
            }
            DBCW.Rs1.Close();
            // טבלאת סטיות מעל 3% ירוקים
            charts = xlWorkSheet4.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(00, 364, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.     
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "% Green tires with more than 3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            range = xlWorkSheet4.get_Range("B" + (pos - 1) + ":C" + (k - 1).ToString());
            //range = xlWorkSheet4.get_Range("B" + (pos - 1) + )
            chart.ApplyDataLabels();
            //if (k != pos + 1)
            //{
            //    range.Sort(range.Columns[2, Type.Missing],
            //      Excel.XlSortOrder.xlDescending,
            //      range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
            //      Type.Missing,
            //      Excel.XlSortOrder.xlDescending,
            //       Excel.XlYesNoGuess.xlGuess,
            //      Type.Missing, Type.Missing,
            //      Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            //      Excel.XlSortDataOption.xlSortNormal,
            //      Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            //}

            if (k != pos + 1)
            {
                range = xlWorkSheet4.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            /////////////
            // -שליפה טבלת צמיגים ירוקים חורגים מתחת 3%
            StrSql = "select substring(EMPLOYEE,3,5),count(*),sum(DATA1),sum(DATA2),value(PRATI,'*'),value(FAMILY,'*') from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                       "(substring(PRODUCT, 10, 1)) = '0' and (DATA2/DATA1 - 1) * 100 < -3";// ירוק
            //
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*')";
            StrSql += " order by (sum(DATA1)/sum(DATA2)) desc";
            k++;
            xlWorkSheet4.Cells[k, 1] = "Green ";
            xlWorkSheet4.Cells[k, 3] = "Below -3%";
            k++;
            pos = k;
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet4.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim();//"Catalog";
                xlWorkSheet4.Cells[k, 1] = DBCW.Rs1.GetString(0) + "-" + DBCW.Rs1.GetString(4).Trim();// +" " + DBCW.Rs1.GetString(5).Trim();
                
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet4.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet4.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                k++;

            }
            DBCW.Rs1.Close();
            // טבלאת סטיות מתחת 3%- ירוקים
            charts = xlWorkSheet4.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 546, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.    
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",C" + pos + ":C" + (k - 1).ToString());
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "% Green tires with less than -3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            //if (k != pos + 1)
            //{
            //    range = xlWorkSheet4.get_Range("B" + (pos - 1) + ":C" + (k - 1).ToString());
            //    range.Sort(range.Columns[2, Type.Missing],
            //     Excel.XlSortOrder.xlDescending,
            //     range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
            //     Type.Missing,
            //     Excel.XlSortOrder.xlDescending,
            //      Excel.XlYesNoGuess.xlGuess,
            //     Type.Missing, Type.Missing,
            //     Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            //     Excel.XlSortDataOption.xlSortNormal,
            //     Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            //}

            if (k != pos + 1)
            {
                range = xlWorkSheet4.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            ////////////////////////////
            // שליפה טבלת קרקסים חורגים מאותה מידה
            StrSql = "select substring(EMPLOYEE,3,5),count(*),sum(DATA3),sum(DATA4),value(PRATI,'*'),value(FAMILY,'*') from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "((substring(PRODUCT, 10, 1)) = '1' ) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*')";
            StrSql += " order by (sum(DATA3)-sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            k++;
            xlWorkSheet4.Cells[k, 1] = "Carcas";
            xlWorkSheet4.Cells[k, 3] = "Tires";
            k++;
            pos = k;
            j = pos;
            while (DBCW.Rs1.Read())
            {
                if (!DBCW.Rs1.IsDBNull(4))
                {
                    xlWorkSheet4.Cells[k, 1] = DBCW.Rs1.GetString(0) + "-" + DBCW.Rs1.GetString(4).Trim();// +" " + DBCW.Rs1.GetString(5).Trim();
                }
                //xlWorkSheet2.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                xlWorkSheet4.Cells[k, 2] = Math.Round(((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb), 2); //"Sum Spec";
                k++;
            }
            DBCW.Rs1.Close();

            // טבלאת קרקסים חורגים
            charts = xlWorkSheet4.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 0, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas deviation by Employee",
                              CategoryTitle: "Employee",
                              ValueTitle: "Weight(Kg)");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            //if (k != pos + 1)
            //{
            //    range = xlWorkSheet4.get_Range("A" + pos + ":B" + (k - 1).ToString());
            //    range.Sort(range.Columns[2, Type.Missing],
            //      Excel.XlSortOrder.xlDescending,
            //      range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
            //      Type.Missing,
            //      Excel.XlSortOrder.xlDescending,
            //       Excel.XlYesNoGuess.xlGuess,
            //      Type.Missing, Type.Missing,
            //      Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
            //      Excel.XlSortDataOption.xlSortNormal,
            //      Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            //}

            if (k != pos + 1)
            {
                range = xlWorkSheet4.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }


            StrSql = "select substring(EMPLOYEE,3,5),count(*),sum(DATA3),sum(DATA4),value(PRATI,'*'),value(FAMILY,'*') from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                     "((substring(PRODUCT, 10, 1)) = '1' ) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*')";
            StrSql += " order by (sum(DATA3)/sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            tiresum = new string[k, 2];
            p = 0;
            while (DBCW.Rs1.Read())
            {
                tiresum[p, 0] = DBCW.Rs1.GetString(0).Trim();
                tiresum[p, 1] = DBCW.Rs1.GetString(1); //"Count";
                xlWorkSheet4.Cells[j, 3] = DBCW.Rs1.GetString(0) + "-" + DBCW.Rs1.GetString(4).Trim();// +" " + DBCW.Rs1.GetString(5).Trim();
                xlWorkSheet4.Cells[j, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Deviation %";
                p++;
                j++;
            }
            // טבלאת אחוזי סטייה
            charts = xlWorkSheet4.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 182, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet4.get_Range("C" + pos + ":C" + (k - 1).ToString() + ",D" + pos + ":D" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet4.get_Range("C" + pos + ":C" + (pos + 7).ToString() + ",D" + pos + ":D" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas deviation percentage",
                              CategoryTitle: "Employee",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet4.get_Range("C" + pos + ":D" + (k - 1).ToString());
                range.Sort(range.Columns[2, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[2, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }



            // טבלאת קרקסים חורגים מעל 3%
            StrSql = "select substring(EMPLOYEE,3,5),count(*),sum(DATA3),sum(DATA4),value(PRATI,'*'),value(FAMILY,'*') from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "(substring(PRODUCT, 10, 1) = '1' and (DATA4/DATA3 - 1) * 100 > 3) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*')";
            StrSql += " order by (sum(DATA3)/sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            k++;
            xlWorkSheet4.Cells[k, 1] = "Carcas";
            xlWorkSheet4.Cells[k, 3] = "Above 3%";
            k++;
            pos = k;
            DBCW.Q_Run1(StrSql);
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet4.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                xlWorkSheet4.Cells[k, 1] = DBCW.Rs1.GetString(0) + "-" + DBCW.Rs1.GetString(4).Trim();// +" " + DBCW.Rs1.GetString(5).Trim();
                
                //xlWorkSheet2.Cells[k, 3] = ((double)DBCW.Rs1.GetDecimal(3) * lb) - ((double)DBCW.Rs1.GetDecimal(2) * lb); //"Sum Spec";
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet4.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet4.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                //xlWorkSheet2.Cells[k, 4] = Math.Round((((double)DBCW.Rs1.GetDecimal(3) * lb) / ((double)DBCW.Rs1.GetDecimal(2) * lb) - 1) * 100, 2) + "%";//"Diviation %";
                k++;
            }
            DBCW.Rs1.Close();

            // טבלאת סטיות מעל 3% קרקסים
            charts = xlWorkSheet4.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 364, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //range = xlWorkSheet2.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",C" + pos + ":C" + (k - 1).ToString());
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas tires with more than 3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet4.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                  Excel.XlSortOrder.xlDescending,
                  range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                  Type.Missing,
                  Excel.XlSortOrder.xlDescending,
                   Excel.XlYesNoGuess.xlGuess,
                  Type.Missing, Type.Missing,
                  Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                  Excel.XlSortDataOption.xlSortNormal,
                  Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }

            // -טבלאת קרקסים חורגים מעל 3%
            StrSql = "select substring(EMPLOYEE,3,5),count(*),sum(DATA3),sum(DATA4),value(PRATI,'*'),value(FAMILY,'*') from STWIND.PRRP left join " +
                         "ISUFKV.ISAVL10  on substring(EMPLOYEE,1,7)=trim(OVED) and MIFAL='01' " +
               "where PDATE between '" + DTPicker_FromW.Value.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000") + "' and '" + DTPicker_ToW.Value.ToString("yyyy-MM-dd-23.29.59.999999") + "' and " +
                      "(substring(PRODUCT, 10, 1) = '1' and (DATA4/DATA3 - 1) * 100 < -3) "; // קרקס
            //and ABS(DATA4/DATA3 - 1) * 100 > 3
            if (comboB_Machine.Text.Trim() != "")
                StrSql += "and MACHINE='" + comboB_Machine.Text.Trim() + "' ";
            if (comboB_Size.Text.Trim() != "")
                StrSql += "and DESC='" + comboB_Size.Text.Trim() + "' ";
            if (comboB_Emp.Text.Trim() != "")
                StrSql += "and EMPLOYEE='00" + comboB_Emp.Text.Trim().Substring(0, 5) + "0' ";
            if (comboB_ID.Text.Trim() != "")
                StrSql += "and substring(DATA12,4," + comboB_ID.Text.Trim().Length + ")='" + comboB_ID.Text.Trim() + "' ";

            StrSql += " group by substring(EMPLOYEE,3,5),value(PRATI,'*'),value(FAMILY,'*')";
            StrSql += " order by (sum(DATA3)/sum(DATA4)) desc";
            DBCW.Q_Run1(StrSql);
            k++;
            xlWorkSheet4.Cells[k, 1] = "Carcas";
            xlWorkSheet4.Cells[k, 3] = "Below 3%";
            k++;
            pos = k;
            while (DBCW.Rs1.Read())
            {
                xlWorkSheet4.Cells[k, 2] = DBCW.Rs1.GetString(1).Trim(); //"Catalog";
                xlWorkSheet4.Cells[k, 1] = DBCW.Rs1.GetString(0) + "-" + DBCW.Rs1.GetString(4).Trim();// +" " + DBCW.Rs1.GetString(5).Trim();
                
                for (int r = 0; r < p; r++)
                {
                    if (DBCW.Rs1.GetString(0).Trim() == tiresum[r, 0])
                    {
                        //xlWorkSheet4.Cells[k, 3] = Math.Round((double.Parse(DBCW.Rs1.GetString(1).Trim()) / int.Parse(tiresum[r, 1])) * 100, 3) + "%";
                        xlWorkSheet4.Cells[k, 3] = DBCW.Rs1.GetString(1).Trim();
                    }
                }
                k++;

            }
            DBCW.Rs1.Close();

            // טבלאת סטיות מתחת 3%- קרקסים
            charts = xlWorkSheet4.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(510, 546, 450, 182) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            // Set chart range.            
            if ((k - pos) <= 7)
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (k - 1).ToString() + ",B" + pos + ":B" + (k - 1).ToString());
            }
            else
            {
                range = xlWorkSheet4.get_Range("A" + pos + ":A" + (pos + 7).ToString() + ",B" + pos + ":B" + (pos + 7).ToString());
            }
            //
            chart.SetSourceData(range);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlColumns,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Carcas tires with less than -3% deviation",
                              CategoryTitle: "Tires",
                              ValueTitle: "Percentage");
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();
            if (k != pos + 1)
            {
                range = xlWorkSheet4.get_Range("A" + (pos - 1) + ":C" + (k - 1).ToString());
                range.Sort(range.Columns[3, Type.Missing],
                 Excel.XlSortOrder.xlDescending,
                 range.Columns[1, Type.Missing], Type.Missing, Excel.XlSortOrder.xlDescending,
                 Type.Missing,
                 Excel.XlSortOrder.xlDescending,
                  Excel.XlYesNoGuess.xlGuess,
                 Type.Missing, Type.Missing,
                 Excel.XlSortOrientation.xlSortColumns, Excel.XlSortMethod.xlPinYin,
                 Excel.XlSortDataOption.xlSortNormal,
                 Excel.XlSortDataOption.xlSortNormal, Excel.XlSortDataOption.xlSortNormal);
            }
            
            ////////////////////// 2015 VS.  2016  //////////////////////
            for (i = 1; i < 13; i++)
            {
                xlWorkSheet5.Cells[2, i+2] = i;   
            }
            xlWorkSheet5.Cells[2, i + 3] = "Annual";
            xlWorkSheet5.Cells[3, i + 3] = "0.32%";
            xlWorkSheet5.Cells[4, i + 3] = "0.3483%";
            //xlWorkSheet5.Cells[5, i + 3] = "115,068";

            // הזנת השנים שבגרף
            //DateTime _date = DateTime.Now.AddYears(-1);
            //var _dateString = _date.ToString("yyyy");
            xlWorkSheet5.Cells[3, 2] = "2015";
            //_date = DateTime.Now;
            //_dateString = _date.ToString("yyyy");
            xlWorkSheet5.Cells[4, 2] = "2016";
            // שנה קודמת
            xlWorkSheet5.Cells[3, 3] = "0.19%";
            xlWorkSheet5.Cells[3, 4] = "0.06%";
            xlWorkSheet5.Cells[3, 5] = "0.37%";
            xlWorkSheet5.Cells[3, 6] = "0.08%";
            xlWorkSheet5.Cells[3, 7] = "0.18%";
            xlWorkSheet5.Cells[3, 8] = "0.48%";
            xlWorkSheet5.Cells[3, 9] = "0.31%";
            xlWorkSheet5.Cells[3, 10] = "0.32%";
            xlWorkSheet5.Cells[3, 11] = "0.16%";
            xlWorkSheet5.Cells[3, 12] = "0.43%";
            xlWorkSheet5.Cells[3, 13] = "0.65%";
            xlWorkSheet5.Cells[3, 14] = "0.71%";
            //שנה נוכחית
            xlWorkSheet5.Cells[4, 3] = "0.63%";
            xlWorkSheet5.Cells[4, 4] = "0.23%";
            xlWorkSheet5.Cells[4, 5] = "0.22%";
            xlWorkSheet5.Cells[4, 6] = "0.14%";
            xlWorkSheet5.Cells[4, 7] = "0.03%";
            xlWorkSheet5.Cells[4, 8] = "0.35%";
            xlWorkSheet5.Cells[4, 9] = "0.25%";
            xlWorkSheet5.Cells[4, 10] = "0.34%";
            xlWorkSheet5.Cells[4, 11] = "0.34%";
            xlWorkSheet5.Cells[4, 12] = "0.48%";
            xlWorkSheet5.Cells[4, 13] = "0.57%";
            xlWorkSheet5.Cells[4, 14] = "0.62%";
            // סכומים
            //xlWorkSheet5.Cells[5, 3] = "17,884";
            //xlWorkSheet5.Cells[5, 4] = "16,953";
            //xlWorkSheet5.Cells[5, 5] = "18,473";
            //xlWorkSheet5.Cells[5, 6] = "16,605";
            //xlWorkSheet5.Cells[5, 7] = "15,863";
            //xlWorkSheet5.Cells[5, 8] = "15,117";
            //xlWorkSheet5.Cells[5, 9] = "14,173";
            //xlWorkSheet5.Cells[5, 10] = "";
            //xlWorkSheet5.Cells[5, 11] = "";
            //xlWorkSheet5.Cells[5, 12] = "";
            //xlWorkSheet5.Cells[5, 13] = "";
            //xlWorkSheet5.Cells[5, 14] = "";
            // חישובים
            //range = xlWorkSheet5.get_Range("P5");
            //range.Formula = "Sum(C5:I5)";
            //range = xlWorkSheet5.get_Range("P5");
            ////double x = Convert.ToDouble(range);
            //range2 = xlWorkSheet5.get_Range("C5");
            //object val = range2.Value;
            //object val1 = range.Value;
            //xlWorkSheet5.Cells[6, 3] = Math.Round(((double)val / (double)val1) * 100, 0) + "%";
            //range2 = xlWorkSheet5.get_Range("D5");
            //val = range2.Value;
            //val1 = range.Value;
            //xlWorkSheet5.Cells[6, 4] = Math.Round(((double)val / (double)val1) * 100, 0) + "%";
            //range2 = xlWorkSheet5.get_Range("E5");
            //val = range2.Value;
            //val1 = range.Value;
            //xlWorkSheet5.Cells[6, 5] = Math.Round(((double)val / (double)val1) * 100, 0) + "%";
            //range2 = xlWorkSheet5.get_Range("F5");
            //val = range2.Value;
            //val1 = range.Value;
            //xlWorkSheet5.Cells[6, 6] = Math.Round(((double)val / (double)val1) * 100, 0) + "%";
            //range2 = xlWorkSheet5.get_Range("G5");
            //val = range2.Value;
            //val1 = range.Value;
            //xlWorkSheet5.Cells[6, 7] = Math.Round(((double)val / (double)val1) * 100, 0) + "%";
            //range2 = xlWorkSheet5.get_Range("H5");
            //val = range2.Value;
            //val1 = range.Value;
            //xlWorkSheet5.Cells[6, 8] = Math.Round(((double)val / (double)val1) * 100, 0) + "%";
            //range2 = xlWorkSheet5.get_Range("I5");
            //val = range2.Value;
            //val1 = range.Value;
            //xlWorkSheet5.Cells[6, 9] = Math.Round(((double)val / (double)val1)*100,0)  + "%";
            // יצירת טבלה 
            charts = xlWorkSheet5.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
            chartObject = charts.Add(0, 90, 850, 400) as Microsoft.Office.Interop.Excel.ChartObject;
            chart = chartObject.Chart;
            range = xlWorkSheet5.get_Range("B1:P4");
            chart.SetSourceData(range, Excel.XlRowCol.xlRows);
            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
            chart.ChartWizard(Source: range,
                              PlotBy: Excel.XlRowCol.xlRows,
                              CategoryLabels: 2,
                              SeriesLabels: 1,
                              Gallery: Excel.XlChartType.xlColumnClustered,  //.xlColumnClustered,                              
                              Title: "Excess weight - monthly",
                              CategoryTitle: "Months",
                              ValueTitle: "Percentage");
            
            //סידור נתונים בסדר עולה
            chart.ApplyDataLabels();

         
            


                        DBCW.Close_Conn();

                        xlApp.Visible = true;
                        releaseObject(xlWorkSheet);
                        releaseObject(xlWorkBook);
                        releaseObject(xlApp);
                    }
         
        

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

       
    }
}
